%% Analysis of visual and auditory stimuli synchronicity 
% current data sets were recorded with Audacity

close all
% load('timing.mat');

[y,Fs]=audioread('CircleRoman.wav'); 
visual=y(:,1); %channel 1 was recorded with the diodes;
auditory=y(:,2); %channel 2 was recorded with the microphone

starting=27.1; %starting time in s
ending=219;

%visual_trim=visual(22*Fs:575*Fs); %for bla8
%auditory_trim=auditory(22*Fs:575*Fs); %for bla8
% visual_trim=visual(starting*Fs:ending*Fs); %for bla10
% auditory_trim=auditory(starting*Fs:ending*Fs);%for bla10

visual_trim=visual; %for bla10
auditory_trim=abs(auditory);%for bla10
hold on
plot(abs((visual_trim)));
plot(abs((auditory_trim)));
hold off

%setting values
peak_dis=1.5;
sigmas_v=3.5;
sigmas_a=1; %for setting the auditory threshold
%visual_threshold=mean(visual_trim)+std(visual_trim)*sigmas_v; %set threshold to determine the visual stimuli onsets
auditory_threshold=mean(auditory_trim)+std(auditory_trim)*sigmas_a; %set threshold to determine the auditory stimuli onsets
stim_dur=1;
% shmooth
% windowWidth =31
% polynomialOrder = 30
% visual_trim = sgolayfilt(visual_trim, polynomialOrder, windowWidth);

% visual_trim = smooth(visual_trim,300);
% auditory_trimC = smooth(abs(auditory_trim),100);

visual_trimC = diff(abs(visual_trim));
% auditory_trimC = abs(diff(auditory_trim));

    %%%%%%%%%%%%%%%%%%%%%%%%%   
   D = designfilt('bandpassfir', 'FilterOrder', 20, ...
       'CutoffFrequency1', 230, 'CutoffFrequency2', 590,...
       'SampleRate', Fs);

%   x = rand(1000,1);
  auditory_trimC = filtfilt(D,auditory_trim);
  auditory_trimC = smooth(abs(auditory_trimC),100);
  visual_trimD = smooth(abs(visual_trimC),100);
  %%%%%%%%%%%%%%%%%%%%
  D = designfilt('bandpassfir', 'FilterOrder', 200, ...
       'CutoffFrequency1', 120, 'CutoffFrequency2', 121,...
       'SampleRate', Fs);

%   x = rand(1000,1);
  visual_trimE = filtfilt(D,visual_trim);
  visual_trimF = smooth(abs(visual_trimE),1000);
%%%%%%%%%%%%%%%%%%%%%%%%%%
% int=findchangepts(visual_trim)
%finding the visual signal peaks with MATLAB 'findpeaks'
[pks_v locs_v]=findpeaks(visual_trimC,'MinPeakHeight',0.04,'WidthReference','halfprom','MinPeakDistance',15000,'Annotate','peaks'); %specify the number of trials of NPeaks, the minimum distance between the stimuli
% [pks_v locs_v]=findpeaks(visual_trim,'MinPeakHeight',0.02,'WidthReference','halfprom','MinPeakWidth',700,'Annotate','peaks'); %specify the number of trials of NPeaks, the minimum distance between the stimuli
[pks_a locs_a]=findpeaks(auditory_trimC,'MinPeakHeight',1*10^(-3),'MinPeakDistance',15000);%,'MinPeakHeight',0.06,'SortStr','ascend');

%[~,locs_Qwave]=findpeaks(visual_trim,'NPeaks',100,'MinPeakDistance',Fs*4);

%look for the first ones passing the threshold
%the chunk lengths are based on the distance between the visual stimuli peaks
onsets_v=[];
onsets_a=[];
%rest=auditory_trim(locs_v(1)-Fs*0.01:end); %start searching for the auditory onsets based on 10 ms before the first visual stimuli peak

for i=1:length(locs_v)

    chunk_start=locs_v(i)-Fs*0.01; %define the start of the chunk
    chunk_end=locs_v(i)+Fs*stim_dur;
    chunk=auditory_trimC;%(chunk_start:chunk_end);
    ind_a=find(chunk>auditory_threshold);
    onsets_a(i)=chunk_start+ind_a(1);%marking the onset of auditory stimulus in each chunk
    amp_a(i)=chunk(ind_a(1));%the amplitude of the onset in each chunk

end
 
t = 0:seconds(30):minutes(3);
fac_v=5; %amplify the visual channel amplitude for the plots
fac_a=2; %amplify the auditory channel amplitude for the plots
figure
plot(([1:length(auditory_trimC)]/Fs),auditory_trimC*fac_a,'b')
hold on
plot(([1:length(visual_trimC)]/Fs),visual_trimC*fac_v,'r')
plot(locs_v/Fs,pks_v*fac_v,'o')
plot(locs_a/Fs,pks_a,'x')
% vline(videoOnset,'r')
% vline(soundOnset,'b')
% plot(onsets_a,amp_a*fac_a,'kx')
% plot([0 length(visual_trim)],[visual_threshold*fac visual_threshold*fac],'m')
% plot([0 length(auditory_trim)],[auditory_threshold auditory_threshold],'k')
xlabel('sample points')
ylabel('Arbitrary Intensity')
legend({'Auditory';'Visual'})
mean_asynchrony=mean(onsets_a-locs_v(1:end)')/Fs*1000;%in ms
mean_asynchrony
%plot(locs_Qwave,visual_trim(locs_Qwave),'x')


