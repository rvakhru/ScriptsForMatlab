% Clear the workspace and the screen
sca;
close all;
clearvars;

PsychDefaultSetup(1);
screens = Screen('Screens');
screenNumber = max(screens);

white = WhiteIndex(screenNumber);
black = BlackIndex(screenNumber);
gray = white / 2;

[window, windowRect] = PsychImaging('OpenWindow', screenNumber, gray);

% GAMMA CaliBRATION
gammaVal =  1.75 ;
linCLUT=repmat(linspace(0,1,256)',1,3);
Screen('LoadNormalizedGammaTable', 0, linCLUT ,0);
gammaCLUT=repmat(linspace(0,1,256)'.^(1/gammaVal),1,3);
Screen('LoadNormalizedGammaTable', 0, gammaCLUT ,0);
%Screen('LoadNormalizedGammaTable', 0, gammaTable1*[1 1 1] ,0);
Screen('ColorRange', window, 255);

 %%%%-------------------- 06.06.2018
lum1=((16.9-0.7)/(46.7-0.7)); %%%%green and red luminance ratio 
lum2=((16.9-0.7)/(8.4-0.7)); %%%%red and blue luminance ratio
Red = 16.9;Green = 46.7;Blue  = 8.4;Black = 0.7;

%--GRAY----------  
mycol1 = ceil(255.*hsl2rgbmm(0.2,240,0,lum1,lum2));  % Color the screen gray = 12.8 cd/m2  
mycol1 = hsl2rgbS([240,0,0.2],Red,Green,Blue,Black);  % Color the screen gray = 12.8 cd/m2  

Screen('FillRect', window, mycol1); 
Screen('Flip', window);
KbStrokeWait;
%--BROWN---------- 
mycol1 = ceil(255.*hsl2rgbmm(0.19,20,1,lum1,lum2));     % Color the screen Orange/brown   = 12.8 cd/m2  
Screen('FillRect', window, mycol1); 
Screen('Flip', window);
KbStrokeWait;
%--BLUE----------  
mycol1 = ceil(255.*hsl2rgbmm(0.2,210,1,lum1,lum2));    % Color the screen blue  = 12.8 cd/m2  
Screen('FillRect', window, mycol1); 
Screen('Flip', window);
KbStrokeWait;
sca;
