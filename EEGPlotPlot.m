process = uigetdir('T:\#Common\Projects\Behavioral\Data\');
session    = menu('Session?', {'Pre','Post', 'Cond'}); sessionName = {'TrPre','TrPost','TrCond'};
if ~any(exist('STUDY','var') && strcmp(['1Pp_' sessionName{session} '_'  num2str(date) '.study'], STUDY.filename))
    [STUDY, ~] = pop_loadstudy('filename', ['1Pp_' sessionName{session} '_'  num2str(date) '.study'],'filepath',[process filesep 'Studies' filesep]);
end
mychans = {STUDY.changrp.name};
[~,index] = sortrows(mychans.'); channels = mychans(index); clear index
[tmp, tmpval] = pop_chansel(channels, 'withindex', 'on');
elGroup    = menu('Group electrodes?', {'Yes', 'No'}); elGroupName = {'on','off'};

%% Plot data
[STUDY, ALLEEG] = pop_loadstudy('filename', ['1Pp_' sessionName{session} '_'  num2str(date) '.study'],'filepath',[process filesep 'Studies' filesep]);
STUDY = pop_statparams(STUDY, 'condstats','on','mcorrect','none','alpha',0.05);
STUDY = pop_erpparams(STUDY, 'plotconditions','together','topotime',[]);
[STUDY, erpdata, erptimes] = std_erpplot(STUDY,ALLEEG,'channels', mychans, 'noplot', 'on');

%% 1 Plot RAW DATA
STUDY = std_erpplot(STUDY,ALLEEG,'channels', strsplit(tmpval,' '),'averagechan',elGroupName{elGroup});
legend(STUDY.conditionEx, 'FontSize', 10);

%% 2 ROI (average)
my_ROI = {{'O1' 'Oz' 'O2' },{'C1' 'Cz' 'C2'}};
cond_2_average = [[1 5];[2 6];[3 7];[4 8]];
colors ={'r','g','r','g'};
linstyle ={'-','-',':',':'};
cc = nan(3,1);
for r=1:length(my_ROI)
    figure
    for c =1:length(my_ROI{r})
      cc(c) = find(strcmp(mychans,my_ROI{r}(c)));     %%%%%%%%%%%%%%%% find the electordes belonging to a ROI
    end
    for i=1:length(cond_2_average)
        myerp1 = mean(erpdata{cond_2_average(i,1)}(:,cc),2); %%%%%%%%%%%%%%%% average for left and right, this makes sense for occipital electordes but not for others perhaps
        myerp2 = mean(erpdata{cond_2_average(i,2)}(:,cc),2);
        myerp = (myerp1+ myerp2)/2;
        plot(erptimes, myerp,'linewidth',2,'linestyle',linstyle{i},'color',colors{i}), hold on
        title(my_ROI{r})
    end
end
% plot(erptimes, erpdata{cond_2_average(end)},'linewidth',2,'linestyle',':','color','k'), hold on
% xlim([-100 800])