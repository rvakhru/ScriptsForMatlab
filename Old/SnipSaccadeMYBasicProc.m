magentaTrl = allData([allData.BlType]==0&[allData.BlPrePost]==2);
scaleTrl = linspace(1,25,25);
meanEventTel = zeros(length(magentaTrl),250);
seEventTel = zeros(length(magentaTrl),250);
types = {'LowS','HighS','LowV','HighV'};
    meanEventSel = zeros(length(magentaTrlSel),250);
    seEventTel = zeros(length(magentaTrlSel),250);
for type = 1:length(types)
    if type == 1
        magentaTrlSel = magentaTrl([magentaTrl.SP]==1&[magentaTrl.condition]==2);
    elseif type == 2
        magentaTrlSel = magentaTrl([magentaTrl.SP]==2&[magentaTrl.condition]==2);
    elseif type == 3
        magentaTrlSel = magentaTrl([magentaTrl.BC]==1&[magentaTrl.condition]==1);
    elseif type == 4
        magentaTrlSel = magentaTrl([magentaTrl.BC]==2&[magentaTrl.condition]==1);
    end
    
    meanEventSel.(types{type}) = zeros(length(magentaTrlSel),250);
    seEventTel.(types{type}) = zeros(length(magentaTrlSel),250);
    for eventTrl = 0:25-2
        for trl = 1:length(magentaTrlSel)
            meanEventSel.(types{type})(trl,eventTrl+1) = nanmean(magentaTrlSel(trl).posSel(magentaTrlSel(trl).TARGET_t + eventTrl*10+1:magentaTrlSel(trl).TRIAL_END_m+(eventTrl+1)*10,4));
            seEventTel.(types{type})(trl,eventTrl+1) = nanstd(magentaTrlSel(trl).posSel(magentaTrlSel(trl).TARGET_t + eventTrl*10+1:magentaTrlSel(trl).TRIAL_END_m+(eventTrl+1)*10,4))/sqrt(10);
        end
        meanEventSelTotal.(types{type})(eventTrl+1) = nanmean(meanEventSel.(types{type})(:,eventTrl+1));
        seEventSelTotal.(types{type})(eventTrl+1) = nanmean(seEventTel.(types{type})(:,eventTrl+1));
    end

end
figure;
hold on
for type = 1:length(types)
    errorbar(1:24,meanEventSelTotal.(types{type})-nanmin(meanEventSelTotal.(types{type})),...
        seEventSelTotal.(types{type})); % errorbar
end