function rgb=hsl2rgbS(hsl,Red,Green,Blue,Black)

% HSL system used here is a modified version of matlab (hsv), in which one
% takes into account the unequality of luminances of red, green and blue
% channels.

%%%%%%%%%FROM hsl2rgbmm%%%%%%%%%%%%
lum1=((Red-Black)/(Green-Black)); %%%%green and red luminance ratio
lum2=((Red-Black)/(Blue-Black)); %%%%red and blue luminance ratio

L0=Red+Green+Blue;

fRED=(L0-Black)/(1+1/lum1+1/lum2);
fGREEN=fRED/lum1;
fBLUE=fRED/lum2;

% Normalise factors
factot=fRED+fGREEN+fBLUE;
fac=[fRED,fGREEN,fBLUE]/factot;

%%%%%%%%%%%%%%%%%%%%%%ADAM%%%%%%%%%%%%

H=hsl(1);
S=hsl(2);
L=hsl(3);

n=fac/sqrt(sum(fac.^2));
nR=[0,-sqrt(2)/2,sqrt(2)/2];

u=cross(n,nR);
u=u/sqrt(sum(u.^2));
v=cross(n,u);

M=[u;v;n];	% Rotation matrix
x=[S*cosd(H),S*sind(H),0];
x=M'*x';
if(~prod(x<eps))
	x=x/sqrt(sum(x.^2));
end

if(~prod(x<eps))
% Determine saturation
vR=[];
vG=[];
vB=[];
vlambda=[];

% R=0
R=0;
lambda=(R-L)/x(1);
G=L+lambda*x(2);
B=L+lambda*x(3);
vR=[vR,R];
vG=[vG,G];
vB=[vB,B];
vlambda=[vlambda,lambda];

% R=1
R=1;
lambda=(R-L)/x(1);
G=L+lambda*x(2);
B=L+lambda*x(3);
vR=[vR,R];
vG=[vG,G];
vB=[vB,B];
vlambda=[vlambda,lambda];

% G=0
G=0;
lambda=(G-L)/x(2);
R=L+lambda*x(1);
B=L+lambda*x(3);
vR=[vR,R];
vG=[vG,G];
vB=[vB,B];
vlambda=[vlambda,lambda];

% G=1
G=1;
lambda=(G-L)/x(2);
R=L+lambda*x(1);
B=L+lambda*x(3);
vR=[vR,R];
vG=[vG,G];
vB=[vB,B];
vlambda=[vlambda,lambda];

% B=0
B=0;
lambda=(B-L)/x(3);
R=L+lambda*x(1);
G=L+lambda*x(2);
vR=[vR,R];
vG=[vG,G];
vB=[vB,B];
vlambda=[vlambda,lambda];

% B=1
B=1;
lambda=(B-L)/x(3);
R=L+lambda*x(1);
G=L+lambda*x(2);
vR=[vR,R];
vG=[vG,G];
vB=[vB,B];
vlambda=[vlambda,lambda];

i=find(vR<=1 & vR>=0 & vG<=1 & vG>=0 & vB<=1 & vB>=0 & vlambda>=0);

rho=S*vlambda(i(1));

x=[rho*cosd(H),rho*sind(H),0]';
end

rgb=M'*x+L;

rgb=rgb';

%%%%%****
rgb = ceil(255.*rgb);
