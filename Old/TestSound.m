%% SOUND

% Initialize Sound driver
InitializePsychSound(1);
KbName('UnifyKeyNames');

% Check if already any sound port opens, then close.
count = PsychPortAudio('GetOpenDeviceCount');
if (count>0)
    PsychPortAudio('Close');                            % Better to close everything to prevent interaction
end

devices1 = PsychPortAudio('GetDevices' ,[] ,[]);        % Which Audio devices do we have
soundDevice = devices1(length(devices1)).DeviceIndex; 	% This is our best Audio device, check the specifications
freq = devices1(soundDevice + 1).DefaultSampleRate;     % Finally, define the frequency of this device.

% Specify sound parameters
SoundDur = .25;         % duration of the sound
N = freq*SoundDur;      % Sample size in second this many
t = (1:N)*(1/freq);     % Duration of stimulation
freq1 = 350;            % Note: (F4) Low pitch  volume must be 0.2 measured on 6.06.18 system sound was 10 Romans cabin
freq2 = 1050;           % Note: (C6) High pitch volume must be 0.04
rampDur = 0.005;        % Duration of the initial ramp where sound intensity gradually increases
% rampMat = createOnOffRamp(rampDur,length(t),freq);
% rampMat=ones(size(rampMat)); % if no ramp
myVar.pahandle = PsychPortAudio('Open', soundDevice, 1, 1, freq); % For experiment sounds (advanced audio)

%% SOUND: Generate variables with specific sounds

base_R = [zeros(1,length(t));sin(2*pi*freq1*t) ]; % Low RIGHT
base_L = [sin(2*pi*freq1*t);zeros(1,length(t)) ]; % Low LEFT
test_R = [zeros(1,length(t));sin(2*pi*freq2*t) ]; % High RIGHT
test_L = [sin(2*pi*freq2*t);zeros(1,length(t)) ]; % High LEFT

noSound= [zeros(1,length(t));zeros(1,length(t))];         % No sound - empty
both_Hi= [sin(2*pi*freq2*t);sin(2*pi*freq2*t) ]; % High LEFT
both_Lo= [sin(2*pi*freq1*t);sin(2*pi*freq1*t) ]; % Low LEFT

mysounds = {base_R;base_L;test_R;test_L;noSound;both_Lo;both_Hi}; % Combine all in one variable
for s=1:7; % number of sound types/ greate actual sounds
    myVar.bufferhandle{s} = PsychPortAudio('CreateBuffer' ,myVar.pahandle, mysounds{s});
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Show INSTRUCTION.

steps = 0.05;
VolLowPitch =0.2;
VolHighPitch =0.04;
pitch = [VolLowPitch VolHighPitch];
sounds = [6 7];
cont = 1;
nn = 2;
%% RUN TEST
counts =0;
while 1
    [KeyIsDown, ~, KeyCode] = KbCheck();
    sav = 0;
    if KeyIsDown
        counts=counts+1;
        if     strcmp(KbName(KeyCode),'RightArrow')
            pitch(nn) = pitch(nn) + steps;
        elseif strcmp(KbName(KeyCode),'LeftArrow')
            pitch(nn) = pitch(nn) - steps;
        elseif strcmp(KbName(KeyCode),'DownArrow')
            steps = steps/2;
        elseif strcmp(KbName(KeyCode),'UpArrow')
            steps = steps*2;
        elseif strcmp(KbName(KeyCode),'ESCAPE')
            break;
        elseif strcmp(KbName(KeyCode),'s')
            sav = 1;
        elseif strcmp(KbName(KeyCode),'n')
            count = count +1;
            nn = mod(count,2)+1;
            while KeyIsDown
                [KeyIsDown, ~, ~] = KbCheck();
            end
        end
        if pitch(nn) < 0
            pitch(nn) = 0;
        elseif pitch(nn) > 1
            pitch(nn) = 1;
        end
        if strcmp(KbName(KeyCode),'RightArrow')||strcmp(KbName(KeyCode),'LeftArrow')||strcmp(KbName(KeyCode),'space')||strcmp(KbName(KeyCode),'Return')||strcmp(KbName(KeyCode),'n')||strcmp(KbName(KeyCode),'s')
            PsychPortAudio('FillBuffer', myVar.pahandle, myVar.bufferhandle{sounds(nn)}); % prepare sound
            
            PsychPortAudio('Stop', myVar.pahandle);
            PsychPortAudio('Volume', myVar.pahandle, pitch(nn));       % Adjust volume
            PsychPortAudio('Start', myVar.pahandle);                             % Play
            fprintf('%dSound: %d, Volume: %2.3f\t\n',sav,sounds(nn),pitch(nn))
        end
        WaitSecs(.1);
        %             while KeyIsDown
        %                 [KeyIsDown, ~, ~] = KbCheck();
        %             end
    end
end