
clear all
close all
clc
experiment              = menu('What Experiment?', {'1','2','3','4','5','6','7','8'});
whichone                  = abs(menu('Which measure to use?', {'dPrime','Accuracy','RT'}));
rootdir_fig= 'C:\Work\69. SCM Experiment With Reward_05.03\';

%% Load DATA
if experiment == 1
    rootdir= 'Y:\#Common\Projects\Behavioral\Data\55.1 SCM SimpleCrossModal\';
    subjects = {'18855566'; '19283746';'29384756';'31463624';'44751960';'96709616';'Roman06';'Tomke'}; %wrong set og Pp
    % subjects = {'18855566';'34214459'; '31463624';'44751960';'96709616'};
    subjects = {'18855566'; '19283746';'29384756';'31463624';'44751960';'96709616';'Tomke';'34214459'}; %wrong set og Pp,'34214459' added by Arezoo: why this subject was not included?
    experiment_name = 'Exp1';
    rel_blocks= 2:5;
    rel_blocks= [9:2:21];
elseif experiment == 2
    rootdir= 'W:\#Common\Projects\Behavioral\Data\Sound only Test 16.01\';
    subjects = {'Adem'; 'Barbara';'Helge';'Hendrik';'Kilian';'Roman1'};
    experiment_name = 'Exp2';
    rel_blocks= 2:9;
elseif experiment == 3
    rootdir= 'W:\#Common\Projects\Behavioral\Data\55.1 SCM Visual_Sound_Only Test 31.01\';
    subjects = {'R9'; 'Helge_01.02';'Hendrik_31.01';'Kilian_02.02';'R7';'R8'};
    experiment_name = 'Exp3';
    rel_blocks = 3:10;
elseif experiment == 4
    rootdir= 'W:\#Common\Projects\Behavioral\Data\60. SCM NewInstructions 21.02\';
    subjects = {'39417064';'31104305';'17012172';'68773507'};
    % subjects = {'31104305'};
    experiment_name = 'Exp4';
    rel_blocks= 2:5;
    rel_blocks= 7:2:21;
elseif experiment == 5
    rootdir= 'Y:\apoores\Data\fromRoman\';
    subjects = {'14592466';'48965498';'58372543';'37074631';'38252522';'47090933';'78749075';'48745299';'60515942';'56624400';'72410876';'21300406';'73028404';'59714179'};
    experiment_name = 'Exp5';
    rel_blocks= 2:5;
    %rel_blocks= 7:2:21;
    %     rel_blocks= 7:2:21;
    rel_blocks = [7:2:21];
elseif experiment == 6
    rootdir= 'Y:\#Common\Projects\Behavioral\Data\79 Simple Cross-Modal - low 26.03\';
    subjects = {'88476163' ,'87238769','58748256','99170697','88476163' ,'87238769','58748256'}; % 88476163 87238769
    experiment_name = 'Exp6';
    rel_blocks= [2 3 10 11];
    %  rel_blocks= [5  15];
    rel_blocks= [5 7 9   13 15 17];
    % rel_blocks= [4 6 8 12 14 16 20 22 24]; % conditioning
    % rel_blocks= [19 21 23 25]; % mixed
elseif experiment == 7
    rootdir= 'W:\apoores\Data\fromRoman\';
    subjects = {'88476163' ,'87238769','58748256','99170697','88476163' ,'87238769','58748256','74620667','18231554','93881044','89140978',...
        '23123662','47130574','74553204','24509065','68630199','71548078','66597433','10579857','13589008'}; %%% subject ,'45003453' had not learned auditory or visual tasks: performance ~ 0.55
    experiment_name = 'Exp7';
    rel_blocks= [2 3 10 11];
    %  rel_blocks= [5  15];
    rel_blocks= [5 7 9 13 15 17];
    % rel_blocks= [4 6 8 12 14 16 20 22 24]; % conditioning
    % rel_blocks= [19 21 23 25]; % mixed
elseif experiment == 8 %(all together)
    rootdir= 'Y:\apoores\Data\fromRoman\';
    subjects = {'88476163' ,'87238769','58748256','99170697','88476163' ,'87238769','58748256',...%%%% exp7 
        '74620667','18231554','93881044','89140978','23123662','47130574','74553204','24509065','68630199','71548078','66597433','10579857','13589008',...%%%% exp6
        '14592466','48965498','58372543','37074631','38252522','47090933','78749075','48745299','60515942','56624400','72410876','21300406','73028404','59714179',...%%%% exp5
        '39417064','31104305','17012172','68773507',...%%%% exp4
        '18855566', '19283746','29384756','31463624','44751960','96709616','Tomke','34214459'}; %%%% exp1 
    experiment_name = 'Exp8';
%     rel_blocks= [2 3 10 11];
%     %  rel_blocks= [5  15];
%     rel_blocks= [5 7 9 13 15 17];
%     % rel_blocks= [4 6 8 12 14 16 20 22 24]; % conditioning
%     % rel_blocks= [19 21 23 25]; % mixed    
end
%% Predefine Variables
s.subAccu = zeros(length(subjects),1);
s.subSP_H = zeros(length(subjects),1);
s.subSP_L = zeros(length(subjects),1);
s.subBC_M = zeros(length(subjects),1);
s.subBC_O = zeros(length(subjects),1);
s.subNeut = zeros(length(subjects),1);
s.subSVol = zeros(length(subjects),3);
s.subBCvH = zeros(length(subjects),1);
s.subBCvL = zeros(length(subjects),1);
s.subSPvH = zeros(length(subjects),1);
s.subSPvL = zeros(length(subjects),1);
s.subRewV = zeros(length(subjects),2);
s.subDPri = zeros(length(subjects),2);

% RelPosition SOUND
s.subSPdH = zeros(length(subjects),1);
s.subSPdL = zeros(length(subjects),1);
s.subSPsH = zeros(length(subjects),1);
s.subSPsL = zeros(length(subjects),1);
s.subSPvdH = zeros(length(subjects),1);
s.subSPvdL = zeros(length(subjects),1);
s.subSPvsH = zeros(length(subjects),1);
s.subSPvsL = zeros(length(subjects),1);
% RelPosition VISUAL
s.subBCdH = zeros(length(subjects),1);
s.subBCdL = zeros(length(subjects),1);
s.subBCsH = zeros(length(subjects),1);
s.subBCsL = zeros(length(subjects),1);
s.subBCvdH = zeros(length(subjects),1);
s.subBCvdL = zeros(length(subjects),1);
s.subBCvsH = zeros(length(subjects),1);
s.subBCvsL = zeros(length(subjects),1);
s.moveav_subBCvH= nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.moveav_subBCvL= nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.moveav_subSPvH= nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.moveav_subSPvL= nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.effect_ColorValue = nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.effect_SoundValue = nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials

for i=[1:length(subjects)]
    
    if experiment ==8 && i<=7
        rel_blocks = [5 7 9 13 15 17 19 21 23 25 ]; %%%last 4 blocks are mixed
        rel_blocks = [5 7 9 13 15 17 ];
        %rel_blocks= [2 3 10 11];    %%%%baseline 
    elseif   experiment ==8 && i>7  && i<21  %%%% Arezoo's crossmodal, Roman's visual
        rel_blocks = [5 7 9];        
        %rel_blocks= [2 3 ];        %%%%baseline
    elseif experiment ==8 && i>=21  && i<=38
        rel_blocks = [7:2:21];
        %rel_blocks= 2:5;           %%%%baseline
    elseif experiment ==8 && ismember(i,[40  41 45])
        rel_blocks = [9:2:23];
        %rel_blocks= 2:5;           %%%%baseline
    elseif experiment ==8 && ismember(i,[39  42 43 44 46 ])
        rel_blocks = [7:2:21];
        %rel_blocks= 2:5;           %%%%baseline
    end
    
    if experiment ==7 && i>7
        rel_blocks = [5 7 9];
        % rel_blocks = [ 2 3];
    elseif experiment ==7 && i<=7
        rel_blocks = [13 15 17 ];
        % rel_blocks = [ 10 11];
        rel_blocks= [2 3 10 11];  
    end
    
    if experiment ==1 && ismember(i,[2 3 7])
        rel_blocks = [9:2:23];
        % rel_blocks = [ 2 3];
    elseif experiment ==1 && ~ismember(i,[2 3 7])
        rel_blocks = [7:2:21 ];
        % rel_blocks = [ 10 11];
    end
    load([rootdir subjects{i} '\' subjects{i} '__allData.mat'])
    
    vecAccu =[];  vecSoPi =[];
    vecCond =[];  vecBoxC =[];
    vecSoVo =[];  vecValu =[];
    vecGabO =[];  vecRelP =[]; vecRT=[];
    vccboxXLoc= []; vccgaborXLoc= [];
    vectonset= [];
    vecError =[];
    %rel_blocks =rel_blocks(1);
    for j=rel_blocks % gather information into vectors
        % combine information from Pp into one vector
        %%%%%%%%%only analyze first 36
        %         if length([block(j).trials.error])>36
%                     for b=17:length([block(j).trials.error])
%                        block(j).trials(b).error=1;
%                     end
        %         end
        vecAccu = [vecAccu block(j).trials([block(j).trials.error]==0).Accuracy];
        vecRT   = [vecRT   block(j).trials([block(j).trials.error]==0).RT];
        vecSoPi = [vecSoPi block(j).trials([block(j).trials.error]==0).SP];
        vecBoxC = [vecBoxC block(j).trials([block(j).trials.error]==0).BC];
        vecCond = [vecCond block(j).trials([block(j).trials.error]==0).condition];
        vecValu = [vecValu block(j).trials([block(j).trials.error]==0).StimValue];
        vecSoVo = [vecSoVo block(j).trials([block(j).trials.error]==0).SVol];
        vecRelP = [vecRelP block(j).trials([block(j).trials.error]==0).relativePos];
        vecGabO = [vecGabO block(j).trials([block(j).trials.error]==0).gaborOri];
        vccboxXLoc= [vccboxXLoc block(j).trials([block(j).trials.error]==0).boxXLoc];
        gaborXLoc =[block(j).trials([block(j).trials.error]==0).GL];
        vccgaborXLoc= [vccgaborXLoc gaborXLoc];
        vectonset = [vectonset [ block(j).trials([block(j).trials.error]==0).targetOnset]-...
            [block(j).trials([block(j).trials.error]==0).fixationOnset]];
        
        vecError = [vecError block(j).trials.error];
        
    end
    
%     % combine information from Pp into one vector
    %trls=1:36%length(vecAccu);
    trls = find(abs(zscore(vecRT))<4);
    s.strange_RT{i}=vecRT(abs(zscore(vecRT))>4);
    trls =trls(round(1:end));
    vecAccu = vecAccu(trls);
    vecRT   = vecRT(trls);
    vecSoPi = vecSoPi(trls);
    vecBoxC = vecBoxC(trls);
    vecCond = vecCond(trls);
    vecValu = vecValu(trls);
    vecSoVo = vecSoVo(trls);
    vecRelP = vecRelP(trls);
    vecGabO = vecGabO(trls);
    vccboxXLoc= vccboxXLoc(trls);
    vccgaborXLoc= vccgaborXLoc(trls);
    vectonset = vectonset(trls);
    
    vecError = vecError(trls);
    %%%%%%%%%%% to remove outliers
    outlier = abs(zscore(vecRT))>400; %%%% to remove slow RTs use>4;
    %%%%%%%%%%%%% to keep track of repetitions
    GL_reps = [1 diff(vccgaborXLoc)]; %%% keeps track of repetitions of the side
    
    
    
    % fill with nanmean values of desired conditions across subjects
    s.subAccu(i)  = nanmean(vecAccu);                            % accuracy
    s.subRT(i)    = nanmean(vecRT);                            % RT
    
    
    h       =     nanmean(vecAccu(vecGabO== 1));
    fA      = nanmean( 1 - vecAccu(vecGabO==-1));
    N = sum(vecGabO==-1); %%%%% for correction of h and fA when they are 1 or 0 see http://www.kangleelab.com/sdt-d-prime-calculation---other-tips.html
    s.subDPri(i)  = Dprime2(h,fA,N);                % dPrime
    %     s.subSVol(i,1)= unique(vecSoVo(vecSoPi==0));  % No    sound
    %     s.subSVol(i,2)= unique(vecSoVo(vecSoPi==1));  % Quite sound
    %     s.subSVol(i,3)= unique(vecSoVo(vecSoPi==2));  % Loud  sound
    s.subRewV(i,:)= inf.rewardValueB;             % Value of cues (mode)
    s.subRewS(i,:)= inf.rewardValueS;
    s.subleanV{i}= [inf.learn.Visual];
    s.rel_blocks{i}= rel_blocks;
    %     s.subleanS{i}= [inf.learn.Sound];
    %    s.subPSE(i)= unique([block(21).trials.PSE]);
    %% Process in dPrime
    selection{1} = (vecCond==0);                            % SubNeut
    
    selection{2} = (vecCond==1 & vecBoxC==1);               % SubBC_O
    selection{3} = (vecCond==1 & vecBoxC==2);               % SubBC_M
    selection{4} = (vecCond==1 & vecValu==1);               % SubBCvL
    selection{5} = (vecCond==1 & vecValu==2);               % SubBCvH
    
    selection{6} = (vecCond==2 & vecSoPi==1);               % SubSP_L
    selection{7} = (vecCond==2 & vecSoPi==2);               % SubSP_H
    selection{8} = (vecCond==2 & vecValu==1);               % SubSPvL
    selection{9} = (vecCond==2 & vecValu==2);               % SubSPvH
    
    %RelPosition VISUAL SIDE
    selection{10} = (vecCond==1 & vecBoxC==1 & vecRelP==1); % SubBCsO
    selection{11} = (vecCond==1 & vecBoxC==2 & vecRelP==1); % SubBCsM
    selection{12} = (vecCond==1 & vecBoxC==1 & vecRelP==2); % SubBCdO
    selection{13} = (vecCond==1 & vecBoxC==2 & vecRelP==2); % SubBCdM
    %RelPosition SOUND SIDE
    selection{14} = (vecCond==2 & vecSoPi==1 & vecRelP==1); % SubSPsL
    selection{15} = (vecCond==2 & vecSoPi==2 & vecRelP==1); % SubSPsH
    selection{16} = (vecCond==2 & vecSoPi==1 & vecRelP==2); % SubSPdL
    selection{17} = (vecCond==2 & vecSoPi==2 & vecRelP==2); % SubSPdH
    
    %RelPosition VISUAL VALUE
    selection{18} = (vecCond==1 & vecValu==1 & vecRelP==1); % SubBCvsL
    selection{19} = (vecCond==1 & vecValu==2 & vecRelP==1); % SubBCvsH
    selection{20} = (vecCond==1 & vecValu==1 & vecRelP==2); % SubBCvdL
    selection{21} = (vecCond==1 & vecValu==2 & vecRelP==2); % SubBCvdH
    
    %RelPosition SOUND VALUE
    selection{22} = (vecCond==2 & vecValu==1 & vecRelP==1); % SubSPvsL
    selection{23} = (vecCond==2 & vecValu==2 & vecRelP==1); % SubSPvsH
    selection{24} = (vecCond==2 & vecValu==1 & vecRelP==2); % SubSPvdL
    selection{25} = (vecCond==2 & vecValu==2 & vecRelP==2); % SubSPvdH
    
    selection{26} = (vecCond==1 );                          % SubV
    selection{27} = (vecCond==2 );                          % SubA
    
    selection{28} = (vecCond==1 & vecRelP==1);               % SubVs
    selection{29} = (vecCond==1 & vecRelP==2);               % SubVd
    
    selection{30} = (vecCond==2 & vecRelP==1);               % SubAs
    selection{31} = (vecCond==2 & vecRelP==2);               % SubAd
    
    forProcess = {'subNeut';...
        'subBC_O';'subBC_M';'subBCvL';'subBCvH';...
        'subSP_L';'subSP_H';'subSPvL';'subSPvH';...
        'subBCsO';'subBCsM';'subBCdO';'subBCdM';...
        'subSPsL';'subSPsH';'subSPdL';'subSPdH';...
        'subBCvsL';'subBCvsH';'subBCvdL';'subBCvdH';...
        'subSPvsL';'subSPvsH';'subSPvdL';'subSPvdH';...
        'subV';'subA';'subVs';'subVd';'subAs';'subAd'};
    for step = 1:length(forProcess)
        
        gtrl=find(vecError==0);
        ptrl_error=gtrl(vecError(gtrl(gtrl<length(gtrl))+1)==1); %%%% error on next trial
        mytrls = find(selection{step}&outlier==0);
%         if ~isempty(mytrls) 
%            mytrls= mytrls(round(2:8));
%         end
        %         if step>1 & ~isempty(mytrls)
        %         mytrls = mytrls(2:15);
        %         end
        % mytrls(ismember(mytrls,ptrl_error))=[];
        h                =   nanmean(vecAccu(mytrls(vecGabO(mytrls)== 1)));
        fA               =   nanmean(1-nanmean(vecAccu(mytrls(vecGabO(mytrls)== -1))));
        N = sum(vecAccu(mytrls(vecGabO(mytrls)== -1)));
        %N=50;
        if whichone ==1
            s.(forProcess{step})(i) = Dprime2(h,fA,N);                % dPrime
        elseif whichone ==2
            s.(forProcess{step})(i) = nanmean(vecAccu(selection{step}&  outlier==0))./nanmean(vecRT(selection{step}&  outlier==0)); %%%%% for efficiency, i.e. accuracy/RT
            s.(forProcess{step})(i) = nanmean(vecAccu(selection{step}&  outlier==0)); %%%%% for accuracy;
            s.(forProcess{step})(i) = nanmean(vecAccu(mytrls))./nanmean(vecRT(mytrls)); %%%%% efficiency score, see https://www.journalofcognition.org/articles/10.5334/joc.6/;
           % s.(forProcess{step})(i) = nanmean(vecAccu(mytrls)); %%%%% for accuracy;
        elseif whichone ==3
            s.(forProcess{step})(i) = nanmean(vecRT(mytrls)); %%%%% for accuracy;
        end
    end
    mytrls1 = find(selection{4}&outlier==0);   %%%%% moving average of high  reward color trials
    mytrls2 = find(selection{5}&outlier==0);   %%%%% moving average of low   reward color trials
    mintrl = min(length(mytrls1),length(mytrls2)); %%%% find which of the two has lower number of trials and get from both those trials
    s.mintrl_BC(i)= mintrl;
    s.moveav_subBCvH(i,1:mintrl) = cumsum(vecAccu(mytrls2(1:mintrl)))./(1:mintrl);  %%%%%%% compute moving average of your effect
    s.moveav_subBCvL(i,1:mintrl) = cumsum(vecAccu(mytrls1(1:mintrl)))./(1:mintrl);
    s.effect_ColorValue(i,1:mintrl) = s.moveav_subBCvH(i,1:mintrl)-s.moveav_subBCvL(i,1:mintrl);  %%%%%%%% effect is Hight minus Low
    
    
    mytrls1 = find(selection{8}&outlier==0);   %%%%% moving average of high  reward sound trials
    mytrls2 = find(selection{9}&outlier==0);   %%%%% moving average of low   reward sound trials
    mintrl = min(length(mytrls1),length(mytrls2)); %%%% find which of the two has lower number of trials and get from both those trials
    s.mintrl_SP(i)= mintrl;
    s.moveav_subSPvH(i,1:mintrl) = cumsum(vecAccu(mytrls2(1:mintrl)))./(1:mintrl);
    s.moveav_subSPvL(i,1:mintrl) = cumsum(vecAccu(mytrls1(1:mintrl)))./(1:mintrl);
    s.effect_SoundValue(i,1:mintrl) = s.moveav_subSPvH(i,1:mintrl)-s.moveav_subSPvL(i,1:mintrl);  %%%%%%%% effect is Hight minus Low
    
end
%%
close all hidden
Pp = [1:length(subjects)]; %%%5 7:21 were with Arezoo's crossmodal
% Pp(s.subNeut(Pp)>0.85 | s.subNeut(Pp)<0.55)=[];
% Pp(s.subAccu(Pp)>0.95 | s.subAccu(Pp)<0.55)=[];
if experiment ==8 
     Pp(38)=[];  %%%% for experiment 8 baseline s.subAccu 0.46%
end
%   Pp =[     1     2     3     4     5     6     7    21    22    23    24    25    26    27    28    29    30    31    32    33    34    35    36    37    39    40    41    42 ...
%     43    44    45    46 ]; %(above criterion for baseline)
%Pp = [1:7 21:length(subjects)];
%Pp = [1:34 39:length(subjects)];
%Pp = [1:length(subjects)];
%Pp = Pp(s.subRewS(Pp,2)==1);
%% Figure 100 change of effect size across trials for visual
% 
figure(200), axis square, hold on;
effect_mean = (nanmean(s.effect_ColorValue(Pp,:),1));
eff_sem=(nanstd(s.effect_ColorValue(Pp,:),0,1)./sqrt(length(Pp)));
eff_sem=1.*eff_sem; %%%% for confiedence interval (now is 90% for 95% should multiply by 1.96)

y1=effect_mean-eff_sem; y2=effect_mean+eff_sem;
y2(isnan(y1))=[];y1(isnan(y1))=[];
h=patch([1:length(y1) fliplr(1:length(y1))], [y2  fliplr(y1)],[0.5 0.5 0.5])
plot(1:size(effect_mean,2), nanmean(effect_mean,1),'r')
plot([1:size(effect_mean,2)],zeros(1,size(effect_mean,2)),':k','linewidth',2)
axis([-1 80 -0.15 0.15])
xlabel('ntrials')
ylabel('Performance Difference High reward minus Low Reward')
for t=1:size(s.effect_ColorValue(Pp,:),2)
    [h pp(t)]=ttest(s.effect_SoundValue(Pp,t),0);
    if pp(t)<0.05
        plot(t,-0.5,'sk','MarkerFaceColor',[0 0 0])
    end
end
%%
figure(201), axis square, hold on;
effect_mean = (nanmean(s.effect_SoundValue(Pp,:),1));
eff_sem=(nanstd(s.effect_SoundValue(Pp,:),0,1)./sqrt(length(Pp)));
eff_sem=1.*eff_sem; %%%% for confiedence interval (now is 90% for 95% should multiply by 1.96)

y1=effect_mean-eff_sem; y2=effect_mean+eff_sem;
y2(isnan(y1))=[];y1(isnan(y1))=[];
h=patch([1:length(y1) fliplr(1:length(y1))], [y2  fliplr(y1)],[0.5 0.5 0.5])
plot(1:size(effect_mean,2), nanmean(effect_mean,1),'r')
plot([1:size(effect_mean,2)],zeros(1,size(effect_mean,2)),':k','linewidth',2)
axis([-1 80 -0.15 0.15])

xlabel('ntrials')
ylabel('Performance Difference High reward minus Low Reward')
for t=1:size(s.effect_SoundValue(Pp,:),2)
    [h pp(t)]=ttest(s.effect_SoundValue(Pp,t),0);
    if pp(t)<0.05
        plot(t,-0.5,'sk','MarkerFaceColor',[0 0 0])
    end
end
%% Figure 1 Sound Pitch High-Low value
figure(1);
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSP_H(Pp))  nanmean(s.subSP_L(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSP_H(Pp))  nanmean(s.subSP_L(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSP_H(Pp)) nanstd(s.subSP_L(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none');
ax = gca;
ax.XTickLabel = {'neutral','HighPitch','LowPitch'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p]= ttest(s.subSP_H(Pp),s.subSP_L(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(subjects)) ', date:' date ', P= ' num2str(p)])
% print ('-f1' ,'-dpsc2',[rootdir_fig 'sounds.ps'])

%% Figure 2 Sound
figure(2);
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSPvH(Pp)) nanmean(s.subSPvL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean( s.subSPvH(Pp)) nanmean(s.subSPvL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSPvH(Pp)) nanstd(s.subSPvL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Nautral','HighValSound','LowValSound'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p]= ttest(s.subSPvH(Pp),s.subSPvL(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', P= ' num2str(p)])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%% Figure 3 Box Color Orange-Magenta value
figure(3);
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBC_M(Pp))  nanmean(s.subBC_O(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBC_M(Pp))  nanmean(s.subBC_O(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBC_M(Pp)) nanstd(s.subBC_O(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'neutral','Magenta','Orange'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p]= ttest( s.subBC_M(Pp),s.subBC_O(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(subjects)) ', date:' date ', P= ' num2str(p)])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'colors.ps'])

%% Figure 4 Box Color value Low High
figure(4);
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvH(Pp)) nanmean(s.subBCvL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvH(Pp)) nanmean(s.subBCvL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBCvH(Pp)) nanstd(s.subBCvL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Nautral','HighValCol','LowValCol'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p]= ttest(s.subBCvH(Pp),s.subBCvL(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', P= ' num2str(p)])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%% Figure 5
figure(5)
bar(1, nanmean(s.subBCvH(Pp)-s.subBCvL(Pp))), hold on;
errorbar(1, nanmean(s.subBCvH(Pp)-s.subBCvL(Pp)),nanstd(s.subBCvH(Pp)-s.subBCvL(Pp))./sqrt(length(Pp))...
    ,'k','linewidth',3, 'linestyle','none')
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', P= ' num2str(p)])

%% Figure 6
figure(6)
bar(1, nanmean(s.subSPvH(Pp)-s.subSPvL(Pp))), hold on;
errorbar(1, nanmean(s.subSPvH(Pp)-s.subSPvL(Pp)),nanstd(s.subSPvH(Pp)-s.subSPvL(Pp))./sqrt(length(Pp))...
    ,'k','linewidth',3, 'linestyle','none')
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', P= ' num2str(p)])
%% Figure 7 Box Color value Low High
figure(7);
subplot(1,2,1)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCsM(Pp)) nanmean(s.subBCsO(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCsM(Pp)) nanmean(s.subBCsO(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBCsM(Pp)) nanstd(s.subBCsO(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','MCoS','OCoS'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p]= ttest(s.subBCsM(Pp),s.subBCsO(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', P= ' num2str(p)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%% Figure 8 Box Color value Low High
figure(7);
subplot(1,2,2)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCdM(Pp)) nanmean(s.subBCdO(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCdM(Pp)) nanmean(s.subBCdO(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBCdM(Pp)) nanstd(s.subBCdO(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','MCoD','OCoD'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p]= ttest(s.subBCdM(Pp),s.subBCdO(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', P= ' num2str(p)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])


%% Figure 9 Box Color value Low High
figure(9);
subplot(1,2,1)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvsH(Pp)) nanmean(s.subBCvsL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvsH(Pp)) nanmean(s.subBCvsL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBCvsH(Pp)) nanstd(s.subBCvsL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HVCoS','LVCoS'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p]= ttest(s.subBCvsH(Pp),s.subBCvsL(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', P= ' num2str(p)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%% Figure 10 Box Color value Low High
figure(9);
subplot(1,2,2)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvdH(Pp)) nanmean(s.subBCvdL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvdH(Pp)) nanmean(s.subBCvdL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBCvdH(Pp)) nanstd(s.subBCvdL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HVCoD','LVCoD'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p]= ttest(s.subBCvdH(Pp),s.subBCvdL(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', P= ' num2str(p)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%% Figure 11 Sound
figure(11);
subplot(1,2,1)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSPsH(Pp)) nanmean(s.subSPsL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean( s.subSPsH(Pp)) nanmean(s.subSPsL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSPsH(Pp)) nanstd(s.subSPsL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HPSoS','LPSoS'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p]= ttest(s.subSPsH(Pp),s.subSPsL(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', P= ' num2str(p)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%% Figure 12 Sound
figure(11);
subplot(1,2,2)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSPdH(Pp)) nanmean(s.subSPdL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean( s.subSPdH(Pp)) nanmean(s.subSPdL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSPdH(Pp)) nanstd(s.subSPdL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HPSoD','LPSoD'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p]= ttest(s.subSPdH(Pp),s.subSPdL(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', P= ' num2str(p)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%% Figure 13 Sound
figure(13);
subplot(1,2,1)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSPvsH(Pp)) nanmean(s.subSPvsL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean( s.subSPvsH(Pp)) nanmean(s.subSPvsL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSPvsH(Pp)) nanstd(s.subSPvsL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HVSoS','LVSoS'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p]= ttest(s.subSPvsH(Pp),s.subSPvsL(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', P= ' num2str(p)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
%print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%% Figure 14 Sound
figure(13);
subplot(1,2,2)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSPvdH(Pp)) nanmean(s.subSPvdL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean( s.subSPvdH(Pp)) nanmean(s.subSPvdL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSPvdH(Pp)) nanstd(s.subSPvdL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HVSoD','LVSoD'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p]= ttest(s.subSPvdH(Pp),s.subSPvdL(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', P= ' num2str(p)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
%print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%
%% Figure 13 Sound
figure(130);
bar(1:5,[nanmean(s.subNeut(Pp)) nanmean(s.subSPvsH(Pp)) nanmean(s.subSPvsL(Pp))  nanmean(s.subSPvdH(Pp)) nanmean(s.subSPvdL(Pp))],'w'), hold on
errorbar(1:5,[nanmean(s.subNeut(Pp)) nanmean( s.subSPvsH(Pp)) nanmean(s.subSPvsL(Pp))  nanmean(s.subSPvdH(Pp)) nanmean(s.subSPvdL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSPvsH(Pp)) nanstd(s.subSPvsL(Pp)) nanstd(s.subSPvdH(Pp)) nanstd(s.subSPvdL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HVSoS','LVSoS','HVSoD','LVSoD'};
if whichone==1
    ax.YLim = [1 5];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p1]= ttest(s.subSPvsH(Pp),s.subSPvsL(Pp));
[h p2]= ttest(s.subSPvdH(Pp),s.subSPvdL(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', P-sameside= ' num2str(p1) ', P-diffside= ' num2str(p2)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P-sameside= ' num2str(p1) ', P-diffside= ' num2str(p2)])
%print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%
display(['nanmean sound volume across subjects: ' num2str(nanmean(s.subSVol))])
%%
%% Figure 2 Sound
figure(14);
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subV(Pp)) nanmean(s.subA(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean( s.subV(Pp)) nanmean(s.subA(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subV(Pp)) nanstd(s.subA(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Nautral','AllVisual','AllAuditory'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p]= ttest(s.subV(Pp),s.subA(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', visual vs auditory P= ' num2str(p)])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%% Figure 2 Sound
figure(15);
bar(1:5,[nanmean(s.subNeut(Pp)) nanmean(s.subVs(Pp)) nanmean(s.subVd(Pp)) nanmean(s.subAs(Pp)) nanmean(s.subAd(Pp))],'r'), hold on
errorbar(1:5,[nanmean(s.subNeut(Pp)) nanmean(s.subVs(Pp)) nanmean(s.subVd(Pp)) nanmean(s.subAs(Pp)) nanmean(s.subAd(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subVs(Pp)) nanstd(s.subVd(Pp)) nanstd(s.subAs(Pp)) nanstd(s.subAd(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Nautral','V-S','V-D','A-S','A-D'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p1]= ttest(s.subVs(Pp),s.subVd(Pp));
[h p2]= ttest(s.subAs(Pp),s.subAd(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', visual P= ' num2str(p1) ', auditory P= ' num2str(p2)])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%% Figure 2 Sound
figure(16);
bar(1:9,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvsH(Pp)) nanmean(s.subBCvdH(Pp)) nanmean(s.subBCvsL(Pp)) nanmean(s.subBCvdL(Pp)) nanmean(s.subSPvsH(Pp))...
    nanmean(s.subSPvdH(Pp)) nanmean(s.subSPvsL(Pp)) nanmean(s.subSPvdL(Pp))],'r'), hold on
errorbar(1:9,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvsH(Pp)) nanmean(s.subBCvdH(Pp)) nanmean(s.subBCvsL(Pp)) nanmean(s.subBCvdL(Pp)) nanmean(s.subSPvsH(Pp))...
    nanmean(s.subSPvdH(Pp)) nanmean(s.subSPvsL(Pp)) nanmean(s.subSPvdL(Pp))],...
    [[nanstd(s.subNeut(Pp)) nanstd(s.subBCvsH(Pp)) nanstd(s.subBCvdH(Pp)) nanstd(s.subBCvsL(Pp)) nanstd(s.subBCvdL(Pp)) nanstd(s.subSPvsH(Pp))...
    nanstd(s.subSPvdH(Pp)) nanstd(s.subSPvsL(Pp)) nanstd(s.subSPvdL(Pp))]]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Nautral','HV-S','HV-D','LV-S','LV-D','HA-S','HA-D','LA-S','LA-D'};
if whichone==1
    ax.YLim = [1 3];
elseif whichone==2
    ax.YLim = [.7 0.9];
elseif whichone==3
    ax.YLim = [.3 0.9];
end
[h p1]= ttest(s.subVs(Pp),s.subVd(Pp));
[h p2]= ttest(s.subAs(Pp),s.subAd(Pp));
title([ experiment_name ', Blocks:' num2str(rel_blocks)...
    ', N= ' num2str(length(Pp)) ', date:' date ', visual P= ' num2str(p1) ', auditory P= ' num2str(p2)])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
