clearvars
close all

process = 'C:\Users\arezoo\Desktop\test\';
timeWind = [-.15 .8]; filter = [.5 30]; mybaseline =[-150 0];
TrPreC  = {'S 11','S 12','S 13','S 14','S 15','S 16','S 17','S 18','S 19'}; TrPreCex = {'ViLoL','ViHiL','AuLoL','AuHiL','Neut','ViLoR','ViHiR','AuLoR','AuHiR'};
TrPost  = {'S 41','S 42','S 43','S 44','S 45','S 46','S 47','S 48','S 49'}; TrPostex = {'ViLoL','ViHiL','AuLoL','AuHiL','Neut','ViLoR','ViHiR','AuLoR','AuHiR'};
TrCond  = {'S 71','S 72','S 73','S 74','S 76','S 77','S 78','S 79'};        TrCondex = {'ViLoL','ViHiL','AuLoL','AuHiL','ViLoR','ViHiR','AuLoR','AuHiR'};
PrePost = {TrPreC,TrPost,TrCond}; PrePostex = {TrPreCex,TrPostex,TrCondex}; PrePostText = {'TrPre','TrPost','TrCond'};

%% Preprocess EEG Data
EEGDataProcessing('load', process, 'epoch',timeWind, 'filter', filter, 'events', [PrePost{:}], 'ref', [20 64]);

%% Create STUDIES
if 0 == exist([process filesep 'Studies'],'dir'),mkdir([startPath filesep 'Studies']); end
load([process filesep 'doneFileEEG.mat']);
for session = 1:length(PrePost)         % for Pre-Post-Cond
    triggers = PrePost{session}; indexcount=0; STUDY = [];ALLEEG = [];
    mystr =cell(1,length(doneFileEEG)*length(triggers));
    for subs = 1:length(doneFileEEG)       % For each Pp
        for conds = 1:length(triggers)  % For each trigger
            indexcount=indexcount+1;
            if any(strcmp(triggers(conds), {'S 11' 'S 12' 'S 16' 'S 17' 'S 41' 'S 42' 'S 46' 'S 47' 'S 71' 'S 72' 'S 76' 'S 77'})); modality = 'visual'; else modality = 'sound';  end
            if any(strcmp(triggers(conds), {'S 11' 'S 13' 'S 16' 'S 18' 'S 41' 'S 43' 'S 46' 'S 48' 'S 71' 'S 73' 'S 76' 'S 78'})); value = 'low'; else value = 'high';  end
            if any(strcmp(triggers(conds), {'S 11' 'S 12' 'S 13' 'S 14' 'S 41' 'S 42' 'S 43' 'S 44' 'S 71' 'S 72' 'S 73' 'S 74'})); side = 'left'; else side = 'right';  end
            if any(strcmp(triggers(conds), {'S 11' 'S 12' 'S 13' 'S 14' 'S 16' 'S 17' 'S 18' 'S 19'})); learn = 1; elseif any(strcmp(triggers(conds),{'S 71','S 72','S 73','S 74','S 76','S 77','S 78','S 79'})), learn = 0; else learn = 2;  end
            if any(strcmp(triggers(conds), {'S 15' 'S 45'})); value = 'no'; modality = 'neutral'; side = 'no'; end
            mystr{indexcount}={'index' indexcount 'load' [process filesep [doneFileEEG(subs).Pp] filesep 'EEG' filesep 'PreProcessed' filesep [doneFileEEG(subs).Pp] '_' triggers{conds} '.set']...
                'subject' [doneFileEEG(subs).Pp] 'session' learn 'condition' triggers{conds}}; % trigger-by-trigger task
        end
    end
    
    % Create study with commands for all triggers
    [STUDY, ALLEEG] = std_editset(STUDY, ALLEEG, 'name',[process num2str(length(doneFileEEG)) '_' PrePostText{session} '_' num2str(date) 'subjects.study'],'commands',...
        mystr, 'updatedat','on','rmclust','on');
    STUDY.conditionEx = PrePostex{session};
    % Precompilation (necessarz for data continuity)
    [STUDY, ALLEEG] = std_precomp(STUDY, ALLEEG, {},'interp','on','recompute','on','erp','on','erpparams',{'rmbase' mybaseline });
    % Save study
    [STUDY, ALLEEG] = pop_savestudy(STUDY, ALLEEG, 'filename',[num2str(length(doneFileEEG)) 'Pp_' PrePostText{session} '_'  num2str(date) '.study'],'filepath',[process filesep 'Studies']);
end
disp('STRUCTURES CREATED');
%% Triggers meanings
% S11: Vis LowV LEFT
% S12: Vis HigV LEFT
% S13: Aud LowV LEFT
% S14: Aud HigV LEFT
% S15: Neutal
% S16: Vis LowV RIGHT
% S17: Vis HigV RIGHT
% S18: Aud LowV RIGHT
% S19: Aud HigV RIGHT
%
% S21: Vis LowV LEFT
% S22: Vis HigV LEFT
% S23: Aud LowV LEFT
% S24: Aud HigV LEFT
% S25: Neutal
% S26: Vis HigV RIGHT
% S27: Aud LowV RIGHT
% S28: Aud LowV RIGHT
% S29: Aud HigV RIGHT

% S71: Vis LowV LEFT
% S72: Vis HigV LEFT
% S73: Aud LowV LEFT
% S74: Aud HigV LEFT
% S76: Vis HigV RIGHT
% S77: Aud LowV RIGHT
% S78: Aud LowV RIGHT
% S79: Aud HigV RIGHT