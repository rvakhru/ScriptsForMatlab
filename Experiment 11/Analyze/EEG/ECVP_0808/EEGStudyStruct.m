clearvars
close all

process = 'T:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA\';
process = uigetdir(process);process = [process filesep];
uniName = inputdlg('Pick A Unique Name','Normal');
timeWind = [-.1 0.8]; filter = [.2 35]; mybaseline =[-100 0];
TrPreC  = {'S 11','S 12','S 13','S 14','S 15','S 16','S 17','S 18','S 19','S 20'}; TrPreCex = {'ViLoL','ViHiL','AuLoL','AuHiL','ViLoR','ViHiR','AuLoR','AuHiR','NeutL','NeutR'};
TrPost  = {'S 41','S 42','S 43','S 44','S 45','S 46','S 47','S 48','S 49','S 50'}; TrPostex = {'ViLoL','ViHiL','AuLoL','AuHiL','ViLoR','ViHiR','AuLoR','AuHiR','NeutL','NeutR'};
TrCond  = {'S 71','S 72','S 73','S 74','S 75','S 76','S 77','S 78'};               TrCondex = {'ViLoL','ViHiL','AuLoL','AuHiL','ViLoR','ViHiR','AuLoR','AuHiR'};
PrePost = {TrPreC,TrPost,TrCond}; PrePostex = {TrPreCex,TrPostex,TrCondex}; PrePostText = {'TrPre','TrPost','TrCond'}; %,TrPost,TrCond ,TrPostex,TrCondex  ,'TrPost','TrCond'

%% Preprocess EEG Data
EEGDataProcessing('load', process,'sampling',1000, 'epoch',timeWind, 'filter', filter, 'events', [PrePost{:}], 'ref', [] ,'pName', uniName{:});

%% Create STUDIES
if 0 == exist([process filesep 'Studies'],'dir'),mkdir([process filesep 'Studies']); end
load([process 'doneFileEEG_' uniName{:} '.mat']);
[tmp, tmpval] = pop_chansel({doneFileEEG.Pp}, 'withindex', 'on');
doneFileEEG = doneFileEEG(tmp);
for session = 1:length(PrePost)         % for Pre-Post-Cond
    triggers = PrePost{session}; indexcount=0; STUDY = [];ALLEEG = [];
    mystr =cell(1,length(doneFileEEG)*length(triggers));
    for subs = 1:length(doneFileEEG)       % For each Pp
        if subs<10,subname = ['0' num2str(subs)];else subname = num2str(subs);end
        for conds = 1:length(triggers)  % For each trigger
            indexcount=indexcount+1;
            if any(strcmp(triggers(conds), {'S 11' 'S 12' 'S 15' 'S 16' 'S 41' 'S 42' 'S 45' 'S 46' 'S 71' 'S 72' 'S 75' 'S 76'})); modality = 'visual'; else modality = 'sound';  end
            if any(strcmp(triggers(conds), {'S 11' 'S 13' 'S 15' 'S 17' 'S 41' 'S 43' 'S 45' 'S 47' 'S 71' 'S 73' 'S 75' 'S 77'})); value = 'low'; else value = 'high';  end
            if any(strcmp(triggers(conds), {'S 11' 'S 12' 'S 13' 'S 14' 'S 19' 'S 41' 'S 42' 'S 43' 'S 44' 'S 49' 'S 71' 'S 72' 'S 73' 'S 74'})); side = 'left'; else side = 'right';  end
            if any(strcmp(triggers(conds), {'S 11' 'S 12' 'S 13' 'S 14' 'S 15' 'S 16' 'S 17' 'S 18' 'S 19' 'S 20'})); learn = 1; elseif any(strcmp(triggers(conds),{'S 71','S 72','S 73','S 74','S 75','S 76','S 77','S 78','S 79'})), learn = 0; else learn = 2;  end
            if any(strcmp(triggers(conds), {'S 19' 'S 20' 'S 49' 'S 50'})); value = 'no'; modality = 'neutral'; end
            mystr{indexcount}={'index' indexcount 'load' [process [doneFileEEG(subs).Pp] filesep 'EEG' filesep 'PreProcessed' uniName{:} filesep [doneFileEEG(subs).Pp] '_' triggers{conds} '.set']...
                'subject' subname 'session' [] 'condition' triggers{conds}}; % trigger-by-trigger task
        end
    end
    
    % Create study with commands for all triggers
    [STUDY, ALLEEG] = std_editset(STUDY, ALLEEG, 'name',[process num2str(length(doneFileEEG)) 'Pp_' PrePostText{session} '_' num2str(date) '.study'],'commands',...
        mystr, 'updatedat','on','rmclust','on');
    STUDY.conditionEx = PrePostex{session};
    % Precompilation (necessarz for data continuity)
    [STUDY, ALLEEG] = std_precomp(STUDY, ALLEEG, {},'interp','on','recompute','on','erp','on','erpparams',{'rmbase' mybaseline });
    % Save study
    [STUDY, ALLEEG] = pop_savestudy(STUDY, ALLEEG, 'filename', [PrePostText{session} '_' uniName{:} '.study'],'filepath',[process 'Studies']);
end
disp('STRUCTURES CREATED');
%% Triggers meanings
% S11: Vis LowV LEFT
% S12: Vis HigV LEFT
% S13: Aud LowV LEFT
% S14: Aud HigV LEFT
% S15: Vis LowV RIGHT
% S16: Vis HigV RIGHT
% S17: Aud LowV RIGHT
% S18: Aud HigV RIGHT
% S19: Neut     LEFT
% S20: Neut     RIGHT
%
% S21: Vis LowV LEFT
% S22: Vis HigV LEFT
% S23: Aud LowV LEFT
% S24: Aud HigV LEFT
% S25: Vis LowV RIGHT
% S26: Vis HigV RIGHT
% S27: Aud LowV RIGHT
% S28: Aud HigV RIGHT
% S29: Neut     LEFT
% S30: Neut     RIGHT

% S71: Vis LowV LEFT
% S72: Vis HigV LEFT
% S73: Aud LowV LEFT
% S15: Vis LowV RIGHT
% S16: Vis HigV RIGHT
% S17: Aud LowV RIGHT
% S18: Aud HigV RIGHT