% function ProcessEDF(block)
% edition 11.12
clearvars
addpath W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\Analyze\EyeTracker\EXTRACT_DATA
start_path = fullfile('C:\');
myPath = uigetdir(start_path);
cd(myPath);
SubFold = cd;
% filePattern = fullfile(SubFold, '*.*');
folders = dir;
if 2 == exist('doneFileEYE.mat','file')
%     load('allData'); % problem here
    load('doneFileEYE');
    doneFileInx = length(doneFileEYE)+1;
else
    allData = [];
    doneFileEYE = struct; 
    doneFileEYE.Pp = [];
    doneFileInx = 1;
end

for fls = 3:length(folders) % for each folder (Participant)
    if folders(fls).isdir && ~any(strcmp({doneFileEYE.Pp}, folders(fls).name)) && all(~strcmp(folders(fls).name,{'99999999','Studies'})) %&& ~isnan(str2double(folders(fls).name)) % It must be new folder
        inFolder = [SubFold filesep folders(fls).name];
        load([inFolder filesep strcat(folders(fls).name,'_', '_allData','.mat')]); % load BLOCKS
        % allBlocks = [myVar.strGabPre myVar.strGabPost];
        allBlocks = 1:length(block);
        for i = 1:3
            bloPost = [];
            for blo = 2:length(block)
                if ismember(block(blo).trials(1).BlPrePost,[i]), bloPost = [bloPost block(blo).trials]; end
            end
            meanRT(i) = nanmean([bloPost([bloPost.error]==0).RT]); sdRT(i) = nanstd([bloPost([bloPost.error]==0).RT]);
        end
        files = dir(inFolder);
        for targFiles = 3:length(files)                                             % all files (1,2 is navigation)
            for targBlocks = 1:length(allBlocks)                                    % all blocks
                if strcmp(files(targFiles).name,...
                        [folders(fls).name '_Block_' num2str(allBlocks(targBlocks)) '.edf'])
                    %% Extract Eye data
                    name = [inFolder filesep files(targFiles).name];                % locate directory
                    tempASC = readEDFASC(name,1,1);                                 % Process current block
                    
                    %% Combine structures
                    temToStore = block(allBlocks(targBlocks)).trials;               % PREPARE block structure
                    tempComb = catstruct(temToStore,tempASC);                       % COMBINE EYE and BLOCK
                    tempComb([tempComb.error]==5)=[];                               % Error 5 - no eye data, remove                    
                    
                    %% Remove extra fields
                    if isfield(tempComb,'CAL_m'),       tempComb = rmfield(tempComb,'CAL_m');       tempComb = rmfield(tempComb,'CAL_t');end                    
                    if isfield(tempComb,'SACCADE_m'),   tempComb = rmfield(tempComb,'SACCADE_m');   tempComb = rmfield(tempComb,'SACCADE_t'); end
                    if isfield(tempComb,'VALIDATE_t'),  tempComb = rmfield(tempComb,'VALIDATE_t');  tempComb = rmfield(tempComb,'VALIDATE_m');end
                    if isfield(tempComb,'DRIFTCORRECT_m'),tempComb = rmfield(tempComb,'DRIFTCORRECT_m'); tempComb = rmfield(tempComb,'DRIFTCORRECT_t');end
                    
                    %% SELECT FIXATION and TARGET
                    selectionFixation = arrayfun(@(x)...                                        %Prepare FIXATION only
                        tempComb(x).pos(tempComb(x).FIXATION_t+1:tempComb(x).TARGET_t,:),...
                        1:length(tempComb), 'UniformOutput',false);
                    selection = arrayfun(@(x)...                                                %Prepare TARGET only
                        tempComb(x).pos(tempComb(x).TARGET_t+1:tempComb(x).TARGET_OFF_t,:),...
                        1:length(tempComb), 'UniformOutput',false);
                    
                    %% ESTIMATE MOVEMENT
                    maxDistSample = arrayfun(@(x) abs(max(selection{x}(:,2))-myVar.centerX),... % Screen CENTER
                        1:length(selection), 'UniformOutput', 0);
                    dist = arrayfun(@(x) ...                                                    % FIXATION BASELINE
                        sqrt((nanmean(selection{x}(:,2)) - nanmean(selectionFixation{x}(end-198:end,2)))^2 +...
                             (nanmean(selection{x}(:,3)) - nanmean(selectionFixation{x}(end-198:end,3)))^2),...
                        1:length(selection), 'UniformOutput', 0);
                    %% Combine structures
                    for i=1:length(selection)
                        tempComb(i).posSel  = selection{i};
                        tempComb(i).maxDist = maxDistSample{i}/Scr.pixelsperdegree;
                        tempComb(i).badRT   = tempComb(i).RT> (meanRT(tempComb(i).BlPrePost)+2.5*sdRT(tempComb(i).BlPrePost))|...
                            tempComb(i).RT<(meanRT(tempComb(i).BlPrePost)-2.5*sdRT(tempComb(i).BlPrePost));
                        tempComb(i).dist    = dist{i}/Scr.pixelsperdegree;
                    end
                    
                    %% Combine blocks
                    block(allBlocks(targBlocks)).trials = tempComb;
%                     if 1 == exist('allData','var'), allData = [allData tempComb]; else allData = tempComb; end % STRUC with all Pp
                end
            end
        end
        doneFileEYE(doneFileInx).Pp = folders(fls).name;
        doneFileEYE(doneFileInx).path = inFolder;
        doneFileEYE(doneFileInx).date = date;
        save([inFolder filesep strcat(folders(fls).name,'_', '_allDataEyE','.mat')],'block','Scr','myVar','inf');
        save('doneFileEYE','doneFileEYE');
        doneFileInx = doneFileInx+1;
    end
%     save('allData','allData','doneFileEYE','Scr','myVar','inf','-v7.3');
end
disp('PROCESSING IS COMPLETE');