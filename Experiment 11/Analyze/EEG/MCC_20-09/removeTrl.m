function [EEG,mydiff,myindex] = removeTrl(EEG,inFolderEye)
load(inFolderEye);
%% Remove errors like Drift or Calibration 
toDelete = [];
for events = 1:length(EEG.event)
    if  any(strcmp([EEG.event(events).type],{'S254'}))
        for eventsBack = events:-1:1
            if  any(strcmp([EEG.event(eventsBack).type],{'S  1', 'S 31', 'S 61'}))
                 toDelete = [toDelete eventsBack:events]; break;
            end
        end
    end
end
EEG.event(toDelete) = [];
EEG.urevent(toDelete) = [];


%% Remove unwanted trials
countT = 1;k = 1;
trlErrRep = struct;
trlProblem = false;
trl = 1; bl = 2;
toDelete = [];

for events = 1:length(EEG.event)
    if any(strcmp([EEG.event(events).type],{'S255'})), trl = 1; bl = bl+1; end
%     if any(strcmp([EEG.event(events).type],{'S 39'})),toDelete= [toDelete events+1]; end
    if any(strcmp([EEG.event(events).type],{'S  1', 'S 31', 'S 61'}))
        EEG.event(events).trl = trl; EEG.event(events).bl = bl;
        newTrl = true;
        for enentIN = events+1:length(EEG.event)
            EEG.event(enentIN).trl = trl; EEG.event(enentIN).bl = bl;
            if any(strcmp([EEG.event(enentIN).type],{'S  5','S 35','S 65'})),
                EEG.event(enentIN).type = 'S  5';
            end
            
            % Decode my conditions Vakhrushev 20.09
            EEG.event(enentIN).bl =                 bl;
            EEG.event(enentIN).trl =                trl;
            EEG.event(enentIN).condition =          block(bl).trials(trl).condition; 
            EEG.event(enentIN).conSort =            block(bl).trials(trl).conSort; 
            EEG.event(enentIN).conditionType =      block(bl).trials(trl).conditionType;             
            EEG.event(enentIN).GL =                 block(bl).trials(trl).GL;   
            EEG.event(enentIN).gaborOri =           block(bl).trials(trl).gaborOri;   
            EEG.event(enentIN).BL =                 block(bl).trials(trl).BL;   
            EEG.event(enentIN).BC =                 block(bl).trials(trl).BC;   
              
            EEG.event(enentIN).SL =                 block(bl).trials(trl).SL;   
            EEG.event(enentIN).SP =                 block(bl).trials(trl).SP;   
            
            EEG.event(enentIN).RT =                 block(bl).trials(trl).RT;   
            EEG.event(enentIN).boxCol =             block(bl).trials(trl).boxCol; 
            EEG.event(enentIN).soundPitch =         block(bl).trials(trl).soundPitch;
            EEG.event(enentIN).StimValue =          block(bl).trials(trl).StimValue; 
            EEG.event(enentIN).Accuracy =           block(bl).trials(trl).Accuracy;   
            EEG.event(enentIN).keyPressed =         block(bl).trials(trl).keyPressed;   
            EEG.event(enentIN).expMode =            block(bl).trials(trl).expMode; 
            
            EEG.event(enentIN).BlPrePost =          block(bl).trials(trl).BlPrePost; 
            EEG.event(enentIN).Bl_Tr_Cn_Rp_Go_m =   block(bl).trials(trl).Bl_Tr_Cn_Rp_Go_m; 

            
            if any(strcmp([EEG.event(enentIN).code],{'VLL','VHL','AHL','AHL','VHR','VHR','AHR','AHR','N_L','n_R'}))
                trlErrRep(countT).Cond = [EEG.event(enentIN).code];
            end
            % 'S 30','S 60','S 90',
%             if trl==1, trlProblem= true; newTrl = false; end
            %if  (block(bl).trials(trl).error ~= 0 && newTrl && block(bl).trials(trl).Accuracy ~= 0)
            if  newTrl && (block(bl).trials(trl).error ~= 0 || block(bl).trials(trl).dist > 0.9 || block(bl).trials(trl).badRT)
                trlProblem= true; newTrl = false;
                trlErrRep(countT).problem = [EEG.event(enentIN).type];
                trlErrRep(countT).trl = trl;
                trlErrRep(countT).bl = bl;
                
            elseif any(strcmp([EEG.event(enentIN).type],{'S  1', 'S 31', 'S 61'}))
                if trlProblem
%                     EEG.event(events:enentIN) = [];
                    toDelete = [toDelete events:enentIN-1];
                    
                    trlErrRep(countT).duration = [EEG.event(enentIN).latency] - [EEG.event(events).latency];
                    trlErrRep(countT).start = events;
                    trlErrRep(countT).end = enentIN;
                    trlErrRep(countT).steps = enentIN-events;
                    trlErrRep(countT).code = [EEG.event(events+1).code];
                    countT = countT+1;
                end
                trl = trl + 1; k = k+1; trlProblem = false; break;
            end
        end
    end
end
EEG.event(toDelete) = [];
EEG.urevent(toDelete) = [];
end