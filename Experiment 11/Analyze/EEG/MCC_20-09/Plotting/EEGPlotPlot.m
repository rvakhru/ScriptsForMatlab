[process, locat] = uigetfile('T:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA\Studies\*.*');
% if ~any(exist('STUDY','var') && strcmp(process, processed))
[STUDY, ALLEEG] = pop_loadstudy('filename', process,'filepath',locat); processed = process; name = strsplit(process,'.'); name = name{1};
% end
mychans = {STUDY.changrp.name};
mysubs ={ALLEEG.filepath};
% [~,index] = sortrows(mychans.'); channels = mychans(index); clear index
% [tmp, tmpval] = pop_chansel(channels, 'withindex', 'on');

%% Plot data
% [STUDY, ALLEEG] = pop_loadstudy('filename', process,'filepath',locat);
STUDY = pop_statparams(STUDY, 'condstats','on','mcorrect','none','alpha',0.05);
STUDY = pop_erpparams(STUDY, 'plotconditions','together','topotime',[]);
[STUDY, erpdata, erptimes] = std_erpplot(STUDY,ALLEEG,'channels', mychans, 'noplot', 'on');
save([locat 'EEGPlootPlot_test' name '.mat'],'mychans','mysubs','erpdata','erptimes','locat','process');