%function [] = EEGexp11()
% Analyze EEG data for experiment 11
%
% 1.4.1 Main effect of VALUE
% (d�) Simultaneous valuable cue increases the sensitivity of discrimination of the visual target.
% (RT) Simultaneous high-value cue decreases reaction times.
% (EEG) Within modal valuable and cross-modal valuable cues both modulate early visual sensory areas (Posterior electrodes).
% (timeEEG) High-value cue evoked ERP responses occur earlier than low-value cue modulation.
%
% 1.4.2 Main effect of MODALITY
% (EEG) Within modal valuable cue modulation of ERP components is stronger in amplitude than the between-modal condition.
% (timeEEG) Within modal valuable cue, modulation occurs earlier than the between-modal condition.
%
% 1.4.3 Correlations
% (d�/EEG)Change in the behavioral target sensitivity correlates positively with the change in amplitude and latency of early ERP components due to reward.
% (pupil/EEG)Change of the pupil size correlates positively with the change in amplitude and latency of early ERP components due to reward.
% (d� error)The effect of reward is independent of the eye movements.
clear all
%% Obtain specific data
%  load('W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA\Studies\EEGPlootPlot_TrPost_noErrorAlphaAvg.mat');
% load('W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\\DATA_F\Studies\EEGPlootPlot_TrPost_5Epoch2times.mat');
load('W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA_F\Studies\EEGPlootPlot_TrPre_5Epoch2times.mat');
% Using MyERP
figure;
%plot(erptimes, mean(myerp,3));
mybaseline =[-100 0];
winlength = 30;
individualpeak = 0; 
my_ROI = {'O1' 'O2' 'PO7' 'PO8'};
for i = 1:length(my_ROI)
    electrodes(i) = find(strcmp(mychans,my_ROI{i}));
end
allWindow = 1:700;
p1Window = (-1)*mybaseline(1)+[70:170];
n1Window = (-1)*mybaseline(1)+[180:250];
p3Window = (-1)*mybaseline(1)+[300:600];
p1n1Window = (-1)*mybaseline(1)+[180:200];

% p1Window = (-1)*mybaseline(1)+[120:140];
% n1Window = (-1)*mybaseline(1)+[160:190];

%% Preallocate
NSamp = size(erpdata{1},1);
NCahn = size(erpdata{1},2);
NSub = size(erpdata{1},3);
myP1VL_P1_window = nan(NSub,winlength);
myP1VH_P1_window = nan(NSub,winlength);
myP1SL_P1_window = nan(NSub,winlength);
myP1SH_P1_window = nan(NSub,winlength);
myP1N_P1_window = nan(NSub,winlength);


%% Process data
% myP1VL = squeeze(myerp(50:250,:,:));
myP1VLL = mean(erpdata{1}(:,electrodes,:),2);
myP1VHL = mean(erpdata{2}(:,electrodes,:),2);
myP1SLL = mean(erpdata{3}(:,electrodes,:),2);
myP1SHL = mean(erpdata{4}(:,electrodes,:),2);
myP1VLR = mean(erpdata{5}(:,electrodes,:),2);
myP1VHR = mean(erpdata{6}(:,electrodes,:),2);
myP1SLR = mean(erpdata{7}(:,electrodes,:),2);
myP1SHR = mean(erpdata{8}(:,electrodes,:),2);
myP1N_L = mean(erpdata{9}(:,electrodes,:),2);
myP1N_R = mean(erpdata{10}(:,electrodes,:),2);


myP1VL = squeeze(myP1VLL + myP1VLR)/2;
myP1VH = squeeze(myP1VHL + myP1VHR)/2;
myP1SL = squeeze(myP1SLL + myP1SLR)/2;
myP1SH = squeeze(myP1SHL + myP1SHR)/2;
myP1N  = squeeze(myP1N_L + myP1N_R)/2;

if individualpeak
    [myP1VL_P1, myP1VL_loc_P1] = max(myP1VL(p1Window,:));
    [myP1VH_P1, myP1VH_loc_P1] = max(myP1VH(p1Window,:));
    [myP1SL_P1, myP1SL_loc_P1] = max(myP1SL(p1Window,:));
    [myP1SH_P1, myP1SH_loc_P1] = max(myP1SH(p1Window,:));
    [myP1N_P1, myP1N_loc_P1] =   max(myP1N (p1Window,:));
else
    [myP1VL_P1, myP1VL_loc_P1] = max(mean(myP1VL(p1Window,:),2));
    [myP1VH_P1, myP1VH_loc_P1] = max(mean(myP1VH(p1Window,:),2));
    [myP1SL_P1, myP1SL_loc_P1] = max(mean(myP1SL(p1Window,:),2));
    [myP1SH_P1, myP1SH_loc_P1] = max(mean(myP1SH(p1Window,:),2));
    [myP1N_P1, myP1N_loc_P1] =   max(mean(myP1N(p1Window,:),2));
    
    myP1VL_P1 = repmat(myP1VL_P1,[1 NSub]);
    myP1VH_P1 = repmat(myP1VH_P1,[1 NSub]);
    myP1SL_P1 = repmat(myP1SL_P1,[1 NSub]);
    myP1SH_P1 = repmat(myP1SH_P1,[1 NSub]);
    myP1N_P1 = repmat(myP1N_P1,[1 NSub]);
    
    myP1VL_loc_P1 = repmat(myP1VL_loc_P1,[1 NSub]);
    myP1VH_loc_P1 = repmat(myP1VH_loc_P1,[1 NSub]);
    myP1SL_loc_P1 = repmat(myP1SL_loc_P1,[1 NSub]);
    myP1SH_loc_P1 = repmat(myP1SH_loc_P1,[1 NSub]);
    myP1N_loc_P1 = repmat(myP1N_loc_P1,[1 NSub]);
end


%% For each participant

for i = 1:NSub
    myP1VL_P1_window(i,:) = [p1Window(myP1VL_loc_P1(i))-winlength/2:p1Window(myP1VL_loc_P1(i))+winlength/2-1];
    myP1VH_P1_window(i,:) = [p1Window(myP1VH_loc_P1(i))-winlength/2:p1Window(myP1VH_loc_P1(i))+winlength/2-1];
    myP1SL_P1_window(i,:) = [p1Window(myP1SL_loc_P1(i))-winlength/2:p1Window(myP1SL_loc_P1(i))+winlength/2-1];
    myP1SH_P1_window(i,:) = [p1Window(myP1SH_loc_P1(i))-winlength/2:p1Window(myP1SH_loc_P1(i))+winlength/2-1];
    myP1N_P1_window(i,:) =  [p1Window(myP1N_loc_P1(i))-winlength/2:p1Window(myP1N_loc_P1(i))+winlength/2-1];
end
figure(1); hold on;
for i = 1:NSub
    plot(erptimes(myP1SH_P1_window(i,:)),myP1SH(myP1SH_P1_window(i,:),i),'linewidth', 5);
    mySH_P1_window_mean(i) = mean(myP1SH(myP1SH_P1_window(i,:),i));
    mySL_P1_window_mean(i) = mean(myP1SL(myP1SL_P1_window(i,:),i));
    myVH_P1_window_mean(i) = mean(myP1VH(myP1VH_P1_window(i,:),i));
    myVL_P1_window_mean(i) = mean(myP1VL(myP1VL_P1_window(i,:),i));
    myN_P1_window_mean(i) = mean(myP1N(myP1N_P1_window(i,:),i));
%     
%       mySH_P1_window_mean(i) = mean(mean(myP1SH(:,i)));
%     mySL_P1_window_mean(i) = mean(mean(myP1SL(:,i)));
%     myVH_P1_window_mean(i) = mean(mean(myP1VH(:,i)));
%     myVL_P1_window_mean(i) = mean(mean(myP1VL(:,i)));
%     myN_P1_window_mean(i) = mean(mean(myP1N(:,i)));
end


hold on
plot(erptimes(allWindow),mean((myP1SH(allWindow,:,:)),2),'linewidth',3); % Mean

plot(erptimes(allWindow),mean((myP1SH(allWindow,:,:)),2),'linewidth',3); % Mean


plot(erptimes(allWindow),(myP1SH(allWindow,:,:)),'linewidth',.5);           % For Participant
% plot(erptimes(allWindow),(myP1SL(allWindow,:,:)),'linewidth',.5);           % For Participant

hold on
%plot(erptimes(allWindow),(myP1SL(allWindow,:,:)),'linewidth',3);
plot([erptimes(p1Window(1))    erptimes(p1Window(1))],[-5  20],'-.m')
plot([erptimes(p1Window(end))  erptimes(p1Window(end))  ],[-5  20],'-.m')

plot([erptimes(n1Window(1))    erptimes(n1Window(1))],[-5  20 ],'-.k')
plot([erptimes(n1Window(end))  erptimes(n1Window(end))  ],[-5   20 ],'-.k')
ylim( [-5   20 ])

% plot(erptimes(p1Window((myP1VL_loc_P1))),(myP1VL_P1),'*k');
% plot(erptimes(p1Window((myP1VH_loc_P1))),(myP1VH_P1),'*k');
% plot(erptimes(p1Window((myP1SL_loc_P1))),(myP1SL_P1),'*k');
plot(erptimes(p1Window((myP1SH_loc_P1))),(myP1SH_P1),'*k');
% plot(erptimes(p1Window((myP1N_loc_P1))),(myP1N_P1),'*k');
% plot(erptimes(p1Window),(myP1VH(p1Window,:,:)),'linewidth',3);





figure;[h p]= ttest(mySH_P1_window_mean, mySL_P1_window_mean);
boxplot([mySH_P1_window_mean', mySL_P1_window_mean'],'Labels',{'Sound High Val','Sound Low Val'});
title(['Sound ERP; P = ' num2str(p)]);
% checkWind(3,c) = p; 

figure;[h p]= ttest(myVL_P1_window_mean, myVH_P1_window_mean);
boxplot([myVH_P1_window_mean', myVL_P1_window_mean'],'Labels',{'Visual High Val','Visual Low Val'});
title(['Visual ERP; P = ' num2str(p)]);
% checkWind(4,c) = p; 

figure;[h p]= ttest(myVL_P1_window_mean, myVH_P1_window_mean);
bar([mean(myVH_P1_window_mean'-myVL_P1_window_mean')]), hold on;
errorbar(1,mean(myVH_P1_window_mean'-myVL_P1_window_mean'),std(myVH_P1_window_mean'-myVL_P1_window_mean'));
title(['2 visual based on individual peaks']);

figure;[h p]= ttest(mySL_P1_window_mean, mySH_P1_window_mean);
bar([mean(mySH_P1_window_mean'-mySL_P1_window_mean')]), hold on;
errorbar(1,mean(mySH_P1_window_mean'-mySL_P1_window_mean'),std(mySH_P1_window_mean'-mySL_P1_window_mean'));
title(['1 sound based on individual peaks']);
%% power calculation for ANOVA
mean(mean([ myVH_P1_window_mean' - myVL_P1_window_mean'  mySH_P1_window_mean' - mySL_P1_window_mean'],2))
std(mean([  myVH_P1_window_mean' - myVL_P1_window_mean' mySH_P1_window_mean' - mySL_P1_window_mean'],2))

mean(mean([  mySH_P1_window_mean' - mySL_P1_window_mean'],2))
std(mean([  mySH_P1_window_mean' - mySL_P1_window_mean'],2))
%%
close all
% hold on
% area([erptimes(p1Window(1)) erptimes(p1Window(end)) erptimes(p1Window(end)) erptimes(p1Window(1))],[-3 -3 7 7],'FaceColor',[0.95 0.95 0.95],'EdgeColor',[1 1 1])
% area([erptimes(n1Window(1)) erptimes(n1Window(end)) erptimes(n1Window(end)) erptimes(n1Window(1))],[-3 -3 7 7],'FaceColor',[0.85 0.85 0.85],'EdgeColor',[1 1 1])
figure('Position', [10 10 600 600])

% subplot(1,2,2)
hold on
% area([erptimes(p1Window(1)) erptimes(p1Window(end)) erptimes(p1Window(end)) erptimes(p1Window(1))],[-3 -3 7 7],'FaceColor',[0.75 0.75 0.55],'EdgeColor',[1 1 1])
% area([erptimes(n1Window(1)) erptimes(n1Window(end)) erptimes(n1Window(end)) erptimes(n1Window(1))],[-3 -3 7 7],'FaceColor',[0.65 0.85 0.85],'EdgeColor',[1 1 1])
% area([erptimes(p3Window(1)) erptimes(p3Window(end)) erptimes(p3Window(end)) erptimes(p3Window(1))],[-3 -3 7 7],'FaceColor',[0.95 0.85 0.95],'EdgeColor',[1 1 1])
% area([erptimes(p1n1Window(1)) erptimes(p1n1Window(end)) erptimes(p1n1Window(end)) erptimes(p1n1Window(1))],[-3 -3 7 7],'FaceColor',[0.55 0.65 0.35],'EdgeColor','none')
rP = rectangle('Position',[erptimes(p1Window(1)) 0 erptimes(p1Window(end))-erptimes(p1Window(1)) 7]');
rN = rectangle('Position',[erptimes(n1Window(1))  0 erptimes(n1Window(end))-erptimes(n1Window(1)) 7]');
rP.EdgeColor = 'k'; rP.LineWidth = 1; rP.LineStyle = '--';
rN.EdgeColor = 'b'; rN.LineWidth = 1; rN.LineStyle = '--';

LVS = plot(erptimes(allWindow),mean((myP1SL(allWindow,:,:)),2),'r','linewidth',3); % Mean
HVS = plot(erptimes(allWindow),mean((myP1SH(allWindow,:,:)),2),'g','linewidth',3); % Mean
NS = plot(erptimes(allWindow),mean((myP1N(allWindow,:,:)),2),':k','linewidth',3); % Mean

[a,b ] = boundedline(erptimes(allWindow), mean((myP1SL(allWindow,:,:)),2), nanstd((myP1SL(allWindow,:,:)),0,2)./sqrt(size((myP1SL(allWindow,:,:)),2)),'b','linewidth', 4,...
            erptimes(allWindow), mean((myP1SH(allWindow,:,:)),2), nanstd((myP1SH(allWindow,:,:)),0,2)./sqrt(size((myP1SH(allWindow,:,:)),2)),'r')
NS = plot(erptimes(allWindow),mean((myP1N(allWindow,:,:)),2),':k','linewidth',3); % Mean

        % outlinebounds(a,b)
axis([mybaseline(1) allWindow(end)+mybaseline(1) -2 8])
ylim([-2 7])
set(gca,'FontSize', 20)
set(gca,'XTick',0:200:600);
set(gca,'YTick',-1:2:7);
drawaxis(gca, 'x', 0, 'movelabel', 1);
drawaxis(gca, 'y', 0, 'movelabel', 1);

box 'on'; axis square;

set(gca,'Visible','off')
set(findall(gca, 'type', 'text'), 'visible', 'on')
% legend([HVS LVS NS],{'High Value','Low Value', 'Neutral'},'Orientation','horizontal','Location','north','FontSize',12,'EdgeColor','none')
% title('Auditory ERPs: O1/O2,PO7/PO8')
ylabel('\muV')
xlabel('ms')
[h,p]=ttest( (myP1SL(allWindow,:,:))',(myP1SH(allWindow,:,:))')
scatter(find(p<0.05),zeros(length(find(p<0.05)),1)-.75','k','Marker', 's');


figure('Position', [10 10 600 600])
% subplot(1,2,1)
hold on
box 'on'; axis square;
rP = rectangle('Position',[erptimes(p1Window(1)) 0 erptimes(p1Window(end))-erptimes(p1Window(1)) 4]');
rN = rectangle('Position',[erptimes(n1Window(1))  0 erptimes(n1Window(end))-erptimes(n1Window(1)) 4]');
rP.EdgeColor = 'k'; rP.LineWidth = 1; rP.LineStyle = '--';
rN.EdgeColor = 'b'; rN.LineWidth = 1; rN.LineStyle = '--';

LVV = plot(erptimes(allWindow),mean((myP1VL(allWindow,:,:)),2),'r','linewidth',3); % Mean
HVV = plot(erptimes(allWindow),mean((myP1VH(allWindow,:,:)),2),'g','linewidth',3); % Mean

[a,b ] = boundedline(erptimes(allWindow), mean((myP1VL(allWindow,:,:)),2), nanstd((myP1VL(allWindow,:,:)),0,2)./sqrt(size((myP1VL(allWindow,:,:)),2)),'b','linewidth', 4,...
            erptimes(allWindow), mean((myP1VH(allWindow,:,:)),2), nanstd((myP1VH(allWindow,:,:)),0,2)./sqrt(size((myP1VH(allWindow,:,:)),2)),'r')
NVV = plot(erptimes(allWindow),mean((myP1N(allWindow,:,:)),2),':k','linewidth',3); % Mean


% legend([HVV LVV NVV],{'High Value','Low Value', 'Neutral'},'Orientation','horizontal','Location','north','FontSize',12,'EdgeColor','none');
legend('boxoff');
% title('Visual ERPs: O1/O2, PO7/PO8')
ylabel('\muV')
axis([mybaseline(1) allWindow(end)+mybaseline(1) -1 3])
ylim([-1 4])
set(gca,'FontSize', 20)
set(gca,'XTick',0:200:600);
set(gca,'YTick',-1:2:4);
drawaxis(gca, 'x', 0, 'movelabel', 1);
drawaxis(gca, 'y', 0, 'movelabel', 1);
text(115,6,'P1','FontSize',30);
text(190,6,'N1','FontSize',30);
ylabel('\muV')
xlabel('ms')
set(gca,'Visible','off')
set(findall(gca, 'type', 'text'), 'visible', 'on')
[h,p]=ttest( (myP1VL(allWindow,:,:))',(myP1VH(allWindow,:,:))')
scatter(find(p<0.05),zeros(length(find(p<0.05)),1)-.5','k','Marker', 's');

% plot(erptimes(p1Window((myP1VH_loc_P1))),(myP1VH_P1),'*k');
% plot(erptimes(p1Window((myP1VL_loc_P1))),(myP1VL_P1),'*k');
% plot(erptimes(p1Window((myP1SH_loc_P1))),(myP1SH_P1),'*k');
% plot(erptimes(p1Window((myP1SL_loc_P1))),(myP1SL_P1),'*k');



% plot([erptimes(p1Window(1)) erptimes(p1Window(1)) ],[-3 7 ],'-.k')
% plot([erptimes(p1Window(end)) erptimes(p1Window(end))],[-3 7 ],'-.k')
% 
% plot([erptimes(n1Window(1)) erptimes(n1Window(1)) ],[-3 7 ],'-.b')
% plot([erptimes(n1Window(end)) erptimes(n1Window(end))],[-3 7 ],'-.b')


% for i = 1:NSub
%     plot(erptimes(allWindow),myP1SH(allWindow,i),'g'); % Mean
%     plot(erptimes(allWindow),myP1SL(allWindow,i),'r'); % Mean
%     pause
%     
% end
%%
% close all
figure;
hold on
area([erptimes(p1Window(1)) erptimes(p1Window(end)) erptimes(p1Window(end)) erptimes(p1Window(1))],[-3 -3 7 7],'FaceColor',[0.95 0.95 0.95],'EdgeColor',[1 1 1])
area([erptimes(n1Window(1)) erptimes(n1Window(end)) erptimes(n1Window(end)) erptimes(n1Window(1))],[-3 -3 7 7],'FaceColor',[0.85 0.85 0.85],'EdgeColor',[1 1 1])

plot(erptimes(allWindow),mean(myP1SH(allWindow,:)-myP1SL(allWindow,:),2),':b','linewidth',3); % Mean
plot(erptimes(allWindow),mean(myP1VH(allWindow,:)-myP1VL(allWindow,:),2),'c','linewidth',3); % Mean
% plot(erptimes(allWindow),mean((myP1VL(allWindow,:,:)),2),'r','linewidth',5); % Mean
% plot(erptimes(allWindow),mean((myP1VH(allWindow,:,:)),2),'g','linewidth',5); % Mean
% % plot(erptimes(p1Window((myP1VH_loc_P1))),(myP1VH_P1),'*k');
% % plot(erptimes(p1Window((myP1VL_loc_P1))),(myP1VL_P1),'*k');
% % plot(erptimes(p1Window((myP1SH_loc_P1))),(myP1SH_P1),'*k');
% % plot(erptimes(p1Window((myP1SL_loc_P1))),(myP1SL_P1),'*k');


%plot(erptimes(allWindow),mean((myP1N(allWindow,:,:)),2),':k','linewidth',3); % Mean

% plot([erptimes(p1Window(1)) erptimes(p1Window(1)) ],[-3 7 ],'-.k')
% plot([erptimes(p1Window(end)) erptimes(p1Window(end))],[-3 7 ],'-.k')
% 
% plot([erptimes(n1Window(1)) erptimes(n1Window(1)) ],[-3 7 ],'-.b')
% plot([erptimes(n1Window(end)) erptimes(n1Window(end))],[-3 7 ],'-.b')

axis([mybaseline(1) allWindow(end)+mybaseline(1) -2 2])
% for i = 1:NSub
%     plot(erptimes(allWindow),myP1SH(allWindow,i),'g'); % Mean
%     plot(erptimes(allWindow),myP1SL(allWindow,i),'r'); % Mean
%     pause
%     
% end
%%
% %% Plot  and visualize each channel
% for i=1:size(erpdata{1},2)
%     figure(i), plot(erptimes,(mean(erpdata{3}(:,i,:),3)+mean(erpdata{7}(:,i,:),3))/2,'r'), hold on
%     plot(erptimes,(mean(erpdata{4}(:,i,:),3)+mean(erpdata{8}(:,i,:),3))/2,'g'), hold on
%     xlim([-100 500])
%     title([ 'Auditory: ' mychans{i}])
%     print(['-f' num2str(i)],'-dpsc2','chans.ps','-append')
% 
%     figure(i+64), plot(erptimes,(mean(erpdata{1}(:,i,:),3)+mean(erpdata{5}(:,i,:),3))/2,'r'), hold on
%     plot(erptimes,(mean(erpdata{2}(:,i,:),3)+mean(erpdata{4}(:,i,:),3))/2,'g'), hold on
%     xlim([-100 500])
%     title([ 'Visual: ' mychans{i}])
%     print(['-f' num2str(i+64)],'-dpsc2','chans.ps','-append')
%     close all
% end
%% First look at visual, auditory, bimodal-same, and bimodal-opposite
HV= myVH_P1_window_mean;
LV= myVL_P1_window_mean;
HA= mySH_P1_window_mean;
LA= mySL_P1_window_mean;
anovdata_rew_modality_names = {'HV';'LV';'HA';'LA'};
anovdata_rew_modality = [HV;LV;HA;LA]';

bimodncond=length(anovdata_rew_modality_names);
subfactor=cell(bimodncond,1);

for  i=1:bimodncond
    subfactor{i}= ['F' num2str(i)];
end

t = array2table(anovdata_rew_modality,'VariableNames',subfactor);
%factorNames = {'modality','reward','latencybin','part'};
factorNames = {'modality','reward'}; % for each part


within = table({'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
    {'H';'L';'H';'L'},... %reward: high or low
    'VariableNames',factorNames);
%
% fit the repeated measures model
rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);

% run my repeated measures anova here
%[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
[ranovatbl] = ranova(rm, 'WithinModel','modality*reward')

%% Compare methods

delete (['sizeData.txt']);
textfile = fopen(['sizeData.txt'],'wt');
fprintf(textfile,'Pp\tV_L\tV_H\tA_L\tA_H\n');
for i = 1:NSub
 fprintf(textfile,'Pp%d\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t\n',i,myVL_P1_window_mean(i),myVH_P1_window_mean(i),mySL_P1_window_mean(i),mySH_P1_window_mean(i));
end
fclose all;
save('IndividPeaks20ms','myVL_P1_window_mean','myVH_P1_window_mean','mySL_P1_window_mean','mySH_P1_window_mean','myVL_P1_window_mean','myN_P1_window_mean');