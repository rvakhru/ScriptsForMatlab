%function [] = EEGexp11()
% Analyze EEG data for experiment 11
%
% 1.4.1 Main effect of VALUE
% (d�) Simultaneous valuable cue increases the sensitivity of discrimination of the visual target.
% (RT) Simultaneous high-value cue decreases reaction times.
% (EEG) Within modal valuable and cross-modal valuable cues both modulate early visual sensory areas (Posterior electrodes).
% (timeEEG) High-value cue evoked ERP responses occur earlier than low-value cue modulation.
%
% 1.4.2 Main effect of MODALITY
% (EEG) Within modal valuable cue modulation of ERP components is stronger in amplitude than the between-modal condition.
% (timeEEG) Within modal valuable cue, modulation occurs earlier than the between-modal condition.
%
% 1.4.3 Correlations
% (d�/EEG)Change in the behavioral target sensitivity correlates positively with the change in amplitude and latency of early ERP components due to reward.
% (pupil/EEG)Change of the pupil size correlates positively with the change in amplitude and latency of early ERP components due to reward.
% (d� error)The effect of reward is independent of the eye movements.
clear all
close all
%% Obtain specific data
%  load('W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA\Studies\EEGPlootPlot_TrPost_noErrorAlphaAvg.mat');
my_pre=load('W:\apoores\Data\fromRoman\5Epoch2times\EEGPlootPlot_test1_TrPre.mat');
load('W:\apoores\Data\fromRoman\5Epoch2times\EEGPlootPlot_test1_TrPost.mat');
load('W:\apoores\Data\fromRoman\5Epoch2times\EEGPlootPlot_test1_TrPre.mat');
load('W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA_F\Studies\EEGPlootPlot_testTrPost_ECVP_0808.mat');
%load('W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA_F\Studies\4Arezoo\studies/EEGPlootPlot_test1_Post.mat')

% load('W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA\Studies\EEGPlootPlot_testTrPost_5Epoch2times.mat');
% Using MyERP
%%
figure;
%plot(erptimes, mean(myerp,3));
adjust_pre =0;
do_contra=1; %%%%% only take contralateral responses
mybaseline =[-100 0];
winlength = 30;
winsignlength = 10; %%%% see:https://www.pnas.org/content/106/52/22456 and http://www.jneurosci.org/content/jneuro/34/29/9817.full.pdf
individualpeak = 0;
ploterrorbars =0;
averageall =1;
mybehavior=load('behData15_08FromAcc.mat');
mybehavior_pre=load('pre_beh5_eye.mat');
my_ROI = {'O1' 'O2' 'PO7' 'PO8' }; %%%%%see here for how to do 2*2 ANOVA https://sccn.ucsd.edu/pipermail/eeglablist/2011/003839.html
%my_ROI = {'PO8'}; %%%%%see here for how to do 2*2 ANOVA https://sccn.ucsd.edu/pipermail/eeglablist/2011/003839.html

myside =[ 2 2 1 1];
% myside =[ 2 2 2 2 2 2];
% my_ROI = { 'PO4' 'PO8' 'P6'  }; %%%%%exploratory analysis for auditory
% 390:420 do_contra=1
%my_ROI = {'PO8'  'PO4' 'P6'}; %%%%%see here for how to do 2*2 ANOVA https://sccn.ucsd.edu/pipermail/eeglablist/2011/003839.html
%myside =[ 2 2 2];

%%%%%my_ROI = { 'Pz'   }; see during conditioing for sounds window 320:420
%my_ROI = {'F1' 'FC1' 'F2' 'FC2' }; %%%%%see here for how to do 2*2 ANOVA https://sccn.ucsd.edu/pipermail/eeglablist/2011/003839.html
%my_ROI = {'Fz' 'FCz'}; %%%%%see here for how to do 2*2 ANOVA https://sccn.ucsd.edu/pipermail/eeglablist/2011/003839.html

%my_ROI = { 'Fz' 'FCz' 'FC2' 'FC1' 'F3' 'F4' 'FC3' 'FC4'};%https://shamslab.psych.ucla.edu/wp-content/uploads/sites/57/2016/01/NeuroscienceLetters2004.pdf
%%my_ROI = { 'FCz'  'FC3' 'FC4'};%https://shamslab.psych.ucla.edu/wp-content/uploads/sites/57/2016/01/NeuroscienceLetters2004.pdf
%my_ROI = { 'FCz'  'Fz' };
%%http://www.woldorfflab.org/pdfs/Talsma_JoCN_2005.pdf
%%http://www.jneurosci.org/content/34/29/9817
rmb =0;
%my_ROI = {'P1' 'P2' 'POz' 'Pz' }; %%%%%%P300 visual
% my_ROI = { 'T7' 'T8'};
%my_ROI = {'CP3' 'CP4'}; %%%%%%P300 auditory

%%%%https://www.researchgate.net/publication/262265296_Utilization_of_reward-prospect_enhances_preparatory_attention_and_reduces_stimulus_conflict/figures?lo=1

for i = 1:length(my_ROI)
    electrodes(i) = find(strcmp(mychans,my_ROI{i}));
end

allWindow = 1:900;
bWindow = 1:(-1)*mybaseline(1);
p1Window = (-1)*mybaseline(1)+[390:430]; %%% for difference 100:125 (pre_post) 390:410 https://www.psychologie.hu-berlin.de/de/prof/neuro/lit/rohr_rahman2015
p1Window_plot = (-1)*mybaseline(1)+[400:420]; %%%390:410
n1Window = (-1)*mybaseline(1)+[200:220];
p3Window = (-1)*mybaseline(1)+[300:600];
p1n1Window = (-1)*mybaseline(1)+[180:200];

% p1Window = (-1)*mybaseline(1)+[120:140];
% n1Window = (-1)*mybaseline(1)+[160:190];

%% Preallocate
NSamp = size(erpdata{1},1);
NCahn = size(erpdata{1},2);
NSub = size(erpdata{1},3);
%sujs = [1:11 14:NSub]; %%%% 14 and 15 are strange
sujs =1:NSub;
%sujs = sujs(mybehavior.s.subRewS(sujs,1)==1);
%sujs = sujs(mybehavior.s.subAccu(sujs)>median(mybehavior.s.subAccu(sujs)));
NSub = length(sujs);
myP1VL_P1_window = nan(NSub,winlength);
myP1VH_P1_window = nan(NSub,winlength);
myP1SL_P1_window = nan(NSub,winlength);
myP1SH_P1_window = nan(NSub,winlength);
myP1N_P1_window = nan(NSub,winlength);


%% Process data
% myP1VL = squeeze(myerp(50:250,:,:));
for e=1:length(my_ROI)
    
    if adjust_pre
        myP1VLL = mean(erpdata{1}(:,electrodes(e),sujs)-my_pre.erpdata{1}(:,electrodes(e),sujs),2);
        myP1VHL = mean(erpdata{2}(:,electrodes(e),sujs)-my_pre.erpdata{2}(:,electrodes(e),sujs),2);
        myP1SLL = mean(erpdata{3}(:,electrodes(e),sujs)-my_pre.erpdata{3}(:,electrodes(e),sujs),2);
        myP1SHL = mean(erpdata{4}(:,electrodes(e),sujs)-my_pre.erpdata{4}(:,electrodes(e),sujs),2);
        myP1VLR = mean(erpdata{5}(:,electrodes(e),sujs)-my_pre.erpdata{5}(:,electrodes(e),sujs),2);
        myP1VHR = mean(erpdata{6}(:,electrodes(e),sujs)-my_pre.erpdata{6}(:,electrodes(e),sujs),2);
        myP1SLR = mean(erpdata{7}(:,electrodes(e),sujs)-my_pre.erpdata{7}(:,electrodes(e),sujs),2);
        myP1SHR = mean(erpdata{8}(:,electrodes(e),sujs)-my_pre.erpdata{8}(:,electrodes(e),sujs),2);
        myP1N_L = mean(erpdata{9}(:,electrodes(e),sujs)-my_pre.erpdata{9}(:,electrodes(e),sujs),2);
        myP1N_R = mean(erpdata{10}(:,electrodes(e),sujs)-my_pre.erpdata{10}(:,electrodes(e),sujs),2);
    else
        myP1VLL = mean(erpdata{1}(:,electrodes(e),sujs),2);
        myP1VHL = mean(erpdata{2}(:,electrodes(e),sujs),2);
        myP1SLL = mean(erpdata{3}(:,electrodes(e),sujs),2);
        myP1SHL = mean(erpdata{4}(:,electrodes(e),sujs),2);
        myP1VLR = mean(erpdata{5}(:,electrodes(e),sujs),2);
        myP1VHR = mean(erpdata{6}(:,electrodes(e),sujs),2);
        myP1SLR = mean(erpdata{7}(:,electrodes(e),sujs),2);
        myP1SHR = mean(erpdata{8}(:,electrodes(e),sujs),2);
        myP1N_L = mean(erpdata{1}(:,electrodes(e),sujs),2);
        myP1N_R = mean(erpdata{2}(:,electrodes(e),sujs),2);
    end
    if ~do_contra
        myP1VL(:,:,e) = squeeze(myP1VLL + myP1VLR)/2;
        myP1VH(:,:,e) = squeeze(myP1VHL + myP1VHR)/2;
        myP1SL(:,:,e) = squeeze(myP1SLL + myP1SLR)/2;
        myP1SH(:,:,e) = squeeze(myP1SHL + myP1SHR)/2;
        myP1N(:,:,e)  = squeeze(myP1N_L + myP1N_R)/2;
    else
        if myside(e) ==1
            myP1VL(:,:,e) = squeeze(myP1VLR + myP1VLR)/2;
            myP1VH(:,:,e) = squeeze(myP1VHR + myP1VHR)/2;
            myP1SL(:,:,e) = squeeze(myP1SLR + myP1SLR)/2;
            myP1SH(:,:,e) = squeeze(myP1SHR + myP1SHR)/2;
            myP1N(:,:,e)  = squeeze(myP1N_R + myP1N_R)/2;
        else
            myP1VL(:,:,e) = squeeze(myP1VLL + myP1VLL)/2;
            myP1VH(:,:,e) = squeeze(myP1VHL + myP1VHL)/2;
            myP1SL(:,:,e) = squeeze(myP1SLL + myP1SLL)/2;
            myP1SH(:,:,e) = squeeze(myP1SHL + myP1SHL)/2;
            myP1N(:,:,e)  = squeeze(myP1N_L + myP1N_L)/2;
        end
    end
    
end




myel =[1:length(my_ROI)];
myP1VL = squeeze(mean(myP1VL(:,:,myel),3));
myP1VH= squeeze(mean(myP1VH(:,:,myel),3));
myP1SL= squeeze(mean(myP1SL(:,:,myel),3));
myP1SH= squeeze(mean(myP1SH(:,:,myel),3));
myP1N= squeeze(mean(myP1N(:,:,myel),3));
%% Process data
% % myP1VL = squeeze(myerp(50:250,:,:));
%
% myP1VLL = mean(erpdata{1}(:,electrodes,sujs),2);
% myP1VHL = mean(erpdata{2}(:,electrodes,sujs),2);
% myP1SLL = mean(erpdata{3}(:,electrodes,sujs),2);
% myP1SHL = mean(erpdata{4}(:,electrodes,sujs),2);
% myP1VLR = mean(erpdata{5}(:,electrodes,sujs),2);
% myP1VHR = mean(erpdata{6}(:,electrodes,sujs),2);
% myP1SLR = mean(erpdata{7}(:,electrodes,sujs),2);
% myP1SHR = mean(erpdata{8}(:,electrodes,sujs),2);
% myP1N_L = mean(erpdata{1}(:,electrodes,sujs),2);
% myP1N_R = mean(erpdata{2}(:,electrodes,sujs),2);
%
%
% myP1VL = squeeze(myP1VLL + myP1VLR)/2;
% myP1VH = squeeze(myP1VHL + myP1VHR)/2;
% myP1SL = squeeze(myP1SLL + myP1SLR)/2;
% myP1SH = squeeze(myP1SHL + myP1SHR)/2;
% myP1N  = squeeze(myP1N_L + myP1N_R)/2;
%
% myP1VL = myP1VL-repmat(mean(myP1VL(1:100,:)),NSamp,1);
% myP1VH = myP1VH-repmat(mean(myP1VH(1:100,:)),NSamp,1);
% myP1SL = myP1SL-repmat(mean(myP1SL(1:100,:)),NSamp,1);
% myP1SH = myP1SH-repmat(mean(myP1SH(1:100,:)),NSamp,1);
% myP1N  = myP1N-repmat(mean(myP1VL(1:100,:)),NSamp,1);
%
if individualpeak
    [myP1VL_P1, myP1VL_loc_P1] = min(myP1VL(p1Window,:));
    [myP1VH_P1, myP1VH_loc_P1] = min(myP1VH(p1Window,:));
    [myP1SL_P1, myP1SL_loc_P1] = min(myP1SL(p1Window,:));
    [myP1SH_P1, myP1SH_loc_P1] = min(myP1SH(p1Window,:));
    [myP1N_P1, myP1N_loc_P1] =   min(myP1N (p1Window,:));
else
    if rmb ==0
        [myP1VL_P1, myP1VL_loc_P1] = min(mean(myP1VL(p1Window,:),2));
        [myP1VH_P1, myP1VH_loc_P1] = min(mean(myP1VH(p1Window,:),2));
        [myP1SL_P1, myP1SL_loc_P1] = min(mean(myP1SL(p1Window,:),2));
        [myP1SH_P1, myP1SH_loc_P1] = min(mean(myP1SH(p1Window,:),2));
        [myP1N_P1, myP1N_loc_P1] =   min(mean(myP1N(p1Window,:),2));
    else
        %%%%%%remove the baseline difference
        
        [myP1VL_P1, myP1VL_loc_P1] = min(mean(myP1VL(p1Window,:)-repmat(mean(myP1VL(bWindow,:),1),[length(p1Window) 1]),2));
        [myP1VH_P1, myP1VH_loc_P1] = min(mean(myP1VH(p1Window,:)-repmat(mean(myP1VH(bWindow,:),1),[length(p1Window) 1]),2));
        [myP1SL_P1, myP1SL_loc_P1] = min(mean(myP1SL(p1Window,:)-repmat(mean(myP1SL(bWindow,:),1),[length(p1Window) 1]),2));
        [myP1SH_P1, myP1SH_loc_P1] = min(mean(myP1SH(p1Window,:)-repmat(mean(myP1SH(bWindow,:),1),[length(p1Window) 1]),2));
        [myP1N_P1, myP1N_loc_P1] =   min(mean(myP1N(p1Window,:)-repmat(mean(myP1N(bWindow,:),1),[length(p1Window) 1]),2));
        
        %     %%%%%%remove the baseline difference
    end
    myP1VL_P1 = repmat(myP1VL_P1,[1 NSub]);
    myP1VH_P1 = repmat(myP1VH_P1,[1 NSub]);
    myP1SL_P1 = repmat(myP1SL_P1,[1 NSub]);
    myP1SH_P1 = repmat(myP1SH_P1,[1 NSub]);
    myP1N_P1 = repmat(myP1N_P1,[1 NSub]);
    
    myP1VL_loc_P1 = repmat(myP1VL_loc_P1,[1 NSub]);
    myP1VH_loc_P1 = repmat(myP1VH_loc_P1,[1 NSub]);
    myP1SL_loc_P1 = repmat(myP1SL_loc_P1,[1 NSub]);
    myP1SH_loc_P1 = repmat(myP1SH_loc_P1,[1 NSub]);
    myP1N_loc_P1 = repmat(myP1N_loc_P1,[1 NSub]);
end


%% For each participant

for i = 1:NSub
    myP1VL_P1_window(i,:) = [p1Window(myP1VL_loc_P1(i))-winlength/2:p1Window(myP1VL_loc_P1(i))+winlength/2-1];
    myP1VH_P1_window(i,:) = [p1Window(myP1VH_loc_P1(i))-winlength/2:p1Window(myP1VH_loc_P1(i))+winlength/2-1];
    myP1SL_P1_window(i,:) = [p1Window(myP1SL_loc_P1(i))-winlength/2:p1Window(myP1SL_loc_P1(i))+winlength/2-1];
    myP1SH_P1_window(i,:) = [p1Window(myP1SH_loc_P1(i))-winlength/2:p1Window(myP1SH_loc_P1(i))+winlength/2-1];
    myP1N_P1_window(i,:) =  [p1Window(myP1N_loc_P1(i))-winlength/2:p1Window(myP1N_loc_P1(i))+winlength/2-1];
end
figure(1); hold on;
for i = 1:NSub
    plot(erptimes(myP1SH_P1_window(i,:)),myP1SH(myP1SH_P1_window(i,:),i),'linewidth', 5);
    if rmb ==0
        mySH_P1_window_mean(i) = mean(myP1SH(myP1SH_P1_window(i,:),i));
        mySL_P1_window_mean(i) = mean(myP1SL(myP1SL_P1_window(i,:),i));
        myVH_P1_window_mean(i) = mean(myP1VH(myP1VH_P1_window(i,:),i));
        myVL_P1_window_mean(i) = mean(myP1VL(myP1VL_P1_window(i,:),i));
        myN_P1_window_mean(i) = mean(myP1N(myP1N_P1_window(i,:),i));
    else
        %%%%%%remove the baseline difference
        mySH_P1_window_mean(i) = mean(myP1SH(myP1SH_P1_window(i,:),i)-repmat(mean(myP1SH(bWindow,i),1),[winlength 1]));
        mySL_P1_window_mean(i) = mean(myP1SL(myP1SL_P1_window(i,:),i)-repmat(mean(myP1SL(bWindow,i),1),[winlength 1]));
        myVH_P1_window_mean(i) = mean(myP1VH(myP1VH_P1_window(i,:),i)-repmat(mean(myP1VH(bWindow,i),1),[winlength 1]));
        myVL_P1_window_mean(i) = mean(myP1VL(myP1VL_P1_window(i,:),i)-repmat(mean(myP1VL(bWindow,i),1),[winlength 1]));
        myN_P1_window_mean(i) = mean(myP1N(myP1N_P1_window(i,:),i)-repmat(mean(myP1N(bWindow,i),1),[winlength 1]));
    end
    if averageall  %%%%% for instance for P300 where we average within the window
        mySH_P1_window_mean(i) = mean(mean(myP1SH(p1Window,i)));
        mySL_P1_window_mean(i) = mean(mean(myP1SL(p1Window,i)));
        myVH_P1_window_mean(i) = mean(mean(myP1VH(p1Window,i)));
        myVL_P1_window_mean(i) = mean(mean(myP1VL(p1Window,i)));
        myN_P1_window_mean(i) = mean(mean(myP1N(p1Window,i)));
    end
end


hold on
plot(erptimes(allWindow),mean((myP1SH(allWindow,:,:)),2),'linewidth',3); % Mean

plot(erptimes(allWindow),mean((myP1SH(allWindow,:,:)),2),'linewidth',3); % Mean


plot(erptimes(allWindow),(myP1SH(allWindow,:,:)),'linewidth',.5);           % For Participant
% plot(erptimes(allWindow),(myP1SL(allWindow,:,:)),'linewidth',.5);           % For Participant

hold on
%plot(erptimes(allWindow),(myP1SL(allWindow,:,:)),'linewidth',3);
plot([erptimes(p1Window(1))    erptimes(p1Window(1))],[-5  20],'-.m')
plot([erptimes(p1Window(end))  erptimes(p1Window(end))  ],[-5  20],'-.m')

plot([erptimes(n1Window(1))    erptimes(n1Window(1))],[-5  20 ],'-.k')
plot([erptimes(n1Window(end))  erptimes(n1Window(end))  ],[-5   20 ],'-.k')
ylim( [-5   20 ])

% plot(erptimes(p1Window((myP1VL_loc_P1))),(myP1VL_P1),'*k');
% plot(erptimes(p1Window((myP1VH_loc_P1))),(myP1VH_P1),'*k');
% plot(erptimes(p1Window((myP1SL_loc_P1))),(myP1SL_P1),'*k');
plot(erptimes(p1Window((myP1SH_loc_P1))),(myP1SH_P1),'*k');
% plot(erptimes(p1Window((myP1N_loc_P1))),(myP1N_P1),'*k');
% plot(erptimes(p1Window),(myP1VH(p1Window,:,:)),'linewidth',3);





% figure;[h p]= ttest(mySH_P1_window_mean, mySL_P1_window_mean);
% boxplot([mySH_P1_window_mean', mySL_P1_window_mean'],'Labels',{'Sound High Val','Sound Low Val'});
% title(['Sound ERP; P = ' num2str(p)]);
% % checkWind(3,c) = p;
%
% figure;[h p]= ttest(myVL_P1_window_mean, myVH_P1_window_mean);
% boxplot([myVH_P1_window_mean', myVL_P1_window_mean'],'Labels',{'Visual High Val','Visual Low Val'});
% title(['Visual ERP; P = ' num2str(p)]);
% % checkWind(4,c) = p;
%
% figure;[h p]= ttest(myVL_P1_window_mean, myVH_P1_window_mean);
% bar([mean(myVH_P1_window_mean'-myVL_P1_window_mean')]), hold on;
% errorbar(1,mean(myVH_P1_window_mean'-myVL_P1_window_mean'),std(myVH_P1_window_mean'-myVL_P1_window_mean'));
% title(['2 visual based on individual peaks']);
%
% figure;[h p]= ttest(mySL_P1_window_mean, mySH_P1_window_mean);
% bar([mean(mySH_P1_window_mean'-mySL_P1_window_mean')]), hold on;
% errorbar(1,mean(mySH_P1_window_mean'-mySL_P1_window_mean'),std(mySH_P1_window_mean'-mySL_P1_window_mean'));
% title(['1 sound based on individual peaks']);
% %% power calculation for ANOVA
% mean(mean([ myVH_P1_window_mean' - myVL_P1_window_mean'  mySH_P1_window_mean' - mySL_P1_window_mean'],2))
% std(mean([  myVH_P1_window_mean' - myVL_P1_window_mean' mySH_P1_window_mean' - mySL_P1_window_mean'],2))
%
% mean(mean([  mySH_P1_window_mean' - mySL_P1_window_mean'],2))
% std(mean([  mySH_P1_window_mean' - mySL_P1_window_mean'],2))
%%
%%
%close all
% hold on
% area([erptimes(p1Window(1)) erptimes(p1Window(end)) erptimes(p1Window(end)) erptimes(p1Window(1))],[-3 -3 7 7],'FaceColor',[0.95 0.95 0.95],'EdgeColor',[1 1 1])
% area([erptimes(n1Window(1)) erptimes(n1Window(end)) erptimes(n1Window(end)) erptimes(n1Window(1))],[-3 -3 7 7],'FaceColor',[0.85 0.85 0.85],'EdgeColor',[1 1 1])

HV= myVH_P1_window_mean;
LV= myVL_P1_window_mean;
HA= mySH_P1_window_mean;
LA= mySL_P1_window_mean;
[h p_v]= ttest(HV,LV);
[h p_a]= ttest(HA,LA);
figure('Position', [10 10 900 600])

subplot(2,1,2), axis square
hold on
area([erptimes(p1Window_plot(1)) erptimes(p1Window_plot(end)) erptimes(p1Window_plot(end)) erptimes(p1Window_plot(1))],[-2.5  -2.5  7   7 ],'FaceColor',[0.6 0.6 0.6],'EdgeColor',[0.6 0.6 0.6])
area([erptimes(n1Window(1)) erptimes(n1Window(end)) erptimes(n1Window(end)) erptimes(n1Window(1))],[-2.5  -2.5 7 7 ],'FaceColor',[0.8 0.8 0.8],'EdgeColor',[0.8 0.8 0.8])
% area([erptimes(p3Window(1)) erptimes(p3Window(end)) erptimes(p3Window(end)) erptimes(p3Window(1))],[-3 -3 7 7],'FaceColor',[0.95 0.85 0.95],'EdgeColor',[1 1 1])
%area([erptimes(p1n1Window(1)) erptimes(p1n1Window(end)) erptimes(p1n1Window(end)) erptimes(p1n1Window(1))],[-3 -3 7 7],'FaceColor',[0.55 0.65 0.35],'EdgeColor','none')


if ploterrorbars
    ers=patch([erptimes(allWindow) fliplr(erptimes(allWindow))],[(mean(myP1SL(allWindow,:),2)+nanstd(myP1SL(allWindow,:),0,2)./sqrt(NSub))'...
        fliplr((mean(myP1SL(allWindow,:),2)-nanstd(myP1SL(allWindow,:),0,2)./sqrt(NSub))')],'b');
    alpha(ers,.3)
    ers=patch([erptimes(allWindow) fliplr(erptimes(allWindow))],[(mean(myP1SH(allWindow,:),2)+nanstd(myP1SH(allWindow,:),0,2)./sqrt(NSub))'...
        fliplr((mean(myP1SH(allWindow,:),2)-nanstd(myP1SH(allWindow,:),0,2)./sqrt(NSub))')],'r');
    alpha(ers,.3)
    %     ers=patch([erptimes(allWindow) fliplr(erptimes(allWindow))],[(mean(myP1N(allWindow,:),2)+nanstd(myP1N(allWindow,:),0,2)./sqrt(NSub))'...
    %         fliplr((mean(myP1N(allWindow,:),2)-nanstd(myP1N(allWindow,:),0,2)./sqrt(NSub))')],'k');
    alpha(ers,.3)
end

LVS = plot(erptimes(allWindow),mean((myP1SL(allWindow,:)),2),':b','linewidth',2); % Mean
HVS = plot(erptimes(allWindow),mean((myP1SH(allWindow,:)),2),'r','linewidth',2); % Mean
NS = plot(erptimes(allWindow),mean((myP1N(allWindow,:)),2),':k','linewidth',1); % Mean
%diffi = plot(erptimes(allWindow),mean((myP1SH(allWindow,:)-myP1SL(allWindow,:)),2),'-.g','linewidth',2); % Mean
% l1=plot([erptimes(allWindow)],(mean(myP1SL(allWindow,:),2)+nanstd(myP1SL(allWindow,:),0,2)./sqrt(NSub))','r','linewidth',12), hold on;
% l2=plot([erptimes(allWindow)],(mean(myP1SL(allWindow,:),2)-nanstd(myP1SL(allWindow,:),0,2)./sqrt(NSub))','r','linewidth',12);
axis([mybaseline(1)+50 allWindow(end)+mybaseline(1)-200  -5.5 6.5])
% legend([HVS LVS NS],{'High Value','Low Value', 'Neutral'},'Orientation','horizontal','Location','north','FontSize',12,'EdgeColor','none')
title([ 'Auditory ERPs:' my_ROI  'P = ' num2str(p_a)])
ylabel('\muV')
%set(gca,'Ydir','reverse')
sigbins=1:winsignlength-1:length(mean(myP1SH(allWindow,:),2));
for sigi =1:length(sigbins)-1
    [h_aud(sigi) p_aud(sigi)]= ttest(mean(myP1SH(allWindow(sigbins(sigi):sigbins(sigi+1)),:),1)-mean(myP1SL(allWindow(sigbins(sigi):sigbins(sigi+1)),:),1));
    if p_aud(sigi)<0.05
        plot(erptimes(allWindow(sigbins(sigi):sigbins(sigi+1))),repmat(-1,winsignlength,1),'color',[0 0 0],'linewidth',2)
        %     else
        %         plot(erptimes(allWindow(sigbins(sigi):sigbins(sigi+1))),repmat(-1,winsignlength,1),'color',[0.9 0.9 0.9],'linewidth',2)
    end
end

subplot(2,1,1), axis square
hold on
area([erptimes(p1Window_plot(1)) erptimes(p1Window_plot(end)) erptimes(p1Window_plot(end)) erptimes(p1Window_plot(1))],[-2.5  -2.5 7 7 ],'FaceColor',[0.6 0.6 0.6],'EdgeColor',[0.6 0.6 0.6])
area([erptimes(n1Window(1)) erptimes(n1Window(end)) erptimes(n1Window(end)) erptimes(n1Window(1))],[-2.5  -2.5  7  7 ],'FaceColor',[0.8 0.8 0.8],'EdgeColor',[0.8 0.8 0.8])
% area([erptimes(p3Window(1)) erptimes(p3Window(end)) erptimes(p3Window(end)) erptimes(p3Window(1))],[-3 -3 7 7],'FaceColor',[0.95 0.85 0.95],'EdgeColor',[1 1 1])
%area([erptimes(p1n1Window(1)) erptimes(p1n1Window(end)) erptimes(p1n1Window(end)) erptimes(p1n1Window(1))],[-3 -3 7 7],'FaceColor',[0.55 0.65 0.35],'EdgeColor','none')


if ploterrorbars
    ers=patch([erptimes(allWindow) fliplr(erptimes(allWindow))],[(mean(myP1VL(allWindow,:),2)+nanstd(myP1VL(allWindow,:),0,2)./sqrt(NSub))'...
        fliplr((mean(myP1VL(allWindow,:),2)-nanstd(myP1VL(allWindow,:),0,2)./sqrt(NSub))')],'b');
    alpha(ers,.3)
    ers=patch([erptimes(allWindow) fliplr(erptimes(allWindow))],[(mean(myP1VH(allWindow,:),2)+nanstd(myP1VH(allWindow,:),0,2)./sqrt(NSub))'...
        fliplr((mean(myP1VH(allWindow,:),2)-nanstd(myP1VH(allWindow,:),0,2)./sqrt(NSub))')],'r');
    alpha(ers,.3)
    %     ers=patch([erptimes(allWindow) fliplr(erptimes(allWindow))],[(mean(myP1N(allWindow,:),2)+nanstd(myP1N(allWindow,:),0,2)./sqrt(NSub))'...
    %         fliplr((mean(myP1N(allWindow,:),2)-nanstd(myP1N(allWindow,:),0,2)./sqrt(NSub))')],'k');
    
    alpha(ers,.3)
end

LVV = plot(erptimes(allWindow),mean((myP1VL(allWindow,:)),2),':b','linewidth',2); % Mean
HVV = plot(erptimes(allWindow),mean((myP1VH(allWindow,:)),2),'r','linewidth',2); % Mean
NVV = plot(erptimes(allWindow),mean((myP1N(allWindow,:)),2),':k','linewidth',1); % Mean
%diffi = plot(erptimes(allWindow),mean((myP1VH(allWindow,:)-myP1VL(allWindow,:)),2),'-.g','linewidth',2); % Mean

%legend([HVV LVV NVV],{'High Value','Low Value', 'Neutral'},'Orientation','horizontal','Location','north','FontSize',12,'EdgeColor','none');
% legend('boxoff');
title([ 'Visual ERPs:' my_ROI  'P = ' num2str(p_v)])
ylabel('\muV')
axis([mybaseline(1)+50 allWindow(end)+mybaseline(1)-200  -5.5 4.5])
%set(gca,'Ydir','reverse')

sigbins=1:winsignlength-1:length(mean(myP1VH(allWindow,:),2));
for sigi =1:length(sigbins)-1
    [h_vis(sigi) p_vis(sigi)]= ttest(mean(myP1VH(allWindow(sigbins(sigi):sigbins(sigi+1)),:),1)-mean(myP1VL(allWindow(sigbins(sigi):sigbins(sigi+1)),:),1));
    if p_vis(sigi)<0.05
        plot(erptimes(allWindow(sigbins(sigi):sigbins(sigi+1))),repmat(-1,winsignlength,1),'color',[0 0 0],'linewidth',2)
        %     else
        %         plot(erptimes(allWindow(sigbins(sigi):sigbins(sigi+1))),repmat(-1,winsignlength,1),'color',[0.9 0.9 0.9],'linewidth',2)
    end
end

%text(115,6,'P1','FontSize',30);
%text(190,6,'N1','FontSize',30);

% plot(erptimes(p1Window((myP1VH_loc_P1))),(myP1VH_P1),'*k');
% plot(erptimes(p1Window((myP1VL_loc_P1))),(myP1VL_P1),'*k');
% plot(erptimes(p1Window((myP1SH_loc_P1))),(myP1SH_P1),'*k');
% plot(erptimes(p1Window((myP1SL_loc_P1))),(myP1SL_P1),'*k');



% plot([erptimes(p1Window(1)) erptimes(p1Window(1)) ],[-3 7 ],'-.k')
% plot([erptimes(p1Window(end)) erptimes(p1Window(end))],[-3 7 ],'-.k')
%
% plot([erptimes(n1Window(1)) erptimes(n1Window(1)) ],[-3 7 ],'-.b')
% plot([erptimes(n1Window(end)) erptimes(n1Window(end))],[-3 7 ],'-.b')


% for i = 1:NSub
%     plot(erptimes(allWindow),myP1SH(allWindow,i),'g'); % Mean
%     plot(erptimes(allWindow),myP1SL(allWindow,i),'r'); % Mean
%     pause
%
% end
%%
baseline= -100;
%maplim= [-1 1];
maplim = [-6 2.5];
mytimes = [250];
myconds =[1 2];
x_lim=[-100 700];
%y_lim = [-7 4];
y_lim = [-6.5 3.1];
load('chanlocs','chanloc')
erp_HA = mean((erpdata{4}(:,1:64,sujs)+erpdata{4}(:,1:64,sujs))/2,3);
erp_LA = mean((erpdata{3}(:,1:64,sujs)+erpdata{3}(:,1:64,sujs))/2,3);
%erp_HA = mean((erpdata{4}(:,1:64,sujs)+erpdata{8}(:,1:64,sujs))/2 - (my_pre.erpdata{4}(:,1:64,sujs)+my_pre.erpdata{8}(:,1:64,sujs))/2,3);
%erp_LA = mean((erpdata{3}(:,1:64,sujs)+erpdata{7}(:,1:64,sujs))/2 - (my_pre.erpdata{3}(:,1:64,sujs)+my_pre.erpdata{7}(:,1:64,sujs))/2,3);

erp_diff = squeeze(erp_HA-erp_LA)'; %%%% first dimensions is channels, second is ERP data points

figure(6),title(['Auditory, Post: ' num2str(mytimes-100) 'ms'])
%topoplot(mean(myerp{myconds(1)}(:,[250:500]),2),chanloc,'electrodes','ptslabels');
topoplot(mean(erp_diff(:,mytimes),2),chanloc,'electrodes','ptslabels');cbar('vert',0,[-1 1]);
%topoplot(diff,chanloc);cbar('vert',0,[-1 1]*max(abs(diff)));


%%
baseline= -100;
%maplim= [-1 1];
maplim = [-6 2.5];
mytimes = [400];
y_lim = [-6.5 3.1];
load('chanlocs','chanloc')
erp_HA = mean((erpdata{2}(:,1:64,sujs)+erpdata{2}(:,1:64,sujs))/2,3);
erp_LA = mean((erpdata{1}(:,1:64,sujs)+erpdata{1}(:,1:64,sujs))/2,3);
%erp_HA = mean((erpdata{4}(:,1:64,sujs)+erpdata{8}(:,1:64,sujs))/2 - (my_pre.erpdata{4}(:,1:64,sujs)+my_pre.erpdata{8}(:,1:64,sujs))/2,3);
%erp_LA = mean((erpdata{3}(:,1:64,sujs)+erpdata{7}(:,1:64,sujs))/2 - (my_pre.erpdata{3}(:,1:64,sujs)+my_pre.erpdata{7}(:,1:64,sujs))/2,3);

erp_diff = squeeze(erp_HA-erp_LA)'; %%%% first dimensions is channels, second is ERP data points

figure(61),title(['Visual, Post: ' num2str(mytimes-100) 'ms'])
topoplot(mean(erp_diff(:,mytimes),2),chanloc,'electrodes','ptslabels');cbar('vert',0,[-1 1]);
%topoplot(diff,chanloc);cbar('vert',0,[-1 1]*max(abs(diff)));
title(['Post Visual 300ms'])

%%
if ~adjust_pre
    erp_HV = mean((erpdata{2}(:,1:64,sujs)+erpdata{6}(:,1:64,sujs))/2,3);
    erp_LV = mean((erpdata{1}(:,1:64,sujs)+erpdata{5}(:,1:64,sujs))/2,3);
else
    erp_HV = mean((erpdata{2}(:,1:64,sujs)+erpdata{6}(:,1:64,sujs))/2 - (my_pre.erpdata{2}(:,1:64,sujs)+my_pre.erpdata{6}(:,1:64,sujs))/2,3);
    erp_LV = mean((erpdata{1}(:,1:64,sujs)+erpdata{5}(:,1:64,sujs))/2 - (my_pre.erpdata{1}(:,1:64,sujs)+my_pre.erpdata{5}(:,1:64,sujs))/2,3);
end
erp_diff = squeeze(erp_HV-erp_LV)'; %%%% first dimensions is channels, second is ERP data points
erp_diff = squeeze(erp_HV-erp_LV)'; %%%% first dimensions is channels, second is ERP data points
figure(7);timtopo(erp_diff,chanloc,'limits',[x_lim y_lim],'maplimits',maplim ,'plottimes',mytimes ,'title','DEVdiff');

diff = mean(erp_diff(:,[442]),2);
figure(8)
%topoplot(mean(myerp{myconds(1)}(:,[250:500]),2),chanloc,'electrodes','ptslabels');
topoplot(diff,chanloc,'electrodes','ptslabels');cbar('vert',0,[-1 1]*max(abs(diff)));
%topoplot(diff,chanloc);cbar('vert',0,[-1 1]*max(abs(diff)));

%%
%%
if ~adjust_pre
    erp_HV = mean((erpdata{2}(:,1:64,sujs)+erpdata{6}(:,1:64,sujs))/2,3);
    erp_LV = mean((erpdata{1}(:,1:64,sujs)+erpdata{5}(:,1:64,sujs))/2,3);
    erp_diff = squeeze(erp_HV-erp_LV)'; %%%% first dimensions is channels, second is ERP data points
    
else
    erp_HV = mean((erpdata{2}(:,1:64,sujs)+erpdata{6}(:,1:64,sujs))/2 - (my_pre.erpdata{2}(:,1:64,sujs)+my_pre.erpdata{6}(:,1:64,sujs))/2,3);
    erp_N = mean((erpdata{9}(:,1:64,sujs)+erpdata{10}(:,1:64,sujs))/2 - (my_pre.erpdata{9}(:,1:64,sujs)+my_pre.erpdata{10}(:,1:64,sujs))/2,3);
    erp_diff = squeeze(erp_HV-erp_N)'; %%%% first dimensions is channels, second is ERP data points
    
end
figure(7);timtopo(erp_diff,chanloc,'limits',[x_lim y_lim],'maplimits',maplim ,'plottimes',mytimes ,'title','DEVdiff');

diff = mean(erp_diff(:,[220:320]),2);
figure(9)
%topoplot(mean(myerp{myconds(1)}(:,[250:500]),2),chanloc,'electrodes','ptslabels');
topoplot(diff,chanloc,'electrodes','ptslabels');cbar('vert',0,[-1 1]*max(abs(diff)));
%topoplot(diff,chanloc);cbar('vert',0,[-1 1]*max(abs(diff)));



%%
% close all
figure;
hold on
% area([erptimes(p1Window(1)) erptimes(p1Window(end)) erptimes(p1Window(end)) erptimes(p1Window(1))],[-3 -3 7 7],'FaceColor',[0.95 0.95 0.95],'EdgeColor',[1 1 1])
% area([erptimes(n1Window(1)) erptimes(n1Window(end)) erptimes(n1Window(end)) erptimes(n1Window(1))],[-3 -3 7 7],'FaceColor',[0.85 0.85 0.85],'EdgeColor',[1 1 1])

plot(erptimes(allWindow),(mean(myP1SH(allWindow,:)-myP1SL(allWindow,:),2)),'b','linewidth',3); % Mean
plot(erptimes(allWindow),(mean(myP1VH(allWindow,:)-myP1VL(allWindow,:),2)),'c','linewidth',3); % Mean
plot(erptimes([1 end]),[mean(mean(myP1SH(allWindow(1:100),:)-myP1SL(allWindow(1:100),:),2)) mean(mean(myP1SH(allWindow(1:100),:)-myP1SL(allWindow(1:100),:),2))],':b','linewidth',2); % Mean
plot(erptimes([1 end]),[mean(mean(myP1VH(allWindow(1:100),:)-myP1VL(allWindow(1:100),:),2)) mean(mean(myP1VH(allWindow(1:100),:)-myP1VL(allWindow(1:100),:),2))],':c','linewidth',2); % Mean

axis([mybaseline(1) allWindow(end)+mybaseline(1) -2 2])

%%
% %% Plot  and visualize each channel
% for i=1:size(erpdata{1},2)
%     figure(i), plot(erptimes,(mean(erpdata{3}(:,i,:),3)+mean(erpdata{7}(:,i,:),3))/2,'r'), hold on
%     plot(erptimes,(mean(erpdata{4}(:,i,:),3)+mean(erpdata{8}(:,i,:),3))/2,'g'), hold on
%     xlim([-100 500])
%     title([ 'Auditory: ' mychans{i}])
%     print(['-f' num2str(i)],'-dpsc2','chans.ps','-append')
%
%     figure(i+64), plot(erptimes,(mean(erpdata{1}(:,i,:),3)+mean(erpdata{5}(:,i,:),3))/2,'r'), hold on
%     plot(erptimes,(mean(erpdata{2}(:,i,:),3)+mean(erpdata{4}(:,i,:),3))/2,'g'), hold on
%     xlim([-100 500])
%     title([ 'Visual: ' mychans{i}])
%     print(['-f' num2str(i+64)],'-dpsc2','chans.ps','-append')
%     close all
% end
%% First look at visual, auditory, bimodal-same, and bimodal-opposite
HV= myVH_P1_window_mean;
LV= myVL_P1_window_mean;
HA= mySH_P1_window_mean;
LA= mySL_P1_window_mean;
N= myN_P1_window_mean;
anovdata_rew_modality_names = {'HV';'LV';'HA';'LA'};
anovdata_rew_modality = [HV;LV;HA;LA]';

bimodncond=length(anovdata_rew_modality_names);
subfactor=cell(bimodncond,1);

for  i=1:bimodncond
    subfactor{i}= ['F' num2str(i)];
end

t = array2table(anovdata_rew_modality,'VariableNames',subfactor);
%factorNames = {'modality','reward','latencybin','part'};
factorNames = {'modality','reward'}; % for each part


within = table({'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
    {'H';'L';'H';'L'},... %reward: high or low
    'VariableNames',factorNames);
%
% fit the repeated measures model
rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);

% run my repeated measures anova here
%[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
[ranovatbl] = ranova(rm, 'WithinModel','modality*reward')
%%
HV_beh= mybehavior.s.subBCvH(sujs)'-mybehavior_pre.s.subBCvH(sujs)';
LV_beh= mybehavior.s.subBCvL(sujs)'-mybehavior_pre.s.subBCvL(sujs)';
HA_beh= mybehavior.s.subSPvH(sujs)'-mybehavior_pre.s.subSPvH(sujs)';
LA_beh= mybehavior.s.subSPvL(sujs)'-mybehavior_pre.s.subSPvH(sujs)';
N_beh = mybehavior.s.subNeut(sujs)'-mybehavior_pre.s.subNeut(sujs)';

HV_beh= mybehavior.s.subBCvH(sujs)';
LV_beh= mybehavior.s.subBCvL(sujs)';
HA_beh= mybehavior.s.subSPvH(sujs)';
LA_beh= mybehavior.s.subSPvL(sujs)';
N_beh = mybehavior.s.subNeut(sujs)';
anovdata_rew_modality_names = {'HV_beh';'LV_beh';'HA_beh';'LA_beh'};
anovdata_rew_modality = [HV_beh;LV_beh;HA_beh;LA_beh]';

bimodncond=length(anovdata_rew_modality_names);
subfactor=cell(bimodncond,1);

for  i=1:bimodncond
    subfactor{i}= ['F' num2str(i)];
end

t = array2table(anovdata_rew_modality,'VariableNames',subfactor);
%factorNames = {'modality','reward','latencybin','part'};
factorNames = {'modality','reward'}; % for each part


within = table({'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
    {'H';'L';'H';'L'},... %reward: high or low
    'VariableNames',factorNames);
%
% fit the repeated measures model
rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);

% run my repeated measures anova here
%[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
[ranovatbl] = ranova(rm, 'WithinModel','modality*reward')
%% Correlate EEG and behavior
[r_visb p_visb ] = corrcoef(HV-LV,HV_beh-LV_beh)
[r_audb p_audb] = corrcoef(HA-LA,HA_beh-LA_beh)
%%
[b1,bint,r, rint,stats] = regress(HV_beh'-LV_beh', [ ones(size(HV))' HV'-LV']);
figure, plot(HV-LV,HV_beh-LV_beh,'ok','MarkerSize',8,'MarkerFaceColor','m'), hold on
plot(-6:6,(-6:6).*b1(2)+b1(1),':k')


[b2,bint,r, rint,stats] = regress(HA_beh'-LA_beh', [ ones(size(HA))' HA'-LA']);
figure,plot(HA-LA,HA_beh-LA_beh,'ok','MarkerSize',8,'MarkerFaceColor','m'), axis square, hold on
plot(-4:4,(-4:4).*b2(2)+b2(1),':k')
%%
figure
%bar(1:2,[mean(HA); mean(LA)],[std(HA);std(LA)],'k')
errorbar(1:2,[mean(HA); mean(LA)],[std(HA);std(LA)]./sqrt((NSub)),'color',[0.5 0.5 0.5],'linewidth',3), axis square, hold on
%errorbar([1.05 2.05],[mean(HV); mean(LV)],[std(HV);std(LV)]./sqrt((NSub)),'color',[0 0 0],'linewidth',3)
ylim([1.5 4.5])

%%
%close all
figi= figure;

for i=1:37
    subplot(10,4,i), plot(erptimes(allWindow),myP1SL(allWindow,i),'b','linewidth',1), axis tight, hold on; % Mean
    plot(erptimes(allWindow),myP1SH(allWindow,i),'r','linewidth',1); % Mean
    title(num2str(i))
end
%print(figi,'-dpdf','subs1.pdf','-fillpage')


figi= figure;

plot(erptimes(allWindow),mean(myP1SL(allWindow,:),2),'b','linewidth',1), hold on; % Mean
plot(erptimes(allWindow),mean(myP1SH(allWindow,:),2),'r','linewidth',1); % Mean

%%
for i=1: size(erpdata{1},2)
    erp_LA1= squeeze(mean((erpdata{3}(p1Window,:,sujs)+erpdata{7}(p1Window,:,sujs))/2));
    erp_HA1= squeeze(mean((erpdata{4}(p1Window,:,sujs)+erpdata{8}(p1Window,:,sujs))/2));
    erp_LV1= squeeze(mean((erpdata{1}(p1Window,:,sujs)+erpdata{5}(p1Window,:,sujs))/2));
    erp_HV1= squeeze(mean((erpdata{2}(p1Window,:,sujs)+erpdata{6}(p1Window,:,sujs))/2));
    [r p] = corrcoef(erp_HA1(i,:)-erp_LA1(i,:),HA_beh-LA_beh);
    r_audbb(i,:)=r(2); p_audbb(i,:)=p(2);
    
    [r p] = ttest(erp_HA1(i,:)-erp_LA1(i,:));
    r_aud2(i,:)=r; p_aud2(i,:)=p;
    
    [r p] = corrcoef(erp_HV1(i,:)-erp_LV1(i,:),HV_beh-LV_beh);
    r_visbb(i,:)=r(2); p_visbb(i,:)=p(2);
    
    [r p] = corrcoef(((erp_LV1(i,:)-erp_HV1(i,:))+(erp_HA1(i,:)-erp_LA1(i,:)))/2,((LV_beh-HV_beh)+(HA_beh-LA_beh))/2);
    r_audvis(i,:)=r(2); p_audvis(i,:)=p(2);
end
figure
topoplot(p_audbb,chanloc,'electrodes','ptslabels');cbar('vert',0,[0.01 0.1 ]);
figure
topoplot(r_audbb,chanloc,'electrodes','ptslabels');cbar('vert',0,[-1 1]*max(abs(r_audbb)));

figure
topoplot(p_visbb,chanloc,'electrodes','ptslabels');cbar('vert',0,[0.01 0.1 ]);
figure
topoplot(r_visbb,chanloc,'electrodes','ptslabels');cbar('vert',0,[-1 1]*max(abs(r_visbb)));

figure
topoplot(r_audvis,chanloc,'electrodes','ptslabels');cbar('vert',0,[-1 1]*max(abs(r_audvis)));

%% Compare methods
%
% delete (['sizeData.txt']);
% textfile = fopen(['sizeData.txt'],'wt');
% fprintf(textfile,'Pp\tV_L\tV_H\tA_L\tA_H\n');
% for i = 1:NSub
%  fprintf(textfile,'Pp%d\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t\n',i,myVL_P1_window_mean(i),myVH_P1_window_mean(i),mySL_P1_window_mean(i),mySH_P1_window_mean(i));
% end
% fclose all;
% save('IndividPeaks20ms','myVL_P1_window_mean','myVH_P1_window_mean','mySL_P1_window_mean','mySH_P1_window_mean','myVL_P1_window_mean','myN_P1_window_mean');