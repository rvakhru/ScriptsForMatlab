%function [] = EEGexp11()
% Analyze EEG data for experiment 11
%
% 1.4.1 Main effect of VALUE
% (d�) Simultaneous valuable cue increases the sensitivity of discrimination of the visual target.
% (RT) Simultaneous high-value cue decreases reaction times.
% (EEG) Within modal valuable and cross-modal valuable cues both modulate early visual sensory areas (Posterior electrodes).
% (timeEEG) High-value cue evoked ERP responses occur earlier than low-value cue modulation.
%
% 1.4.2 Main effect of MODALITY
% (EEG) Within modal valuable cue modulation of ERP components is stronger in amplitude than the between-modal condition.
% (timeEEG) Within modal valuable cue, modulation occurs earlier than the between-modal condition.
%
% 1.4.3 Correlations
% (d�/EEG)Change in the behavioral target sensitivity correlates positively with the change in amplitude and latency of early ERP components due to reward.
% (pupil/EEG)Change of the pupil size correlates positively with the change in amplitude and latency of early ERP components due to reward.
% (d� error)The effect of reward is independent of the eye movements.
close all
%% 1.1 Retreave our DATA
partOfExpI = 2; partOfExp = {'PreCond', 'PostCond', 'Conditioning'};
if partOfExpI == 1
    load('W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA_F\Studies\EEGPlootPlot_testTrPre_ECVP_0808.mat');
elseif partOfExpI == 2
    load('W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA_F\Studies\EEGPlootPlot_testTrPost_ECVP_0808.mat');
elseif partOfExpI == 3
    load('W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA_F\Studies\EEGPlootPlot_testTrCond_ECVP_0808.mat');
end
% load('W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA_F\Studies\EEGPlootPlot_testTrCond_ECVP_0808.mat');


mybaseline =[-100 0];                   % Baseline
winlength = 30;                         % window that we are analysing
individualpeak = 0;                     % INPORTANT! are we looking for a mean peak?
p1 = 1;                                 % Statistics for each component? P1 or N1?
my_ROI = {'O1' 'O2' 'PO7' 'PO8'};       % Electrodes
%  my_ROI = {'O1' 'PO7'};
%my_ROI = {'Fz' 'FCz' 'Cz' 'CPz' 'Pz'};
%my_ROI = {'O1' 'PO7' 'P5' 'PO3' 'P1' 'P3' 'O2' 'PO8' 'P6' 'PO4' 'P2' 'P4'}; %%%%%see here for how to do 2*2 ANOVA https://sccn.ucsd.edu/pipermail/eeglablist/2011/003839.html

for i = 1:length(my_ROI)                % Find electrodes from our dataset
    electrodes(i) = find(strcmp(mychans,my_ROI{i}));
end

%% 1.2 Define WINDOWS
allWindow = 1:800;
p1Window = (-1)*mybaseline(1)+[70:170];     % P1: 70-170
n1Window = (-1)*mybaseline(1)+[180:250];    % N1: 180-250
p3Window = (-1)*mybaseline(1)+[300:600];    % P3: 300-600
p1n1Window = (-1)*mybaseline(1)+[180:200];

% if ~p1, p1Window = (-1)*mybaseline(1)+[180:250];end
% p1Window = (-1)*mybaseline(1)+[300:600];
% n1Window = (-1)*mybaseline(1)+[160:190];

%% 1.3 Preallocate
% Based on chosen windows we create a vector for each participants.
NSamp = size(erpdata{1},1);
NCahn = size(erpdata{1},2);
NSub = size(erpdata{1},3);
myP1VL_P1_window = nan(NSub,winlength);
myP1VH_P1_window = nan(NSub,winlength);
myP1SL_P1_window = nan(NSub,winlength);
myP1SH_P1_window = nan(NSub,winlength);
myP1N_P1_window = nan(NSub,winlength);


%% 2.1 Process data
% Extract triggers and trasfom them to conditions
myP1VLL = mean(erpdata{1}(:,electrodes,:),2);   % VLL
myP1VHL = mean(erpdata{2}(:,electrodes,:),2);   % VHL
myP1SLL = mean(erpdata{3}(:,electrodes,:),2);   % SLL
myP1SHL = mean(erpdata{4}(:,electrodes,:),2);   % SHL
myP1VLR = mean(erpdata{5}(:,electrodes,:),2);   % VLR
myP1VHR = mean(erpdata{6}(:,electrodes,:),2);   % VHR
myP1SLR = mean(erpdata{7}(:,electrodes,:),2);   % SLR
myP1SHR = mean(erpdata{8}(:,electrodes,:),2);   % SHR
myP1N_L = mean(erpdata{1}(:,electrodes,:),2);   % N_L
myP1N_R = mean(erpdata{1}(:,electrodes,:),2);  % N_R

% Combine different sides dogether
myP1VL = squeeze(myP1VLL + myP1VLR)/2;
myP1VH = squeeze(myP1VHL + myP1VHR)/2;
myP1SL = squeeze(myP1SLL + myP1SLR)/2;
myP1SH = squeeze(myP1SHL + myP1SHR)/2;
myP1N  = squeeze(myP1N_L + myP1N_R)/2;

myN1VL = squeeze(myP1VLL + myP1VLR)/2;
myN1VH = squeeze(myP1VHL + myP1VHR)/2;
myN1SL = squeeze(myP1SLL + myP1SLR)/2;
myN1SH = squeeze(myP1SHL + myP1SHR)/2;
myN1N  = squeeze(myP1N_L + myP1N_R)/2;

% % Combine different sides dogether
% myP1VL = squeeze(myP1VLL);
% myP1VH = squeeze(myP1VHL);
% myP1SL = squeeze(myP1SLL);
% myP1SH = squeeze(myP1SHL);
% myP1N  = squeeze(myP1N_L);
% 
% % Combine different sides dogether
% myP1VL = squeeze(myP1VLR);
% myP1VH = squeeze(myP1VHR);
% myP1SL = squeeze(myP1SLR);
% myP1SH = squeeze(myP1SHR);
% myP1N  = squeeze(myP1N_R);
% 

%% 2.2 Find a point of maximum within a window of our interest
if individualpeak
    % Here we find only peaks of each participant
    [myP1VL_P1, myP1VL_loc_P1] = max(myP1VL(p1Window,:));
    [myP1VH_P1, myP1VH_loc_P1] = max(myP1VH(p1Window,:));
    [myP1SL_P1, myP1SL_loc_P1] = max(myP1SL(p1Window,:));
    [myP1SH_P1, myP1SH_loc_P1] = max(myP1SH(p1Window,:));
    [myP1N_P1, myP1N_loc_P1] =   max(myP1N (p1Window,:));
    
    % Here we find only peaks of each participant
    [myN1VL_N1, myN1VL_loc_N1] = max(myN1VL(n1Window,:));
    [myN1VH_N1, myN1VH_loc_N1] = max(myN1VH(n1Window,:));
    [myN1SL_N1, myN1SL_loc_N1] = max(myN1SL(n1Window,:));
    [myN1SH_N1, myN1SH_loc_N1] = max(myN1SH(n1Window,:));
    [myN1N_N1, myN1N_loc_N1] =   max(myN1N (n1Window,:));
else

    % P1 Here we find an average peach between all participants
    [myP1VL_P1, myP1VL_loc_P1] = max(mean(myP1VL(p1Window,:),2));
    [myP1VH_P1, myP1VH_loc_P1] = max(mean(myP1VH(p1Window,:),2));
    [myP1SL_P1, myP1SL_loc_P1] = max(mean(myP1SL(p1Window,:),2));
    [myP1SH_P1, myP1SH_loc_P1] = max(mean(myP1SH(p1Window,:),2));
    [myP1N_P1, myP1N_loc_P1] =   max(mean(myP1N(p1Window,:),2));

    % N1 Here we find an average peach between all participants
    [myN1VL_N1, myN1VL_loc_N1] = max(mean(myN1VL(n1Window,:),2));
    [myN1VH_N1, myN1VH_loc_N1] = max(mean(myN1VH(n1Window,:),2));
    [myN1SL_N1, myN1SL_loc_N1] = max(mean(myN1SL(n1Window,:),2));
    [myN1SH_N1, myN1SH_loc_N1] = max(mean(myN1SH(n1Window,:),2));
    [myN1N_N1, myN1N_loc_N1] =   max(mean(myN1N(n1Window,:),2));
    
    
    % P1 Preallocate to keep the condistancy of our script 
    myP1VL_P1 = repmat(myP1VL_P1,[1 NSub]);
    myP1VH_P1 = repmat(myP1VH_P1,[1 NSub]);
    myP1SL_P1 = repmat(myP1SL_P1,[1 NSub]);
    myP1SH_P1 = repmat(myP1SH_P1,[1 NSub]);
    myP1N_P1 = repmat(myP1N_P1,[1 NSub]);
    
    myP1VL_loc_P1 = repmat(myP1VL_loc_P1,[1 NSub]);
    myP1VH_loc_P1 = repmat(myP1VH_loc_P1,[1 NSub]);
    myP1SL_loc_P1 = repmat(myP1SL_loc_P1,[1 NSub]);
    myP1SH_loc_P1 = repmat(myP1SH_loc_P1,[1 NSub]);
    myP1N_loc_P1 = repmat(myP1N_loc_P1,[1 NSub]);
    
    % N1 Preallocate to keep the condistancy of our script 
    myN1VL_N1 = repmat(myN1VL_N1,[1 NSub]);
    myN1VH_N1 = repmat(myN1VH_N1,[1 NSub]);
    myN1SL_N1 = repmat(myN1SL_N1,[1 NSub]);
    myN1SH_N1 = repmat(myN1SH_N1,[1 NSub]);
    myN1N_N1 = repmat(myN1N_N1,[1 NSub]);
    
    myN1VL_loc_N1 = repmat(myN1VL_loc_N1,[1 NSub]);
    myN1VH_loc_N1 = repmat(myN1VH_loc_N1,[1 NSub]);
    myN1SL_loc_N1 = repmat(myN1SL_loc_N1,[1 NSub]);
    myN1SH_loc_N1 = repmat(myN1SH_loc_N1,[1 NSub]);
    myN1N_loc_N1 = repmat(myN1N_loc_N1,[1 NSub]);
end


%% 2.2 EXTRACT WINDOWS from the data

% we define dindow that we are interested (In case of average peak this window is the same for all participants)
for i = 1:NSub
    % P1 window
    myP1VL_P1_window(i,:) = [p1Window(myP1VL_loc_P1(i))-winlength/2:p1Window(myP1VL_loc_P1(i))+winlength/2-1];
    myP1VH_P1_window(i,:) = [p1Window(myP1VH_loc_P1(i))-winlength/2:p1Window(myP1VH_loc_P1(i))+winlength/2-1];
    myP1SL_P1_window(i,:) = [p1Window(myP1SL_loc_P1(i))-winlength/2:p1Window(myP1SL_loc_P1(i))+winlength/2-1];
    myP1SH_P1_window(i,:) = [p1Window(myP1SH_loc_P1(i))-winlength/2:p1Window(myP1SH_loc_P1(i))+winlength/2-1];
    myP1N_P1_window(i,:) =  [p1Window(myP1N_loc_P1(i))-winlength/2:p1Window(myP1N_loc_P1(i))+winlength/2-1];
    % N1 window
    myN1VL_N1_window(i,:) = [n1Window(myN1VL_loc_N1(i))-winlength/2:n1Window(myN1VL_loc_N1(i))+winlength/2-1];
    myN1VH_N1_window(i,:) = [n1Window(myN1VH_loc_N1(i))-winlength/2:n1Window(myN1VH_loc_N1(i))+winlength/2-1];
    myN1SL_N1_window(i,:) = [n1Window(myN1SL_loc_N1(i))-winlength/2:n1Window(myN1SL_loc_N1(i))+winlength/2-1];
    myN1SH_N1_window(i,:) = [n1Window(myN1SH_loc_N1(i))-winlength/2:n1Window(myN1SH_loc_N1(i))+winlength/2-1];
    myN1N_N1_window(i,:) =  [n1Window(myN1N_loc_N1(i))-winlength/2:n1Window(myN1N_loc_N1(i))+winlength/2-1];
end

%% 3.1 Plot cutting results
% Plot individual peaks and highlight
% windows that we are going to analyze
figure(1); hold on;
for i = 1:NSub
    % P1 window
    plot(erptimes(myP1SH_P1_window(i,:)),myP1SH(myP1SH_P1_window(i,:),i),'linewidth', 5);
    mySH_P1_window_mean(i) = mean(myP1SH(myP1SH_P1_window(i,:),i));
    mySL_P1_window_mean(i) = mean(myP1SL(myP1SL_P1_window(i,:),i));
    myVH_P1_window_mean(i) = mean(myP1VH(myP1VH_P1_window(i,:),i));
    myVL_P1_window_mean(i) = mean(myP1VL(myP1VL_P1_window(i,:),i));
    myN_P1_window_mean(i) = mean(myP1N(myP1N_P1_window(i,:),i));
    % N1 window
    mySH_N1_window_mean(i) = mean(myP1SH(myN1SH_N1_window(i,:),i));
    mySL_N1_window_mean(i) = mean(myP1SL(myN1SL_N1_window(i,:),i));
    myVH_N1_window_mean(i) = mean(myP1VH(myN1VH_N1_window(i,:),i));
    myVL_N1_window_mean(i) = mean(myP1VL(myN1VL_N1_window(i,:),i));
    myN_N1_window_mean(i)  = mean(myP1N(myN1N_N1_window(i,:),i));
%       mySH_P1_window_mean(i) = mean(mean(myP1SH(:,i)));
%     mySL_P1_window_mean(i) = mean(mean(myP1SL(:,i)));
%     myVH_P1_window_mean(i) = mean(mean(myP1VH(:,i)));
%     myVL_P1_window_mean(i) = mean(mean(myP1VL(:,i)));
%     myN_P1_window_mean(i) = mean(mean(myP1N(:,i)));
end


hold on
plot(erptimes(allWindow),mean((myP1SH(allWindow,:,:)),2),'linewidth',3); % Mean
plot(erptimes(allWindow),mean((myP1SH(allWindow,:,:)),2),'linewidth',3); % Mean
plot(erptimes(allWindow),(myP1SH(allWindow,:,:)),'linewidth',.5);        % For Participant
plot(erptimes(allWindow),(myP1SL(allWindow,:,:)),'linewidth',.5);        % For Participant

hold on
%plot(erptimes(allWindow),(myP1SL(allWindow,:,:)),'linewidth',3);
plot([erptimes(p1Window(1))    erptimes(p1Window(1))],[-5  20],'-.m')
plot([erptimes(p1Window(end))  erptimes(p1Window(end))  ],[-5  20],'-.m')

plot([erptimes(n1Window(1))    erptimes(n1Window(1))],[-5  20 ],'-.k')
plot([erptimes(n1Window(end))  erptimes(n1Window(end))  ],[-5   20 ],'-.k')
ylim( [-5   20 ])

% plot(erptimes(p1Window((myP1VL_loc_P1))),(myP1VL_P1),'*k');
% plot(erptimes(p1Window((myP1VH_loc_P1))),(myP1VH_P1),'*k');
% plot(erptimes(p1Window((myP1SL_loc_P1))),(myP1SL_P1),'*k');
plot(erptimes(p1Window((myP1SH_loc_P1))),(myP1SH_P1),'*k');
% plot(erptimes(p1Window((myP1N_loc_P1))),(myP1N_P1),'*k');
% plot(erptimes(p1Window),(myP1VH(p1Window,:,:)),'linewidth',3);





figure;[h p]= ttest(mySH_P1_window_mean, mySL_P1_window_mean);
boxplot([mySH_P1_window_mean', mySL_P1_window_mean'],'Labels',{'Sound High Val','Sound Low Val'});
title(['Sound ERP; P = ' num2str(p)]);
% checkWind(3,c) = p; 

figure;[h p]= ttest(myVL_P1_window_mean, myVH_P1_window_mean);
boxplot([myVH_P1_window_mean', myVL_P1_window_mean'],'Labels',{'Visual High Val','Visual Low Val'});
title(['Visual ERP; P = ' num2str(p)]);
% checkWind(4,c) = p; 

figure;[h p]= ttest(myVL_P1_window_mean, myVH_P1_window_mean);
bar([mean(myVH_P1_window_mean'-myVL_P1_window_mean')]), hold on;
errorbar(1,mean(myVH_P1_window_mean'-myVL_P1_window_mean'),std(myVH_P1_window_mean'-myVL_P1_window_mean'));
title(['2 visual based on individual peaks']);

figure;[h p]= ttest(mySL_P1_window_mean, mySH_P1_window_mean);
bar([mean(mySH_P1_window_mean'-mySL_P1_window_mean')]), hold on;
errorbar(1,mean(mySH_P1_window_mean'-mySL_P1_window_mean'),std(mySH_P1_window_mean'-mySL_P1_window_mean'));
title(['1 sound based on individual peaks']);
%% power calculation for ANOVA
mean(mean([ myVH_P1_window_mean' - myVL_P1_window_mean'  mySH_P1_window_mean' - mySL_P1_window_mean'],2))
std(mean([  myVH_P1_window_mean' - myVL_P1_window_mean' mySH_P1_window_mean' - mySL_P1_window_mean'],2))

mean(mean([  mySH_P1_window_mean' - mySL_P1_window_mean'],2))
std(mean([  mySH_P1_window_mean' - mySL_P1_window_mean'],2))
%%
close all
% hold on
% area([erptimes(p1Window(1)) erptimes(p1Window(end)) erptimes(p1Window(end)) erptimes(p1Window(1))],[-3 -3 7 7],'FaceColor',[0.95 0.95 0.95],'EdgeColor',[1 1 1])
% area([erptimes(n1Window(1)) erptimes(n1Window(end)) erptimes(n1Window(end)) erptimes(n1Window(1))],[-3 -3 7 7],'FaceColor',[0.85 0.85 0.85],'EdgeColor',[1 1 1])
figure('Position', [10 10 600 600])

% subplot(1,2,2)
hold on
% area([erptimes(p1Window(1)) erptimes(p1Window(end)) erptimes(p1Window(end)) erptimes(p1Window(1))],[-3 -3 7 7],'FaceColor',[0.75 0.75 0.55],'EdgeColor',[1 1 1])
% area([erptimes(n1Window(1)) erptimes(n1Window(end)) erptimes(n1Window(end)) erptimes(n1Window(1))],[-3 -3 7 7],'FaceColor',[0.65 0.85 0.85],'EdgeColor',[1 1 1])
% area([erptimes(p3Window(1)) erptimes(p3Window(end)) erptimes(p3Window(end)) erptimes(p3Window(1))],[-3 -3 7 7],'FaceColor',[0.95 0.85 0.95],'EdgeColor',[1 1 1])
% area([erptimes(p1n1Window(1)) erptimes(p1n1Window(end)) erptimes(p1n1Window(end)) erptimes(p1n1Window(1))],[-3 -3 7 7],'FaceColor',[0.55 0.65 0.35],'EdgeColor','none')
rP = rectangle('Position',[erptimes(p1Window(1)) 0 erptimes(p1Window(end))-erptimes(p1Window(1)) 7]');
rN = rectangle('Position',[erptimes(n1Window(1))  0 erptimes(n1Window(end))-erptimes(n1Window(1)) 7]');
rP.EdgeColor = 'k'; rP.LineWidth = 1; rP.LineStyle = '--';
rN.EdgeColor = 'b'; rN.LineWidth = 1; rN.LineStyle = '--';

LVS = plot(erptimes(allWindow),mean((myP1SL(allWindow,:,:)),2),'r','linewidth',3); % Mean
HVS = plot(erptimes(allWindow),mean((myP1SH(allWindow,:,:)),2),'g','linewidth',3); % Mean
NS = plot(erptimes(allWindow),mean((myP1N(allWindow,:,:)),2),':k','linewidth',3); % Mean

[a,b ] = boundedline(erptimes(allWindow), mean((myP1SL(allWindow,:,:)),2), nanstd((myP1SL(allWindow,:,:)),0,2)./sqrt(size((myP1SL(allWindow,:,:)),2)),'b','linewidth', 4,...
            erptimes(allWindow), mean((myP1SH(allWindow,:,:)),2), nanstd((myP1SH(allWindow,:,:)),0,2)./sqrt(size((myP1SH(allWindow,:,:)),2)),'r')
% NS = plot(erptimes(allWindow),mean((myP1N(allWindow,:,:)),2),':k','linewidth',3); % Mean

        % outlinebounds(a,b)
axis([mybaseline(1) allWindow(end)+mybaseline(1) -2 8])
ylim([-2 7])
set(gca,'FontSize', 20)
set(gca,'XTick',0:200:600);
set(gca,'YTick',-1:2:7);
drawaxis(gca, 'x', 0, 'movelabel', 1);
drawaxis(gca, 'y', 0, 'movelabel', 1);

% box 'on'; axis square;

set(gca,'Visible','off')
set(findall(gca, 'type', 'text'), 'visible', 'on')
% legend([HVS LVS NS],{'High Value','Low Value', 'Neutral'},'Orientation','horizontal','Location','north','FontSize',12,'EdgeColor','none')
% title('Auditory ERPs: O1/O2,PO7/PO8')
ylabel('\muV')
xlabel('ms')
[h,p]=ttest( (myP1SL(allWindow,:,:))',(myP1SH(allWindow,:,:))')

pWindW = [p(1:4) nan(size((myP1SL(allWindow,:,:)),1)-10,1)' p(end-5:end)];
for i =5:size((myP1SL(allWindow,:,:)),1)-5
    % Walking window using individual differances
    [h,pWacrossPp] = ttest( (myP1SL(i-4:i+5,:,:))',(myP1SH(i-4:i+5,:,:))');
    pWindWA(i)=mean(pWacrossPp);
    % Walking window using means
    [h,pWwithinPp] = ttest(mean(myP1SL(i-4:i+5,:,:),1),mean(myP1SH(i-4:i+5,:,:),1));
    pWindW(i)=mean(pWwithinPp);
end
p= pWindW;

 scatter(find(p<0.05)-100,zeros(length(find(p<0.05)),1)-.75','k','Marker', 's');


figure('Position', [10 10 600 600])
% subplot(1,2,1)
hold on
% box 'on'; axis square;
rP = rectangle('Position',[erptimes(p1Window(1)) 0 erptimes(p1Window(end))-erptimes(p1Window(1)) 4]');
rN = rectangle('Position',[erptimes(n1Window(1))  0 erptimes(n1Window(end))-erptimes(n1Window(1)) 4]');
rP.EdgeColor = 'k'; rP.LineWidth = 1; rP.LineStyle = '--';
rN.EdgeColor = 'b'; rN.LineWidth = 1; rN.LineStyle = '--';

LVV = plot(erptimes(allWindow),mean((myP1VL(allWindow,:,:)),2),'r','linewidth',3); % Mean
HVV = plot(erptimes(allWindow),mean((myP1VH(allWindow,:,:)),2),'g','linewidth',3); % Mean

[a,b ] = boundedline(erptimes(allWindow), mean((myP1VL(allWindow,:,:)),2), nanstd((myP1VL(allWindow,:,:)),0,2)./sqrt(size((myP1VL(allWindow,:,:)),2)),'b','linewidth', 4,...
            erptimes(allWindow), mean((myP1VH(allWindow,:,:)),2), nanstd((myP1VH(allWindow,:,:)),0,2)./sqrt(size((myP1VH(allWindow,:,:)),2)),'r')
% NVV = plot(erptimes(allWindow),mean((myP1N(allWindow,:,:)),2),':k','linewidth',3); % Mean


% legend([HVV LVV NVV],{'High Value','Low Value', 'Neutral'},'Orientation','horizontal','Location','north','FontSize',12,'EdgeColor','none');
legend('boxoff');
% title('Visual ERPs: O1/O2, PO7/PO8')
ylabel('\muV')
axis([mybaseline(1) allWindow(end)+mybaseline(1) -1 3])
ylim([-1 4.5])
set(gca,'FontSize', 20)
set(gca,'XTick',0:200:600);
set(gca,'YTick',-1:2:4);
drawaxis(gca, 'x', 0, 'movelabel', 1);
drawaxis(gca, 'y', 0, 'movelabel', 1);
text(115,6,'P1','FontSize',30);
text(190,6,'N1','FontSize',30);
ylabel('\muV')
xlabel('ms')
set(gca,'Visible','off')
set(findall(gca, 'type', 'text'), 'visible', 'on')
[h,p]=ttest( (myP1VL(allWindow,:,:))',(myP1VH(allWindow,:,:))');

pWindW = [p(1:4) nan(size((myP1VL(allWindow,:,:)),1)-10,1)' p(end-5:end)];
for i =5:size((myP1VL(allWindow,:,:)),1)-5
    % Waling window with inndividual differances
    [h,pWacrossPp] = ttest( (myP1VL(i-4:i+5,:,:))',(myP1VH(i-4:i+5,:,:))');
    pWindA(i)=mean(pWacrossPp);
    % Walking window with means
    [h,pWwithinPp] = ttest(mean(myP1VL(i-4:i+5,:,:),1),mean(myP1VH(i-4:i+5,:,:),1));
    pWindW(i)=mean(pWwithinPp);
end
p= pWindW;

 scatter(find(p<0.05)-100,zeros(length(find(p<0.05)),1)-.5','k','Marker', 's');

% plot(erptimes(p1Window((myP1VH_loc_P1))),(myP1VH_P1),'*k');
% plot(erptimes(p1Window((myP1VL_loc_P1))),(myP1VL_P1),'*k');
% plot(erptimes(p1Window((myP1SH_loc_P1))),(myP1SH_P1),'*k');
% plot(erptimes(p1Window((myP1SL_loc_P1))),(myP1SL_P1),'*k');



% plot([erptimes(p1Window(1)) erptimes(p1Window(1)) ],[-3 7 ],'-.k')
% plot([erptimes(p1Window(end)) erptimes(p1Window(end))],[-3 7 ],'-.k')
% 
% plot([erptimes(n1Window(1)) erptimes(n1Window(1)) ],[-3 7 ],'-.b')
% plot([erptimes(n1Window(end)) erptimes(n1Window(end))],[-3 7 ],'-.b')


% for i = 1:NSub
%     plot(erptimes(allWindow),myP1SH(allWindow,i),'g'); % Mean
%     plot(erptimes(allWindow),myP1SL(allWindow,i),'r'); % Mean
%     pause
%     
% end
%%
% close all
figure;
hold on
area([erptimes(p1Window(1)) erptimes(p1Window(end)) erptimes(p1Window(end)) erptimes(p1Window(1))],[-3 -3 7 7],'FaceColor',[0.95 0.95 0.95],'EdgeColor',[1 1 1])
area([erptimes(n1Window(1)) erptimes(n1Window(end)) erptimes(n1Window(end)) erptimes(n1Window(1))],[-3 -3 7 7],'FaceColor',[0.85 0.85 0.85],'EdgeColor',[1 1 1])

plot(erptimes(allWindow),mean(myP1SH(allWindow,:)-myP1SL(allWindow,:),2),':b','linewidth',3); % Mean
plot(erptimes(allWindow),mean(myP1VH(allWindow,:)-myP1VL(allWindow,:),2),'c','linewidth',3); % Mean
% plot(erptimes(allWindow),mean((myP1VL(allWindow,:,:)),2),'r','linewidth',5); % Mean
% plot(erptimes(allWindow),mean((myP1VH(allWindow,:,:)),2),'g','linewidth',5); % Mean
% % plot(erptimes(p1Window((myP1VH_loc_P1))),(myP1VH_P1),'*k');
% % plot(erptimes(p1Window((myP1VL_loc_P1))),(myP1VL_P1),'*k');
% % plot(erptimes(p1Window((myP1SH_loc_P1))),(myP1SH_P1),'*k');
% % plot(erptimes(p1Window((myP1SL_loc_P1))),(myP1SL_P1),'*k');


%plot(erptimes(allWindow),mean((myP1N(allWindow,:,:)),2),':k','linewidth',3); % Mean

% plot([erptimes(p1Window(1)) erptimes(p1Window(1)) ],[-3 7 ],'-.k')
% plot([erptimes(p1Window(end)) erptimes(p1Window(end))],[-3 7 ],'-.k')
% 
% plot([erptimes(n1Window(1)) erptimes(n1Window(1)) ],[-3 7 ],'-.b')
% plot([erptimes(n1Window(end)) erptimes(n1Window(end))],[-3 7 ],'-.b')

axis([mybaseline(1) allWindow(end)+mybaseline(1) -2 2])
% for i = 1:NSub
%     plot(erptimes(allWindow),myP1SH(allWindow,i),'g'); % Mean
%     plot(erptimes(allWindow),myP1SL(allWindow,i),'r'); % Mean
%     pause
%     
% end
%%
% %% Plot  and visualize each channel
% for i=1:size(erpdata{1},2)
%     figure(i), plot(erptimes,(mean(erpdata{3}(:,i,:),3)+mean(erpdata{7}(:,i,:),3))/2,'r'), hold on
%     plot(erptimes,(mean(erpdata{4}(:,i,:),3)+mean(erpdata{8}(:,i,:),3))/2,'g'), hold on
%     xlim([-100 500])
%     title([ 'Auditory: ' mychans{i}])
%     print(['-f' num2str(i)],'-dpsc2','chans.ps','-append')
% 
%     figure(i+64), plot(erptimes,(mean(erpdata{1}(:,i,:),3)+mean(erpdata{5}(:,i,:),3))/2,'r'), hold on
%     plot(erptimes,(mean(erpdata{2}(:,i,:),3)+mean(erpdata{4}(:,i,:),3))/2,'g'), hold on
%     xlim([-100 500])
%     title([ 'Visual: ' mychans{i}])
%     print(['-f' num2str(i+64)],'-dpsc2','chans.ps','-append')
%     close all
% end
%% P1 Anova First look at visual, auditory, bimodal-same, and bimodal-opposite
HV= myVH_P1_window_mean;
LV= myVL_P1_window_mean;
HA= mySH_P1_window_mean;
LA= mySL_P1_window_mean;
anovdata_rew_modality_names = {'HV';'LV';'HA';'LA'};
anovdata_rew_modality = [HV;LV;HA;LA]';

bimodncond=length(anovdata_rew_modality_names);
subfactor=cell(bimodncond,1);

for  i=1:bimodncond
    subfactor{i}= ['F' num2str(i)];
end

t = array2table(anovdata_rew_modality,'VariableNames',subfactor);
%factorNames = {'modality','reward','latencybin','part'};
factorNames = {'modality','reward'}; % for each part


within = table({'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
    {'H';'L';'H';'L'},... %reward: high or low
    'VariableNames',factorNames);
%
% fit the repeated measures model
rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);

% run my repeated measures anova here
%[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
[ranovatbl] = ranova(rm, 'WithinModel','modality*reward')

% %% FROM AREZOO
% % Save first
% %    save('pre_beh5ECVP_0808P1');
% %% Including Pre as a factor:note that Pre data is saved in a file
% %% (pre_beh5.mat) that is loaded after data of post is calculated
% %   pre=load('pre_beh5ECVP_0808P1.mat');
% HV= myVH_P1_window_mean;
% LV= myVL_P1_window_mean;
% HA= mySH_P1_window_mean;
% LA= mySL_P1_window_mean;
% HV_pre= pre.myVH_P1_window_mean;
% LV_pre= pre.myVL_P1_window_mean;
% HA_pre= pre.mySH_P1_window_mean;
% LA_pre= pre.mySL_P1_window_mean;
% anovdata_rew_modality_names = {'HV';'LV';'HA';'LA';'HV_pre';'LV_pre';'HA_pre';'LA_pre'};
% anovdata_rew_modality = [HV;LV;HA;LA;HV_pre;LV_pre;HA_pre;LA_pre]';
% 
% bimodncond=length(anovdata_rew_modality_names);
% subfactor=cell(bimodncond,1);
% 
% for  i=1:bimodncond
%     subfactor{i}= ['F' num2str(i)];
% end
% 
% t = array2table(anovdata_rew_modality,'VariableNames',subfactor);
% %factorNames = {'modality','reward','latencybin','part'};
% factorNames = {'modality','reward','pre_post'}; % for each part
% 
% 
% within = table({'V';'V';'A';'A';'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
%     {'H';'L';'H';'L';'H';'L';'H';'L'},... %reward: high or low
%      {'Post';'Post';'Post';'Post';'Pre';'Pre';'Pre';'Pre'},... %phase: pre or post
%     'VariableNames',factorNames);
% %
% % fit the repeated measures model
% rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);
% 
% % run my repeated measures anova here
% %[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
% [ranovatbl] = ranova(rm, 'WithinModel','modality*reward*pre_post')

%% N1 Anova First look at visual, auditory, bimodal-same, and bimodal-opposite
HV= myVH_N1_window_mean;
LV= myVL_N1_window_mean;
HA= mySH_N1_window_mean;
LA= mySL_N1_window_mean;
anovdata_rew_modality_names = {'HV';'LV';'HA';'LA'};
anovdata_rew_modality = [HV;LV;HA;LA]';

bimodncond=length(anovdata_rew_modality_names);
subfactor=cell(bimodncond,1);

for  i=1:bimodncond
    subfactor{i}= ['F' num2str(i)];
end

t = array2table(anovdata_rew_modality,'VariableNames',subfactor);
%factorNames = {'modality','reward','latencybin','part'};
factorNames = {'modality','reward'}; % for each part


within = table({'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
    {'H';'L';'H';'L'},... %reward: high or low
    'VariableNames',factorNames);
%
% fit the repeated measures model
rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);

% run my repeated measures anova here
%[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
[ranovatbl] = ranova(rm, 'WithinModel','modality*reward')

% %% FROM AREZOO
% % Save first
% %    save('pre_beh5ECVP_0808P1');
% %% Including Pre as a factor:note that Pre data is saved in a file
% %% (pre_beh5.mat) that is loaded after data of post is calculated
% %   pre=load('pre_beh5ECVP_0808P1.mat');
% HV= myVH_P1_window_mean;
% LV= myVL_P1_window_mean;
% HA= mySH_P1_window_mean;
% LA= mySL_P1_window_mean;
% HV_pre= pre.myVH_P1_window_mean;
% LV_pre= pre.myVL_P1_window_mean;
% HA_pre= pre.mySH_P1_window_mean;
% LA_pre= pre.mySL_P1_window_mean;
% anovdata_rew_modality_names = {'HV';'LV';'HA';'LA';'HV_pre';'LV_pre';'HA_pre';'LA_pre'};
% anovdata_rew_modality = [HV;LV;HA;LA;HV_pre;LV_pre;HA_pre;LA_pre]';
% 
% bimodncond=length(anovdata_rew_modality_names);
% subfactor=cell(bimodncond,1);
% 
% for  i=1:bimodncond
%     subfactor{i}= ['F' num2str(i)];
% end
% 
% t = array2table(anovdata_rew_modality,'VariableNames',subfactor);
% %factorNames = {'modality','reward','latencybin','part'};
% factorNames = {'modality','reward','pre_post'}; % for each part
% 
% 
% within = table({'V';'V';'A';'A';'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
%     {'H';'L';'H';'L';'H';'L';'H';'L'},... %reward: high or low
%      {'Post';'Post';'Post';'Post';'Pre';'Pre';'Pre';'Pre'},... %phase: pre or post
%     'VariableNames',factorNames);
% %
% % fit the repeated measures model
% rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);
% 
% % run my repeated measures anova here
% %[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
% [ranovatbl] = ranova(rm, 'WithinModel','modality*reward*pre_post')

%% Lets try to plot P1 plot
% figure(3)
figure('Position', [0 0 1050 1000]);
boxplot([myVH_P1_window_mean' myVL_P1_window_mean' mySH_P1_window_mean' mySL_P1_window_mean' myN_P1_window_mean'],...
    'Labels',{'Visual High','Visual Low','Auditory High','Auditory Low','Neutral'});

color = ['k', 'b', 'r', 'b', 'r'];
h = findobj(gca,'Tag','Box');
for j=1:length(h)
   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
end

legend('boxoff');
box 'off'
ylim([-2 10])
ylabel('P1 Peak amplitude (mv)');
set(gca,'FontSize', 20)
set(gca,'YTick',-4:2:10);
hline(0,'--k')
set(gca,'color','none')
% Change the boxplot color from blue to green
% a = get(get(gca,'children'),'children');   % Get the handles of all the objects
% t = get(a,'tag');   % List the names of all the objects 
% box1 = a(6);   % The 7th object is the first box
% set(box1, 'Color', 'g');   % Set the color of the first box to green
% %% Lets try to plot Post-Pre
% figure(4)
% boxplot([myVH_P1_window_mean'-pre.myVH_P1_window_mean' myVL_P1_window_mean'-pre.myVL_P1_window_mean' mySH_P1_window_mean'-pre.mySH_P1_window_mean' mySL_P1_window_mean'-pre.mySL_P1_window_mean' myN_P1_window_mean'-pre.myN_P1_window_mean'],...
%     'Labels',{'Visual High','Visual Low','Auditory High','Auditory Low','Neutral'});
% ylim([-6 5])
% color = ['k', 'b', 'r', 'b', 'r'];
% h = findobj(gca,'Tag','Box');
% for j=1:length(h)
%    patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
% end
% 
% legend('boxoff');
% box 'off'
% ylabel('P3 Peak amplitude (mv)');
%  set(gca,'FontSize', 20)
% set(gca,'YTick',-6:2:5);
% hline(0,'--k');
% % Change the boxplot color from blue to green
% % a = get(get(gca,'children'),'children');   % Get the handles of all the objects
% % t = get(a,'tag');   % List the names of all the objects 
% % box1 = a(6);   % The 7th object is the first box
% % set(box1, 'Color', 'g');   % Set the color of the first box to green
%% Lets try to plot N1 plot
% figure(3)
figure('Position', [0 0 1050 1000]);
boxplot([myVH_N1_window_mean' myVL_N1_window_mean' mySH_N1_window_mean' mySL_N1_window_mean' myN_N1_window_mean'],...
    'Labels',{'Visual High','Visual Low','Auditory High','Auditory Low','Neutral'});

color = ['k', 'b', 'r', 'b', 'r'];
h = findobj(gca,'Tag','Box');
for j=1:length(h)
   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
end

legend('boxoff');
box 'off'
ylim([-2 10])
ylabel('P1 Peak amplitude (mv)');
set(gca,'FontSize', 20)
set(gca,'YTick',-4:2:10);
hline(0,'--k')
set(gca,'color','none')

%% Including between factors
% %%
% % pre=load('pre_beh5.mat','s','Pp');
% HV= s.subBCvH(Pp)';
% LV= s.subBCvL(Pp)';
% HA= s.subSPvH(Pp)';
% LA= s.subSPvL(Pp)';
% N= s.subNeut(Pp)';
% HV_pre= pre.s.subBCvH(Pp)';
% LV_pre= pre.s.subBCvL(Pp)';
% HA_pre= pre.s.subSPvH(Pp)';
% LA_pre= pre.s.subSPvL(Pp)';
% N_pre= pre.s.subNeut(Pp)';
% anovdata_rew_modality_names = {'HV';'LV';'HA';'LA';'HV_pre';'LV_pre';'HA_pre';'LA_pre'};
% anovdata_rew_modality = [HV;LV;HA;LA;HV_pre;LV_pre;HA_pre;LA_pre]';
% 
% bimodncond=length(anovdata_rew_modality_names);
% subfactor=cell(bimodncond,1);
% 
% for  i=1:bimodncond
%     subfactor{i}= ['F' num2str(i)];
% end
% 
% t = array2table([anovdata_rew_modality s.subRewS(Pp,1) s.subRewV(Pp,1) s.subAccu(Pp)],...
%     'VariableNames',[subfactor; {'SoundP'} ;{'BoxColor'};{'Accuracy'}]);
% %factorNames = {'modality','reward','latencybin','part'};
% factorNames = {'modality','reward','pre_post'}; % for each part
% 
% 
% within = table({'V';'V';'A';'A';'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
%     {'H';'L';'H';'L';'H';'L';'H';'L'},... %reward: high or low
%      {'Post';'Post';'Post';'Post';'Pre';'Pre';'Pre';'Pre'},... %phase: pre or post
%     'VariableNames',factorNames);
% 
% 
% % % fit the repeated measures model
% % rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);
% 
% % fit the repeated measures model
% rm = fitrm(t,['F1-F' num2str(bimodncond) '~SoundP+BoxColor+Accuracy'],'WithinDesign',within);
% 
% % run my repeated measures anova here
% %[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
% [ranovatbl] = ranova(rm, 'WithinModel','modality*reward')

%% Compare methods

delete (['sizeData.txt']);
textfile = fopen(['sizeData.txt'],'wt');
fprintf(textfile,'Pp\tV_L\tV_H\tA_L\tA_H\n');
for i = 1:NSub
 fprintf(textfile,'Pp%d\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t\n',i,myVL_P1_window_mean(i),myVH_P1_window_mean(i),mySL_P1_window_mean(i),mySH_P1_window_mean(i));
end
fclose all;
save('IndividPeaks20ms','myVL_P1_window_mean','myVH_P1_window_mean','mySL_P1_window_mean','mySH_P1_window_mean','myVL_P1_window_mean','myN_P1_window_mean');

%% TopoPlots our windows
baseline= -100;
maplim = [-6 2.5];
mytimes = [135 155]+abs(baseline);
x_lim=[-100 700];
y_lim = [-6.5 3.1];
load('chanlocs','chanloc')
sujs = 1: NSub;
erp_HA = mean((erpdata{4}(:,1:64,sujs)+erpdata{8}(:,1:64,sujs))/2,3);
erp_LA = mean((erpdata{3}(:,1:64,sujs)+erpdata{7}(:,1:64,sujs))/2,3);
erp_diff = squeeze(erp_HA-erp_LA)'; %%%% first dimensions is channels, second is ERP data points
erp_diff = squeeze(erp_LA)'; %%%% first dimensions is channels, second is ERP data points

figure(5);timtopo(erp_diff,chanloc,'limits',[x_lim y_lim],'maplimits',maplim ,'plottimes',mytimes ,'title','DEVdiff');

diff = mean(erp_diff(:,mytimes(1):mytimes(2)),2); %%% plus baseline

%% Original Arezoo topoplot
%maplim= [-1 1];
maplim = [-6 2.5];
mytimes = [400];
myconds =[1 2];
x_lim=[-100 700];
%y_lim = [-7 4];
y_lim = [-6.5 3.1];
load('chanlocs','chanloc')
erp_HA = mean((erpdata{4}(:,1:64,sujs)+erpdata{8}(:,1:64,sujs))/2,3);
erp_LA = mean((erpdata{3}(:,1:64,sujs)+erpdata{7}(:,1:64,sujs))/2,3);
%erp_HA = mean((erpdata{4}(:,1:64,sujs)+erpdata{8}(:,1:64,sujs))/2 - (my_pre.erpdata{4}(:,1:64,sujs)+my_pre.erpdata{8}(:,1:64,sujs))/2,3);
%erp_LA = mean((erpdata{3}(:,1:64,sujs)+erpdata{7}(:,1:64,sujs))/2 - (my_pre.erpdata{3}(:,1:64,sujs)+my_pre.erpdata{7}(:,1:64,sujs))/2,3);

erp_diff = squeeze(erp_HA-erp_LA)'; %%%% first dimensions is channels, second is ERP data points

%erp_diff = squeeze(erp_LA)'; %%%% first dimensions is channels, second is ERP data points
%erp_diff = squeeze(erp_LA)';
%erp_diff = squeeze(erp_HA-erp_LA)';
% figure(3);timtopo(mean(myerp{myconds(1)},3)',chanloc,'limits',[x_lim y_lim],'maplimits',maplim ,'plottimes',mytimes ,'title','High'); % high reward
% figure(4);timtopo(mean(myerp{myconds(2)},3)',chanloc,'limits',[x_lim y_lim],'maplimits',maplim ,'plottimes',mytimes ,'title','Low'); % low reward
%figure(5);timtopo(mean(erp_diff(:,mytimes),2),chanloc,'limits',[x_lim y_lim],'maplimits',maplim ,'plottimes',mytimes ,'title','DEVdiff');

%diff = mean(erp_diff(:,[ 289:310]),2); %%% plus baseline
figure(6)
%topoplot(mean(myerp{myconds(1)}(:,[250:500]),2),chanloc,'electrodes','ptslabels');
topoplot(mean(erp_diff(:,mytimes),2),chanloc,'electrodes','ptslabels');cbar('vert',0,[-5 5]);
%topoplot(diff,chanloc);cbar('vert',0,[-1 1]*max(abs(diff)));
title(['Conditioing 176ms'])
%% Sum of high and low value for visual and sound for P1 and N1
% figure(6);
figure('Position', [10 10 1000 600]);

% Take some rundom condition to calculate window for N1
% This is needed for topo pltos

[~, myN1VL_loc_N1] = min(mean(myP1VL(n1Window,:),2));
[~, myN1VH_loc_N1] = min(mean(myP1VH(n1Window,:),2));
[~, myN1SL_loc_N1] = min(mean(myP1SL(n1Window,:),2));
[~, myN1SH_loc_N1] = min(mean(myP1SH(n1Window,:),2));

myP1V_window = round(mean([myP1VL_loc_P1(1) myP1VH_loc_P1(1)]));
myP1S_window = round(mean([myP1SL_loc_P1(1) myP1SH_loc_P1(1)]));
myN1V_window = round(mean([myN1VL_loc_N1 myN1VH_loc_N1]));
myN1S_window = round(mean([myN1SL_loc_N1 myN1SH_loc_N1]));

my_P1V_window =  [p1Window(myP1V_window)-winlength/2:p1Window(myP1V_window)+winlength/2-1];
my_P1S_window =  [p1Window(myP1S_window)-winlength/2:p1Window(myP1S_window)+winlength/2-1];

my_N1V_window =  [n1Window(myN1V_window)-winlength/2:n1Window(myN1V_window)+winlength/2-1];
my_N1S_window =  [n1Window(myN1S_window)-winlength/2:n1Window(myN1S_window)+winlength/2-1];

p1n1window = [my_P1V_window; my_N1V_window; my_P1S_window;my_N1S_window];  p1n1windowI = {'P1 Vis', 'N1 Vis','P1 Snd', 'N1 Snd'};
for i = 1:4
    if i <=2, o = 0; else o = 2; end;
    mytimes = p1n1window(i,:);
%     mytimes = 260;
%     erp_HA = mean((erpdata{2+o}(:,1:64,sujs)+erpdata{6+o}(:,1:64,sujs))/2,3);
%     erp_LA = mean((erpdata{1+o}(:,1:64,sujs)+erpdata{5+o}(:,1:64,sujs))/2,3);
    
    erp_HA = (erpdata{2+o}(:,1:64,sujs)+erpdata{6+o}(:,1:64,sujs))/2;
    erp_LA = (erpdata{1+o}(:,1:64,sujs)+erpdata{5+o}(:,1:64,sujs))/2;
    erp_diff = (erp_HA+erp_LA)/2; %%%% first dimensions is channels, second is ERP data points
%   erp_HA = (erpdata{1}(:,1:64,sujs)); erp_diff = erp_HA;
%     erp_diff = squeeze(erp_HA)'; %%%% first dimensions is channels, second is ERP data points
%     diff = mean(erp_diff(:,mytimes(1):mytimes(2)),1); %%% plus baseline
    diff = mean(mean(erp_diff(mytimes,:,:),3),1); %%% plus baseline
    for elI = 1:64
        meanERP1=squeeze(mean(erpdata{2+o}(mytimes,1:64,sujs))+mean(erpdata{6+o}(mytimes,1:64,sujs)));
        meanERP2=squeeze(mean(erpdata{1+o}(mytimes,1:64,sujs))+mean(erpdata{5+o}(mytimes,1:64,sujs)));
        [~,pP1(elI)] = ttest(meanERP1(elI,:),meanERP2(elI,:));
    end
    sigEl = find(pP1<.05);
    
    subplot(2,2,i)
    topoplot(diff,chanloc,'emarker2',{sigEl,'o','r'}); % if with electrodes (diff,chanloc,'electrodes','ptslabels')
%     figure; topoplot([],EEG.chanlocs,'style','blank','electrodes','labelpoint','chaninfo',EEG.chaninfo);
    title([ p1n1windowI{i} ' ' partOfExp{partOfExpI} ' [' num2str([mytimes(1) mytimes(end)]-abs(baseline)) '] ms.'],'FontSize',16);
    caxis([-5 5]);
    if i == 2, cbar('vert',0,[-5 5]);end;
end

%topoplot(diff,chanloc);cbar('vert',0,[-1 1]*max(abs(diff)));

%% Differance of high and low value for visual and sound for P1 and N1
% figure(6);
figure('Position', [10 10 1000 600]);

% Take some rundom condition to calculate window for N1
% This is needed for topo pltos

[~, myN1VL_loc_N1] = min(mean(myP1VL(n1Window,:),2));
[~, myN1VH_loc_N1] = min(mean(myP1VH(n1Window,:),2));
[~, myN1SL_loc_N1] = min(mean(myP1SL(n1Window,:),2));
[~, myN1SH_loc_N1] = min(mean(myP1SH(n1Window,:),2));

myP1V_window = round(mean([myP1VL_loc_P1(1) myP1VH_loc_P1(1)]));
myP1S_window = round(mean([myP1SL_loc_P1(1) myP1SH_loc_P1(1)]));
myN1V_window = round(mean([myN1VL_loc_N1 myN1VH_loc_N1]));
myN1S_window = round(mean([myN1SL_loc_N1 myN1SH_loc_N1]));

my_P1V_window =  [p1Window(myP1V_window)-winlength/2:p1Window(myP1V_window)+winlength/2-1];
my_P1S_window =  [p1Window(myP1S_window)-winlength/2:p1Window(myP1S_window)+winlength/2-1];

my_N1V_window =  [n1Window(myN1V_window)-winlength/2:n1Window(myN1V_window)+winlength/2-1];
my_N1S_window =  [n1Window(myN1S_window)-winlength/2:n1Window(myN1S_window)+winlength/2-1];

p1n1window = [my_P1V_window; my_N1V_window; my_P1S_window;my_N1S_window];  p1n1windowI = {'P1 Vis', 'N1 Vis','P1 Snd', 'N1 Snd'};
for i = 1:4
    if i <=2, o = 0; else o = 2; end;
    mytimes = p1n1window(i,:);
%     mytimes = 260;
%     erp_HA = mean((erpdata{2+o}(:,1:64,sujs)+erpdata{6+o}(:,1:64,sujs))/2,3);
%     erp_LA = mean((erpdata{1+o}(:,1:64,sujs)+erpdata{5+o}(:,1:64,sujs))/2,3);
    
    erp_HA = (erpdata{2+o}(:,1:64,sujs)+erpdata{6+o}(:,1:64,sujs))/2;
    erp_LA = (erpdata{1+o}(:,1:64,sujs)+erpdata{5+o}(:,1:64,sujs))/2;
    erp_diff = (erp_HA-erp_LA)/2; %%%% first dimensions is channels, second is ERP data points
%   erp_HA = (erpdata{1}(:,1:64,sujs)); erp_diff = erp_HA;
%     erp_diff = squeeze(erp_HA)'; %%%% first dimensions is channels, second is ERP data points
%     diff = mean(erp_diff(:,mytimes(1):mytimes(2)),1); %%% plus baseline
    diff = mean(mean(erp_diff(mytimes,:,:),3),1); %%% plus baseline
    for elI = 1:64
        meanERP1=squeeze(mean(erpdata{2+o}(mytimes,1:64,sujs))+mean(erpdata{6+o}(mytimes,1:64,sujs)));
        meanERP2=squeeze(mean(erpdata{1+o}(mytimes,1:64,sujs))+mean(erpdata{5+o}(mytimes,1:64,sujs)));
        [~,pP1(elI)] = ttest(meanERP1(elI,:),meanERP2(elI,:));
    end
    sigEl = find(pP1<.05);
    
    subplot(2,2,i)
    topoplot(diff,chanloc,'emarker2',{sigEl,'o','r'}); % if with electrodes (diff,chanloc,'electrodes','ptslabels')
%     figure; topoplot([],EEG.chanlocs,'style','blank','electrodes','labelpoint','chaninfo',EEG.chaninfo);
    title([ p1n1windowI{i} ' ' partOfExp{partOfExpI} ' [' num2str([mytimes(1) mytimes(end)]-abs(baseline)) '] ms.'],'FontSize',16);
    caxis([-.25 .25]);
    if i == 2, cbar('vert',0,[-.25 .25]);end;
end

%topoplot(diff,chanloc);cbar('vert',0,[-1 1]*max(abs(diff)));

%% Plot high / Low individually P1
figure('Position', [10 10 1000 600]);
p1n1window = [my_P1V_window; my_P1V_window; my_P1S_window;my_P1S_window];  p1n1windowI = {'P1 Vis Low', 'P1 Vis High','P1 Snd Low', 'P1 Snd High'};
for i = 1:4
    if i <=2, o = 0; else o = 2; end;
    mytimes = p1n1window(i,:);
%     erp_HA = mean((erpdata{2+o}(:,1:64,sujs)+erpdata{6+o}(:,1:64,sujs))/2,3);
%     erp_LA = mean((erpdata{1+o}(:,1:64,sujs)+erpdata{5+o}(:,1:64,sujs))/2,3);
    
    erp_HA = erpdata{2+o}(:,1:64,sujs)+erpdata{6+o}(:,1:64,sujs);
    erp_LA = erpdata{1+o}(:,1:64,sujs)+erpdata{5+o}(:,1:64,sujs);
    if mod(i,2)==1, erp_diff=squeeze(erp_LA); else erp_diff=squeeze(erp_HA); end;
%     erp_diff = squeeze(erp_HA-erp_LA); %%%% first dimensions is channels, second is ERP data points
%     erp_diff = squeeze(erp_LA)'; %%%% first dimensions is channels, second is ERP data points
%     diff = mean(erp_diff(:,mytimes(1):mytimes(2)),1); %%% plus baseline
    diff = mean(mean(erp_diff(mytimes,:,:),3),1); %%% plus baseline
    for elI = 1:64
        meanERP1=squeeze(mean(erpdata{2+o}(mytimes,1:64,sujs))+mean(erpdata{6+o}(mytimes,1:64,sujs)));
        meanERP2=squeeze(mean(erpdata{1+o}(mytimes,1:64,sujs))+mean(erpdata{5+o}(mytimes,1:64,sujs)));
        [~,pP1(elI)] = ttest(meanERP1(elI,:),meanERP2(elI,:));
    end
    sigEl = find(pP1<.05);
    
    subplot(2,2,i)
    topoplot(diff,chanloc,'emarker2',{sigEl,'o','r'}); % if with electrodes (diff,chanloc,'electrodes','ptslabels')
%     figure; topoplot([],EEG.chanlocs,'style','blank','electrodes','labelpoint','chaninfo',EEG.chaninfo);
    title([ p1n1windowI{i} ' ' partOfExp{partOfExpI} ' [' num2str([mytimes(1) mytimes(end)]-abs(baseline)) '] ms.'],'FontSize',16);
    caxis([-15 15]);
    if i == 2, cbar('vert',0,[-15 15]);end;
end

%% Plot high / Low individually N1
figure('Position', [10 10 1000 600]);
p1n1window = [my_N1V_window; my_N1V_window; my_N1S_window;my_N1S_window];  p1n1windowI = {'N1 Vis Low', 'N1 Vis High','N1 Snd Low', 'N1 Snd High'};
for i = 1:4
    if i <=2, o = 0; else o = 2; end;
    mytimes = p1n1window(i,:);
%     erp_HA = mean((erpdata{2+o}(:,1:64,sujs)+erpdata{6+o}(:,1:64,sujs))/2,3);
%     erp_LA = mean((erpdata{1+o}(:,1:64,sujs)+erpdata{5+o}(:,1:64,sujs))/2,3);
    
    erp_HA = erpdata{2+o}(:,1:64,sujs)+erpdata{6+o}(:,1:64,sujs);
    erp_LA = erpdata{1+o}(:,1:64,sujs)+erpdata{5+o}(:,1:64,sujs);
    if mod(i,2)==1, erp_diff=squeeze(erp_LA); else erp_diff=squeeze(erp_HA); end;
%     erp_diff = squeeze(erp_HA-erp_LA); %%%% first dimensions is channels, second is ERP data points
%     erp_diff = squeeze(erp_LA)'; %%%% first dimensions is channels, second is ERP data points
%     diff = mean(erp_diff(:,mytimes(1):mytimes(2)),1); %%% plus baseline
    diff = mean(mean(erp_diff(mytimes,:,:),3),1); %%% plus baseline
    for elI = 1:64
        meanERP1=squeeze(mean(erpdata{2+o}(mytimes,1:64,sujs))+mean(erpdata{6+o}(mytimes,1:64,sujs)));
        meanERP2=squeeze(mean(erpdata{1+o}(mytimes,1:64,sujs))+mean(erpdata{5+o}(mytimes,1:64,sujs)));
        [~,pP1(elI)] = ttest(meanERP1(elI,:),meanERP2(elI,:));
    end
    sigEl = find(pP1<.05);
    
    subplot(2,2,i)
    topoplot(diff,chanloc,'emarker2',{sigEl,'o','r'}); % if with electrodes (diff,chanloc,'electrodes','ptslabels')
%     figure; topoplot([],EEG.chanlocs,'style','blank','electrodes','labelpoint','chaninfo',EEG.chaninfo);
    title([ p1n1windowI{i} ' ' partOfExp{partOfExpI} ' [' num2str([mytimes(1) mytimes(end)]-abs(baseline)) '] ms.'],'FontSize',16);
    caxis([-15 15]);
    if i == 2, cbar('vert',0,[-15 15]);end;
end
%% FROM EEGLAB
% 
% [STUDY ALLEEG] = pop_loadstudy('filename', 'TrPost_ECVP_0808.study', 'filepath', 'W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA_F\Studies');
% STUDY = std_makedesign(STUDY, ALLEEG, 1, 'variable1','condition','variable2','','name','STUDY.design 1','pairing1','on','pairing2','on','delfiles','off','defaultdesign','off','values1',{{'S 41' 'S 45'} {'S 42' 'S 46'} {'S 41' 'S 42' 'S 45' 'S 46'}},'subjselect',{'01' '02' '03' '04' '05' '06' '07' '08' '09' '10' '11' '12' '13' '14' '15' '16' '17' '18' '19' '20' '21' '22' '23' '24' '25' '26' '27' '28' '29' '30' '31' '32' '33' '34' '35'});
% [STUDY ALLEEG] = std_precomp(STUDY, ALLEEG, {},'interp','on','erp','on','erpparams',{'rmbase' [-100 0] });
% 
% STUDY = pop_statparams(STUDY, 'condstats','on','alpha',0.05);
% STUDY = pop_erpparams(STUDY, 'topotime',[191 220] );
% STUDY = std_erpplot(STUDY,ALLEEG,'ylim',[-5 5],'channels',{'Fp1' 'Fz' 'F3' 'F7' 'FT9' 'FC5' 'FC1' 'C3' 'T7' 'CP5' 'CP1' 'Pz' 'P3' 'P7' 'O1' 'Oz' 'O2' 'P4' 'P8' 'TP10' 'CP6' 'CP2' 'Cz' 'C4' 'T8' 'FT10' 'FC6' 'FC2' 'F4' 'F8' 'Fp2' 'AF7' 'AF3' 'AFz' 'F1' 'F5' 'FT7' 'FC3' 'C1' 'C5' 'TP7' 'CP3' 'P1' 'P5' 'PO7' 'PO3' 'POz' 'PO4' 'PO8' 'P6' 'P2' 'CPz' 'CP4' 'TP8' 'C6' 'C2' 'FC4' 'FT8' 'F6' 'AF8' 'AF4' 'F2' 'FCz' 'TP9'});
