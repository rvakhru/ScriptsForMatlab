clearvars;
clc;
close all;

%% Load data

% select EyeData_Current folder
myPath = 'W:\#Common\Projects\Behavioral\Data\Exp10_FrameSize_200718_30c708_EYE'; %uigetdir();
% specify subjects
subjects = {'19092986';'20778795';'34625722';'39638331';'54209683';...
    '56233273';'63814295';'64281890';'64513972';'66183624';...
    '66756786';'76544188';'76673293';'77119373';'771564958';...
    '80293610';'823580191';'82474313';'88285333';'97171172';...
    '722588215';'25136665';'614306382';'519247782';'586753597';...
    '671510924';'755392277';'798082253'}; %Circle Inside Task Relevant Pilot
% myPath = 'W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA_F';
% subjects = {'802228596';'522387509';'651170259';'759768504';'877132171';'320201718';'741799883';'735242692';'767328026';'466389979';'590114653';'461335192';'501531386';'118642576';'261525683';'723182163';'713680469';'881415462';'301960686';'329893397';'453998271';'562924424';'644208360';'935749766';'922050020';'89397817';'244855064';'29628872';'780035157';'692353186';'770865144';'999393855';'658780312';'836740040';'91378291';'68465542';'837262810'}; %Circle Inside Task Relevant Pilot
%% Data extraction and analysis
conds = {'PreCond','PostCond','Conditioning'};

crossData = struct;
data_subj = struct;
subject_means = struct;
figNum = 0; format shortG;

for cond = 1:length(conds)
    for sub_no = 1:length(subjects)
        
        
        %load data
        load([myPath filesep subjects{sub_no} filesep subjects{sub_no} '__allDataEyE.mat']);
        fprintf('Process: %d/%d %s ', sub_no, length(subjects), subjects{sub_no});
        timeOn = GetSecs();
        
        %save exp mode for each Pp
        reward_dist(sub_no) = inf.expMode;
        
        allBlock = [];
        for i = 2:length(block)
            %check if current trial contains eye position info
            if isfield(block(i).trials, 'pos')
                allBlock = [allBlock block(i).trials];
            end
        end
        
        % Choose Gabor
        %cond = 3; %1 = pre-cond, 2 = post-cond, 3 = conditioning
        allBlock = allBlock([allBlock.BlPrePost] == cond);
        % Remove error trials
        allBlock = allBlock([allBlock.error]==0);
        % Remove eye movements
        allBlock = allBlock(abs(zscore([allBlock.RT]))<300 & [allBlock.dist]<1);
        % Define 5 conditions
        if cond == 3
            types_5cond = {'VisualLow', 'VisualHigh', 'SoundLow', 'SoundHigh'};
            values_5cond = {[1 5] [2 6] [3 7] [4 8]}; % conditionType values
        else
            types_5cond = {'VisualLow', 'VisualHigh', 'SoundLow', 'SoundHigh', 'Neutral'};
            values_5cond = {[1 5] [2 6] [3 7] [4 8] [9 10]}; % conditionType values
        end
        
        %save mean RT for each Pp
        mean_RT(sub_no) = nanmean([allBlock.RT]);
        all_mean_RT = mean(mean_RT);
        
        % Preallocate a structure
        pupil_data = struct;
        whw =  10;           % Half width of the window to be used to eliminate values close to Nan values
        Fc  =  5;            % Cut-off frequency for filtering of pupil data
        Fs   = 1000;         % sampling rate of eye-tracker
        counts = [];
        maximal = [];
        
        % Extract pupil sizes according to the conditions
        for cond5 = 1:length(types_5cond)
            countRep = 1;
            for x = 1:length(allBlock)
                if ismember([allBlock(x).conditionType],values_5cond{cond5})
                    allBlock(x).myPos = allBlock(x).pos(allBlock(x).TARGET_t-100:end,4);
                    
                    % preprocess
                    pupil_data(countRep).(types_5cond{cond5}) = preproc_pupilOld((1:length([allBlock(x).myPos]))',...
                        allBlock(x).myPos,whw,Fc,Fs);
                    
                    % extracting data from -100 up to response
                    tmp = allBlock(x).pos(allBlock(x).TARGET_t-100:allBlock(x).TRIAL_END_t,4)';
                    
                    short_pupil(x,1:length(tmp)) = tmp;
                    my_short_pupil(countRep).(types_5cond{cond5}) = preproc_pupilOld((1:length(tmp))',...
                        short_pupil(x,1:length(tmp)),whw,Fc,Fs);
                    
                    countRep = countRep +1;
                end
            end
            fprintf('.');
            %finding the longest trial
            counts = arrayfun(@(f) numel(f.(types_5cond{cond5})), my_short_pupil);
            maximal = [maximal max(counts)];
        end
        
        max_length = max(maximal);
        
        % filling in trials with NaNs
        for cond5 = 1:length(types_5cond)
            for i = 1:length(my_short_pupil)
                if isempty(my_short_pupil(i).(types_5cond{cond5}))
                    my_short_pupil(i).(types_5cond{cond5}) = NaN(max_length,1);
                end
                if numel(my_short_pupil(i).(types_5cond{cond5}))<max_length
                    my_short_pupil(i).(types_5cond{cond5})(end:max_length) = [NaN];
                end
            end
            for i = 1:length(my_short_pupil)
                base = nanmean([my_short_pupil(i).(types_5cond{cond5})(1:100)]);
                base_my_short_pupil(i).(types_5cond{cond5}) = ([my_short_pupil(i).(types_5cond{cond5})]- base);
                percent_sig_chg(i).(types_5cond{cond5}) = base_my_short_pupil(i).(types_5cond{cond5})/nanmean([my_short_pupil(i).(types_5cond{cond5})]); %take 100ms as baseline and calculate % signal change
            end
            data_subj(sub_no).(types_5cond{cond5}) = nanmean([percent_sig_chg.(types_5cond{cond5})],2); %average for each timepoint on each trial
            subject_means(sub_no).(types_5cond{cond5}) = nanmean([data_subj(sub_no).(types_5cond{cond5})(100:round(all_mean_RT*1000))]); %average across specific timepoint to get amplitude
            %             subject_means(sub_no).(types_5cond{cond5}) = nanmean([data_subj(sub_no).(types_5cond{cond5})]); %average across timepoint to get amplitude
            crossData.(conds{cond}).data_subj(sub_no).(types_5cond{cond5}) = data_subj(sub_no).(types_5cond{cond5});
            crossData.(conds{cond}).subject_means(sub_no).(types_5cond{cond5}) = subject_means(sub_no).(types_5cond{cond5});
        end
        timeOff = GetSecs();
        fprintf('%dsec.\n', (length(subjects)*length(conds)-sub_no*cond)*2);
    end
    
    %% Plotting pupil size
    
    max_length = [];
    for cond5 = 1:length(types_5cond)
        counts = arrayfun(@(f) numel(f.(types_5cond{cond5})), data_subj);
        maximal = [maximal max(counts)];
    end
    max_length = max(maximal);
    for cond5 = 1:length(types_5cond)
        for i = 1:length(data_subj)
            if isempty(data_subj(i).(types_5cond{cond5}))
                data_subj(i).(types_5cond{cond5}) = NaN(max_length,1);
            end
            if numel(data_subj(i).(types_5cond{cond5}))<max_length
                data_subj(i).(types_5cond{cond5})(end:max_length) = [NaN];
            end
        end
    end
    
    
    %% FIGURE 1
    figNum = figNum +1;
    fig(figNum) = figure(figNum);
    set(fig(figNum), 'Position', [0 0 1024 768]);
    xlim([-100 1000]); ylim([-0.02 0.08]);
    xlabel('Time(ms)'); ylabel('% Signal Change');
    
    title(['Pupil size change during ', conds{cond},' task, n=', num2str(length(subjects))])
    xbars = [min(ylim) (all_mean_RT*1000)];
    lgd = legend;
    patch([min(ylim) (all_mean_RT*1000) (all_mean_RT*1000) min(ylim)], [xbars(1) xbars(1), xbars(2) xbars(2)], [0.85 0.85 0.85]);
    
    hold on;
    for cond5 = 1:length(types_5cond)
        time_vec = linspace(-100,length([nanmean([data_subj.(types_5cond{cond5})],2)]),length([nanmean([data_subj.(types_5cond{cond5})],2)]));
        if mod(cond5,2), myLine = '--'; else myLine = '-'; end
        if cond5 == 5, myCol = 'k'; myLine = '-'; elseif mod(2,cond5), myCol = 'r'; else myCol = 'b'; end
        %         p(cond5) = plot(time_vec,[nanmean([data_subj.(types_5cond{cond5})],2)], [myLine myCol], 'LineWidth',2,'DisplayName',(types_5cond{cond5}));
    end
    if cond == 3
        [l,p] = boundedline(time_vec, [nanmean([data_subj.(types_5cond{1})],2)]', nanstd([data_subj.(types_5cond{1})]')./...
            sqrt(size([data_subj.(types_5cond{1})],2)) , '--b','linewidth', 2,...
            time_vec, [nanmean([data_subj.(types_5cond{2})],2)]', nanstd([data_subj.(types_5cond{2})]')./...
            sqrt(size([data_subj.(types_5cond{2})],2)) , '-b',...
            time_vec, [nanmean([data_subj.(types_5cond{3})],2)]', nanstd([data_subj.(types_5cond{3})]')./...
            sqrt(size([data_subj.(types_5cond{3})],2)) , '--r',...
            time_vec, [nanmean([data_subj.(types_5cond{4})],2)]', nanstd([data_subj.(types_5cond{4})]')./...
            sqrt(size([data_subj.(types_5cond{4})],2)) , '-r');
    else
        [l,p] = boundedline(time_vec, [nanmean([data_subj.(types_5cond{5})],2)]', nanstd([data_subj.(types_5cond{5})]')./...
            sqrt(size([data_subj.(types_5cond{5})],2)) , '-k',...
            time_vec, [nanmean([data_subj.(types_5cond{1})],2)]', nanstd([data_subj.(types_5cond{1})]')./...
            sqrt(size([data_subj.(types_5cond{1})],2)) , '--b','linewidth', 2,...
            time_vec, [nanmean([data_subj.(types_5cond{2})],2)]', nanstd([data_subj.(types_5cond{2})]')./...
            sqrt(size([data_subj.(types_5cond{2})],2)) , '-b',...
            time_vec, [nanmean([data_subj.(types_5cond{3})],2)]', nanstd([data_subj.(types_5cond{3})]')./...
            sqrt(size([data_subj.(types_5cond{3})],2)) , '--r',...
            time_vec, [nanmean([data_subj.(types_5cond{4})],2)]', nanstd([data_subj.(types_5cond{4})]')./...
            sqrt(size([data_subj.(types_5cond{4})],2)) , '-r');
    end
    %   outlinebounds(l,p);
    if cond == 3
        legend([l(1:end)],{'Visual Low','Visual High','Audio Low','Audio High'}, 'location', 'northwest');
    else
        legend([l(1:end)],{'Neutral','Visual Low','Visual High','Audio Low','Audio High'}, 'location', 'northwest');
    end
    vline(250, 'r--', 'target offset');
    vline(0, 'k--', 'target onset');
    vline((all_mean_RT*1000),'b--','RT');
    set(gca,'FontSize',16);
    
    %% FIGURE 2: REWARD EFFECTS
    figNum = figNum +1;
    fig(figNum) = figure(figNum);
    set(fig(figNum), 'Position', [0 0 1024 768]);
    xlim([-100 1000]);ylim([-0.005 0.01]);
    title(['Reward effect for each modality, n=', num2str(length(subjects))]);
    
    
    if cond == 3
        visual = ([nanmean([data_subj.(types_5cond{2})],2)]'-[nanmean([data_subj.(types_5cond{1})],2)]');
        auditory = ([nanmean([data_subj.(types_5cond{4})],2)]'-[nanmean([data_subj.(types_5cond{3})],2)]');
        visual_err = ((nanstd([data_subj.(types_5cond{2})]')-nanstd([data_subj.(types_5cond{1})]'))./sqrt(length(subjects)));
        auditory_err = (nanstd([data_subj.(types_5cond{4})]')-nanstd([data_subj.(types_5cond{3})]')./sqrt(length(subjects)));
        
    else
        visual = ([nanmean([data_subj.(types_5cond{2})],2)]'-[nanmean([data_subj.(types_5cond{5})],2)]')...
            -([nanmean([data_subj.(types_5cond{1})],2)]'-[nanmean([data_subj.(types_5cond{5})],2)]');
        auditory = ([nanmean([data_subj.(types_5cond{4})],2)]'-[nanmean([data_subj.(types_5cond{5})],2)]')...
            -([nanmean([data_subj.(types_5cond{3})],2)]'-[nanmean([data_subj.(types_5cond{5})],2)]');
        visual_err = nanstd([data_subj.(types_5cond{2})]-[data_subj.(types_5cond{1})],0,2)./sqrt(length(subjects));
        auditory_err = nanstd([data_subj.(types_5cond{4})]-[data_subj.(types_5cond{3})],0,2)./sqrt(length(subjects));


    end
    
    xbars = [min(ylim) (all_mean_RT*1000)];
    lgd = legend;
    patch([min(ylim) (all_mean_RT*1000) (all_mean_RT*1000) min(ylim)], [xbars(1) xbars(1), xbars(2) xbars(2)], [0.95 0.95 0.95]);
    [e,r] = boundedline(time_vec,visual,visual_err,'-b','linewidth', 2,...
        time_vec,auditory,auditory_err,'-r');
    outlinebounds(e,r);
            hold on         
        plot(time_vec,nanmean([data_subj.(types_5cond{4})]-[data_subj.(types_5cond{3})],2) ,'b'), 
    
        plot(time_vec,nanmean([data_subj.(types_5cond{4})]-[data_subj.(types_5cond{3})],2) + ...
           nanstd([data_subj.(types_5cond{4})]-[data_subj.(types_5cond{3})],0,2)./sqrt(length(subjects)),'--k')
       plot(time_vec,nanmean([data_subj.(types_5cond{4})]-[data_subj.(types_5cond{3})],2) - ...
           nanstd([data_subj.(types_5cond{4})]-[data_subj.(types_5cond{3})],0,2)./sqrt(length(subjects)),'--g')
    legend([e(1:end)],{'Visual','Auditory'}, 'location', 'northeast');
    xlabel('Time(ms)');
    ylabel('% Signal Change');
    vline(250, 'r--', 'target offset');
    vline(0, 'k--', 'target onset');
    vline((all_mean_RT*1000),'b--','RT');
    set(gca,'FontSize',16);
    if cond ~= 3,hline(0,'k-','Neutral Baseline'); end
    
    
    
    %% FIGURE 4: BAR PLOT
    figNum = figNum +1;
    fig(figNum) = figure(figNum);
    set(fig(figNum), 'Position', [0 0 1024 768]);
    if cond == 3
        subject_matrix = [nanmean([subject_means.VisualLow]) nanmean([subject_means.VisualHigh]);...
            nanmean([subject_means.SoundLow]) nanmean([subject_means.SoundHigh])];
        
        subject_matrix_std = [nanstd([subject_means.VisualLow])./size([subject_means.VisualLow],2)...
            nanstd([subject_means.VisualHigh])./size([subject_means.VisualHigh],2);...
            nanstd([subject_means.SoundLow])./size([subject_means.SoundLow],2)...
            nanstd([subject_means.SoundHigh])./size([subject_means.SoundHigh],2);];
        figure
        b = bar(subject_matrix);
        set(gca,'Xtick',1:2,'xticklabel',{'Visual','Auditory'});
        legend([b(1),b(2)],{'LowValue','HighValue'},'location', 'northeast');
        set(gca,'FontSize',16); b(1).FaceColor = [1 0 0]; b(2).FaceColor = [0 0 1]; hold on;
        errorbar([0.86  1.14  1.86 2.14],[subject_matrix(1,:) subject_matrix(2,:)],...
            [subject_matrix_std(1,:) subject_matrix_std(2,:)], 'Color','k', 'LineStyle', 'none');
    else
        %             subject_matrix = [NaN nanmean([subject_means.VisualLow]) NaN nanmean([subject_means.VisualHigh]) NaN;...
        %             NaN nanmean([subject_means.SoundLow]) NaN nanmean([subject_means.SoundHigh]) NaN; NaN NaN nanmean([subject_means.Neutral]) NaN NaN];
        subject_matrix = [nanmean([subject_means.VisualLow]) NaN nanmean([subject_means.VisualHigh]);...
            nanmean([subject_means.SoundLow]) NaN nanmean([subject_means.SoundHigh]); NaN nanmean([subject_means.Neutral]) NaN ];
        
        
        subject_matrix_std = [ nanstd([subject_means.VisualLow])./size([subject_means.VisualLow],2) NaN...
            nanstd([subject_means.VisualHigh])./size([subject_means.VisualHigh],2);...
            nanstd([subject_means.SoundLow])./size([subject_means.SoundLow],2) NaN...
            nanstd([subject_means.SoundHigh])./size([subject_means.SoundHigh],2);...
            NaN nanstd([subject_means.Neutral])./size([subject_means.Neutral],2) NaN];
        
%         subject_matrix = crossData.PostCond.subject_matrix- crossData.PreCond.subject_matrix;
%         subject_matrix_std = crossData.PostCond.subject_matrix_std- crossData.PreCond.subject_matrix_std;
        b = bar(subject_matrix);
        set(gca,'Xtick',1:3,'xticklabel',{'Visual','Auditory','Neutral'});
        legend([b(3),b(1),b(2)],{'HighValue','LowValue','Neutral'},'location', 'northeast');
        set(gca,'FontSize',16); b(1).FaceColor = [1 0 0]; b(2).FaceColor = [0 0 0]; b(3).FaceColor = [0 0 1]; hold on;
        errorbar([0.78  1  1.22    1.78  2  2.22    2.78 3   3.4000],[subject_matrix(1,:) subject_matrix(2,:) subject_matrix(3,:)],...
            [subject_matrix_std(1,:) subject_matrix_std(2,:) subject_matrix_std(3,:)], 'Color','k', 'LineStyle', 'none');
        
    end
    crossData.(conds{cond}).subject_matrix = subject_matrix;
    crossData.(conds{cond}).subject_matrix_std = subject_matrix_std;
    
%     ylim([0.01 0.02])
    title(['Pupil size changes from target onset to mean RT of ', conds{cond},', n=',num2str(length(subjects))])
    
    %% PREPARE: Statistics
    
    % 1. Create separate arrays of HV, LV, HA, LA means. Join them into one
    % array and create a separate cell array with their names.
    HV = [subject_means.VisualHigh];
    LV = [subject_means.VisualLow];
    HA = [subject_means.SoundHigh];
    LA = [subject_means.SoundLow];
    
    % for Differance
%     HV = [crossData.PostCond.subject_means.VisualHigh- crossData.PreCond.subject_means.VisualHigh];
%     LV = [crossData.PostCond.subject_means.VisualLow- crossData.PreCond.subject_means.VisualLow];
%     HA = [crossData.PostCond.subject_means.SoundHigh- crossData.PreCond.subject_means.SoundHigh];
%     LA = [crossData.PostCond.subject_means.SoundLow- crossData.PreCond.subject_means.SoundLow];

    modalities_numbers = [HV; LV; HA; LA]';
    modalities_names = {'HV'; 'LV'; 'HA'; 'LA'};
    
    % 2. Create a table from a cell array nx1, where n is amount of modalities.
    % Remember to assign names (F1-F4) to 4 possible conditions.
    conditions_number = length(modalities_names);
    modalities_array = cell(conditions_number, 1);
    for i=1:conditions_number
        modalities_array{i} = ['F' num2str(i)];
    end
    % F1 - HV, F2 - LV, F3 - HA, F4 - LA
    % why not  modalities_array{i} = [modalities_names];
    
    modalities_table = array2table(modalities_numbers, ...
        'VariableNames', modalities_array);
    
    % 3. Create a table with within-subject factors: modality and reward. It
    % should consist of 2 cell arrays and variable names are also a cell array.
    within = table({'V';'V';'A';'A'}, {'H';'L';'H';'L'},... %preserve the order
        'VariableNames', {'modality', 'reward'});
    
    % 4. Fit the repeated measures model. Concatenate the model itself
    % as names of factors, their numbers and between-subject factors.
    % Specify within-subject design.
    rm = fitrm(modalities_table, ...
        'F1-F4~1', 'WithinDesign', within);
    
    % 5.  Run repeated measures anova for both within-subj factors.
    
    ranovatbl = ranova(rm, 'WithinModel','modality*reward');
    tbl = multcompare(rm, 'modality', 'By', 'reward');
    
    %% FIGURE 5: Comparing High vs Low Reward in time
    figNum = figNum +1;
    fig(figNum) = figure(figNum);
    set(fig(figNum), 'Position', [0 0 1024 768]);
    
    % FIGURE 3: BOXPLOT
    subplot(2,2,1);
    if cond == 3
        subject_matrix = [subject_means.VisualLow; subject_means.VisualHigh;...
            subject_means.SoundLow; subject_means.SoundHigh]';
    else
        subject_matrix = [subject_means.VisualLow; subject_means.VisualHigh;...
            subject_means.SoundLow; subject_means.SoundHigh; subject_means.Neutral]';
    end
    boxplot(subject_matrix, types_5cond,'whisker', 1);
    ylim([-0.06 0.1]);
    title(['Pupil size changes from target onset to mean RT of ', conds{cond},', n=',num2str(length(subjects))]);
    
    % for both modalities
    reward_means_subj = struct;
    for sub_no = 1:length(subjects)
        reward_means_subj(sub_no).High = nanmean([data_subj(sub_no).VisualHigh data_subj(sub_no).SoundHigh],2);
        reward_means_subj(sub_no).Low = nanmean([data_subj(sub_no).VisualLow data_subj(sub_no).SoundLow],2);
    end
    reward_means.High = nanmean([reward_means_subj.High],2);
    reward_means.Low = nanmean([reward_means_subj.Low],2);
    
    % plotting pupil size change
    subplot(2,2,2);
    plot(time_vec,[reward_means.High], 'LineWidth',2);
    hold on;
    plot(time_vec,[reward_means.Low],'LineWidth',2);
    xlim([-100 1500]);
    ylim([-0.02 0.05]);
    vline(0, 'k--', 'target onset');
    legend('High','Low', 'location', 'southeast');
    xlabel('Time, ms');
    ylabel('% Signal Change');
    title(['Pupil size change for low and high value, n=', num2str(length(subjects))])
    
    % boxplot
    subplot(2,2,3);
    %boxplot_reward_current = figure;
    boxplot([reward_means.High(1:1500) reward_means.Low(1:1500)], {'High', 'Low'});
    title(['Pupil size change for low and high value, n=', num2str(length(subjects))]);
    
    % plotting difference Low vs High pupil size change
    subplot(2,2,4);
    plot([reward_means.Low] - [reward_means.High])
    xlim([0 1500]);
    title(['Difference Low vs High, n=', num2str(length(subjects))]);
    vline(100, 'r--', 'target onset');
    xlabel('Time, ms');
    
    % stats
    
    % paired ttests
    [h, p] = ttest([reward_means_subj.High], [reward_means_subj.Low],'Dim',2);
    figure;
    plot(p);
    xlim([0 500]);
    ylim([0 0.1]);
    title(['Timing of significant difference for low vs high, n=', num2str(length(subjects))])
    hline(0.05, 'r--', '    p-value 0.05');
    
    
    % anova + multiple comparisons
    reward_table = [[reward_means.High(1:1500)]; [reward_means.Low(1:1500)]];
    reward_groups = cell(1,length(reward_table));
    reward_groups(1:1500) = {'HighValue'};
    reward_groups(1500:end) = {'LowValue'};
    reshape(reward_groups, length(reward_groups),1);
    [p_anova,tbl,stats] = anovan(reward_table,{reward_groups});
    comparisons_table = multcompare(stats);
end
