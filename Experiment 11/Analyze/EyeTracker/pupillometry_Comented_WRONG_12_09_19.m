% PUPILLOMETRY IS PROCESSING EYE DATA AND CALCULATING STATISTICS ON IT
%
% This script takes block structure that is already includes eye data and pehavioral responses
% The script extracts this data into one big block and later extracts
% and separates pupilsize data into a structure with desired conditions.
%
% STRUCTURE:
%
% 1. LOAD DATA
% 2. DATA EXTRACTION AND ANALYSIS
% 3. EXTRACT DATA FROM 'allDataEyE.mat'
% 4. EXCITING PART: Extract pupil sizes according to the conditions
% 5. BASELINE and NORMALIZE DATA
% 6. PLOT THE DATA
%
% Vakhrushev, Antono 12.09.2019

clearvars; clc; close all; close all hidden;

slope_calc = 0;
TRPL = 0;

%% 1. LOAD DATA

% Pilot Data: Specify subjects
% myPath = 'W:\#Common\Projects\Antono\Exp11_EEG_EYE_Circle\DATA\CircleInside_TaskRelevant\Pilot\Data'; %uigetdir();
% subjects = {'51339677','74892169','174636117','308371706','533440903','651170259','770883157'}; %Circle Inside Task Relevant Pilot

% Pp used in U4 Workshop
% subjects ={'51339677','74892169','174636117','308371706','533440903','651170259','770883157','2308149','29628872','244855064','89397817','68220649'%,'997506577','935749766','274642591'}; %%%% should be out headphone wrong '635418447'
% subjects ={'51339677','74892169','174636117','308371706','533440903','651170259','770883157','2308149','29628872','244855064','89397817','68220649','997506577','274642591','460365483','691885551','361836210','723249755','759768504','935749766'}; %pending:'935749766' %%%% should be out headphone wrong '635418447'

if TRPL
    % JESSICA'S SUBJECTS
    myPath= 'W:\#Common\Projects\Antono\Exp11_EEG_EYE_Circle\DATA\CircleInside_TaskRelevant\Data';
    subjects = { '51339677','74892169','174636117','308371706','533440903','651170259','770883157','2308149','29628872',...
        '244855064','89397817','68220649','997506577','274642591','460365483','361836210','723249755','759768504',...
        '935749766','431826010','198895673','326983557','894073080','836370445','894073080','466420224','30063240',...
        '404159757','304361840','893517963','996801295','146624172','883218578','553187045','848451010','824648861'};%,...
    %'594360327'};
else
    % ROMAN'S SUBJECTS
    myPath = 'W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA_F';
    subjects ={'320201718','522387509','651170259','741799883','759768504',...
        '802228596','877132171','735242692','767328026','461335192',...
        '466389979','118642576','261525683','723182163','713680469',...
        '881415462','301960686','329893397','453998271','562924424',... % one before last
        '644208360','922050020', '935749766','89397817','244855064',...
        '29628872','692353186' ,'780035157','770865144','999393855',... % two before last
        '658780312','836740040','91378291','68465542','837262810'}; %%%% wrong reference electrode ,'331717863'
end

%% 2. DATA EXTRACTION AND ANALYSIS

conds = {'Pre-cond','Post-cond','Conditioning'};
zcrit1 = 2.5;        % but also see: https://www.nature.com/articles/s41598-018-34252-7
% https://core.ac.uk/download/pdf/85215435.pdf
% http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.454.3890&rep=rep1&type=pdf
% http://d-scholarship.pitt.edu/7948/1/Seo.pdf

zcrit2 = 0.9%*58/38;  % Mistake in computation of PPD
if ~TRPL zcrit2=0.9; end

whw =  10;           % Half width of the window to be used to eliminate values close to Nan values
Fc  =  5;            % Cut-off frequency for filtering of pupil data
Fs   = 1000;         % sampling rate of eye-tracker

%% 2.1 Setting parameters to be analyed, if prev_xyz = 0, it will run default (per conditions: pre- / post-/ cond-)
prev_highreward = 0;
n1_trl_val = 0; %1: low reward; 2: high reward; 0: neutral

prev_acc = 0;
n1_trl_acc = 0;

prev_cue = 0; %this will take previous trial same type of cue and same value

tStatistics = struct;
data_subj = struct; %structure to store timecourse of pupil for plotting
subject_means = struct; %structure to store values for ANOVA calculation
figNum = 0; format shortG;

%% 3. EXTRACT DATA FROM 'allDataEyE.mat'

for condPrePost = 2%:length(conds) % Uncomment if we need all conditions at once
    % Note: 1 = pre-cond, 2 = post-cond, 3 = conditioning
    for sub_no = 1:length(subjects)
        
        %% 3.1 Extract Data
        %load data
        load([myPath filesep subjects{sub_no} filesep subjects{sub_no} '__allDataEyE.mat']);
        fprintf('\nProcess: %d/%d %s ', sub_no, length(subjects), subjects{sub_no});
        
        %save exp mode for each Pp
        reward_dist(sub_no) = inf.expMode;
        
        %pre-allocation
        allBlock = [];      % Preallocate big block
        vecRT_zcrit   = []; % Preallocate vector for RT
        
        if sub_no == 19 && TRPL% for Pp 935749766, bcs he has only 1 error trial at block 37 which causes the error in this part I assume
            for i = 2:length(block)-1
                if isfield(block(i).trials, 'pos') %check if current trial contains eye position info
                    allBlock = [allBlock block(i).trials];
                end
                vecRT_zcrit = [vecRT_zcrit block(i).trials(([block(i).trials.BlPrePost] == 1 | [block(i).trials.BlPrePost] == 2)).RT]; % Flag trials with Gabor task
            end
        else
            for i = 2:length(block)
                if isfield(block(i).trials, 'pos')          % check if current trial contains eye position info
                    allBlock = [allBlock block(i).trials];  % Combile trails from current block with the big block
                end
                vecRT_zcrit = [vecRT_zcrit block(i).trials(([block(i).trials.BlPrePost] == 1 | [block(i).trials.BlPrePost] == 2)).RT];% Flag trials with Gabor task
                
                % To check number of trials per condition run this:
                % sum([allBlock.error] == 0); % sum([allBlock.error] == 0& [allBlock.condition] == 2 & [allBlock.StimValue] == 1)
            end
        end
        
        %% 3.2 Filter Data
        
        allBlock = allBlock([allBlock.BlPrePost] == condPrePost); % Only select trials in specific blocks
        
        % Remove error trials
        allBlock = allBlock([allBlock.error]==0);
        
        % Only include trials without error:
        % pre-cond (160tr/32c),
        % post-cond(320tr/64c),
        % conditioning1(144tr/36c),
        % conditioning2(128tr/32c - last 2 blocks)
        
        % Remove eye movements
        allBlock = allBlock(abs(zscore([allBlock.RT]))<zcrit1  & [allBlock.dist]<zcrit2);
        n_trials(sub_no) = length(allBlock);
        
        % Check It: Determine outliers based on pre and post:
        % allBlock = allBlock(( [allBlock.RT]>= nanmean(vecRT_zcrit)- zcrit1*nanstd(vecRT_zcrit) &...
        % [allBlock.RT]<nanmean(vecRT_zcrit)+zcrit1*nanstd(vecRT_zcrit)) & [allBlock.dist]<zcrit2);
        
        %% 3.3 Define Our conditions (5)
        if condPrePost == 3
            types_5cond = {'VisualLow', 'VisualHigh', 'SoundLow', 'SoundHigh'};
            values_5cond = {[1 5] [2 6] [3 7] [4 8]}; % conditionType values
        else
            types_5cond = {'VisualLow', 'VisualHigh', 'SoundLow', 'SoundHigh', 'Neutral'};
            values_5cond = {[1 5] [2 6] [3 7] [4 8] [9 10]}; % conditionType values
        end
        % To check 1: Match Eyetracker/trigger/Condition/Value/Side for
        % each line. This is for visualisation
        %for i = 1:length(allBlock)
        %    xxx=strsplit((allBlock(i).Bl_Tr_Cn_Rp_Go_m));
        %    fprintf('CondT: %d/%s/%d/%d/%d/%d\n',i,xxx{4},[allBlock(i).conditionType],[allBlock(i).condition],[allBlock(i).StimValue],[allBlock(i).GL]);
        %end
        
        % To check 2: Find distortions between trigger number and our
        % condition selection
        %beh = ([allBlock.StimValue]==1 & [allBlock.condition] == 1); % Low value visual condition
        %trigBeh =  ismember([allBlock.conditionType],[1 5]);         % Low value visual Trigger
        %sum(beh ~= trigBeh) % correct if 0!
        
        %% 4. EXCITING PART: Extract pupil sizes according to the conditions
        counts = [];            % Give a vector of the number datapoints at each trial (EyeTracker)
        maximal = [];           % Find the longest trial for each condition
        my_short_pupil = [];    % Structure to store trials in rows and conditions in columns with time-series data of EyeTracker (Pupil)
        
        %% 4.2 Go over trials and condition and extract data: -100 to Button press
        for cond5 = 1:length(types_5cond)                                   %for each condition...
            countRep = 1; tmpLong = [];
            for trial = 1:length(allBlock)                                  %for each trial...
                if ismember([allBlock(trial).conditionType],values_5cond{cond5})%check whether the current trl being looked at matches with specific condition
                    
                    tmp = []; %empty the tmp EACH ITERATION! IMPORTANT! OR data will mix between conditions
                    
                    % 1. Extracting data from -100 up to response
                    % 1.1 To analyze previous correct/incorrect trial OR
                    % low/high reward OR both combined
                    if prev_acc %prev_highreward %&& prev_acc
                        if  trial ~= 1  && allBlock(trial-1).Accuracy == n1_trl_acc  %allBlock(x-1).condition == 2 % allBlock(x-1).StimValue == n1_trl_val
                            tmp(1:length(allBlock(trial).pos(allBlock(trial).TARGET_t-100:allBlock(trial).BUTTON_PRESSED_t,4)')) = allBlock(trial).pos(allBlock(trial).TARGET_t-100:allBlock(trial).BUTTON_PRESSED_t,4)';
                            my_short_pupil(countRep).(types_5cond{cond5}) = preproc_pupilOld((1:length(tmp))',...
                                tmp,whw,Fc,Fs);
                        end
                        % 1.2 To analyze previous visual or auditory cue
                    elseif prev_cue
                        if  trial ~= 1  && allBlock(trial-1).condition == allBlock(trial).condition && allBlock(trial-1).StimValue == allBlock(trial).StimValue %1 preceded by visual, 2 preceded by auditory, x to check priming
                            tmp(1:length(allBlock(trial).pos(allBlock(trial).TARGET_t-100:allBlock(trial).BUTTON_PRESSED_t,4)')) = allBlock(trial).pos(allBlock(trial).TARGET_t-100:allBlock(trial).BUTTON_PRESSED_t,4)';
                            my_short_pupil(countRep).(types_5cond{cond5}) = preproc_pupilOld((1:length(tmp))',...
                                tmp,whw,Fc,Fs);
                        end
                        % 1.3 Take each trial just as it is
                    else
                        tmp(1:length(allBlock(trial).TARGET_t-100:allBlock(trial).TRIAL_END_t)) = allBlock(trial).pos(allBlock(trial).TARGET_t-100:allBlock(trial).TRIAL_END_t,4)'; % allBlock(x).BUTTON_PRESSED_t
                        my_short_pupil(countRep).(types_5cond{cond5}) = preproc_pupilOld((1:length(tmp))',...
                            tmp,whw,Fc,Fs);
                        %To check: whether raw data matches with
                        %interpolated pupil data
                        %close all; figure;hold on; plot(my_short_pupil(countRep).(types_5cond{cond5}),'r','linewidth',3);plot(tmp);
                        %pause(0.1);
                        tmpLong(1:length(allBlock(trial).TARGET_t-100:allBlock(trial).TRIAL_END_t),countRep) = tmp;
                    end
                end
                %                 if zscore([my_short_pupil.(types_5cond{cond5})])  end
                %                 countRep = countRep +1;
            end
            counts = arrayfun(@(f) numel(f.(types_5cond{cond5})), my_short_pupil); % Give a vector of the number datapoints at each trial (EyeTracker)
            maximal = [maximal max([counts])];                              % Find the longest trial for each condition
            %             figure(); hold on;
            %             for i=1:length(my_short_pupil),plot([my_short_pupil(i).(types_5cond{cond5})],'k','linewidth',2);end, plot(tmpLong,'m');
            fprintf('.');
        end
        
        %%%%%%to understand (Arezoo)
        %         for i=1:length(my_short_pupil)
        %             mysize(i,1) = length(my_short_pupil(i). VisualLow);
        %             mysize(i,2) = length(my_short_pupil(i). VisualHigh);
        %             mysize(i,3) = length(my_short_pupil(i). SoundLow);
        %             mysize(i,4) = length(my_short_pupil(i). SoundHigh);
        %             mysize(i,5) = length(my_short_pupil(i). Neutral);
        %         end
        
        %% 4.3 Calculate average RT values for each participant
        max_length = max(maximal);      % Look for the longest trial across all condition for each participant
        min_RT(sub_no) = nanmin([allBlock.RT]);
        mean_RT(sub_no) = nanmean([allBlock.RT]);
        
        %% 5. BASELINE and NORMALIZE DATA
        base_my_short_pupil = [];       % Variable to store baseline of pupil (100ms before target onset)
        percent_sig_chg = [];           % Varibale to store normalized pupil (i.e. (signal length - base)/base)
        
        % Filling in trials with NaNs
        for cond5 = 1:length(types_5cond)
            for i = 1:length(my_short_pupil)
                
                % If trial does not match current condition, fill with NaNs
                if isempty(my_short_pupil(i).(types_5cond{cond5}))
                    my_short_pupil(i).(types_5cond{cond5}) = NaN(max_length,1);
                end
                
                % If trial is shorter than the longest trial, fill the rest
                % of the datapoints with NaNs
                if numel(my_short_pupil(i).(types_5cond{cond5})) < max_length
                    my_short_pupil(i).(types_5cond{cond5})(end+1:max_length) = NaN;
                end
                
                base = nanmean([my_short_pupil(i).(types_5cond{cond5})(1:100)]);                                    % Take the baseline: mean of 100ms datapoints
                base_my_short_pupil(i).(types_5cond{cond5}) = ([my_short_pupil(i).(types_5cond{cond5})]- base);     % Substract the whole signal with the baseline
                
                % To check: compare signal before baseline and baselined
                %                 figure; plot([base_my_short_pupil(i).(types_5cond{cond5})]); hold on;  plot([my_short_pupil(i).(types_5cond{cond5})]);
                
                percent_sig_chg(i).(types_5cond{cond5}) = base_my_short_pupil(i).(types_5cond{cond5})./base; %take 100ms as baseline and calculate % signal change
                
            end
            
            data_subj(sub_no).(types_5cond{cond5}) = nanmean([percent_sig_chg.(types_5cond{cond5})],2).*100;                                    % Average for each timepoint on each trial, for the sake of beautiful plot
            subject_means(sub_no).(types_5cond{cond5}) = nanmean([data_subj(sub_no).(types_5cond{cond5})(100:floor(mean_RT(sub_no)*1000))]);    % Only consider data from target onset to RT for each participant
            tStatistics.(types_5cond{cond5})(sub_no,:) = [data_subj(sub_no).(types_5cond{cond5})(1:1000)]';    % Only consider data from target onset to RT for each participant
        end
        
        rejTrl(sub_no) = 0;
        for i = length(base_my_short_pupil):-1:1
            if abs(base_my_short_pupil(i).(types_5cond{cond5})(100)) > 200;
                base_my_short_pupil(i) = [];
                rejTrl(sub_no) = rejTrl(sub_no) +1;
            end
        end
        %         figure; plot([base_my_short_pupil(:).(types_5cond{2})(100)]); %hold on;  plot([my_short_pupil.(types_5cond{cond5})]);
    end
    
    all_mean_RT = mean(mean_RT); % calculate average RT between all Pp (For plotting)
    
    
    %% 6. PLOT THE DATA
    
    max_length = [];
    for cond5 = 1:length(types_5cond)
        counts = arrayfun(@(f) numel(f.(types_5cond{cond5})), data_subj);
        maximal = [maximal max(counts)];
    end
    max_length = max(maximal);
    for cond5 = 1:length(types_5cond)
        for i = 1:length(data_subj)
            if isempty(data_subj(i).(types_5cond{cond5}))
                data_subj(i).(types_5cond{cond5}) = NaN(max_length,1);
            end
            if numel(data_subj(i).(types_5cond{cond5}))<max_length
                data_subj(i).(types_5cond{cond5})(end:max_length) = [NaN];
            end
        end
    end
    
    
    %% FIGURE 1
    figNum = figNum +1;
    fig(figNum) = figure(figNum);
    set(fig(figNum), 'Position', [0 0 1024 768]);
    %     xlim([-100 1000]); ylim([-0.02 0.1]);
    xlim([-100 1000]); ylim([-1 6]);
    xlabel('Time(ms)'); ylabel('% Signal Change');
    
    mean_n_trials = nanmean(n_trials);
    
    %     title(['Pupil size change during ', conds{cond},' task, n=', num2str(length(subjects))])
    %     xbars = [min(ylim) (all_mean_RT*1000)];
    xbars = [min(ylim) mean(mean_RT*1000)];
    %     lgd = legend;
    patch([min(ylim) mean(mean_RT*1000) mean(mean_RT*1000) min(ylim)], [xbars(1) xbars(1), xbars(2) xbars(2)], [0.85 0.85 0.85]);
    
    hold on;
    for cond5 = 1:length(types_5cond)
        time_vec = linspace(-100,length([nanmean([data_subj.(types_5cond{cond5})],2)]),length([nanmean([data_subj.(types_5cond{cond5})],2)]));
        if mod(cond5,2), myLine = '--'; else myLine = '-'; end
        if cond5 == 5, myCol = 'k'; myLine = '-'; elseif mod(2,cond5), myCol = 'r'; else myCol = 'b'; end
    end
    if condPrePost == 3
        [l,p] = boundedline(time_vec, [nanmean([data_subj.(types_5cond{1})],2)]', nanstd([data_subj.(types_5cond{1})]')./...
            sqrt(size([data_subj.(types_5cond{1})],2)) , '--b','linewidth', 2,...
            time_vec, [nanmean([data_subj.(types_5cond{2})],2)]', nanstd([data_subj.(types_5cond{2})]')./...
            sqrt(size([data_subj.(types_5cond{2})],2)) , '-b',...
            time_vec, [nanmean([data_subj.(types_5cond{3})],2)]', nanstd([data_subj.(types_5cond{3})]')./...
            sqrt(size([data_subj.(types_5cond{3})],2)) , '--r',...
            time_vec, [nanmean([data_subj.(types_5cond{4})],2)]', nanstd([data_subj.(types_5cond{4})]')./...
            sqrt(size([data_subj.(types_5cond{4})],2)) , '-r');
    else
        
        [l,p] = boundedline(time_vec, [nanmean([data_subj.(types_5cond{5})],2)]', nanstd([data_subj.(types_5cond{5})]')./...
            sqrt(size([data_subj.(types_5cond{5})],2)) , '-k');
        [l,p] = boundedline(time_vec, [nanmean([data_subj.(types_5cond{1})],2)]', nanstd([data_subj.(types_5cond{1})]')./...
            sqrt(size([data_subj.(types_5cond{1})],2)) , '--b','linewidth', 2,...
            time_vec, [nanmean([data_subj.(types_5cond{2})],2)]', nanstd([data_subj.(types_5cond{2})]')./...
            sqrt(size([data_subj.(types_5cond{2})],2)) , '-b',...
            time_vec, [nanmean([data_subj.(types_5cond{3})],2)]', nanstd([data_subj.(types_5cond{3})]')./...
            sqrt(size([data_subj.(types_5cond{3})],2)) , '--r',...
            time_vec, [nanmean([data_subj.(types_5cond{4})],2)]', nanstd([data_subj.(types_5cond{4})]')./...
            sqrt(size([data_subj.(types_5cond{4})],2)) , '-r');
    end
    %    outlinebounds(l,p);
    if condPrePost == 3
        %         legend([l(1:end)],{'Visual Low','Visual High','Audio Low','Audio High'}, 'location', 'northwest');
    else
        %         legend([l(1:end)],{'Neutral','Visual Low','Visual High','Audio Low','Audio High'}, 'location', 'northwest');
    end
    vline(250, 'r--', 'target offset');
    vline(0, 'k--', 'target onset');
    vline(mean(mean_RT*1000),'b--','RT');
    set(gca,'FontSize',16);
    
    %     % Plot statistics
    %     for i =5:size([tStatistics.(types_5cond{1})],2)-5
    %         % Walking window using means
    %         [~,pWwithinPpVis(i)] = ttest(nanmean(tStatistics.(types_5cond{1})(:,i-4:i+5),2)', nanmean(tStatistics.(types_5cond{2})(:,i-4:i+5),2)');
    %     end
    %     for i =5:size([tStatistics.(types_5cond{3})],2)-5
    %         % Walking window using means
    %         [~,pWwithinPpSnd(i)] = ttest(nanmean(tStatistics.(types_5cond{3})(:,i-4:i+5),2)', nanmean(tStatistics.(types_5cond{4})(:,i-4:i+5),2)');
    %     end
    %     scatter(find(pWwithinPpVis<0.05)-100,zeros(length(find(pWwithinPpVis<0.05)),1),'r','Marker', 's');
    %     scatter(find(pWwithinPpSnd<0.05)-100,zeros(length(find(pWwithinPpVis<0.05)),1),'k','Marker', 's');
    
    %% SLOPE CALCULATION
    
    if slope_calc
        figNum = 10;
        
        figNum = figNum +1;
        fig(figNum) = figure(figNum);
        set(fig(figNum), 'Position', [0 0 1024 768]);
        set(gca,'FontSize',24)
        xlim([-100 1000]); ylim([-6 6]);
        
        if TRPL
            pre_data = load('pupil_data_n36_Pre-cond_AllTrial.mat');
            post_data = load('pupil_data_n36_Post-cond_AllTrial.mat');
            cond_data = load('pupil_data_n36_Conditioning_AllTrial.mat');
        elseif ~TRPL
            pre_data = load('pupil_data_n35_Pre-cond_AllTrial.mat');
            post_data = load('pupil_data_n35_Post-cond_AllTrial.mat');
            cond_data = load ('pupil_data_n35_Conditioning_AllTrial.mat');
        end
        
        %% Calculating Slopes and Intercepts for each Pre- Post- or Cond for each Pp
        all_mean_RT = (mean(pre_data.mean_RT)+mean(post_data.mean_RT))/2; %mean RT of pre- and post-cond only
        length_sig_est = 100:floor(all_mean_RT*1000);
        
        all_four_subj = []; all_four_cond = []; pre_ref_neut = []; post_ref_neut = [];
        % Collapse pupil time-course of four conditions per Pp
        for i = 1:length(pre_data.data_subj)
            
            all_four_cond(:,1) = pre_data.data_subj(i).VisualLow;
            all_four_cond(:,2) = pre_data.data_subj(i).VisualHigh;
            all_four_cond(:,3) = pre_data.data_subj(i).SoundLow;
            all_four_cond(:,4) = pre_data.data_subj(i).SoundHigh;
            
            all_four_subj(:,i) = nanmean(all_four_cond,2);
            pre_ref_neut(:,i) = pre_data.data_subj(i).Neutral;
            
            pre_time_vec_subj = linspace(-100,length([nanmean([pre_data.data_subj.VisualLow],2)]),length([nanmean([pre_data.data_subj.VisualLow],2)]));
            pre_linearCoeff_subj(i,:) = polyfit(pre_time_vec_subj(length_sig_est)', all_four_subj(length_sig_est,i),1); %estimate line from target onset to RT
            
            pre_neut_linearCoeff_subj(i,:) = polyfit(pre_time_vec_subj(length_sig_est)', pre_ref_neut(length_sig_est,i),1); %estimate line from target onset to RT
        end
        
        all_four_subj = []; all_four_cond = [];
        
        for i = 1:length(post_data.data_subj)
            
            all_four_cond(:,1) = post_data.data_subj(i).VisualLow;
            all_four_cond(:,2) = post_data.data_subj(i).VisualHigh;
            all_four_cond(:,3) = post_data.data_subj(i).SoundLow;
            all_four_cond(:,4) = post_data.data_subj(i).SoundHigh;
            
            all_four_subj(:,i) = nanmean(all_four_cond,2);
            post_ref_neut(:,i) = post_data.data_subj(i).Neutral;
            
            post_time_vec_subj = linspace(-100,length([nanmean([post_data.data_subj.VisualLow],2)]),length([nanmean([post_data.data_subj.VisualLow],2)]));
            post_linearCoeff_subj(i,:) = polyfit(post_time_vec_subj(length_sig_est)', all_four_subj(length_sig_est,i), 1);
            
            post_neut_linearCoeff_subj(i,:) = polyfit(post_time_vec_subj(length_sig_est)', post_ref_neut(length_sig_est,i),1); %estimate line from target onset to RT
        end
        
        all_four_subj = []; all_four_cond = [];
        %         all_mean_RT = mean(cond_data.mean_RT); %mean RT of conditioning only
        length_sig_est = 100:floor(all_mean_RT*1000);
        %%%%%%%%% RT for cond put here
        for i = 1:length(cond_data.data_subj)
            
            all_four_cond(:,1) = cond_data.data_subj(i).VisualLow;
            all_four_cond(:,2) = cond_data.data_subj(i).VisualHigh;
            all_four_cond(:,3) = cond_data.data_subj(i).SoundLow;
            all_four_cond(:,4) = cond_data.data_subj(i).SoundHigh;
            
            all_four_subj(:,i) = nanmean(all_four_cond,2);
            
            cond_time_vec_subj = linspace(-100,length([nanmean([cond_data.data_subj.VisualLow],2)]),length([nanmean([cond_data.data_subj.VisualLow],2)]));
            cond_linearCoeff_subj(i,:) = polyfit(cond_time_vec_subj(length_sig_est)', all_four_subj(length_sig_est,i), 1);
        end
        
        % Compare pre and post slope+intercept
        [h slope_pVal] = ttest(pre_linearCoeff_subj(:,1),post_linearCoeff_subj(:,1));
        [h1 intercept_pVal] = ttest(pre_linearCoeff_subj(:,2),post_linearCoeff_subj(:,2));
        
        % Compare neutral pre and post
        [h2 neut_slope_pVal] = ttest(pre_neut_linearCoeff_subj(:,1),post_neut_linearCoeff_subj(:,1));
        [h3 neut_intercept_pVal] = ttest(pre_neut_linearCoeff_subj(:,2),post_neut_linearCoeff_subj(:,2));
        
        % Compare conditioning and post
        [h4 cond_slope_pVal] = ttest(cond_linearCoeff_subj(:,1),post_neut_linearCoeff_subj(:,1));
        [h5 cond_intercept_pVal] = ttest(cond_linearCoeff_subj(:,2),post_neut_linearCoeff_subj(:,2));
        
        
        % Collapse pupil time-course of four conditions (Neutral is
        % separated as reference)
        
        pupil_pre_rew = nanmean([nanmean([pre_data.data_subj.VisualLow],2) nanmean([pre_data.data_subj.VisualHigh],2) nanmean([pre_data.data_subj.SoundLow],2) nanmean([pre_data.data_subj.SoundHigh],2)],2);
        pupil_post_rew = nanmean([nanmean([post_data.data_subj.VisualLow],2) nanmean([post_data.data_subj.VisualHigh],2) nanmean([post_data.data_subj.SoundLow],2) nanmean([post_data.data_subj.SoundHigh],2)],2);
        pupil_cond_rew = nanmean([nanmean([cond_data.data_subj.VisualLow],2) nanmean([cond_data.data_subj.VisualHigh],2) nanmean([cond_data.data_subj.SoundLow],2) nanmean([cond_data.data_subj.SoundHigh],2)],2);
        
        pre_time_vec = linspace(-100,length([nanmean([pre_data.data_subj.VisualLow],2)]),length([nanmean([pre_data.data_subj.VisualLow],2)]));
        post_time_vec = linspace(-100,length([nanmean([post_data.data_subj.VisualLow],2)]),length([nanmean([post_data.data_subj.VisualLow],2)]));
        cond_time_vec = linspace(-100,length([nanmean([cond_data.data_subj.VisualLow],2)]),length([nanmean([cond_data.data_subj.VisualLow],2)]));
        
        %         all_mean_RT = (pre_data.all_mean_RT+post_data.all_mean_RT)/2; %mean RT of pre- and post-cond only
        xbars = [min(ylim) (all_mean_RT*1000)];
        patch([min(ylim) (all_mean_RT*1000) (all_mean_RT*1000) min(ylim)], [xbars(1) xbars(1), xbars(2) xbars(2)], [0.85 0.85 0.85]);
        hold on
        
        plot(pre_time_vec,pupil_pre_rew,'Color',[0.4940 0.1840 0.5560]+0.3,'LineWidth',5)
        hold on
        plot(post_time_vec,pupil_post_rew,'Color',[0.4940 0.1840 0.5560],'LineWidth',5)
        %         plot(cond_time_vec,pupil_cond_rew,'k--','LineWidth',2)
        xlim([-100 1000])
        
        length_sig_est = 100:floor(all_mean_RT*1000);
        
        pre_linearCoeff = polyfit(pre_time_vec(length_sig_est)', pupil_pre_rew(length_sig_est), 1); %estimate line from target onset to RT
        post_linearCoeff = polyfit(post_time_vec(length_sig_est)', pupil_post_rew(length_sig_est), 1);
        cond_linearCoeff = polyfit(cond_time_vec(100:floor(cond_data.all_mean_RT*1000))', pupil_cond_rew(100:floor(cond_data.all_mean_RT*1000)), 1);
        % The x coefficient, slope, is coefficients(1).
        % The constant, the intercept, is coefficients(2).
        % Make fit.  It does NOT need to have the same
        % number of elements as your training set,
        % or the same range, though it could if you want.
        
        %%%%%%%%% 1. Re-check linear fit in a primitive way
        b1 = pre_time_vec(length_sig_est)'\pupil_pre_rew(length_sig_est) %slope pre
        b2 = post_time_vec(length_sig_est)'\pupil_post_rew(length_sig_est) %slope post
        
        X = [ones(length(pre_time_vec(length_sig_est)'),1) pre_time_vec(length_sig_est)'];
        b = X\pupil_pre_rew(length_sig_est) %Intercept + Slope
        
        %%%%%%%%% 2. Re-check linear fit with fitlm
        mdl1 = fitlm(pre_time_vec(length_sig_est),pupil_pre_rew(length_sig_est))
        mdl2 = fitlm(post_time_vec(length_sig_est),pupil_post_rew(length_sig_est))
        
        % Get the estimated values with polyval()
        xFit_pre = linspace(-100,length([nanmean([pre_data.data_subj.VisualLow],2)]),length([nanmean([pre_data.data_subj.VisualLow],2)]));
        xFit_post = linspace(-100,length([nanmean([post_data.data_subj.VisualLow],2)]),length([nanmean([post_data.data_subj.VisualLow],2)]));
        %         xFit_cond = linspace(-100,length([nanmean([cond_data.data_subj.VisualLow],2)]),length([nanmean([cond_data.data_subj.VisualLow],2)]));
        
        %%%% Roman to the rescue to check the regression line above
        %         x_fit = 1:length([pre_data.data_subj.VisualLow]);
        %         y = pre_linearCoeff(1)*x_fit+pre_linearCoeff(2);
        %
        %         y = post_linearCoeff(1)*x_fit+post_linearCoeff(2);
        %         plot(x_fit,y);
        
        %%%% Plotting the regression line
        yFit_pre = polyval(pre_linearCoeff, pre_time_vec);
        yFit_post = polyval(post_linearCoeff, post_time_vec);
        %         yFit_cond = polyval(cond_linearCoeff, cond_time_vec);
        
        % Plot the fit
        hold on;
        plot(xFit_pre, yFit_pre, 'Color',[0.4940 0.1840 0.5560]+0.3,'LineStyle','--','LineWidth', 3);
        plot(xFit_post, yFit_post, 'Color',[0.4940 0.1840 0.5560],'LineStyle','--','LineWidth',3);
        %         plot(xFit_cond, yFit_cond, 'k', 'LineWidth',3);
        
        vline(250, 'r--', 'target offset');
        vline(0, 'k--', 'target onset');
        vline((all_mean_RT*1000),'b--','RT');
        xlabel('Time (ms)');
        ylabel('% Signal Change of pupil diameter');
        % Comparing two regression lines
        %         regstats(pupil_pre_rew(length_sig_est),pupil_post_rew(length_sig_est),'linear')
        
    end
    
    
    %% FIGURE 2: REWARD EFFECTS
    figNum = figNum +1;
    fig(figNum) = figure(figNum);
    set(fig(figNum), 'Position', [0 0 1024 768]);
    xlim([-100 1000]);ylim([-0.02 0.01]);
    title(['Reward effect for each modality, n=', num2str(length(subjects))]);
    
    
    if condPrePost == 3
        visual = ([nanmean([data_subj.(types_5cond{2})],2)]'-[nanmean([data_subj.(types_5cond{1})],2)]');
        auditory = ([nanmean([data_subj.(types_5cond{4})],2)]'-[nanmean([data_subj.(types_5cond{3})],2)]');
        visual_err = ((nanstd([data_subj.(types_5cond{2})]')-nanstd([data_subj.(types_5cond{1})]'))./sqrt(length(subjects)));
        auditory_err = (nanstd([data_subj.(types_5cond{4})]')-nanstd([data_subj.(types_5cond{3})]')./sqrt(length(subjects)));
        
    else
        visual = ([nanmean([data_subj.(types_5cond{2})],2)]'-[nanmean([data_subj.(types_5cond{5})],2)]')...
            -([nanmean([data_subj.(types_5cond{1})],2)]'-[nanmean([data_subj.(types_5cond{5})],2)]');
        auditory = ([nanmean([data_subj.(types_5cond{4})],2)]'-[nanmean([data_subj.(types_5cond{5})],2)]')...
            -([nanmean([data_subj.(types_5cond{3})],2)]'-[nanmean([data_subj.(types_5cond{5})],2)]');
        visual_err = nanstd([data_subj.(types_5cond{2})]-[data_subj.(types_5cond{1})],0,2)./sqrt(length(subjects));
        auditory_err = nanstd([data_subj.(types_5cond{4})]-[data_subj.(types_5cond{3})],0,2)./sqrt(length(subjects));
    end
    
    xbars = [min(ylim) (all_mean_RT*1000)];
    lgd = legend;
    patch([min(ylim) (all_mean_RT*1000) (all_mean_RT*1000) min(ylim)], [xbars(1) xbars(1), xbars(2) xbars(2)], [0.95 0.95 0.95]);
    [e,r] = boundedline(time_vec,visual,visual_err,'-b','linewidth', 2,...
        time_vec,auditory,auditory_err,'-r');
    % outlinebounds(e,r);
    legend([e(1:end)],{'Visual','Auditory'}, 'location', 'northeast');
    xlabel('Time(ms)');
    ylabel('% Signal Change');
    vline(250, 'r--', 'target offset');
    vline(0, 'k--', 'target onset');
    vline((all_mean_RT*1000),'b--','RT');
    set(gca,'FontSize',16);
    if condPrePost ~= 3,hline(0,'k-','Neutral Baseline'); end
    
    
    
    %% FIGURE 4: BAR PLOT
    % figNum = figNum +1;
    % fig(figNum) = figure(figNum);
    % set(fig(figNum), 'Position', [0 0 1024 768]);
    %
    % % subject_means = subject_means_RT;
    %
    % if cond == 3
    %     subject_matrix = [nanmean([subject_means.VisualLow]) nanmean([subject_means.VisualHigh]);...
    %         nanmean([subject_means.SoundLow]) nanmean([subject_means.SoundHigh])];
    %
    %     subject_matrix_std = [nanstd([subject_means.VisualLow],0,2)./sqrt(size([subject_means.VisualLow],2))...
    %         nanstd([subject_means.VisualHigh],0,2)./sqrt(size([subject_means.VisualHigh],2));...
    %         nanstd([subject_means.SoundLow],0,2)./sqrt(size([subject_means.SoundLow],2))...
    %         nanstd([subject_means.SoundHigh],0,2)./sqrt(size([subject_means.SoundHigh],2))];
    %     figure
    %     b = bar(subject_matrix);
    %     set(gca,'Xtick',1:2,'xticklabel',{'Visual','Auditory'});
    %     legend([b(1),b(2)],{'LowValue','HighValue'},'location', 'northeast');
    %     set(gca,'FontSize',16); b(1).FaceColor = [1 0 0]; b(2).FaceColor = [0 0 1]; hold on;
    %     errorbar([0.86  1.14  1.86 2.14],[subject_matrix(1,:) subject_matrix(2,:)],...
    %         [subject_matrix_std(1,:) subject_matrix_std(2,:)], 'Color','k', 'LineStyle', 'none');
    % else
    %     %             subject_matrix = [NaN nanmean([subject_means.VisualLow]) NaN nanmean([subject_means.VisualHigh]) NaN;...
    %     %             NaN nanmean([subject_means.SoundLow]) NaN nanmean([subject_means.SoundHigh]) NaN; NaN NaN nanmean([subject_means.Neutral]) NaN NaN];
    %     subject_matrix = [nanmean([subject_means.VisualLow]) NaN nanmean([subject_means.VisualHigh]);...
    %         nanmean([subject_means.SoundLow]) NaN nanmean([subject_means.SoundHigh]); NaN nanmean([subject_means.Neutral]) NaN ];
    %
    %
    %     subject_matrix_std = [ nanstd([subject_means.VisualLow],0,2)./sqrt(size([subject_means.VisualLow],2)) NaN...
    %         nanstd([subject_means.VisualHigh],0,2)./sqrt(size([subject_means.VisualHigh],2));...
    %         nanstd([subject_means.SoundLow],0,2)./sqrt(size([subject_means.SoundLow],2)) NaN...
    %         nanstd([subject_means.SoundHigh],0,2)./sqrt(size([subject_means.SoundHigh],2));...
    %         NaN nanstd([subject_means.Neutral],0,2)./sqrt(size([subject_means.Neutral],2)) NaN];
    %
    %     b = bar(subject_matrix);
    %     set(gca,'Xtick',1:3,'xticklabel',{'Visual','Auditory','Neutral'});
    %     legend([b(3),b(1),b(2)],{'HighValue','LowValue','Neutral'},'location', 'northeast');
    %     set(gca,'FontSize',16); b(1).FaceColor = [1 0 0]; b(2).FaceColor = [0 0 0]; b(3).FaceColor = [0 0 1]; hold on;
    %     errorbar([0.78  1  1.22    1.78  2  2.22    2.78 3   3.4000],[subject_matrix(1,:) subject_matrix(2,:) subject_matrix(3,:)],...
    %         [subject_matrix_std(1,:) subject_matrix_std(2,:) subject_matrix_std(3,:)], 'Color','k', 'LineStyle', 'none');
    %
    % end
    %
    % ylim([0 0.05])
    % title(['Pupil size changes from target onset to mean RT of ', conds{cond},', n=',num2str(length(subjects))])
    
    %% figure BOX a BETTER one
    figNum = figNum +1;
    fig(figNum) = figure(figNum);
    set(fig(figNum), 'Position', [0 0 1024 768]);
    
    if condPrePost == 3
        b(1) = bar([1 3 4 6 7],[0 0 0 0 0],'w','linewidth',2), axis square, hold on
        b(2) = bar([1 3 4 6 7],[0 nanmean([subject_means.VisualHigh]) 0 0 0],'w','linewidth',2), axis square, hold on
        b(3) = bar([1 3 4 6 7],[0 0 nanmean([subject_means.VisualLow]) 0 0],'w','linewidth',2), axis square, hold on
        b(4) = bar([1 3 4 6 7],[0 0 0 nanmean([subject_means.SoundHigh]) 0],'w','linewidth',2), axis square, hold on
        b(5) = bar([1 3 4 6 7],[0 0 0 0 nanmean([subject_means.SoundLow])],'w','linewidth',2), axis square, hold on
        
        errorbar([1 3 4 6 7],[NaN nanmean([subject_means.VisualHigh])...
            nanmean([subject_means.VisualLow]) nanmean([subject_means.SoundHigh])  nanmean([subject_means.SoundLow])],...
            [NaN nanstd([subject_means.VisualHigh],0,2)...
            nanstd([subject_means.VisualLow],0,2) nanstd([subject_means.SoundHigh],0,2)  nanstd([subject_means.SoundLow],0,2)]...
            ./sqrt(length(subject_means)),'k','linewidth',3, 'linestyle','none')
        ax = gca;
        ax.XTickLabel = {'','','','',''};
        b(1).FaceColor = [0.3 0.3 0.3]; %Neut
        b(2).FaceColor = [0 0 0.5]; b(2).EdgeColor = [0 0 1]; %VH
        b(3).FaceColor = [1 1 1];   b(3).EdgeColor = [0 0 1]; %VL
        b(4).FaceColor = [0.5 0 0]; b(4).EdgeColor = [1 0 0]; %AH
        b(5).FaceColor = [1 1 1];   b(5).EdgeColor = [1 0 0]; %AL
        ylabel('% Signal Change' , 'FontSize', 14);
        
    else
        b(1) = bar([1 3 4 6 7],[nanmean([subject_means.Neutral]) 0 0 0 0],'w','linewidth',2), axis square, hold on
        b(2) = bar([1 3 4 6 7],[0 nanmean([subject_means.VisualHigh]) 0 0 0],'w','linewidth',2), axis square, hold on
        b(3) = bar([1 3 4 6 7],[0 0 nanmean([subject_means.VisualLow]) 0 0],'w','linewidth',2), axis square, hold on
        b(4) = bar([1 3 4 6 7],[0 0 0 nanmean([subject_means.SoundHigh]) 0],'w','linewidth',2), axis square, hold on
        b(5) = bar([1 3 4 6 7],[0 0 0 0 nanmean([subject_means.SoundLow])],'w','linewidth',2), axis square, hold on
        
        errorbar([1 3 4 6 7],[nanmean([subject_means.Neutral]) nanmean([subject_means.VisualHigh])...
            nanmean([subject_means.VisualLow]) nanmean([subject_means.SoundHigh])  nanmean([subject_means.SoundLow])],...
            [nanstd([subject_means.Neutral],0,2) nanstd([subject_means.VisualHigh],0,2)...
            nanstd([subject_means.VisualLow],0,2) nanstd([subject_means.SoundHigh],0,2)  nanstd([subject_means.SoundLow],0,2)]...
            ./sqrt(length(subject_means)),'k','linewidth',3, 'linestyle','none')
        
        ax = gca;
        ax.XTickLabel = {'','','','',''};
        b(1).FaceColor = [0.3 0.3 0.3]; %Neut
        b(2).FaceColor = [0 0 0.5]; b(2).EdgeColor = [0 0 1]; %VH
        b(3).FaceColor = [1 1 1];   b(3).EdgeColor = [0 0 1]; %VL
        b(4).FaceColor = [0.5 0 0]; b(4).EdgeColor = [1 0 0]; %AH
        b(5).FaceColor = [1 1 1];   b(5).EdgeColor = [1 0 0]; %AL
        ylabel('% Signal Change' , 'FontSize', 14);
    end
    % ylim([-.01 0.05])
    ax.YLim = [-1 6];
    title(['Pupil size changes from target onset to mean RT of ', conds{condPrePost},', n=',num2str(length(subjects))]);
    
    %% PREPARE: Statistics
    
    % 1. Create separate arrays of HV, LV, HA, LA means. Join them into one
    % array and create a separate cell array with their names.
    HV = [subject_means.VisualHigh];
    LV = [subject_means.VisualLow];
    HA = [subject_means.SoundHigh];
    LA = [subject_means.SoundLow];
    modalities_numbers = [HV; LV; HA; LA]';
    modalities_names = {'HV'; 'LV'; 'HA'; 'LA'};
    
    % 2. Create a table from a cell array nx1, where n is amount of modalities.
    % Remember to assign names (F1-F4) to 4 possible conditions.
    conditions_number = length(modalities_names);
    modalities_array = cell(conditions_number, 1);
    for i=1:conditions_number
        modalities_array{i} = ['F' num2str(i)];
    end
    % F1 - HV, F2 - LV, F3 - HA, F4 - LA
    % why not  modalities_array{i} = [modalities_names];
    
    modalities_table = array2table(modalities_numbers, ...
        'VariableNames', modalities_array);
    
    % 3. Create a table with within-subject factors: modality and reward. It
    % should consist of 2 cell arrays and variable names are also a cell array.
    within = table({'V';'V';'A';'A'}, {'H';'L';'H';'L'},... %preserve the order
        'VariableNames', {'modality', 'reward'});
    
    % 4. Fit the repeated measures model. Concatenate the model itself
    % as names of factors, their numbers and between-subject factors.
    % Specify within-subject design.
    rm = fitrm(modalities_table, ...
        'F1-F4~1', 'WithinDesign', within);
    
    % 5.  Run repeated measures anova for both within-subj factors.
    
    ranovatbl = ranova(rm, 'WithinModel','modality*reward');
    tbl = multcompare(rm, 'modality', 'By', 'reward');
    
    %% FIGURE 5: Comparing High vs Low Reward in time
    figNum = figNum +1;
    fig(figNum) = figure(figNum);
    set(fig(figNum), 'Position', [0 0 1024 768]);
    
    % FIGURE 3: BOXPLOT
    subplot(2,2,1);
    if condPrePost == 3
        subject_matrix = [subject_means.VisualLow; subject_means.VisualHigh;...
            subject_means.SoundLow; subject_means.SoundHigh]';
    else
        subject_matrix = [subject_means.VisualLow; subject_means.VisualHigh;...
            subject_means.SoundLow; subject_means.SoundHigh; subject_means.Neutral]';
    end
    boxplot(subject_matrix, types_5cond,'whisker', 1);
    ylim([-0.06 0.1]);
    title(['Pupil size changes from target onset to mean RT of ', conds{condPrePost},', n=',num2str(length(subjects))]);
    windStart = 100;
    % for both modalities
    reward_means_subj = struct;
    for sub_no = 1:length(subjects)
        reward_means_subj(sub_no).High = nanmean([data_subj(sub_no).VisualHigh(windStart:round(all_mean_RT*1000)) data_subj(sub_no).SoundHigh(windStart:round(all_mean_RT*1000))],2);
        reward_means_subj(sub_no).Low = nanmean([data_subj(sub_no).VisualLow(windStart:round(all_mean_RT*1000)) data_subj(sub_no).SoundLow(windStart:round(all_mean_RT*1000))],2);
        reward_means_subj(sub_no).VH = nanmean([data_subj(sub_no).VisualHigh(windStart:round(all_mean_RT*1000))],2);
        reward_means_subj(sub_no).VL = nanmean([data_subj(sub_no).VisualLow(windStart:round(all_mean_RT*1000))],2);
        reward_means_subj(sub_no).AH = nanmean([data_subj(sub_no).SoundHigh(windStart:round(all_mean_RT*1000))],2);
        reward_means_subj(sub_no).AL = nanmean([data_subj(sub_no).SoundLow(windStart:round(all_mean_RT*1000))],2);
    end
    reward_means.High = nanmean([reward_means_subj.High],2);
    reward_means.Low = nanmean([reward_means_subj.Low],2);
    reward_means.VH = nanmean([reward_means_subj.VH],2);
    reward_means.VL = nanmean([reward_means_subj.VL],2);
    reward_means.AH = nanmean([reward_means_subj.AH],2);
    reward_means.AL = nanmean([reward_means_subj.AL],2);
    
    % plotting pupil size change
    subplot(2,2,2);
    plot([reward_means.High], 'LineWidth',2);
    hold on;
    plot([reward_means.Low],'LineWidth',2);
    xlim([-100 1500]);
    ylim([-0.02 0.05]);
    vline(0, 'k--', 'target onset');
    legend('High','Low', 'location', 'southeast');
    xlabel('Time, ms');
    ylabel('% Signal Change');
    title(['Pupil size change for low and high value, n=', num2str(length(subjects))])
    
    % boxplot
    subplot(2,2,3);
    %boxplot_reward_current = figure;
    boxplot([reward_means.High(1:end) reward_means.Low(1:end)], {'High', 'Low'});
    title(['Pupil size change for low and high value, n=', num2str(length(subjects))]);
    
    % plotting difference Low vs High pupil size change
    subplot(2,2,4);
    plot([reward_means.Low] - [reward_means.High])
    xlim([0 1500]);
    title(['Difference Low vs High, n=', num2str(length(subjects))]);
    vline(100, 'r--', 'target onset');
    xlabel('Time, ms');
    
    % stats
    
    % paired ttests
    [h, p] = ttest([reward_means_subj.High], [reward_means_subj.Low],'Dim',2); %main effect of reward
    
    [h2,p2]= ttest([subject_means.VisualHigh], [subject_means.VisualLow]); %reward effect VH VL
    [h3,p3]= ttest([subject_means.SoundHigh], [subject_means.SoundLow]); %reward effect AH AL
    [h4,p4]= ttest([subject_means.VisualHigh], [subject_means.SoundHigh],'Dim',2); %reward effect VH AH
    [h5,p5]= ttest([subject_means.VisualLow], [subject_means.SoundLow],'Dim',2); %reward effect AH AL
    
    ttest_result.VH_VL = nanmean(p2);
    ttest_result.AH_AL = nanmean(p3);
    ttest_result.VH_AH = nanmean(p4);
    ttest_result.VL_AL = nanmean(p5);
    
    figure;
    plot(p);
    xlim([0 500]);
    ylim([0 0.1]);
    title(['Timing of significant difference for low vs high, n=', num2str(length(subjects))])
    hline(0.05, 'r--', '    p-value 0.05');
    
    
    % anova + multiple comparisons
    reward_table = [[reward_means.High(1:.5*(length(reward_means.High)))]; [reward_means.Low(1:.5*(length(reward_means.Low)))]];
    reward_groups = cell(1,length(reward_table));
    reward_groups(1:.5*(length(reward_groups))) = {'HighValue'};
    reward_groups(.5*(length(reward_groups))+1:end) = {'LowValue'};
    reshape(reward_groups, length(reward_groups),1);
    [p_anova,tbl,stats] = anovan(reward_table,{reward_groups});
    comparisons_table = multcompare(stats);
    
    %% SAVING FILE
    if prev_highreward == 1 && n1_trl_val == 1
        trl_type = 'prevLow';
    elseif prev_highreward == 1 && n1_trl_val == 2
        trl_type = 'prevHigh';
    elseif prev_highreward == 1 && n1_trl_val == 0
        trl_type = 'prevNeut';
    elseif prev_highreward == 0
        trl_type = 'AllTrial';
    end
    
    if prev_acc == 0
        trl_type_acc = '';
    elseif prev_acc == 1 && n1_trl_acc == 1
        trl_type_acc = 'prevCorr';
    elseif prev_acc == 1 && n1_trl_acc == 0
        trl_type_acc = 'prevIncorr';
    end
    
    save(['pupil_data_n',num2str(length(subjects)),'_',conds{condPrePost},'_',trl_type,trl_type_acc,'.mat'], 'data_subj','subject_means', 'ranovatbl','ttest_result','prev_highreward','n1_trl_val','n1_trl_acc','mean_RT','all_mean_RT');
    display(ranovatbl); display(ttest_result);
end