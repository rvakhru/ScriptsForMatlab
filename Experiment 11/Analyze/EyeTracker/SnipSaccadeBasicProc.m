close all
clearvars
PrePost               = menu('Pre or Post?', {'Pre','Post'});
start_path = fullfile('C:\');
myPath = uigetdir(start_path);
cd(myPath);
SubFold = cd;
% filePattern = fullfile(SubFold, '*.*');
folders = dir;
if 2 == exist('allData.mat','file')
    load('allData'); % problem here
    doneFileInx = length(doneFileEYE)+1;
else
    allData = [];
    doneFileInx = 1;
    doneFileEYE = struct;
    doneFileEYE.Pp = [];
end
for fls = 3:length(folders) % for each folder (Participant)
    if folders(fls).isdir && ~any(strcmp({doneFileEYE.Pp}, folders(fls).name)) && all(~strcmp(folders(fls).name,{'99999999','Studies'})) %&& ~isnan(str2double(folders(fls).name)) % It must be new folder
        inFolder = [SubFold filesep folders(fls).name];
        load([inFolder filesep strcat(folders(fls).name,'_', '_allDataEyE','.mat')]); % load BLOCKS
        bloPost = struct; bloPost = block(2).trials;
        for blo = 3:length(block)
            if ismember(block(blo).trials(1).BlPrePost,[1,2]), bloPost = [bloPost block(blo).trials]; end
        end
        if isfield(bloPost,'badRT'),bloPost = rmfield(bloPost,'badRT'); end
        if isfield(bloPost,'PUPIL_DATA_TYPE_m'),bloPost = rmfield(bloPost,'PUPIL_DATA_TYPE_t'); bloPost = rmfield(bloPost,'PUPIL_DATA_TYPE_m');end
        if 1 == exist('allData','var'), allData = [allData bloPost]; else allData = bloPost; end % STRUC with all Pp
        doneFileEYE(doneFileInx).Pp = folders(fls).name; doneFileInx = doneFileInx+1;
    end
end

save('allData','allData','doneFileEYE','Scr','myVar','inf','-v7.3');

types = {'LowS','HighS','LowV','HighV'}; %

for type = 1:length(types)
    start = clock;
    if type == 1
        magentaTrlSel = allData([allData.BlPrePost]==PrePost&...
            [allData.condition]==2&[allData.StimValue]==1);
    elseif type == 2
        magentaTrlSel = allData([allData.BlPrePost]==PrePost&...
            [allData.condition]==2&[allData.StimValue]==2);
    elseif type == 3
        magentaTrlSel = allData([allData.BlPrePost]==PrePost&...
            [allData.condition]==1&[allData.StimValue]==1);
    elseif type == 4
        magentaTrlSel = allData([allData.BlPrePost]==PrePost&...
            [allData.condition]==1&[allData.StimValue]==2);
    end

    randomOrder = randperm(size(magentaTrlSel,2)); %create random order from that.
    magentaTrlSel = magentaTrlSel(randomOrder);
    magentaTrlSel = magentaTrlSel(1:round(length(magentaTrlSel)));
    fprintf('%d/4 %d\n',type,length(magentaTrlSel));
    
    snipSaccades(magentaTrlSel,...
        'TARGET_t','TRIAL_END_t','plot',1,'pupil',1, 'baselinesub','FIXATION_t','meancriteria',1,'smoothing',100);
    stop = clock; timeIn= stop-start;
    fprintf('Duration: %dmin %1.0fs\n',abs(timeIn(5:6)));
    legend({'SND Low','','','SND High','','', 'Vis Low','','','Vis High','',''});
end