
clear all
close all
fclose all

clc
withEye                 = abs(menu('With Eye Data?', {'Yes','No'})-2);
experiment              = menu('What Experiment?', {'1','2','3','4','5','6','7','8','9','10','11'});
prepost                 = menu('Conditioning?', {'Pre','Post','Conditioning'});
group                   = menu('Group?', {'ALL','1-4','5-7'});
whichone                = menu('Which measure to use?', {'dPrime','Accuracy','RT','Dist','Efficiency','Key'});
preTrl                  = menu('Only trls aftr:', {'No','CondCondSame','CondCondDiff','ValValSame','ValValDiff',...
    'CueSCueSSame','CueSCueSDiff','RelPosCueSSame','RelPosCueSDiff','GabSGabSSame',...
    'GabSGabSDiff','CueSGabSSame','CueSGabSDiff','GabSCueSSame','GabSCueSDiff'});
betweenCond             = menu('Between PP type?', {'No','SoundHH','SoundHL','VisualMH','VisualML'});
rootdir_fig                 = 'C:\Work\69. SCM Experiment With Reward_05.03\';
prepostVec              = {'Precond','Postcond'};
groupVec                = {'ALL','1-4','5-7'};
zcrit1 = 2.5;  %%%%%%%%but also see: https://www.nature.com/articles/s41598-018-34252-7 %%%https://core.ac.uk/download/pdf/85215435.pdf %%%%%http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.454.3890&rep=rep1&type=pdf%%%http://d-scholarship.pitt.edu/7948/1/Seo.pdf
zcrit2 = 0.9;
scalesel1 = [1 3];%[1 3]
scalesel2 = [.7 .9]; % [.7 0.9]
scalesel3 = [.5 1.2];%[.3 0.9]
scalesel5 = [0 2];%[1 3]
scalesel6 = [1 2];%[1 3]
prev_highreward =0;
nbins =3;
%% Load DATA
if experiment == 1
    rootdir= 'Y:\#Common\Projects\Behavioral\Data\55.1 SCM SimpleCrossModal\';
    subjects = {'18855566'; '19283746';'29384756';'31463624';'44751960';'96709616';'Roman06';'Tomke'}; %wrong set og Pp
    % subjects = {'18855566';'34214459'; '31463624';'44751960';'96709616'};
    subjects = {'18855566'; '19283746';'29384756';'31463624';'44751960';'96709616';'Tomke';'34214459'}; %wrong set og Pp,'34214459' added by Arezoo: why this subject was not included?
    experiment_name = 'Exp1';
    rel_blocks= 2:5;
    rel_blocks= [9:2:21];
elseif experiment == 2
    rootdir= 'W:\#Common\Projects\Behavioral\Data\Sound only Test 16.01\';
    subjects = {'Adem'; 'Barbara';'Helge';'Hendrik';'Kilian';'Roman1'};
    experiment_name = 'Exp2';
    rel_blocks= 2:9;
elseif experiment == 3
    rootdir= 'W:\#Common\Projects\Behavioral\Data\55.1 SCM Visual_Sound_Only Test 31.01\';
    subjects = {'R9'; 'Helge_01.02';'Hendrik_31.01';'Kilian_02.02';'R7';'R8'};
    experiment_name = 'Exp3';
    rel_blocks = 3:10;
elseif experiment == 4
    rootdir= 'W:\#Common\Projects\Behavioral\Data\60. SCM NewInstructions 21.02\';
    subjects = {'39417064';'31104305';'17012172';'68773507'};
    % subjects = {'31104305'};
    experiment_name = 'Exp4';
    rel_blocks= 2:5;
    rel_blocks= 7:2:21;
elseif experiment == 5
    rootdir= 'Y:\apoores\Data\fromRoman\';
    subjects = {'14592466';'48965498';'58372543';'37074631';'38252522';'47090933';'78749075';'48745299';'60515942';'56624400';'72410876';'21300406';'73028404';'59714179'};
    experiment_name = 'Exp5';
    rel_blocks= 2:5;
    %rel_blocks= 7:2:21;
    %     rel_blocks= 7:2:21;
    rel_blocks = [7:2:21];
elseif experiment == 6
    rootdir= 'Y:\#Common\Projects\Behavioral\Data\79 Simple Cross-Modal - low 26.03\';
    subjects = {'88476163' ,'87238769','58748256','99170697','88476163' ,'87238769','58748256'}; % 88476163 87238769
    experiment_name = 'Exp6';
    rel_blocks= [2 3 10 11];
    %  rel_blocks= [5  15];
    rel_blocks= [5 7 9   13 15 17];
    % rel_blocks= [4 6 8 12 14 16 20 22 24]; % conditioning
    % rel_blocks= [19 21 23 25]; % mixed
elseif experiment == 7
    rootdir= 'W:\apoores\Data\fromRoman\';
    subjects = {'88476163' ,'87238769','58748256','99170697','88476163' ,'87238769','58748256','74620667','18231554','93881044','89140978',...
        '23123662','47130574','74553204','24509065','68630199','71548078','66597433','10579857','13589008'}; %%% subject ,'45003453' had not learned auditory or visual tasks: performance ~ 0.55
    experiment_name = 'Exp7';
    rel_blocks= [2 3 10 11];
    %  rel_blocks= [5  15];
    rel_blocks= [5 7 9 13 15 17];
    % rel_blocks= [4 6 8 12 14 16 20 22 24]; % conditioning
    % rel_blocks= [19 21 23 25]; % mixed
elseif experiment == 8 %(all together)
    rootdir= 'W:\rvakhru\AllParticipants\';
    rootdir= 'C:\Users\rvakhru\Desktop\AllParticipants\';
    if group == 1
        subjects = {'88476163' ,'87238769','58748256','99170697','88476163' ,'87238769','58748256',...%%%% exp7
            '74620667','18231554','93881044','89140978','23123662','47130574','74553204','24509065','68630199','71548078','66597433','10579857','13589008',...%%%% exp6
            '14592466','48965498','58372543','37074631','38252522','47090933','78749075','48745299','60515942','56624400','72410876','21300406','73028404','59714179',...%%%% exp5
            '39417064','31104305','17012172','68773507',...%%%% exp4
            '18855566', '19283746','29384756','31463624','44751960','96709616','34214459'}; %,'Tomke'%%% exp1
    elseif group == 3
        subjects = {'88476163' ,'87238769','58748256','99170697','88476163' ,'87238769','58748256',...%%%% exp7
            '74620667','18231554','93881044','89140978','23123662','47130574','74553204','24509065','68630199','71548078','66597433','10579857','13589008',...%%%% exp6
            '14592466','48965498','58372543','37074631','38252522','47090933','78749075','48745299','60515942','56624400','72410876','21300406','73028404','59714179'};%%%% exp5
    elseif group == 2
        subjects = {'39417064','31104305','17012172','68773507',...%%%% exp4
            '18855566', '19283746','29384756','31463624','44751960','96709616','34214459'}; %,'Tomke'%%% exp1
    end
    experiment_name = 'Exp8';
elseif experiment == 9 %(all together)
    rootdir= 'W:\#Common\Projects\Behavioral\Data\83_SimpleCrossModal_140618_29d2d_EYE\';
    subjects = {'41497164','98326155','50618557','13946359','77749274'};
    experiment_name = 'Exp9';
elseif any(strcmp(num2str(experiment), {'11','10'})) %(all together)
    rootdir= 'W:\apoores\Data\fromRoman\Jessica\Task_relevant';
    % rootdir= 'W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA';
    rootdir= 'W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA_F';
    %rootdir ='W:\#Common\Projects\Antono\Exp11_EEG_EYE_Circle\DATA\Circle Inside_Task Relevant';
    % subjects = {'443322','1'};
    start_path = (rootdir);
    myPath = (start_path);
    folders = dir(myPath);
    
    %     subjects ={folders.name};
    %     subjects = s  ubjects([folders.isdir]);
    %     subjects = subjects(3:end);
    %     [~, subjects] = pop_chansel(subjects(~ismember(subjects,{'99999999','Studies'})), 'withindex', 'on');
    experiment_name = 'Exp10';
    %     subjects = strsplit(subjects);
    subjects ={'533440903','174636117','308371706','51339677','74892169','770883157','651170259','2308149','244855064','89397817','997506577','935749766','29628872'}; %%%% should be out headphone wrong '635418447'
   % subjects ={'855920445','215370668','418529483','233910659','19997234','648130159','163250929','331717863','802228596','522387509','331717863','802228596','651170259','759768504','877132171','741799883'};
   subjects ={'320201718','522387509','651170259','741799883','759768504','802228596','877132171','735242692','767328026','461335192','466389979','118642576','261525683',...
        '723182163','713680469','881415462','301960686','329893397','453998271','562924424', '644208360','922050020', '935749766',...
        '89397817','244855064','29628872','692353186','780035157',...
        '770865144','999393855','658780312','836740040','91378291','68465542','837262810'}; %%%% wrong reference electrode ,'331717863'
end
delete ([myPath filesep 'sizeData.txt']);
textfile = fopen([myPath filesep 'sizeData.txt'],'wt');
if experiment == 10
    fprintf(textfile,'Pp\texpMode\tPrepost\tV_S_L\tV_S_H\tV_M_L\tV_M_H\tV_L_L\tV_L_H\tA_S_L\tA_S_H\tA_M_L\tA_M_H\tA_L_L\tA_L_H\tN_S_N\tN_M_N\tN_L_N\n');
else
    fprintf(textfile,'Pp\texpMode\tPrepost\tV_L\tV_H\tA_L\tA_H\tN\n');
end

subjects(ismember(subjects,{'99999999','Studies'})) = [];
%% Predefine Variables
s.subAccu = nan(length(subjects),1);
s.subSP_H = nan(length(subjects),1);
s.subSP_L = nan(length(subjects),1);
s.subBC_M = nan(length(subjects),1);
s.subBC_O = nan(length(subjects),1);
s.subNeut = nan(length(subjects),1);
s.subSVol = nan(length(subjects),3);
s.subBCvH = nan(length(subjects),1);
s.subBCvL = nan(length(subjects),1);
s.subSPvH = nan(length(subjects),1);
s.subSPvL = nan(length(subjects),1);
s.subRewV = nan(length(subjects),2);
s.subDPri = nan(length(subjects),2);

% RelPosition SOUND
s.subA    = nan(length(subjects),1);
s.subAs   = nan(length(subjects),1);
s.subAd   = nan(length(subjects),1);
s.subSPdH = nan(length(subjects),1);
s.subSPdL = nan(length(subjects),1);
s.subSPsH = nan(length(subjects),1);
s.subSPsL = nan(length(subjects),1);
s.subSPvdH = nan(length(subjects),1);
s.subSPvdL = nan(length(subjects),1);
s.subSPvsH = nan(length(subjects),1);
s.subSPvsL = nan(length(subjects),1);

% RelPosition VISUAL
s.subV    = nan(length(subjects),1);
s.subVs   = nan(length(subjects),1);
s.subVd   = nan(length(subjects),1);
s.subBCdO = nan(length(subjects),1);
s.subBCdM = nan(length(subjects),1);
s.subBCsO = nan(length(subjects),1);
s.subBCsM = nan(length(subjects),1);
s.subBCvdH = nan(length(subjects),1);
s.subBCvdL = nan(length(subjects),1);
s.subBCvsH = nan(length(subjects),1);
s.subBCvsL = nan(length(subjects),1);
s.moveav_subBCvH= nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.moveav_subBCvL= nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.moveav_subSPvH= nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.moveav_subSPvL= nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.bin_subBCvH = nan(length(subjects),nbins-1);  %%%%%%% for binned accuracy
s.bin_subBCvL= nan(length(subjects),nbins-1);  %%%%%%% for binned accuracy
s.bin_subSPvH= nan(length(subjects),nbins-1);  %%%%%%% for binned accuracy
s.bin_subSPvL= nan(length(subjects),nbins-1);  %%%%%%% for binned accuracy
s.bin_subNeut= nan(length(subjects),nbins-1);  %%%%%%% for binned accuracy
s.vecAccu = cell(length(subjects),5);
s.vecAccu0 = cell(length(subjects),5);
s.vecCond = cell(length(subjects),5);
s.vecRT = cell(length(subjects),5);

s.effect_ColorValue = nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.effect_SoundValue = nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials

% FrameSize
s.subFSvL = nan(length(subjects),1);
s.subFSvH = nan(length(subjects),1);
s.subFMvL = nan(length(subjects),1);
s.subFMvH = nan(length(subjects),1);
s.subFLvL = nan(length(subjects),1);
s.subFLvH = nan(length(subjects),1);
s.subFSBvL= nan(length(subjects),1);
s.subFSBvH= nan(length(subjects),1);
s.subFMBvL= nan(length(subjects),1);
s.subFMBvH= nan(length(subjects),1);
s.subFLBvL= nan(length(subjects),1);
s.subFLBvH= nan(length(subjects),1);
s.subFSAvL= nan(length(subjects),1);
s.subFSAvH= nan(length(subjects),1);
s.subFMAvL= nan(length(subjects),1);
s.subFMAvH= nan(length(subjects),1);
s.subFLAvL= nan(length(subjects),1);
s.subFLAvH= nan(length(subjects),1);
s.subFSN  = nan(length(subjects),1);
s.subFMN  = nan(length(subjects),1);
s.subFLN  = nan(length(subjects),1);
s.Nremovedtrials= nan(length(subjects),1);
s.Nalltrials= nan(length(subjects),1);
s.condNTrl =nan(length(subjects),2,5);
s.vecAccu = cell(length(subjects),5);
s.vecAccu0 = cell(length(subjects),5);
s.vecRT0 = cell(length(subjects),5);
s.vecCondRew0 = cell(length(subjects),5);
s.vecCondMod0  = cell(length(subjects),5);
s.vecRT = cell(length(subjects),5);
for i=1:length(subjects)
%      if i<length(subjects)
%         withEye =1;
%     else
%         withEye =0;
%     end
    if withEye, load([myPath filesep subjects{i} filesep subjects{i} '__allDataEyE.mat']); vecDist = [];
    else        load([myPath filesep subjects{i} filesep subjects{i} '__allData.mat']);
    end
    vecAccu =[];  vecSoPi =[]; vecGabOALL = [];
    vecCond =[];  vecBoxC =[]; vecBoxS = [];
    vecSoVo =[];  vecValu =[];
    vecGabO =[];  vecRelP =[]; vecRT=[]; vecRT_zcrit = []; 
    vccboxXLoc= []; vccgaborXLoc= [];
    vectonset= [];
    vecError =[];
    keyPressed = [];
    %rel_blocks =rel_blocks(1:end);
    rel_blocks =2:length(block);
    for j=rel_blocks % gather information into vectors
        if ~isempty(block(j).trials)
            RelTrl = [block(j).trials.BlPrePost] == prepost;% & [block(j).trials.RT] > 0.1 & [block(j).trials.RT] < 2.25;%&[block(j).trials.BlNumber] ~= 1;
            tempSt = sum([[block(j).trials.SL]; [block(j).trials.BL]]);
            for oi = length(block(j).trials):-1:2
                if preTrl == 1
                    % do nothing
                elseif preTrl == 2
                    if block(j).trials(oi-1).condition == block(j).trials(oi).condition % only trl that followed by same cond.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 3
                    if block(j).trials(oi-1).condition == block(j).trials(oi).condition % only trl that followed by same cond.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 4
                    if block(j).trials(oi).StimValue == 1 || block(j).trials(oi).StimValue == 0 % only trl after highReward.
                        block(j).trials(oi-1) = [];
                    end
                elseif preTrl == 5
                    if block(j).trials(oi).StimValue == 2 || block(j).trials(oi).StimValue == 0 % only trl after highReward.
                        block(j).trials(oi-1) = [];
                    end
                elseif preTrl == 6
                    if tempSt(oi) ~= tempSt(oi-1) % only trl after highReward.
                        block(j).trials(oi-1) = [];
                    end
                elseif preTrl == 7
                    if tempSt(oi) == tempSt(oi-1) % only trl after highReward.
                        block(j).trials(oi-1) = [];
                    end
                elseif preTrl == 8
                    if block(j).trials(oi-1).relativePos ~= tempSt(oi) % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 9
                    if block(j).trials(oi-1).relativePos == tempSt(oi) % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 10
                    if block(j).trials(oi-1).GL ~= block(j).trials(oi).GL  % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 11
                    if block(j).trials(oi-1).GL == block(j).trials(oi).GL  % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 12
                    if tempSt(oi-1) ~= block(j).trials(oi).GL  % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 13
                    if tempSt(oi-1) == block(j).trials(oi).GL  % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 14
                    if block(j).trials(oi-1).GL ~= tempSt(oi)  % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 15
                    if block(j).trials(oi-1).GL == tempSt(oi)  % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                end
            end
            
            if whichone == 6
                vecAccu = [vecAccu block(j).trials([block(j).trials.error]==0&RelTrl).keyPressed]; % Accuracy keyPressed
            else
                vecAccu = [vecAccu block(j).trials([block(j).trials.error]==0&RelTrl).Accuracy]; % Accuracy keyPressed
            end
            
            
            %             block(j).trials = block(j).trials([block(j).trials.gaborOri]==-1);
            %             RelTrl = [block(j).trials.BlPrePost] == prepost%&[block(j).trials.BlNumber] ~= 1;
            vecRT_zcrit   = [vecRT_zcrit   block(j).trials([block(j).trials.error]==0 & ([block(j).trials.BlPrePost] == 1 | [block(j).trials.BlPrePost] == 2 | [block(j).trials.BlPrePost] == 1)).RT]; %%%%Gabor trials RT to remove long RTs
           % vecRT_zcrit(vecRT_zcrit < 0.1 & vecRT_zcrit> 2.25)=[];
            vecRT   = [vecRT   block(j).trials([block(j).trials.error]==0&RelTrl).RT];
            vecSoPi = [vecSoPi block(j).trials([block(j).trials.error]==0&RelTrl).SP];
            vecBoxC = [vecBoxC block(j).trials([block(j).trials.error]==0&RelTrl).BC];
            vecCond = [vecCond block(j).trials([block(j).trials.error]==0&RelTrl).condition];
            vecValu = [vecValu block(j).trials([block(j).trials.error]==0&RelTrl).StimValue];
            vecSoVo = [vecSoVo block(j).trials([block(j).trials.error]==0&RelTrl).SVol];
            vecRelP = [vecRelP block(j).trials([block(j).trials.error]==0&RelTrl).relativePos];
            vecGabO = [vecGabO block(j).trials([block(j).trials.error]==0&RelTrl).gaborOri];
            keyPressed = [keyPressed block(j).trials([block(j).trials.error]==0&RelTrl).keyPressed];
            if experiment == 10
            vecBoxS = [vecBoxS block(j).trials([block(j).trials.error]==0&RelTrl).boxSize];
            else
                vecBoxS = vecGabO;
            end
            vccboxXLoc= [vccboxXLoc block(j).trials([block(j).trials.error]==0&RelTrl).boxXLoc];
            if withEye, vecDist = [vecDist block(j).trials([block(j).trials.error]==0&RelTrl).dist];end
            gaborXLoc =[block(j).trials([block(j).trials.error]==0&RelTrl).GL];
            vccgaborXLoc= [vccgaborXLoc gaborXLoc];
            vectonset = [vectonset [ block(j).trials([block(j).trials.error]==0&RelTrl).targetOnset]-...
                [block(j).trials([block(j).trials.error]==0&RelTrl).fixationOnset]];
            vecGabOALL = [vecGabOALL block(j).trials(RelTrl).gaborOri];
            vecError = [vecError block(j).trials(RelTrl).error];
            
            fprintf('Block:%d Rel:%d\n',j,sum(RelTrl));
        end
    end
    %     vecBoxS = repmat(1,1,length(vecRelP));
    %     % combine information from Pp into one vector
    %     trls=10:40;%length(vecAccu);
    %     trls = find(abs(zscore(vecRT))<zcrit1 & vecDist<=median(vecDist));
    
   
    if withEye, trls = find(abs(zscore(vecRT))<zcrit1 & vecDist<zcrit2);
    else trls = find(abs(zscore(vecRT))<zcrit1);
    end
    
%     if withEye
%         trls = find((vecRT>= mean(vecRT_zcrit)- zcrit1*std(vecRT_zcrit) & vecRT<mean(vecRT_zcrit)+zcrit1*std(vecRT_zcrit)) & vecDist<zcrit2);       
%     else      
%         trls = find((vecRT>= mean(vecRT_zcrit)- zcrit1*std(vecRT_zcrit) & vecRT<mean(vecRT_zcrit)+zcrit1*std(vecRT_zcrit)));        
%     end
%     
    
%     if withEye
%         trls = find(vecRT>= 0.1 & vecRT<2.25 & vecDist<zcrit2);       
%     else      
%         trls = find(vecRT>= 0.1 & vecRT<2.25);        
%     end
    
    %     if withEye
    %         trls = find((vecRT>= 0.1 & vecRT<2.25) & vecDist<zcrit2);
    %     else
    %         trls = find((vecRT>= 0.1 & vecRT<2.25));
    %     end
    s.Nremovedtrials(i)=length(vecRT)-length(trls); %%%% how many trials are removed
    s.Nalltrials(i)=length(vecRT); %%%% how many trials are removed
    
%     [Sn, x_j ] = RousseeuwCrouxSn (vecRT);
% % outliers = X( x_j/Sn > 3) % criterion typically 2 or 3
%     if withEye, trls = find(x_j/Sn<zcrit1 & vecDist<zcrit2);
%     else trls = find(x_j/Sn<zcrit1);
%     end
  
    %     if any([block(2).trials.DRIFTCORRECT_t]>0), driftT = 1; else driftT = 0; end
    
    
    if withEye,
        fprintf('%d | %s : %d %d\n',length(subjects)-i,subjects{i},...
            length(find(abs(vecDist)>zcrit2)),length(find(abs(vecDist)<zcrit2)));
    end
    
    vecAccu = vecAccu(trls);
    vecRT   = vecRT(trls);
    if withEye,vecDist = vecDist(trls);end
    vecSoPi = vecSoPi(trls);
    vecBoxC = vecBoxC(trls);
    vecCond = vecCond(trls);
    vecValu = vecValu(trls);
    vecSoVo = vecSoVo(trls);
    vecRelP = vecRelP(trls);
    vecGabO = vecGabO(trls);
    vecBoxS = vecBoxS(trls);
    vccboxXLoc= vccboxXLoc(trls);
    vccgaborXLoc= vccgaborXLoc(trls);
    vectonset = vectonset(trls);
    
    vecError = vecError(trls);
    keyPressed = keyPressed(trls);
    %%%%%%%%%%% to remove outliers
    outlier = abs(zscore(vecRT))>zcrit1; %%%% to remove slow RTs use>4;
    %%%%%%%%%%%%% to keep track of repetitions
    GL_reps = [1 diff(vccgaborXLoc)]; %%% keeps track of repetitions of the side
    
    
    
    % fill with nanmean values of desired conditions across subjects
    s.subAccu(i)  = nanmean(vecAccu);                          % accuracy
    s.subRT(i)    = nanmean(vecRT);           
    s.subminRT(i)    = nanmin(vecRT); % RT
    
    
    hits       =     nanmean(vecAccu(vecGabO== 1));
    fA         = nanmean( 1 - vecAccu(vecGabO==-1));
    N = sum(vecGabO==-1); %%%%% for correction of h and fA when they are 1 or 0 see http://www.kangleelab.com/sdt-d-prime-calculation---other-tips.html
    s.subDPri(i)  = Dprime2(hits,fA,N);                % dPrime
    %     s.subSVol(i,1)= unique(vecSoVo(vecSoPi==0));  % No    sound
    %     s.subSVol(i,2)= unique(vecSoVo(vecSoPi==1));  % Quite sound
    %     s.subSVol(i,3)= unique(vecSoVo(vecSoPi==2));  % Loud  sound
    s.subRewV(i,:)= inf.rewardValueB;             % Value of cues (mode)
    s.subRewS(i,:)= inf.rewardValueS;
    s.subleanV{i}= [inf.learn.Visual];
    s.rel_blocks{i}= rel_blocks;
    s.age{i}= [inf.age];
    if isfield(inf.learn,'Sound')
        s.subleanS{i}= [inf.learn.Sound];
    else
        s.subleanS{i}= 1;
    end
    s.subPSE(i)= unique([block(2).trials.PSE]);
    %% Process in dPrime
    selection{1} = (vecCond==0);                            % SubNeut
    
    selection{2} = (vecCond==1 & vecBoxC==1);               % SubBC_O
    selection{3} = (vecCond==1 & vecBoxC==2);               % SubBC_M
    selection{4} = (vecCond==1 & vecValu==1);               % SubBCvL
    selection{5} = (vecCond==1 & vecValu==2);               % SubBCvH
    
    selection{6} = (vecCond==2 & vecSoPi==1);               % SubSP_L
    selection{7} = (vecCond==2 & vecSoPi==2);               % SubSP_H
    selection{8} = (vecCond==2 & vecValu==1);               % SubSPvL
    selection{9} = (vecCond==2 & vecValu==2);               % SubSPvH
    
    %RelPosition VISUAL SIDE
    selection{10} = (vecCond==1 & vecBoxC==1 & vecRelP==1); % SubBCsO
    selection{11} = (vecCond==1 & vecBoxC==2 & vecRelP==1); % SubBCsM
    selection{12} = (vecCond==1 & vecBoxC==1 & vecRelP==2); % SubBCdO
    selection{13} = (vecCond==1 & vecBoxC==2 & vecRelP==2); % SubBCdM
    %RelPosition SOUND SIDE
    selection{14} = (vecCond==2 & vecSoPi==1 & vecRelP==1); % SubSPsL
    selection{15} = (vecCond==2 & vecSoPi==2 & vecRelP==1); % SubSPsH
    selection{16} = (vecCond==2 & vecSoPi==1 & vecRelP==2); % SubSPdL
    selection{17} = (vecCond==2 & vecSoPi==2 & vecRelP==2); % SubSPdH
    
    %RelPosition VISUAL VALUE
    selection{18} = (vecCond==1 & vecValu==1 & vecRelP==1); % SubBCvsL
    selection{19} = (vecCond==1 & vecValu==2 & vecRelP==1); % SubBCvsH
    selection{20} = (vecCond==1 & vecValu==1 & vecRelP==2); % SubBCvdL
    selection{21} = (vecCond==1 & vecValu==2 & vecRelP==2); % SubBCvdH
    
    %RelPosition SOUND VALUE
    selection{22} = (vecCond==2 & vecValu==1 & vecRelP==1); % SubSPvsL
    selection{23} = (vecCond==2 & vecValu==2 & vecRelP==1); % SubSPvsH
    selection{24} = (vecCond==2 & vecValu==1 & vecRelP==2); % SubSPvdL
    selection{25} = (vecCond==2 & vecValu==2 & vecRelP==2); % SubSPvdH
    
    selection{26} = (vecCond==1 );                          % SubV
    selection{27} = (vecCond==2 );                          % SubA
    
    selection{28} = (vecCond==1 & vecRelP==1);               % SubVs
    selection{29} = (vecCond==1 & vecRelP==2);               % SubVd
    
    selection{30} = (vecCond==2 & vecRelP==1);               % SubAs
    selection{31} = (vecCond==2 & vecRelP==2);               % SubAd
    
    selection{32} = (vecBoxS==1 & vecValu==1);              % SubFSvL
    selection{33} = (vecBoxS==1 & vecValu==2);              % SubFSvH
    selection{34} = (vecBoxS==2 & vecValu==1);              % SubFMvL
    selection{35} = (vecBoxS==2 & vecValu==2);              % SubFMvH
    selection{36} = (vecBoxS==3 & vecValu==1);              % SubFLvL
    selection{37} = (vecBoxS==3 & vecValu==2);              % SubFLvH
    
    selection{38} = (vecBoxS==1 & vecValu==1& vecCond==1);              % SubFSBvL
    selection{39} = (vecBoxS==1 & vecValu==2& vecCond==1);              % SubFSBvH
    selection{40} = (vecBoxS==2 & vecValu==1& vecCond==1);              % SubFMBvL
    selection{41} = (vecBoxS==2 & vecValu==2& vecCond==1);              % SubFMBvH
    selection{42} = (vecBoxS==3 & vecValu==1& vecCond==1);              % SubFLBvL
    selection{43} = (vecBoxS==3 & vecValu==2& vecCond==1);              % SubFLBvH
    
    selection{44} = (vecBoxS==1 & vecValu==1& vecCond==2);              % SubFSAvL
    selection{45} = (vecBoxS==1 & vecValu==2& vecCond==2);              % SubFSAvH
    selection{46} = (vecBoxS==2 & vecValu==1& vecCond==2);              % SubFMAvL
    selection{47} = (vecBoxS==2 & vecValu==2& vecCond==2);              % SubFMAvH
    selection{48} = (vecBoxS==3 & vecValu==1& vecCond==2);              % SubFLAvL
    selection{49} = (vecBoxS==3 & vecValu==2& vecCond==2);              % SubFLAvH
    
    selection{50} = (vecBoxS==1 & vecCond==0);              % SubFSN
    selection{51} = (vecBoxS==2 & vecCond==0);              % SubFSN
    selection{52} = (vecBoxS==3 & vecCond==0);              % SubFSN
    
    forProcess = {'subNeut';...
        'subBC_O';'subBC_M';'subBCvL';'subBCvH';...
        'subSP_L';'subSP_H';'subSPvL';'subSPvH';...
        'subBCsO';'subBCsM';'subBCdO';'subBCdM';...
        'subSPsL';'subSPsH';'subSPdL';'subSPdH';...
        'subBCvsL';'subBCvsH';'subBCvdL';'subBCvdH';...
        'subSPvsL';'subSPvsH';'subSPvdL';'subSPvdH';...
        'subV';'subA';'subVs';'subVd';...
        'subAs';'subAd';'subFSvL';'subFSvH';...
        'subFMvL';'subFMvH';'subFLvL';'subFLvH';...
        'subFSBvL';'subFSBvH';'subFMBvL';'subFMBvH';...
        'subFLBvL';'subFLBvH';'subFSAvL';'subFSAvH';...
        'subFMAvL';'subFMAvH';'subFLAvL';'subFLAvH';...
        'subFSN';'subFMN';'subFLN'};
    col =1;
   for step = 1:length(forProcess)
        
        mytrls = find(selection{step});
        ntrl1 = length(mytrls);
        % mytrls = mytrls(round(length(mytrls)/2)+1:length(mytrls)); %%%%%
        % consider using ceil or floor for rounding
        % mytrls = mytrls(1:round(length(mytrls)/2));
        
        if prev_highreward
            myrealtrals =[];
            if ~isempty(mytrls)
                tcount=0;
                for t =1:length(mytrls)
                     if mytrls(t)~=1 && vecAccu(mytrls(t)-1)==1 %&& vecValu(mytrls(t)-1)==0 %%vecCond(mytrls(t)-1)==1
                        tcount=tcount+1;
                        %   myrealtrals(tcount) = mytrls(t)-1;
                        myrealtrals(tcount) = mytrls(t);
                    end
                end
            end
            mytrls=  myrealtrals;
        end
        %         if ismember(step, [1 4 5 8 9]) %%%%5 conditions
        %             mntrl=min(cellfun(@sum,selection( [1 4 5 8 9])));  %%% to get exactly the same number of trials for each condition of comparison
        %             mytrls = mytrls(1:mntrl);
        %         end
        
        if ismember(step, [1 4 5 8 9]) %%%%5 conditions
            s.condNTrl(i,1,col)= ntrl1;  %%% before removing any
            if prev_highreward
                s.condNTrl(i,2,col)= length(mytrls);  %%% after removing some
            end
            s.vecAccu{i,col} = vecAccu(mytrls);
            s.vecRT{i,col} = vecRT(mytrls);
            %%%%%%%%%%%% for previous trials
            s.vecCondMod0{i,col} = nan(length(mytrls),1);  %%%%%%%%%%%modality of previous trial
            s.vecCondRew0{i,col} = nan(length(mytrls),1);  %%%%%%%%%%%reward of previous trial
            s.vecAccu0{i,col}=nan(length(mytrls),1);       %%%%% accuracy of previous trial
            s.vecRT0{i,col}=nan(length(mytrls),1);       %%%%% RT of previous trial
            
            s.vecCondRew0{i,col} (~ismember(mytrls,1))= vecValu(mytrls(~ismember(mytrls,1))-1); %%%%%%%%%%%reward of previous trial
            s.vecAccu0{i,col}(~ismember(mytrls,1))= vecAccu(mytrls(~ismember(mytrls,1))-1);     %%%%% accuracy of previous trial
            s.vecRT0{i,col}(~ismember(mytrls,1))= vecRT(mytrls(~ismember(mytrls,1))-1);     %%%%% accuracy of previous trial
            s.vecCondMod0{i,col} (~ismember(mytrls,1))= vecCond(mytrls(~ismember(mytrls,1))-1);  %%%%%%%%%%%modality of previous trial
            col=col+1;
        end
        hits             =   nanmean(vecAccu(mytrls(vecGabO(mytrls)== 1)));
        fA               =   nanmean(1-nanmean(vecAccu(mytrls(vecGabO(mytrls)== -1))));
        %%%%%% sane as writting it as a function of key presses
        hits = nanmean([keyPressed(mytrls(vecGabO(mytrls)== 1))== 1]);
        fA   = nanmean([keyPressed(mytrls(vecGabO(mytrls)== -1))== 1]);
        
        %%%
        %%%%%Correction of fA=0 or 1 based on
        %%%%%https://link.springer.com/content/pdf/10.3758/BF03207704.pdf
        %%%%%this is also used by R
        %%%%%https://www.rdocumentation.org/packages/psycho/versions/0.3.7/topics/dprime,
        %%%%%and gives comparable results if number of trials are very
        %%%%%different across conditions
%         if fA==0 | fA==1 | hits==0 | hits==1
%             fA                  =  (sum(~(vecAccu(mytrls(vecGabO(mytrls)== -1))))+0.5)./(length(~(vecAccu(mytrls(vecGabO(mytrls)== -1))))+1);
%             hits                 =  (sum((vecAccu(mytrls(vecGabO(mytrls)== 1))))+0.5)./(length((vecAccu(mytrls(vecGabO(mytrls)== 1))))+1);
%         end
        %%%%%% sane as writting it as a function of key presses
%         N = length((mytrls(vecGabO(mytrls)== -1)));
        N = 50;
        if whichone ==1
            s.(forProcess{step})(i) = Dprime2(hits,fA,N);                % dPrime
        elseif whichone ==2
            s.(forProcess{step})(i) = nanmean(vecAccu(mytrls)); %%%%% for accuracy;
        elseif whichone ==3
            s.(forProcess{step})(i) = nanmean(vecRT(mytrls)); %%%%% for accuracy;
        elseif whichone ==4
            s.(forProcess{step})(i) = nanmean(vecDist(mytrls)); %%%%% for accuracy;
        elseif whichone ==5
            s.(forProcess{step})(i) = nanmean(vecAccu(mytrls))./nanmean(vecRT(mytrls)); %%%%% efficiency score, see https://www.journalofcognition.org/articles/10.5334/joc.6/;
        elseif whichone ==6
            s.(forProcess{step})(i) = nanmean(vecAccu(mytrls)); %%%%% for accuracy;
        end
    end
    %    maxi = 1;
    if experiment == 10
        maxi = max ([s.subFLN(i) s.subFLBvL(i) s.subFLBvH(i)  s.subFLAvL(i) s.subFLAvH(i)...
            s.subFSN(i) s.subFSBvL(i) s.subFSBvH(i)  s.subFSAvL(i) s.subFSAvH(i)...
            s.subFMN(i) s.subFMBvL(i) s.subFMBvH(i)  s.subFMAvL(i) s.subFMAvH(i)]')/100;
        fprintf(textfile,'%s\t%d\t%d\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\n',subjects{i},inf.expMode,prepost,...
            s.subFSBvL(i)./maxi,s.subFSBvH(i)./maxi,s.subFMBvL(i)./maxi,s.subFMBvH(i)./maxi,s.subFLBvL(i)./maxi,s.subFLBvH(i)./maxi,...
            s.subFSAvL(i)./maxi,s.subFSAvH(i)./maxi,s.subFMAvL(i)./maxi,s.subFMAvH(i)./maxi,s.subFLAvL(i)./maxi,s.subFLAvH(i)./maxi,...
            s.subFSN(i)./maxi,s.subFMN(i)./maxi,s.subFLN(i)./maxi);
    else
        maxi(i) = max([s.subBCvsL(i) s.subBCvsH(i) s.subSPvsL(i)  s.subSPvsH(i) s.subNeut(i)]');
        fprintf(textfile,'%s\t%d\t%d\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\n',subjects{i},inf.expMode,prepost,...
            s.subBCvsL(i)./maxi,s.subBCvsH(i)./maxi,s.subSPvsL(i)./maxi,s.subSPvsH(i)./maxi,s.subNeut(i)./maxi);
    end
   mytrls1 = find(selection{4});   %%%%% moving average of high  reward color trials
    mytrls2 = find(selection{5});   %%%%% moving average of low   reward color trials
    mintrl = min(length(mytrls1),length(mytrls2)); %%%% find which of the two has lower number of trials and get from both those trials
    s.mintrl_BC(i)= mintrl;
    s.moveav_subBCvH(i,1:mintrl) = cumsum(vecAccu(mytrls2(1:mintrl)))./(1:mintrl);  %%%%%%% compute moving average of your effect
    s.moveav_subBCvL(i,1:mintrl) = cumsum(vecAccu(mytrls1(1:mintrl)))./(1:mintrl);
    s.effect_ColorValue(i,1:mintrl) = s.moveav_subBCvH(i,1:mintrl)-s.moveav_subBCvL(i,1:mintrl);  %%%%%%%% effect is Hight minus Low
    
    
    mytrls1 = find(selection{8});   %%%%% moving average of high  reward sound trials
    mytrls2 = find(selection{9});   %%%%% moving average of low   reward sound trials
    mintrl = min(length(mytrls1),length(mytrls2)); %%%% find which of the two has lower number of trials and get from both those trials
    s.mintrl_SP(i)= mintrl;
    s.moveav_subSPvH(i,1:mintrl) = cumsum(vecAccu(mytrls2(1:mintrl)))./(1:mintrl);
    s.moveav_subSPvL(i,1:mintrl) = cumsum(vecAccu(mytrls1(1:mintrl)))./(1:mintrl);
    s.effect_SoundValue(i,1:mintrl) = s.moveav_subSPvH(i,1:mintrl)-s.moveav_subSPvL(i,1:mintrl);  %%%%%%%% effect is Hight minus Low
    
    mytrls0 = find(selection{1});   %%%%% moving average of Neutral trials
    mytrls1 = find(selection{4});   %%%%% moving average of low  reward color trials
    mytrls2 = find(selection{5});   %%%%% moving average of high   reward color trials
    mytrls3 = find(selection{8});   %%%%% moving average of low  reward sound trials
    mytrls4 = find(selection{9});   %%%%% moving average of high   reward sound trials
    
    %     mintrl = min([length(mytrls0),length(mytrls1),length(mytrls2),length(mytrls3),length(mytrls4)]); %%%% find which of the two has lower number of trials and get from both those trials
    %     mybins = round(linspace(1,mintrl,nbins));
    temp  = cell(5,nbins-1);  %%%for all conditions
    temp_trls ={mytrls0;mytrls1;mytrls2;mytrls3;mytrls4};
    for condi =1:5
        bintrls = temp_trls{condi};
        mintrl = length(bintrls);
        mybins = round(linspace(0,mintrl,nbins));
        for bini =1:length(mybins)-1
            mybintrls=bintrls(mybins(bini)+1:mybins(bini+1));
            hits             =   nanmean(vecAccu(mybintrls(vecGabO(mybintrls)== 1)));
             fA               =   nanmean(1-nanmean(vecAccu(mybintrls(vecGabO(mybintrls)== -1))));
            if whichone ==1                
                temp{condi,bini}=  Dprime2(hits,fA,N);  %%%%%%% compute binned average of your effect
            elseif whichone ==2                
                temp{condi,bini}=  mean(vecAccu(mybintrls));  %%%%%%% compute binned average of your effect
            elseif whichone ==3
                temp{condi,bini}=  mean(vecRT(mybintrls));  %%%%%%% compute binned average of your effect
            end
        end
    end
    s.bin_subNeut(i,:) = [temp{1,:}];  %%%%%%% compute binned average of your effect neutral
    s.bin_subBCvH(i,:) = [temp{3,:}];  %%%%%%% compute binned average of your effect VHR
    s.bin_subBCvL(i,:) = [temp{2,:}];  %%%%%%% compute binned average of your effect VLR
    s.bin_subSPvH(i,:) = [temp{5,:}];  %%%%%%% compute binned average of your effect AHR
    s.bin_subSPvL(i,:) = [temp{4,:}];  %%%%%%% compute binned average of your effect ALR
    
end
%%
close all hidden
Pp = [1:length(subjects)]; %%%5 7:21 were with Arezoo's crossmodal
for i=1:length(subjects)
   sl= [s.subleanS{i}];
   vl= [s.subleanV{i}];
   vlearn(i) = mean([vl(1:2:end)]);
   slearn(i) = mean([sl(1:2:end)]);
end
Pp(s.subAccu>0.95 | s.subAccu<0.55 | (slearn+vlearn)'/2<0.55 | (s.Nremovedtrials)./s.Nalltrials>0.2)=[]; %%%%% based on Pre would be 12 and 13
Pp = [1:11 14:37];%%%% based on s.subAccu in pre
%Pp = Pp(s.subAccu(Pp)>median(s.subAccu(Pp)));
%Pp(s.subAccu>0.95 )=[];
%Pp =[ 1     2     3     4     5     6     7     8     9    10    11    12    14    15    16    17    18    19    20    21    22];

rel_blocks = [2];
if betweenCond == 1
    %Do nothing
elseif betweenCond == 2
    Pp = Pp(s.subRewS(Pp,2)==2);
elseif betweenCond == 3
    Pp = Pp(s.subRewS(Pp,2)==1);
elseif betweenCond == 4
    Pp = Pp(s.subRewV(Pp,2)==2);
elseif betweenCond == 5
    Pp = Pp(s.subRewV(Pp,2)==1);
end

% maxi = max([s.subNeut(Pp) s.subSP_H(Pp)  s.subSP_L(Pp) s.subNeut(Pp) s.subBCvH(Pp) s.subBCvL(Pp)]'); %%%%% maximum across conditions


% maxi = max ([s.subFLN(Pp) s.subFLBvL(Pp) s.subFLBvH(Pp)  s.subFLAvL(Pp) s.subFLAvH(Pp)...
%              s.subFSN(Pp) s.subFSBvL(Pp) s.subFSBvH(Pp)  s.subFSAvL(Pp) s.subFSAvH(Pp)...
%              s.subFMN(Pp) s.subFMBvL(Pp) s.subFMBvH(Pp)  s.subFMAvL(Pp) s.subFMAvH(Pp)]')';


%% Figure 100 change of effect size across trials for visual
%
Pp = 1:35;
figure(777), axis square, hold on;
effect_mean = (nanmean(s.effect_ColorValue(Pp,:),1));
eff_sem=(nanstd(s.effect_ColorValue(Pp,:),0,1)./sqrt(length(Pp)));
eff_sem=1.*eff_sem; %%%% for confiedence interval (now is 90% for 95% should multiply by 1.96)

y1=effect_mean-eff_sem; y2=effect_mean+eff_sem;
y2(isnan(y1))=[];y1(isnan(y1))=[];
c1 = plot(1:size(effect_mean,2), nanmean(effect_mean,1),'g','linewidth',3)
h=patch([1:length(y1) fliplr(1:length(y1))], [y2  fliplr(y1)],[0.5 0.8 0.8],'FaceAlpha',.5,'EdgeAlpha',.3)
h.DisplayName = 'Visual';
plot([1:size(effect_mean,2)],zeros(1,size(effect_mean,2)),':k','linewidth',2)
axis([-1 80 -0.15 0.15])
xlabel('Trials')
ylabel('Performance Difference High reward minus Low Reward')
for t=1:size(s.effect_SoundValue(Pp,:),2)
    [h pp(t)]=ttest(s.effect_SoundValue(Pp,t),0);
    if pp(t)<0.05
        plot(t,-0.5,'sk','MarkerFaceColor',[0 0 0])
    end
end

effect_mean = (nanmean(s.effect_SoundValue(Pp,:),1));
eff_sem=(nanstd(s.effect_SoundValue(Pp,:),0,1)./sqrt(length(Pp)));
eff_sem=1.*eff_sem; %%%% for confiedence interval (now is 90% for 95% should multiply by 1.96)

y1=effect_mean-eff_sem; y2=effect_mean+eff_sem;
y2(isnan(y1))=[];y1(isnan(y1))=[];
c2 = plot(1:size(effect_mean,2), nanmean(effect_mean,1),'m','linewidth',3)
h=patch([1:length(y1) fliplr(1:length(y1))], [y2  fliplr(y1)],[0.8 0.5 0.8],'FaceAlpha',.5,'EdgeAlpha',.3);
h.DisplayName = 'Sound';
plot([1:size(effect_mean,2)],zeros(1,size(effect_mean,2)),':k','linewidth',2)
axis([-1 80 -0.15 0.15])

xlabel('ntrials')
ylabel('Performance Difference High reward minus Low Reward')
for t=1:size(s.effect_SoundValue(Pp,:),2)
    [h pp(t)]=ttest(s.effect_SoundValue(Pp,t),0);
    if pp(t)<0.05
        plot(t,-0.5,'sk','MarkerFaceColor',[1 0 0])
    end
end
% legend([c1,c2],{'Color','Sound'});
% %% Figure 100 change of effect size across trials for visual
% %
% figure(200), axis square, hold on;
% effect_mean = (nanmean(s.effect_ColorValue(Pp,:),1));
% eff_sem=(nanstd(s.effect_ColorValue(Pp,:),0,1)./sqrt(length(Pp)));
% eff_sem=1.*eff_sem; %%%% for confiedence interval (now is 90% for 95% should multiply by 1.96)
% 
% y1=effect_mean-eff_sem; y2=effect_mean+eff_sem;
% y2(isnan(y1))=[];y1(isnan(y1))=[];
% h=patch([1:length(y1) fliplr(1:length(y1))], [y2  fliplr(y1)],[0.5 0.5 0.5])
% plot(1:size(effect_mean,2), nanmean(effect_mean,1),'r')
% plot([1:size(effect_mean,2)],zeros(1,size(effect_mean,2)),':k','linewidth',2)
% axis([-1 80 -0.15 0.15])
% xlabel('ntrials')
% ylabel('Performance Difference High reward minus Low Reward')
% for t=1:size(s.effect_SoundValue(Pp,:),2)
%     [h pp(t)]=ttest(s.effect_SoundValue(Pp,t),0);
%     if pp(t)<0.05
%         plot(t,-0.5,'sk','MarkerFaceColor',[0 0 0])
%     end
% end
% %%
% figure(201), axis square, hold on;
% effect_mean = (nanmean(s.effect_SoundValue(Pp,:),1));
% eff_sem=(nanstd(s.effect_SoundValue(Pp,:),0,1)./sqrt(length(Pp)));
% eff_sem=1.*eff_sem; %%%% for confiedence interval (now is 90% for 95% should multiply by 1.96)
% 
% y1=effect_mean-eff_sem; y2=effect_mean+eff_sem;
% y2(isnan(y1))=[];y1(isnan(y1))=[];
% h=patch([1:length(y1) fliplr(1:length(y1))], [y2  fliplr(y1)],[0.5 0.5 0.5])
% plot(1:size(effect_mean,2), nanmean(effect_mean,1),'r')
% plot([1:size(effect_mean,2)],zeros(1,size(effect_mean,2)),':k','linewidth',2)
% axis([-1 80 -0.15 0.15])
% 
% xlabel('ntrials')
% ylabel('Performance Difference High reward minus Low Reward')
% for t=1:size(s.effect_SoundValue(Pp,:),2)
%     [h pp(t)]=ttest(s.effect_SoundValue(Pp,t),0);
%     if pp(t)<0.05
%         plot(t,-0.5,'sk','MarkerFaceColor',[0 0 0])
%     end
% end

%% Figure 1 Sound Pitch High-Low value
figure(1);
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSP_H(Pp))  nanmean(s.subSP_L(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSP_H(Pp))  nanmean(s.subSP_L(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSP_H(Pp)) nanstd(s.subSP_L(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none');
ax = gca;
ax.XTickLabel = {'neutral','HighPitch','LowPitch'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest(s.subSP_H(Pp),s.subSP_L(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str((p))])
% print ('-f1' ,'-dpsc2',[rootdir_fig 'sounds.ps'])
% ax.YLim = [1.3 1.6];
% %% Figure 2 Sound
% figure(2);
% bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSPvH(Pp)) nanmean(s.subSPvL(Pp))],'r'), hold on
% errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean( s.subSPvH(Pp)) nanmean(s.subSPvL(Pp))],...
%     [nanstd(s.subNeut(Pp)) nanstd(s.subSPvH(Pp)) nanstd(s.subSPvL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
% ax = gca;
% ax.XTickLabel = {'Nautral','HighValSound','LowValSound'};
% if whichone==1
%     ax.YLim = scalesel1;
%     ylabel('DPrime' , 'FontSize', 14);
% elseif whichone==2
%     ax.YLim = scalesel2;
%     ylabel('Accuracy' , 'FontSize', 14);
% elseif whichone==3
%     ax.YLim = scalesel3;
%     ylabel('RT' , 'FontSize', 14);
% elseif whichone==5
%     ax.YLim = scalesel5;
%     ylabel('Efficiency' , 'FontSize', 14);
% elseif whichone==6
%     ax.YLim = scalesel6;
%     ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
% end
% [h p]= ttest(s.subSPvH(Pp),s.subSPvL(Pp));
% title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
%     ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
% % print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%% Figure 3 Box Color Orange-Magenta value
figure(3);
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBC_M(Pp))  nanmean(s.subBC_O(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBC_M(Pp))  nanmean(s.subBC_O(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBC_M(Pp)) nanstd(s.subBC_O(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'neutral','Magenta','Orange'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest( s.subBC_M(Pp),s.subBC_O(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])

%%%%%%
%% Figure 2 Sound
figure(17);
bar([1 3 4 6 7],[nanmean(s.subBCvH(Pp)) nanmean(s.subBCvH(Pp)) nanmean(s.subBCvL(Pp)) nanmean(s.subSPvH(Pp))...
    nanmean(s.subSPvL(Pp))],'w'), axis square, hold on
errorbar([1 3 4 6 7],[nanmean(s.subBCvH(Pp)) nanmean(s.subBCvH(Pp)) nanmean(s.subBCvL(Pp)) nanmean(s.subSPvH(Pp))...
    nanmean(s.subSPvL(Pp))],...
    [nanstd(s.subBCvH(Pp)) nanstd(s.subBCvH(Pp)) nanstd(s.subBCvL(Pp)) nanstd(s.subSPvH(Pp))...
    nanstd(s.subSPvL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
%ax = gca;
ax = gca;
ax.XTickLabel = {'Neut','VisHighVal','VisLowVal','AudioHighVal','AudioLowVal'};

if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
title('Effect of Reward value and modality', 'FontSize', 16);
[h p1]= ttest(s.subBCvH(Pp),s.subBCvL(Pp));
[h p2]= ttest(s.subSPvH(Pp),s.subSPvL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', visual P= ' num2str(round(p1,3)) ', auditory P= ' num2str(round(p2,3))]);
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%%
%% Figure 2 Sound
figure(18);
bar([1 2 4 5 ],[ nanmean(s.subBCvH(Pp)-s.subNeut(Pp)) nanmean(s.subBCvL(Pp)-s.subNeut(Pp)) nanmean(s.subSPvH(Pp)-s.subNeut(Pp))...
    nanmean(s.subSPvL(Pp)-s.subNeut(Pp))],'w'), axis square, hold on
errorbar([1 3 4 6 7],[nanmean(s.subNeut(Pp)) nanmean(s.subBCvH(Pp)) nanmean(s.subBCvL(Pp)) nanmean(s.subSPvH(Pp))...
    nanmean(s.subSPvL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBCvH(Pp)) nanstd(s.subBCvL(Pp)) nanstd(s.subSPvH(Pp))...
    nanstd(s.subSPvL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Neut','VisHighVal','VisLowVal','AudioHighVal','AudioLowVal'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
ax = gca;
% ax.YLim = [1.4 1.6];
title('Effect of Reward value and modality', 'FontSize', 16);
[h p1]= ttest(s.subBCvH(Pp),s.subBCvL(Pp));
[h p2]= ttest(s.subSPvH(Pp),s.subSPvL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', visual P= ' num2str(round(p1,3)) ', auditory P= ' num2str(round(p2,3))]);
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%% Figure 2 Sound
figure(170);
boxplot([(s.subAccu(Pp)) (s.subBCvH(Pp)) (s.subBCvL(Pp)) (s.subSPvH(Pp))...
    (s.subSPvL(Pp))]), hold on
% plot(1:5,[(s.subNeut(Pp)) (s.subBCvH(Pp)) (s.subBCvL(Pp)) (s.subSPvH(Pp))...
%     (s.subSPvL(Pp))],'ok'), hold on

% ax = gca;
% ax.XTickLabel = {'Neut','VisHighVal','VisLowVal','AudioHighVal','AudioLowVal'};
% if whichone==1
%     ax.YLim = scalesel1;
%     ylabel('DPrime' , 'FontSize', 14);
% elseif whichone==2
%     ax.YLim = scalesel2;
%     ylabel('Accuracy' , 'FontSize', 14);
% elseif whichone==3
%     ax.YLim = scalesel3;
%     ylabel('RT' , 'FontSize', 14);
% elseif whichone==5
%     ax.YLim = scalesel5;
%     ylabel('Efficiency' , 'FontSize', 14);
% elseif whichone==6
%     ax.YLim = scalesel6;
%     ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
% end
% ax = gca;
% ax.YLim = [1.4 1.6];
%ylim([0.5 1])
title('Effect of Reward value and modality', 'FontSize', 16);
[h p1]= ttest(s.subBCvH(Pp),s.subBCvL(Pp));
[h p2]= ttest(s.subSPvH(Pp),s.subSPvL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', visual P= ' num2str(round(p1,2)) ', auditory P= ' num2str(round(p2,2))]);

%% Figure 2 Sound
figure(180);
boxplot([ (s.subBCvH(Pp))-(s.subNeut(Pp)) (s.subBCvL(Pp))-(s.subNeut(Pp)) (s.subSPvH(Pp))-(s.subNeut(Pp))...
    (s.subSPvL(Pp))-(s.subNeut(Pp))]), hold on



%%
HV= s.subBCvH(Pp)';
LV= s.subBCvL(Pp)';
HA= s.subSPvH(Pp)';
LA= s.subSPvL(Pp)';
N= s.subNeut(Pp)';
anovdata_rew_modality_names = {'HV';'LV';'HA';'LA'};
anovdata_rew_modality = [HV;LV;HA;LA]';

bimodncond=length(anovdata_rew_modality_names);
subfactor=cell(bimodncond,1);

for  i=1:bimodncond
    subfactor{i}= ['F' num2str(i)];
end

t = array2table(anovdata_rew_modality,'VariableNames',subfactor);
%factorNames = {'modality','reward','latencybin','part'};
factorNames = {'modality','reward'}; % for each part


within = table({'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
    {'H';'L';'H';'L'},... %reward: high or low
    'VariableNames',factorNames);
%
% fit the repeated measures model
rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);

% run my repeated measures anova here
%[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
[ranovatbl] = ranova(rm, 'WithinModel','modality*reward')

%%
pre=load('pre_beh5_dprime.mat','s','Pp');
HV= s.subBCvH(Pp)';
LV= s.subBCvL(Pp)';
HA= s.subSPvH(Pp)';
LA= s.subSPvL(Pp)';
N= s.subNeut(Pp)';
HV_pre= pre.s.subBCvH(Pp)';
LV_pre= pre.s.subBCvL(Pp)';
HA_pre= pre.s.subSPvH(Pp)';
LA_pre= pre.s.subSPvL(Pp)';
N_pre= pre.s.subNeut(Pp)';
anovdata_rew_modality_names = {'HV';'LV';'HA';'LA';'HV_pre';'LV_pre';'HA_pre';'LA_pre'};
anovdata_rew_modality = [HV;LV;HA;LA;HV_pre;LV_pre;HA_pre;LA_pre]';

bimodncond=length(anovdata_rew_modality_names);
subfactor=cell(bimodncond,1);

for  i=1:bimodncond
    subfactor{i}= ['F' num2str(i)];
end

t = array2table([anovdata_rew_modality (1:length(Pp))'],'VariableNames',[subfactor;{'Sujs'}]);
%factorNames = {'modality','reward','latencybin','part'};
factorNames = {'modality','reward','pre_post'}; % for each part


within = table({'V';'V';'A';'A';'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
    {'H';'L';'H';'L';'H';'L';'H';'L'},... %reward: high or low
     {'Post';'Post';'Post';'Post';'Pre';'Pre';'Pre';'Pre'},... %phase: pre or post
    'VariableNames',factorNames);
%
% fit the repeated measures model
rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);

% run my repeated measures anova here
%[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*pre_post')
%%
pre=load('pre_beh5_dprime.mat','s','Pp');

HV= s.subBCvH(Pp)';
LV= s.subBCvL(Pp)';
HA= s.subSPvH(Pp)';
LA= s.subSPvL(Pp)';
N= s.subNeut(Pp)';


HV_pre= pre.s.subBCvH(Pp)';
LV_pre= pre.s.subBCvL(Pp)';
HA_pre= pre.s.subSPvH(Pp)';
LA_pre= pre.s.subSPvL(Pp)';
N_pre= pre.s.subNeut(Pp)';



figure(1732), axis square, hold on;
boxplot([((HV+HA)./2-(HV_pre+HA_pre)./2)'  ((LV+LA)./2-(LV_pre+LA_pre)./2)' (N-N_pre)'])
   

figure(1733), axis square, hold on;
% bar([1 ],[ nanmean((HV+HA)./2-(HV_pre+HA_pre)./2)  ],'r'), hold on
% 
% bar([3],[  nanmean((LV+LA)./2-(LV_pre+LA_pre)./2) ],'b')
% 
% bar([5 ],[  nanmean(N-N_pre)],'k')
errorbar([1 ],[ nanmean((HV+HA)./2-(HV_pre+HA_pre)./2)  ],...
    [nanstd((HV+HA)./2-(HV_pre+HA_pre)./2)  ]./sqrt(length(Pp)),'rd','linewidth',3), hold on

errorbar([3],[  nanmean((LV+LA)./2-(LV_pre+LA_pre)./2) ],...
    [ nanstd((LV+LA)./2-(LV_pre+LA_pre)./2)  ]./sqrt(length(Pp)),'bd','linewidth',3)

errorbar([5 ],[  nanmean(N-N_pre)],...
    [ nanstd(N-N_pre)  ]./sqrt(length(Pp)),'kd','linewidth',3)

ax = gca;
ax.XTickLabel = {'High','Low','Neutral'};
ax.XTick = [ 1  3  5 ];
ax.YLim = [-.05 0.1];
plot([0 6],[0 0],':k')
[h p_hh]= ttest((HV+HA)./2-(HV_pre+HA_pre)./2)  %%%%% this is switch versus no switch
[h p_ll]= ttest((LV+LA)./2-(LV_pre+LA_pre)./2)  %%%%% this is switch versus no switch
[h p_n]= ttest((N)-(N_pre))  %%%%% this is switch versus no switch
[h p_hl]= ttest(((HV+HA)./2-(HV_pre+HA_pre)./2),((LV+LA)./2-(LV_pre+LA_pre)./2 ))
%[h p_hl]= ttest((HV+HA)./2-(HV_pre+HA_pre)./2,N-N_pre)


title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', Learning_H P= ' num2str(round(p_hh,3)) ', Learning_L P= ' num2str(round(p_ll,3))...
    ', Learning_N P= ' num2str(round(p_n,3)) ', Difference HIgh and Low P= ' num2str(round(p_hl,3))]);


%%
pre=load('pre_beh5.mat','s','Pp');
HV= s.subBCvH(Pp)';
LV= s.subBCvL(Pp)';
HA= s.subSPvH(Pp)';
LA= s.subSPvL(Pp)';
N= s.subNeut(Pp)';
HV_pre= pre.s.subBCvH(Pp)';
LV_pre= pre.s.subBCvL(Pp)';
HA_pre= pre.s.subSPvH(Pp)';
LA_pre= pre.s.subSPvL(Pp)';
N_pre= pre.s.subNeut(Pp)';
anovdata_rew_modality_names = {'HV';'LV';'HA';'LA'};
anovdata_rew_modality = [HV;LV;HA;LA]';

bimodncond=length(anovdata_rew_modality_names);
subfactor=cell(bimodncond,1);

for  i=1:bimodncond
    subfactor{i}= ['F' num2str(i)];
end
accufac = zeros(length(Pp),1);
for p =1:length(Pp)
    if s.subDPri(Pp(p))>median(s.subDPri(Pp))
        accufac(p)=1;
    end
end

t = array2table([anovdata_rew_modality s.subRewS(Pp,1) s.subRewV(Pp,1) (accufac)],...
    'VariableNames',[subfactor; {'SoundP'} ;{'BoxColor'};{'Accuracy'}]);
t.SoundP = categorical (t.SoundP);
t.BoxColor = categorical (t.BoxColor);
t.Accuracy = categorical (t.Accuracy);

factorNames = {'modality','reward'}; % for each part


within = table({'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
    {'H';'L';'H';'L'},... %reward: high or low
    'VariableNames',factorNames);


% % fit the repeated measures model
% rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);

% fit the repeated measures model
rm = fitrm(t,['F1-F' num2str(bimodncond) '~Accuracy'],'WithinDesign',within);

% run my repeated measures anova here
%[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
[ranovatbl] = ranova(rm, 'WithinModel','modality*reward')
%% Including between factors
pre=load('pre_beh5.mat','s','Pp');
HV= s.subBCvH(Pp)';
LV= s.subBCvL(Pp)';
HA= s.subSPvH(Pp)';
LA= s.subSPvL(Pp)';
N= s.subNeut(Pp)';
HV_pre= pre.s.subBCvH(Pp)';
LV_pre= pre.s.subBCvL(Pp)';
HA_pre= pre.s.subSPvH(Pp)';
LA_pre= pre.s.subSPvL(Pp)';
N_pre= pre.s.subNeut(Pp)';
anovdata_rew_modality_names = {'HV';'LV';'HA';'LA';'HV_pre';'LV_pre';'HA_pre';'LA_pre'};
anovdata_rew_modality = [HV;LV;HA;LA;HV_pre;LV_pre;HA_pre;LA_pre]';

bimodncond=length(anovdata_rew_modality_names);
subfactor=cell(bimodncond,1);

for  i=1:bimodncond
    subfactor{i}= ['F' num2str(i)];
end
accufac = zeros(length(Pp),1);
for p =1:length(Pp)
    if s.subAccu(Pp(p))>median(s.subAccu(Pp))
        accufac(p)=1;
    end
end

t = array2table([anovdata_rew_modality s.subRewS(Pp,1) s.subRewV(Pp,1) accufac],...
    'VariableNames',[subfactor; {'SoundP'} ;{'BoxColor'};{'Accuracy'}]);
t.SoundP = categorical (t.SoundP);
t.BoxColor = categorical (t.BoxColor);
t.Accuracy = categorical (t.Accuracy);

factorNames = {'modality','reward','pre_post'}; % for each part


within = table({'V';'V';'A';'A';'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
    {'H';'L';'H';'L';'H';'L';'H';'L'},... %reward: high or low
     {'Post';'Post';'Post';'Post';'Pre';'Pre';'Pre';'Pre'},... %phase: pre or post
    'VariableNames',factorNames);


% % fit the repeated measures model
% rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);

% fit the repeated measures model
rm = fitrm(t,['F1-F' num2str(bimodncond) '~Accuracy'],'WithinDesign',within);

% run my repeated measures anova here
%[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*pre_post')
%%
%%
pre=load('pre_beh5_dprime.mat','s','Pp');
HV= s.subBCvH(Pp)';
LV= s.subBCvL(Pp)';
HA= s.subSPvH(Pp)';
LA= s.subSPvL(Pp)';
N= s.subNeut(Pp)';
HV_pre= pre.s.subBCvH(Pp)';
LV_pre= pre.s.subBCvL(Pp)';
HA_pre= pre.s.subSPvH(Pp)';
LA_pre= pre.s.subSPvL(Pp)';
N_pre= pre.s.subNeut(Pp)';
anovdata_rew_modality_names = {'HV';'LV';'HA';'LA';'HV_pre';'LV_pre';'HA_pre';'LA_pre'};
anovdata_rew_modality = [HV;LV;HA;LA;HV_pre;LV_pre;HA_pre;LA_pre]';

bimodncond=length(anovdata_rew_modality_names);
subfactor=cell(bimodncond,1);

for  i=1:bimodncond
    subfactor{i}= ['F' num2str(i)];
end

t = array2table([anovdata_rew_modality s.subRewS(Pp,1) s.subRewV(Pp,1) s.subDPri(Pp,1)],...
    'VariableNames',[subfactor; {'SoundP'} ;{'BoxColor'};{'Accuracy'}]);
%factorNames = {'modality','reward','latencybin','part'};
factorNames = {'modality','reward','pre_post'}; % for each part


within = table({'V';'V';'A';'A';'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
    {'H';'L';'H';'L';'H';'L';'H';'L'},... %reward: high or low
     {'Post';'Post';'Post';'Post';'Pre';'Pre';'Pre';'Pre'},... %phase: pre or post
    'VariableNames',factorNames);



% % fit the repeated measures model
% rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);

% fit the repeated measures model
rm = fitrm(t,['F1-F' num2str(bimodncond) '~Accuracy'],'WithinDesign',within);

% run my repeated measures anova here
%[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
[ranovatbl] = ranova(rm, 'WithinModel','modality*reward')

%%
%This is checking the example in R to see if we get the same results in R
%and Matlab: https://www.uvm.edu/~dhowell/StatPages/R/RepeatedMeasuresAnovaR.html
% Load the data here
%%% dat = tdfread('C:\Users\arezoo\Desktop\Tab14-11.dat');
%%% dat2=struct2cell(dat);
%%% dat3=cell2mat(dat2');

% anovdata = dat3(:,2:end);
% 
% bimodncond=size(anovdata,2);
% subfactor=cell(bimodncond,1);
% 
% for  i=1:bimodncond
%     subfactor{i}= ['F' num2str(i)];
% end
% 
% t = array2table([anovdata dat3(:,1)],...
%     'VariableNames',[subfactor; {'Group'}]);
% %factorNames = {'modality','reward','latencybin','part'};
% factorNames = {'Cycle','Phase'}; % for each part
% t.Group = categorical (t.Group);
% 
% within = table({'1';'1';'2';'2';'3';'3';'4';'4'},... %modality: visual, auditory, bimodal
%     {'1';'2';'1';'2';'1';'2';'1';'2'},... %reward: high or low
%     'VariableNames',factorNames);
% 
% 
% 
% % % fit the repeated measures model
% % rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);
% 
% % fit the repeated measures modelf
% rm = fitrm(t,['F1-F' num2str(bimodncond) '~1+Group'],'WithinDesign',within);
% 
% 
% [ranovatbl] = ranova(rm, 'WithinModel','Cycle*Phase')

%%

% pre=load('pre_beh5.mat','s','Pp');
% post=load('post_beh5_dprime.mat','s','Pp');
% Pp = post.Pp;
% s =post.s;
% HV= post.s.subBCvH(Pp)';
% LV= post.s.subBCvL(Pp)';
% HA= post.s.subSPvH(Pp)';
% LA= post.s.subSPvL(Pp)';
% N= post.s.subNeut(Pp)';
% HV_pre= pre.s.subBCvH(Pp)';
% LV_pre= pre.s.subBCvL(Pp)';
% HA_pre= pre.s.subSPvH(Pp)';
% LA_pre= pre.s.subSPvL(Pp)';
% N_pre= pre.s.subNeut(Pp)';
% rew_modality_names = {'HV';'LV';'HA';'LA';'HV_pre';'LV_pre';'HA_pre';'LA_pre'};
% dat = [HV LV HA LA ]';
% dat_pre =[HV_pre LV_pre HA_pre LA_pre]';
% rew_factor = nominal(repmat([ones(length(Pp),1); zeros(length(Pp),1) ],2,1));
% mod_factor = nominal([ones(length(Pp),1); ones(length(Pp),1); zeros(length(Pp),1); zeros(length(Pp),1)]);
% sub_factor = repmat(1:length(Pp),1,4);
% sub_factor = (sub_factor(:));
% Accu_factor = repmat(s.subAccu(Pp),1,4);
% Accu_factor = Accu_factor(:);
% prepost_factor = nominal([repmat(ones(length(Pp),1),2,1);repmat(zeros(length(Pp),1),2,1)]);
% tbl = table(dat-dat_pre, dat_pre,sub_factor ,rew_factor,mod_factor,Accu_factor,'VariableNames',...
%     {'Performance','Performance_pre','Subject','Reward','Modality','Accuracy'});
% lme = fitlme(tbl,'Performance~1+ Reward*Modality+(Reward*Modality|Subject)','FitMethod','REML', 'DummyVarCoding', 'effects', 'CheckHessian', true)


%%

bimodncond=length(anovdata_rew_modality_names);
subfactor=cell(bimodncond,1);

for  i=1:bimodncond
    subfactor{i}= ['F' num2str(i)];
end
accufac = zeros(length(Pp),1);
for p =1:length(Pp)
    if s.subDPri(Pp(p))>median(s.subDPri(Pp))
        accufac(p)=1;
    end
end

t = array2table([anovdata_rew_modality s.subRewS(Pp,1) s.subRewV(Pp,1) (accufac)],...
    'VariableNames',[subfactor; {'SoundP'} ;{'BoxColor'};{'Accuracy'}]);
t.SoundP = categorical (t.SoundP);
t.BoxColor = categorical (t.BoxColor);

factorNames = {'modality','reward','pre_post'}; % for each part


within = table({'V';'V';'A';'A';'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
    {'H';'L';'H';'L';'H';'L';'H';'L'},... %reward: high or low
     {'Post';'Post';'Post';'Post';'Pre';'Pre';'Pre';'Pre'},... %phase: pre or post
    'VariableNames',factorNames);


% % fit the repeated measures model
% rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);

% fit the repeated measures model
rm = fitrm(t,['F1-F' num2str(bimodncond) '~SoundP+BoxColor+Accuracy'],'WithinDesign',within);

% run my repeated measures anova here
%[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*pre_post')

%%
%% Figure 2 Sound
pre=load('pre_beh5.mat','s','Pp');
HV= s.subBCvH(Pp)';
LV= s.subBCvL(Pp)';
HA= s.subSPvH(Pp)';
LA= s.subSPvL(Pp)';
N= s.subNeut(Pp)';
HV_pre= pre.s.subBCvH(Pp)';
LV_pre= pre.s.subBCvL(Pp)';
HA_pre= pre.s.subSPvH(Pp)';
LA_pre= pre.s.subSPvL(Pp)';
N_pre= pre.s.subNeut(Pp)';
figure(1700), subplot(2,1,1), hold on;
bar([1 3 4 6 7],[nanmean(N-N_pre) nanmean(HV-HV_pre) nanmean(LV-LV_pre) nanmean(HA-HA_pre)...
    nanmean(LA-LA_pre)],'w'), axis square, hold on
errorbar([1 3 4 6 7],[nanmean(N-N_pre) nanmean(HV-HV_pre) nanmean(LV-LV_pre) nanmean(HA-HA_pre)...
    nanmean(LA-LA_pre)],...
    [nanstd(N-N_pre) nanstd(HV-HV_pre) nanstd(LV-LV_pre) nanstd(HA-HA_pre)...
    nanstd(LA-LA_pre)]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Neut','VisHighVal','VisLowVal','AudioHighVal','AudioLowVal'};
% if whichone==1
%     ax.YLim = scalesel1;
%     ylabel('DPrime' , 'FontSize', 14);
% elseif whichone==2
%     ax.YLim = scalesel2;
%     ylabel('Accuracy' , 'FontSize', 14);
% elseif whichone==3
%     ax.YLim = scalesel3;
%     ylabel('RT' , 'FontSize', 14);
% elseif whichone==5
%     ax.YLim = scalesel5;
%     ylabel('Efficiency' , 'FontSize', 14);
% elseif whichone==6
%     ax.YLim = scalesel6;
%     ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
% end
ax = gca;
% ax.YLim = [1.4 1.6];
title('Effect of Reward value and modality', 'FontSize', 16);
[h p1]= ttest(HV-HV_pre,LV-LV_pre);
[h p2]= ttest(HA-HA_pre,LA-LA_pre);
[h p3]= ttest((HV+HA)-(HV_pre+HA_pre),(LV+LA)-(LV_pre+LA_pre))
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', visual P= ' num2str(round(p1,3)) ', auditory P= ' num2str(round(p2,3))]);
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%%
pre=load('pre_beh5_RT.mat','s','Pp');
Accu = s.subRT(Pp);
pre_Accu= pre.s.subRT(Pp);

figure(1700), subplot(2,1,2), hold on;
bar([1 3 ],[nanmean(Accu) nanmean(pre_Accu) ],'w'), axis square, hold on
errorbar([1 3 ],[nanmean(Accu) nanmean(pre_Accu) ],...
    [nanstd(Accu) nanstd(pre_Accu) ]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Accu','Pre-Accu'};
ax.XTick = [1 3];

ax = gca;
ax.YLim = [0.6 1];
title('Learning', 'FontSize', 16);
[h p1]= ttest(Accu,pre_Accu);
[h p2]= ttest(HA-HA_pre,LA-LA_pre);
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', Learning P= ' num2str(round(p1,3)) ]);
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%%
%%
pre=load('pre_beh5.mat','s','Pp');
post1=load('post_beh5_1sthalf2.mat','s','Pp');
post2=load('post_beh5_2ndhalf2.mat','s','Pp');

HV1= post1.s.subBCvH(Pp)';
LV1= post1.s.subBCvL(Pp)';
HA1= post1.s.subSPvH(Pp)';
LA1= post1.s.subSPvL(Pp)';
N1= post1.s.subNeut(Pp)';

HV2= post2.s.subBCvH(Pp)';
LV2= post2.s.subBCvL(Pp)';
HA2= post2.s.subSPvH(Pp)';
LA2= post2.s.subSPvL(Pp)';
N2= post2.s.subNeut(Pp)';


HV_pre= pre.s.subBCvH(Pp)';
LV_pre= pre.s.subBCvL(Pp)';
HA_pre= pre.s.subSPvH(Pp)';
LA_pre= pre.s.subSPvL(Pp)';
N_pre= pre.s.subNeut(Pp)';

xloci =[1 11 20 31 40];
figure(1710),  hold on;
bar([xloci xloci+xloci(end)+12],[nanmean(N1-N_pre) nanmean(HV1-HV_pre) nanmean(LV1-LV_pre) nanmean(HA1-HA_pre)...
    nanmean(LA1-LA_pre) nanmean(N2-N_pre) nanmean(HV2-HV_pre) nanmean(LV2-LV_pre) nanmean(HA2-HA_pre)...
    nanmean(LA2-LA_pre)],1,'w'), axis square, hold on
errorbar([xloci xloci+xloci(end)+12],[nanmean(N1-N_pre) nanmean(HV1-HV_pre) nanmean(LV1-LV_pre) nanmean(HA1-HA_pre)...
    nanmean(LA1-LA_pre) nanmean(N2-N_pre) nanmean(HV2-HV_pre) nanmean(LV2-LV_pre) nanmean(HA2-HA_pre)...
    nanmean(LA2-LA_pre)],...
    ([nanstd(N1-N_pre) nanstd(HV1-HV_pre) nanstd(LV1-LV_pre) nanstd(HA1-HA_pre)...
    nanstd(LA1-LA_pre) nanstd(N2-N_pre) nanstd(HV2-HV_pre) nanstd(LV2-LV_pre) nanstd(HA2-HA_pre)...
    nanstd(LA2-LA_pre)])./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Neut','VisHighVal','VisLowVal','AudioHighVal','AudioLowVal'};
% if whichone==1
%     ax.YLim = scalesel1;
%     ylabel('DPrime' , 'FontSize', 14);
% elseif whichone==2
%     ax.YLim = scalesel2;
%     ylabel('Accuracy' , 'FontSize', 14);
% elseif whichone==3
%     ax.YLim = scalesel3;
%     ylabel('RT' , 'FontSize', 14);
% elseif whichone==5
%     ax.YLim = scalesel5;
%     ylabel('Efficiency' , 'FontSize', 14);
% elseif whichone==6
%     ax.YLim = scalesel6;
%     ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
% end
ax = gca;
%ax.YLim = [-0.05 .1];
title('Effect of Reward value and modality', 'FontSize', 16);
[h p1]= ttest(HV1-HV_pre,LV1-LV_pre);
[h p2]= ttest(HA1-HA_pre,LA1-LA_pre);
[h p3]= ttest((HA1-LA1)-(HA_pre-LA_pre),(HV1-LV1)-(HV_pre-LV_pre)) %%% for interaction

[h p4]= ttest(HV2-HV_pre,LV2-LV_pre);
[h p5]= ttest(HA2-HA_pre,LA2-LA_pre);
[h p6]= ttest((HA2-LA2)-(HA_pre-LA_pre),(HV2-LV2)-(HV_pre-LV_pre)) %%% for interaction

%[h p7]= ttest((HA2-LA2)-(HA_pre-LA_pre),(HV2-LV2)-(HV_pre-LV_pre)) %%% for interaction

title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', visual P= ' num2str(round(p1,3)) ', auditory P= ' num2str(round(p2,3))  ', visual2 P= ' num2str(round(p4,3)) ', auditory2 P= ' num2str(round(p5,3))]);
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%%
figure(1720),errorbar([1 2 ],[nanmean(N1-N_pre)  nanmean(N2-N_pre)],...
    [nanstd(N1-N_pre) nanstd(N2-N_pre) ]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle',':'), hold on
errorbar([1.01 2.01 ],[nanmean(HV1-HV_pre)  nanmean(HV2-HV_pre)],...
    [nanstd(HV1-HV_pre) nanstd(HV2-HV_pre) ]./sqrt(length(Pp)),'r','linewidth',3, 'linestyle',':')
errorbar([1.02 2.02 ],[nanmean(LV1-LV_pre)  nanmean(LV2-LV_pre)],...
    [nanstd(LV1-LV_pre) nanstd(LV2-LV_pre) ]./sqrt(length(Pp)),'b','linewidth',3, 'linestyle',':')
errorbar([1.03 2.03 ],[nanmean(HA1-HA_pre)  nanmean(HA2-HA_pre)],...
    [nanstd(HA1-HA_pre) nanstd(HA2-HA_pre) ]./sqrt(length(Pp)),'m','linewidth',3, 'linestyle',':')
errorbar([1.04 2.04 ],[nanmean(LA1-LA_pre)  nanmean(LA2-LA_pre)],...
    [nanstd(LA1-LA_pre) nanstd(LA2-LA_pre) ]./sqrt(length(Pp)),'c','linewidth',3, 'linestyle',':')
%%
%%
%%
pre=load('pre_beh5.mat','s','Pp');

HV = s.bin_subBCvH(Pp,:);
LV = s.bin_subBCvL(Pp,:);
HA = s.bin_subSPvH(Pp,:);
LA = s.bin_subSPvL(Pp,:);
N = s.bin_subNeut(Pp,:);

HV_pre= pre.s.subBCvH(Pp)';
LV_pre= pre.s.subBCvL(Pp)';
HA_pre= pre.s.subSPvH(Pp)';
LA_pre= pre.s.subSPvL(Pp)';
N_pre= pre.s.subNeut(Pp)';
%%%%%% if we want to normalize
% for i=1:length(Pp)
%     maxi(i) = max([max(HV(i,:));max(LV(i,:));max(HA(i,:));max(LA(i,:));max(N(i,:));max(HV_pre(i));max(LV_pre(i));max(HA_pre(i));max(LA_pre(i));max(N_pre(i))]);
%     HV(i,:) = HV(i,:)./maxi(i);
%     LV(i,:) = LV(i,:)./maxi(i);
%     HA(i,:) = HA(i,:)./maxi(i);
%     LA(i,:) = LA(i,:)./maxi(i);
%     N(i,:)  = N(i,:)./maxi(i);
%     
%     HV_pre(i) = HV_pre(i)'./maxi(i);
%     LV_pre(i) = LV_pre(i)'./maxi(i);
%     HA_pre(i) = HA_pre(i)'./maxi(i);
%     LA_pre(i) = LA_pre(i)'./maxi(i);
%     N_pre(i)  = N_pre(i)'./maxi(i);
% end

figure (888),axis square, hold on

 errorbar([1:nbins-1], [mean(N-repmat(N_pre',1,nbins-1)) ],[std(N-repmat(N_pre',1,nbins-1)) ]./sqrt(length(Pp)),'k','linewidth',3), hold on
 errorbar([1:nbins-1], [mean(HV-repmat(HV_pre',1,nbins-1)) ],[std(HV-repmat(HV_pre',1,nbins-1)) ]./sqrt(length(Pp)),'r','linewidth',3), hold on
 errorbar([1:nbins-1], [mean(LV-repmat(LV_pre',1,nbins-1)) ],[std(LV-repmat(LV_pre',1,nbins-1)) ]./sqrt(length(Pp)),':m','linewidth',3), hold on
 errorbar([1:nbins-1], [mean(HA-repmat(HA_pre',1,nbins-1)) ],[std(HA-repmat(HA_pre',1,nbins-1)) ]./sqrt(length(Pp)),'b','linewidth',3), hold on
 errorbar([1:nbins-1], [mean(LA-repmat(LA_pre',1,nbins-1)) ],[std(LA-repmat(LA_pre',1,nbins-1)) ]./sqrt(length(Pp)),':c','linewidth',3), hold on
ax = gca;
ax.YLim = [-0.05 .15];   
   

%
figure (889),axis square, hold on
errorbar([1:nbins-1], [mean((HV-repmat(HV_pre',1,nbins-1))-(LV-repmat(LV_pre',1,nbins-1))) ],...
    [std((HV-repmat(HV_pre',1,nbins-1))-(LV-repmat(LV_pre',1,nbins-1))) ]./sqrt(length(Pp)),'color',[0.1 0.5 0.5],'linewidth',3,'marker','x','markersize',8), hold on
errorbar([1:nbins-1]+0.05, [mean((HA-repmat(HA_pre',1,nbins-1))-(LA-repmat(LA_pre',1,nbins-1))) ],...
    [std((HA-repmat(HA_pre',1,nbins-1))-(LA-repmat(LA_pre',1,nbins-1))) ]./sqrt(length(Pp)),'color',[0.5 0.1 0.5],'linewidth',3,'marker','o','markersize',8), hold on
plot([ 0 4],[0 0],':k')
ax = gca;
ax.YLim = [-0.1 .1];   
 


figure (890), hold on
errorbar([1:nbins-1], [mean((HV)-(LV)) ],...
    [std((HV)-(LV)) ]./sqrt(length(Pp)),'r','linewidth',3), hold on
errorbar([1:nbins-1]+0.2, [mean((HA)-(LA)) ],...
    [std((HA)-(LA)) ]./sqrt(length(Pp)),'b','linewidth',3), hold on
  

figure (900), hold on
errorbar([1:nbins-1], [mean((HV-repmat(HV_pre',1,nbins-1))-(N-repmat(N_pre',1,nbins-1))) ],...
    [std((HV-repmat(HV_pre',1,nbins-1))-(N-repmat(N_pre',1,nbins-1))) ]./sqrt(length(Pp)),'r','linewidth',3), hold on
errorbar([1:nbins-1], [mean((LV-repmat(LV_pre',1,nbins-1))-(N-repmat(N_pre',1,nbins-1))) ],...
    [std((LV-repmat(LV_pre',1,nbins-1))-(N-repmat(N_pre',1,nbins-1))) ]./sqrt(length(Pp)),':m','linewidth',3), hold on
[h p11]=ttest((HA-repmat(HA_pre',1,nbins-1)),(LA-repmat(LA_pre',1,nbins-1)))
[h p12]=ttest((HV-repmat(HV_pre',1,nbins-1)),(LV-repmat(LV_pre',1,nbins-1)))
[h p13]= ttest((HA-repmat(HA_pre',1,nbins-1))-(LA-repmat(LA_pre',1,nbins-1)),(HV-repmat(HV_pre',1,nbins-1))-(LV-repmat(LV_pre',1,nbins-1))) %%% for interaction


%%
pre=load('pre_beh5.mat','s','Pp');

HV = s.bin_subBCvH(Pp,:);
LV = s.bin_subBCvL(Pp,:);
HA = s.bin_subSPvH(Pp,:);
LA = s.bin_subSPvL(Pp,:);
N = s.bin_subNeut(Pp,:);

HV_pre= pre.s.subBCvH(Pp)';
LV_pre= pre.s.subBCvL(Pp)';
HA_pre= pre.s.subSPvH(Pp)';
LA_pre= pre.s.subSPvL(Pp)';
N_pre= pre.s.subNeut(Pp)';



figure(17320), axis square, hold on;
boxplot([((HV+HA)./2-(repmat(HV_pre',1,nbins-1)+repmat(HA_pre',1,nbins-1))./2)  ((LV+LA)./2-(repmat(LV_pre',1,nbins-1)+repmat(LA_pre',1,nbins-1))./2) (N-repmat(N_pre',1,nbins-1))])
   

figure(17330), axis square, hold on;
% bar([1 ],[ nanmean((HV+HA)./2-(HV_pre+HA_pre)./2)  ],'r'), hold on
% 
% bar([3],[  nanmean((LV+LA)./2-(LV_pre+LA_pre)./2) ],'b')
% 
% bar([5 ],[  nanmean(N-N_pre)],'k')

errorbar([1.1 2.1], nanmean(HV-repmat(HV_pre',1,nbins-1)),nanstd(HV-repmat(HV_pre',1,nbins-1))./sqrt(length(Pp)),':c','linewidth',3, 'marker','*'), hold on
errorbar([1.2 2.2], nanmean(HA-repmat(HA_pre',1,nbins-1)),nanstd(HA-repmat(HA_pre',1,nbins-1))./sqrt(length(Pp)),':m','linewidth',3, 'marker','*'), hold on

errorbar([1 2],[ nanmean((HV+HA)./2-(repmat(HV_pre',1,nbins-1)+repmat(HA_pre',1,nbins-1))./2)  ],...
    [nanstd((HV+HA)./2-(repmat(HV_pre',1,nbins-1)+repmat(HA_pre',1,nbins-1))./2)  ]./sqrt(length(Pp)),'r','linewidth',3, 'marker','d'), hold on

errorbar([3.1 4.1], nanmean(LV-repmat(LV_pre',1,nbins-1)),nanstd(LV-repmat(LV_pre',1,nbins-1))./sqrt(length(Pp)),':c','linewidth',3, 'marker','*'), hold on
errorbar([3.2 4.2], nanmean(LA-repmat(LA_pre',1,nbins-1)),nanstd(LA-repmat(LA_pre',1,nbins-1))./sqrt(length(Pp)),':m','linewidth',3, 'marker','*'), hold on
errorbar([3 4],[  nanmean((LV+LA)./2-(repmat(LV_pre',1,nbins-1)+repmat(LA_pre',1,nbins-1))./2) ],...
    [ nanstd((LV+LA)./2-(repmat(LV_pre',1,nbins-1)+repmat(LA_pre',1,nbins-1))./2)  ]./sqrt(length(Pp)),'b','linewidth',3, 'marker','d')

errorbar([5 6 ],[  nanmean(N-repmat(N_pre',1,nbins-1))],...
    [ nanstd(N-repmat(N_pre',1,nbins-1))  ]./sqrt(length(Pp)),'k','linewidth',3, 'marker','d')

ax = gca;
ax.XTickLabel = {'High','Low','Neutral'};
ax.XTick = [ 1  3  5 ];
ax.YLim = [-.05 0.1];
plot([0 6],[0 0],':k')
[h p_hh]= ttest((HV+HA)./2,(repmat(HV_pre',1,nbins-1)+repmat(HA_pre',1,nbins-1))./2)  %%%%% this is switch versus no switch
[h p_ll]= ttest((LV+LA)./2,(repmat(LV_pre',1,nbins-1)+repmat(LA_pre',1,nbins-1))./2)  %%%%% this is switch versus no switch
[h p_n]= ttest(N,repmat(N_pre',1,nbins-1))  %%%%% this is switch versus no switch
%[h p_hl]= ttest(((HV+HA)./2-(repmat(HV_pre',1,nbins-1)+repmat(HA_pre',1,nbins-1))./2),((LV+LA)./2-(repmat(LV_pre',1,nbins-1)+repmat(LA_pre',1,nbins-1))./2 ))
%[h p_hl]= ttest((HV+HA)./2-(HV_pre+HA_pre)./2,N-N_pre)
[h p_hl]= ttest((HA-repmat(HA_pre',1,nbins-1))-(LA-repmat(LA_pre',1,nbins-1)),(HV-repmat(HV_pre',1,nbins-1))-(LV-repmat(LV_pre',1,nbins-1))) %%% for interaction


title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', Learning_H P= ' num2str(round(p_hh,3)) ', Learning_L P= ' num2str(round(p_ll,3))...
    ', Learning_N P= ' num2str(round(p_n,3)) ', Difference HIgh and Low P= ' num2str(round(p_hl,3))]);
%% Looking at the n-1 and n trials dependent on the reward

%%
pre=load('pre_beh5.mat','s','Pp');
post1H=load('post_beh5_afterHigh_nminus1.mat','s','Pp');
post2H=load('post_beh5_afterHigh2.mat','s','Pp');

post1L=load('post_beh5_afterLow_nminus1.mat','s','Pp');
post2L=load('post_beh5_afterLow2.mat','s','Pp');

% pre=load('pre_beh5_RT.mat','s','Pp');
% post1H=load('post_beh5_afterHigh_nminus1_correct_RT.mat','s','Pp');
% post2H=load('post_beh5_afterHigh2_correct_RT.mat','s','Pp');
% 
% post1L=load('post_beh5_afterLow_nminus1_correct_RT.mat','s','Pp');
% post2L=load('post_beh5_afterLow2_correct_RT.mat','s','Pp');



n1h= (post1H.s.subBCvH(Pp)'+post1H.s.subBCvL(Pp)'+post1H.s.subSPvH(Pp)'+ post1H.s.subSPvL(Pp)'+post1H.s.subNeut(Pp)')/5;

n1l= (post1L.s.subBCvH(Pp)'+post1L.s.subBCvL(Pp)'+post1L.s.subSPvH(Pp)'+ post1L.s.subSPvL(Pp)'+post1L.s.subNeut(Pp)')/5;


HV1= post2H.s.subBCvH(Pp)';
LV1= post2H.s.subBCvL(Pp)';
HA1= post2H.s.subSPvH(Pp)';
LA1= post2H.s.subSPvL(Pp)';
N1= post2H.s.subNeut(Pp)';

HV2= post2L.s.subBCvH(Pp)';
LV2= post2L.s.subBCvL(Pp)';
HA2= post2L.s.subSPvH(Pp)';
LA2= post2L.s.subSPvL(Pp)';
N2= post2L.s.subNeut(Pp)';

% %%%%% to test all trials
% HV1= s.subBCvH(Pp)';
% LV1= s.subBCvL(Pp)';
% HA1= s.subSPvH(Pp)';
% LA1= s.subSPvL(Pp)';
% N1= s.subNeut(Pp)';
% 
% HV2= s.subBCvH(Pp)';
% LV2= s.subBCvL(Pp)';
% HA2= s.subSPvH(Pp)';
% LA2= s.subSPvL(Pp)';
% N2=  s.subNeut(Pp)';
% %%%to test all trials

HV_pre= pre.s.subBCvH(Pp)';
LV_pre= pre.s.subBCvL(Pp)';
HA_pre= pre.s.subSPvH(Pp)';
LA_pre= pre.s.subSPvL(Pp)';
N_pre= pre.s.subNeut(Pp)';

% HV_pre= zeros(length(Pp),1)';
% LV_pre= zeros(length(Pp),1)';
% HA_pre= zeros(length(Pp),1)';
% LA_pre= zeros(length(Pp),1)';
% N_pre= zeros(length(Pp),1)';


% figure(1810), subplot(2,1,1), hold on;
% bar([1 3 4 6 7 10 13 14 16 17],[nanmean(n1h) nanmean(HV1) nanmean(LV1) nanmean(HA1)...
%     nanmean(LA1) nanmean(n1l) nanmean(HV2) nanmean(LV2) nanmean(HA2)...
%     nanmean(LA2)],1,'w'), axis square, hold on
% errorbar([1 3 4 6 7 10 13 14 16 17],[nanmean(n1h) nanmean(HV1) nanmean(LV1) nanmean(HA1)...
%     nanmean(LA1) nanmean(n1l) nanmean(HV2) nanmean(LV2) nanmean(HA2)...
%     nanmean(LA2)],...
%     [nanstd(n1h) nanstd(HV1) nanstd(LV1) nanstd(HA1)...
%     nanstd(LA1) nanstd(n1l) nanstd(HV2) nanstd(LV2) nanstd(HA2)...
%     nanstd(LA2)]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
% ax = gca;
% ax.XTickLabel = {'Neut','VisHighVal','VisLowVal','AudioHighVal','AudioLowVal'};
% ax = gca;
% ax.YLim = [.6 1];
% 
% figure(1820),  hold on;
% bar([1 3 6 8 11 13 16 18 21 23 ],[nanmean(n1h) nanmean(n1l) nanmean(HV1) nanmean(HV2) nanmean(LV2) nanmean(LV1) nanmean(HA1)...
%      nanmean(HA2) nanmean(LA2)  nanmean(LA1) ],'w'), axis square, hold on
% errorbar([1 3 6 8 11 13 16 18 21 23  ],[nanmean(n1h) nanmean(n1l) nanmean(HV1) nanmean(HV2) nanmean(LV2) nanmean(LV1) nanmean(HA1)...
%      nanmean(HA2) nanmean(LA2)  nanmean(LA1) ],...
%     [nanstd(n1h) nanstd(n1l) nanstd(HV1) nanstd(HV2) nanstd(LV2) nanstd(LV1) nanstd(HA1)...
%      nanstd(HA2) nanstd(LA2)  nanstd(LA1) ]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
% ax = gca;
% ax.XTick = [1 3 6 8 11 13 16 18 21 23  ];
% ax.XTickLabel = {'n-1h','n-1l','VHNS','VHS','VLNS','VLS','AHNS','AHS','ALNS','ALS'};
% ax.YLim = [.6 1];
% 
% % title('Effect of Reward value and modality', 'FontSize', 16);
% % [h p1]= ttest(HV1,LV1);
% % [h p2]= ttest(HA1,LA1);
% % [h p3]= ttest((HV1+HA1)-(HV_pre+HA_pre),(LV1+LA1)-(LV_pre+LA_pre))
% % 
% % [h p4]= ttest(HV2,LV2);
% % [h p5]= ttest(HA2,LA2);
% % [h p6]= ttest((HV2+HA2)-(HV_pre+HA_pre),(LV2+LA2)-(LV_pre+LA_pre))
% % title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
% %     ', N= ' num2str(length(Pp)) ', visual P= ' num2str(round(p1,3)) ', auditory P= ' num2str(round(p2,3))  ', visual2 P= ' num2str(round(p4,3)) ', auditory2 P= ' num2str(round(p5,3))]);
% % % print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
% 
% 
% figure(1821), subplot(2,1,1), hold on;
% bar([1:2:8],[nanmean((HV1+HA1)/2) nanmean((LV1+LA1)/2) nanmean((HV2+HA2)/2) nanmean((LV2+LA2)/2) ],'w'), axis square, hold on
% errorbar([1:2:8],[nanmean((HV1+HA1)/2) nanmean((LV1+LA1)/2) nanmean((HV2+HA2)/2) nanmean((LV2+LA2)/2) ],...
%     [nanstd((HV1+HA1)/2) nanstd((LV1+LA1)/2) nanstd((HV2+HA2)/2) nanstd((LV2+LA2)/2) ]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
% ax = gca;
% ax.XTick = [1:2:8 ];
% ax.XTickLabel = {'After H-H','After H-L','After L-H','After L-L'};
% ax.YLim = [.6 1];
% [h p1]= ttest((HV1+HA1)/2,(LV1+LA1)/2)
% [h p2]= ttest((HV2+HA2)/2,(LV2+LA2)/2)
% [h p3]= ttest((HV1+HA1)/2-(LV1+LA1)/2,(HV2+HA2)/2-(LV2+LA2)/2) %%%%5 interaction
% [h p4]= ttest((HV1+HA1+HV2+HA2)/4,(LV1+LA1+LV2+LA2)/4) %%%%all High,all low
% 
% title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
%     ', N= ' num2str(length(Pp)) ', After High P= ' num2str(round(p1,3)) ', After Low P= ' num2str(round(p2,3)) ', interaction P= ' num2str(round(p3,3)) ]);
% 
% [h p10]= ttest((HA1-LA1)/2,(HA2-LA2)/2) %%%%% this is the interaction
% [h p11]= ttest((HV1-LV1)/2,(HV2-LV2)/2)
% 
% [h p12]= ttest((HA1+HV1)/2,(HA2+HV2)/2)  %%%%% this is switch versus no switch
% 
% [h p13]= ttest((LA1+LV1)/2,(LA2+LV2)/2)  %%%%% this is switch versus no switch
% 
% [h p120]= ttest((HA1+HV1)/2-n1h,(HA2+HV2)/2-n1l)  %%%%% this is switch versus no switch
% 
% [h p130]= ttest((LA1+LV1)/2-n1h,(LA2+LV2)/2-n1l)  %%%%% this is switch versus no switch
% 
% [h p140]= ttest((LA1+LV1)/2-n1l,(HA1+HV1)/2-n1h)   %%%%% high no switch, low switch
% 
% [h p150]= ttest((LA1+LV1)/2-n1l,(HA2+HV2)/2-n1l)   %%%%% high  switch, low switch
% 
[h p1200]= ttest((HV1)-n1h,(HV2)-n1l)  %%%%% this is switch versus no switch

[h p1201]= ttest((HA1)-n1h,(HA2)-n1l)  %%%%% this is switch versus no switch

[h p1300]= ttest((LV1)-n1l,(LV2)-n1h)  %%%%% this is switch versus no switch
[h p1301]= ttest((LA1)-n1l,(LA2)-n1h)  %%%%% this is switch versus no switch
% 
% 
% [h p140]= ttest(((HA1+HV1)/2-n1h)-((HA2+HV2)/2-n1l),((LV2+LA2)/2-n1l)-((LA1+LV1)/2-n1h))  %%%%% this is interaction that the cost of switch is different for high and low reward
% 
% figure(1830), axis square, hold on;
% errorbar([1 2],[ nanmean((HV1+HA1)./2) nanmean((HV2+HA2)./2) ],...
%     [nanstd((HV1+HA1)./2) nanstd((HV2+HA2)./2) ]./sqrt(length(Pp)),'r','linewidth',3), hold on
% 
% errorbar([4 5],[  nanmean((LV2+LA2)./2) nanmean((LV1+LA1)./2)],...
%     [ nanstd((LV2+LA2)./2)  nanstd((LV1+LA1)./2)]./sqrt(length(Pp)),'b','linewidth',3)
% 
% ax = gca;
% ax.XTickLabel = {'No-Switch-H','Switch-H','No-Switch-L','Switch-L'};
% ax.XTick = [ 1 2 4 5];
% ax.YLim = [.75 0.95];
% title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
%     ', N= ' num2str(length(Pp)) ', High Switch vs. No Switch P= ' num2str(round(p12,3)) ', Low Switch vs. No Switch P= ' num2str(round(p13,3))  ]);
% 
% 
% 
% 
% 
% 
% 
% figure(1831), axis square, hold on;
% errorbar([1 2],[ nanmean((HV1+HA1)./2-n1h) nanmean((HV2+HA2)./2-n1l) ],...
%     [nanstd((HV1+HA1)./2-n1h) nanstd((HV2+HA2)./2-n1l) ]./sqrt(length(Pp)),'r','linewidth',3), hold on
% 
% errorbar([4 5 ],[  nanmean((LV2+LA2)./2-n1l) nanmean((LV1+LA1)./2-n1h)],...
%     [ nanstd((LV2+LA2)./2-n1l)  nanstd((LV1+LA1)./2-n1h)]./sqrt(length(Pp)),'b','linewidth',3)
% 
% plot([0 6],[0 0],':k')
% 
% ax = gca;
% ax.XTickLabel = {'No-Switch-H','Switch-H','No-Switch-L','Switch-L'};
% ax.XTick = [ 1 2 4 5];
% %ax.YLim = [-0.02 0.02];
% title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
%     ', N= ' num2str(length(Pp)) ', High Switch vs. No Switch P= ' num2str(round(p120,3)) ', Low Switch vs. No Switch P= ' num2str(round(p130,3))  ]);

figure(1732), axis square, hold on;
errorbar([1 2],[ nanmean((HV1)-n1h) nanmean((HV2)-n1l) ],...
    [nanstd((HV1)-n1h) nanstd((HV2)-n1l) ]./sqrt(length(Pp)),'r','linewidth',3), hold on

errorbar([4 5 ],[  nanmean((LV2)-n1l) nanmean((LV1)-n1h)],...
    [ nanstd((LV2)-n1l)  nanstd((LV1)-n1h)]./sqrt(length(Pp)),'b','linewidth',3)

errorbar([1.1 2.1],[ nanmean((HA1)-n1h) nanmean((HA2)-n1l) ],...
    [nanstd((HV1)-n1h) nanstd((HV2)-n1l) ]./sqrt(length(Pp)),':r','linewidth',3), hold on

errorbar([4.1 5.1 ],[  nanmean((LA2)-n1l) nanmean((LA1)-n1h)],...
    [ nanstd((LA2)-n1l)  nanstd((LA1)-n1h)]./sqrt(length(Pp)),':b','linewidth',3)

plot([0 6],[0 0],':k')

ax = gca;
ax.XTickLabel = {'No-Switch-H','Switch-H','No-Switch-L','Switch-L'};
ax.XTick = [ 1 2 4 5];
%ax.YLim = [-0.02 0.02];
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', HS_HNS_V P= ' num2str(round(p1200,3)) ', HS_HNS_A P= ' num2str(round(p1201,3))...
    ', LS_LNS_V P= ' num2str(round(p1300,3)) ', LS_LNS_A P= ' num2str(round(p1301,3))]);


figure(1833), axis square, hold on;
errorbar([1 2],[ nanmean((HV1+HA1)./2-(HV_pre+HA_pre)./2) nanmean((HV2+HA2)./2-(HV_pre+HA_pre)./2) ],...
    [nanstd((HV1+HA1)./2-(HV_pre+HA_pre)./2) nanstd((HV2+HA2)./2-(HV_pre+HA_pre)./2) ]./sqrt(length(Pp)),'r','linewidth',3), hold on
errorbar([1.1 2.1],[ nanmean((HV1+HV1)./2-(HV_pre+HV_pre)./2) nanmean((HV2+HV2)./2-(HV_pre+HV_pre)./2) ],...
    [nanstd((HV1+HV1)./2-(HV_pre+HV_pre)./2) nanstd((HV2+HV2)./2-(HV_pre+HV_pre)./2) ]./sqrt(length(Pp)),'c','linewidth',3), hold on
errorbar([1.2 2.2],[ nanmean((HA1+HA1)./2-(HA_pre+HA_pre)./2) nanmean((HA2+HA2)./2-(HA_pre+HA_pre)./2) ],...
    [nanstd((HA1+HA1)./2-(HA_pre+HA_pre)./2) nanstd((HA2+HA2)./2-(HA_pre+HA_pre)./2) ]./sqrt(length(Pp)),'m','linewidth',3), hold on

errorbar([4 5],[  nanmean((LV2+LA2)./2-(LV_pre+LA_pre)./2) nanmean((LV1+LA1)./2-(LV_pre+LA_pre)./2)],...
    [ nanstd((LV2+LA2)./2-(LV_pre+LA_pre)./2)  nanstd((LV1+LA1)./2-(LV_pre+LA_pre)./2)]./sqrt(length(Pp)),'b','linewidth',3)
errorbar([4.1 5.1],[ nanmean((LV2+LV2)./2-(LV_pre+LV_pre)./2) nanmean((LV1+LV1)./2-(LV_pre+LV_pre)./2) ],...
    [nanstd((LV1+LV1)./2-(LV_pre+LV_pre)./2) nanstd((LV1+LV1)./2-(LV_pre+LV_pre)./2) ]./sqrt(length(Pp)),'c','linewidth',3), hold on
errorbar([4.2 5.2],[ nanmean((LA2+LA2)./2-(LA_pre+LA_pre)./2) nanmean((LA1+LA1)./2-(LA_pre+LA_pre)./2) ],...
    [nanstd((LA2+LA2)./2-(LA_pre+LA_pre)./2) nanstd((LA1+LA1)./2-(LA_pre+LA_pre)./2) ]./sqrt(length(Pp)),'m','linewidth',3), hold on

errorbar([6 7],[  nanmean(N1-N_pre) nanmean(N2-N_pre)],...
    [ nanstd(N1-N_pre)  nanstd(N2-N_pre)]./sqrt(length(Pp)),'k','linewidth',3)

ax = gca;
ax.XTickLabel = {'No-Switch-H','Switch-H','No-Switch-L','Switch-L','Switch-previousH','Switch-previousL'};
ax.XTick = [ 1 2 4 5 6 7];
ax.YLim = [-.08 0.15];
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', High Switch vs. No Switch P= ' num2str(round(p12,3)) ', Low Switch vs. No Switch P= ' num2str(round(p13,3))  ]);
plot([0 6],[0 0],':k')
[h p12000]= ttest((HV1+HA1)./2-(HV_pre+HA_pre)./2)  %%%%% this is switch versus no switch
[h p12001]= ttest((HV2+HA2)./2-(HV_pre+HA_pre)./2)  %%%%% this is switch versus no switch

[h p12002]= ttest((LV1+LA1)./2-(LV_pre+LA_pre)./2)  %%%%% this is switch versus no switch
[h p12003]= ttest((LV2+LA2)./2-(LV_pre+LA_pre)./2)  %%%%% this is switch versus no switch
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', Learning_H-Noswith P= ' num2str(round(p12000,3)) ', Learning_H-swith P= ' num2str(round(p12001,3))...
    ', Learning_L-Noswith P= ' num2str(round(p12002,3)) ', Learning_L-swith  P= ' num2str(round(p12003,3))]);
[h p12004]= ttest(((HV1+HA1)./2-(HV_pre+HA_pre)./2)-((LV2+LA2)./2-(LV_pre+LA_pre)./2 ))
[h p12005]= ttest(((HA1+HA1)./2-(HA_pre+HA_pre)./2)-((LA1+LA1)./2-(LA_pre+LA_pre)./2 ))
%%
pre=load('pre_beh5.mat','s','Pp');

HV= s.subBCvH(Pp)';
LV= s.subBCvL(Pp)';
HA= s.subSPvH(Pp)';
LA= s.subSPvL(Pp)';
N= s.subNeut(Pp)';


HV_pre= pre.s.subBCvH(Pp)';
LV_pre= pre.s.subBCvL(Pp)';
HA_pre= pre.s.subSPvH(Pp)';
LA_pre= pre.s.subSPvL(Pp)';
N_pre= pre.s.subNeut(Pp)';


post1H=load('post_beh5_afterHigh_nminus1.mat','s','Pp');
post2H=load('post_beh5_afterHigh2.mat','s','Pp');

post1L=load('post_beh5_afterLow_nminus1.mat','s','Pp');
post2L=load('post_beh5_afterLow2.mat','s','Pp');


post1H_c=load('post_beh5_afterHigh_nminus1_correct.mat','s','Pp');
post2H_c=load('post_beh5_afterHigh2_correct.mat','s','Pp');

post1L_c=load('post_beh5_afterLow_nminus1_correct.mat','s','Pp');
post2L_c=load('post_beh5_afterLow2_correct.mat','s','Pp');

post1H_e=load('post_beh5_afterHigh_nminus1_error.mat','s','Pp');
post2H_e=load('post_beh5_afterHigh2_error.mat','s','Pp');

post1L_e=load('post_beh5_afterLow_nminus1_error.mat','s','Pp');
post2L_e=load('post_beh5_afterLow2_error.mat','s','Pp');


n1h= (post1H.s.subBCvH(Pp)'+post1H.s.subBCvL(Pp)'+post1H.s.subSPvH(Pp)'+ post1H.s.subSPvL(Pp)'+post1H.s.subNeut(Pp)')/5;

n1l= (post1L.s.subBCvH(Pp)'+post1L.s.subBCvL(Pp)'+post1L.s.subSPvH(Pp)'+ post1L.s.subSPvL(Pp)'+post1L.s.subNeut(Pp)')/5;


HV1= post2H.s.subBCvH(Pp)';
LV1= post2H.s.subBCvL(Pp)';
HA1= post2H.s.subSPvH(Pp)';
LA1= post2H.s.subSPvL(Pp)';
N1= post2H.s.subNeut(Pp)';

HV2= post2L.s.subBCvH(Pp)';
LV2= post2L.s.subBCvL(Pp)';
HA2= post2L.s.subSPvH(Pp)';
LA2= post2L.s.subSPvL(Pp)';
N2= post2L.s.subNeut(Pp)';


% figure(1732), axis square, hold on;
% boxplot([((HV+HA)./2-(HV_pre+HA_pre)./2)'  ((LV+LA)./2-(LV_pre+LA_pre)./2)' (N-N_pre)'])


figure(1743), axis square, hold on;
% bar([1 ],[ nanmean((HV+HA)./2-(HV_pre+HA_pre)./2)  ],'r'), hold on
%
% bar([3],[  nanmean((LV+LA)./2-(LV_pre+LA_pre)./2) ],'b')
%
% bar([5 ],[  nanmean(N-N_pre)],'k')
% errorbar([1 3],[ nanmean((HV+HA)./2-(HV_pre+HA_pre)./2)  nanmean((LV+LA)./2-(LV_pre+LA_pre)./2)],...
%     [nanstd((HV+HA)./2-(HV_pre+HA_pre)./2) nanstd((LV+LA)./2-(LV_pre+LA_pre)./2) ]./sqrt(length(Pp)),'r','linewidth',3), hold on
% errorbar([1.1 3.1],[ nanmean((HA+HA)./2-(HA_pre+HA_pre)./2) nanmean(LA-LA_pre)  ],...
%     [nanstd((HA+HA)./2-(HA_pre+HA_pre)./2)  nanstd(LA-LA_pre)]./sqrt(length(Pp)),'m','linewidth',3), hold on
% errorbar([1.2 3.2],[ nanmean((HV+HV)./2-(HV_pre+HV_pre)./2)  nanmean(LV-LV_pre) ],...
%     [nanstd((HV+HV)./2-(HV_pre+HV_pre)./2)  nanstd(LV-LV_pre)  ]./sqrt(length(Pp)),'c','linewidth',3), hold on

errorbar([1.3 3.3],[ nanmean((HA1+HA1)./2-(HA_pre+HA_pre)./2) nanmean((LA1+LA1)./2-(LA_pre+LA_pre)./2)  ],...
    [nanstd((HA1+HA1)./2-(HA_pre+HA_pre)./2)  nanstd((LA1+LA1)./2-(LA_pre+LA_pre)./2)  ]./sqrt(length(Pp)),'m','linewidth',1), hold on
errorbar([1.4 3.4],[ nanmean((HA2+HA2)./2-(HA_pre+HA_pre)./2)  nanmean((LA2+LA2)./2-(LA_pre+LA_pre)./2) ],...
    [nanstd((HA2+HA2)./2-(HA_pre+HA_pre)./2)  nanstd((LA2+LA2)./2-(LA_pre+LA_pre)./2)]./sqrt(length(Pp)),'m:','linewidth',1), hold on

errorbar([1.4 3.4],[ nanmean((HV1+HV1)./2-(HV_pre+HV_pre)./2) nanmean((LV1+LV1)./2-(LV_pre+LV_pre)./2)   ],...
    [nanstd((HV1+HV1)./2-(HV_pre+HV_pre)./2)  nanstd((LV1+LV1)./2-(LV_pre+LV_pre)./2) ]./sqrt(length(Pp)),'c','linewidth',1), hold on
errorbar([1.5 3.5],[ nanmean((HV2+HV2)./2-(HV_pre+HV_pre)./2) nanmean((LV2+LV2)./2-(LV_pre+LV_pre)./2) ],...
    [nanstd((HV2+HV2)./2-(HV_pre+HV_pre)./2) nanstd((LV2+LV2)./2-(LV_pre+LV_pre)./2)  ]./sqrt(length(Pp)),':c','linewidth',1), hold on

errorbar([5 ],[  nanmean(N-N_pre)],...
    [ nanstd(N-N_pre)  ]./sqrt(length(Pp)),'kd','linewidth',3)

ax = gca;
ax.XTickLabel = {'High','Low','Neutral'};
ax.XTick = [ 1  3  5 ];
ax.YLim = [-.05 0.1];
plot([0 6],[0 0],':k')
[h p_hh]= ttest((HV+HA)./2-(HV_pre+HA_pre)./2)  %%%%% this is switch versus no switch
[h p_ll]= ttest((LV+LA)./2-(LV_pre+LA_pre)./2)  %%%%% this is switch versus no switch
[h p_n]= ttest((N)-(N_pre))  %%%%% this is switch versus no switch
[h p_hl]= ttest(((HV+HA)./2-(HV_pre+HA_pre)./2),((LV+LA)./2-(LV_pre+LA_pre)./2 ))
[h p_hla]= ttest(((HA+HA)./2-(HA_pre+HA_pre)./2),((LA+LA)./2-(LA_pre+LA_pre)./2 ))
%[h p_hl]= ttest((HV+HA)./2-(HV_pre+HA_pre)./2,N-N_pre)
[h p_hla2]= ttest(((HA1+HV1)./2-(HA_pre+HV_pre)./2),((LA1+LV1)./2-(LA_pre+LV_pre)./2 ))
[h p_hla3]= ttest(((HA1)./1-(HA_pre)./1),((LA1)./1-(LA_pre)./1 ))
[h p_hla3]= ttest(((HA1+HV1)./2-(HA_pre+HV_pre)./2),((LA1+LV1)./2-(LA_pre+LV_pre)./2 ))


title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', Learning_H P= ' num2str(round(p_hh,3)) ', Learning_L P= ' num2str(round(p_ll,3))...
    ', Learning_N P= ' num2str(round(p_n,3)) ', Difference HIgh and Low P= ' num2str(round(p_hl,3))]);
%%
shoulddoRT =0;

if shoulddoRT
    pre=load('pre_beh5_RT.mat','s','Pp');
        
    post2H=load('post_beh5_afterHigh2_RT.mat','s','Pp');
  %  post1L=load('post_beh5_afterLow_nminus1.mat','s','Pp');
    post2L=load('post_beh5_afterLow2_RT.mat','s','Pp');
        

    post2H_c=load('post_beh5_afterHigh2_correct_RT.mat','s','Pp');
    post2L_c=load('post_beh5_afterLow2_correct_RT.mat','s','Pp');
    post2N_c=load('post_beh5_afterNeut2_correct_RT.mat','s','Pp');
    
        
    post2H_e=load('post_beh5_afterHigh2_error_RT.mat','s','Pp');
    post2L_e=load('post_beh5_afterLow2_error_RT.mat','s','Pp');
    post2N_e=load('post_beh5_afterNeut2_error_RT.mat','s','Pp');
    myYLim = [.8 1];   

else
    
    pre=load('pre_beh5.mat','s','Pp');
    post2H=load('post_beh5_afterHigh2.mat','s','Pp');
    post2L=load('post_beh5_afterLow2.mat','s','Pp');
    
    
    post2H_c=load('post_beh5_afterHigh2_correct.mat','s','Pp');
    post2L_c=load('post_beh5_afterLow2_correct.mat','s','Pp');
    post2N_c=load('post_beh5_afterNeut2_correct.mat','s','Pp');
    
    
    post2H_e=load('post_beh5_afterHigh2_error.mat','s','Pp');
    post2L_e=load('post_beh5_afterLow2_error.mat','s','Pp');
    post2N_e=load('post_beh5_afterNeut2_error.mat','s','Pp');
    
    myYLim = [-.1 .1];
end


HV= s.subBCvH(Pp)';
LV= s.subBCvL(Pp)';
HA= s.subSPvH(Pp)';
LA= s.subSPvL(Pp)';
N= s.subNeut(Pp)';


HV_pre= pre.s.subBCvH(Pp)';
LV_pre= pre.s.subBCvL(Pp)';
HA_pre= pre.s.subSPvH(Pp)';
LA_pre= pre.s.subSPvL(Pp)';
N_pre= pre.s.subNeut(Pp)';

% HV_pre= zeros(length(Pp),1)';
% LV_pre= zeros(length(Pp),1)';
% HA_pre= zeros(length(Pp),1)';
% LA_pre= zeros(length(Pp),1)';
% N_pre= zeros(length(Pp),1)';

% n1h= (post1H.s.subBCvH(Pp)'+post1H.s.subBCvL(Pp)'+post1H.s.subSPvH(Pp)'+ post1H.s.subSPvL(Pp)'+post1H.s.subNeut(Pp)')/5;
% 
% n1l= (post1L.s.subBCvH(Pp)'+post1L.s.subBCvL(Pp)'+post1L.s.subSPvH(Pp)'+ post1L.s.subSPvL(Pp)'+post1L.s.subNeut(Pp)')/5;


HV1= post2H.s.subBCvH(Pp)';
LV1= post2H.s.subBCvL(Pp)';
HA1= post2H.s.subSPvH(Pp)';
LA1= post2H.s.subSPvL(Pp)';
N1= post2H.s.subNeut(Pp)';

HV2= post2L.s.subBCvH(Pp)';
LV2= post2L.s.subBCvL(Pp)';
HA2= post2L.s.subSPvH(Pp)';
LA2= post2L.s.subSPvL(Pp)';
N2= post2L.s.subNeut(Pp)';


HV1_c= post2H_c.s.subBCvH(Pp)';
LV1_c= post2H_c.s.subBCvL(Pp)';
HA1_c= post2H_c.s.subSPvH(Pp)';
LA1_c= post2H_c.s.subSPvL(Pp)';
N1_c= post2H_c.s.subNeut(Pp)';

HV2_c= post2L_c.s.subBCvH(Pp)';
LV2_c= post2L_c.s.subBCvL(Pp)';
HA2_c= post2L_c.s.subSPvH(Pp)';
LA2_c= post2L_c.s.subSPvL(Pp)';
N2_c= post2L_c.s.subNeut(Pp)';

HVn_c= post2N_c.s.subBCvH(Pp)';
LVn_c= post2N_c.s.subBCvL(Pp)';
HAn_c= post2N_c.s.subSPvH(Pp)';
LAn_c= post2N_c.s.subSPvL(Pp)';
Nn_c= post2N_c.s.subNeut(Pp)';

HV1_e= post2H_e.s.subBCvH(Pp)';
LV1_e= post2H_e.s.subBCvL(Pp)';
HA1_e= post2H_e.s.subSPvH(Pp)';
LA1_e= post2H_e.s.subSPvL(Pp)';
N1_e= post2H_e.s.subNeut(Pp)';

HV2_e= post2L_e.s.subBCvH(Pp)';
LV2_e= post2L_e.s.subBCvL(Pp)';
HA2_e= post2L_e.s.subSPvH(Pp)';
LA2_e= post2L_e.s.subSPvL(Pp)';
N2_e= post2L_e.s.subNeut(Pp)';

HVn_e= post2N_e.s.subBCvH(Pp)';
LVn_e= post2N_e.s.subBCvL(Pp)';
HAn_e= post2N_e.s.subSPvH(Pp)';
LAn_e= post2N_e.s.subSPvL(Pp)';
Nn_e= post2N_e.s.subNeut(Pp)';

H1_c= (HV1_c+HA1_c)./2;
L1_c= (LV1_c+LA1_c)./2;
H2_c= (HV2_c+HA2_c)./2;
L2_c= (LV2_c+LA2_c)./2;
Hn_c= (HVn_c+HAn_c)./2;
Ln_c= (LVn_c+LAn_c)./2;

H1_e= (HV1_e+HA1_e)./2;
L1_e= (LV1_e+LA1_e)./2;
H2_e= (HV2_e+HA2_e)./2;
L2_e= (LV2_e+LA2_e)./2;
Hn_e= (HVn_e+HAn_e)./2;
Ln_e= (LVn_e+LAn_e)./2;


% figure(1732), axis square, hold on;
% boxplot([((HV+HA)./2-(HV_pre+HA_pre)./2)'  ((LV+LA)./2-(LV_pre+LA_pre)./2)' (N-N_pre)'])

figure(17429), axis square, hold on;

boxplot([ ((HV1_c+HV1_c)./2-(HV_pre+HV_pre)./2 -(LV1_c+LV1_c)./2-(LV_pre+LV_pre)./2)'...
 ((HV2_c+HV2_c)./2-(HV_pre+HV_pre)./2 -(LV2_c+LV2_c)./2-(LV_pre+LV_pre)./2)'...    
((HA1_c+HA1_c)./2-(HA_pre+HA_pre)./2 -(LA1_c+LA1_c)./2-(LA_pre+LA_pre)./2)'....
((HA2_c+HA2_c)./2-(HA_pre+HA_pre)./2 -(LA2_c+LA2_c)./2-(LA_pre+LA_pre)./2)'...
 ((HV1_e+HV1_e)./2-(HV_pre+HV_pre)./2 -(LV1_e+LV1_e)./2-(LV_pre+LV_pre)./2)'...
 ((HV2_e+HV2_e)./2-(HV_pre+HV_pre)./2 -(LV2_e+LV2_e)./2-(LV_pre+LV_pre)./2)'...    
((HA1_e+HA1_e)./2-(HA_pre+HA_pre)./2 -(LA1_e+LA1_e)./2-(LA_pre+LA_pre)./2)'....
((HA2_e+HA2_e)./2-(HA_pre+HA_pre)./2 -(LA2_e+LA2_e)./2-(LA_pre+LA_pre)./2)'],'boxstyle','filled')


figure(17430), axis square, hold on;

errorbar([1.3 3.3],[ nanmean((HA1_c+HA1_c)./2-(HA_pre+HA_pre)./2) nanmean((LA1_c+LA1_c)./2-(LA_pre+LA_pre)./2)  ],...
    [nanstd((HA1_c+HA1_c)./2-(HA_pre+HA_pre)./2)  nanstd((LA1_c+LA1_c)./2-(LA_pre+LA_pre)./2)  ]./sqrt(length(Pp)),'m','linewidth',2), hold on
errorbar([1.5 3.4],[ nanmean((HA2_c+HA2_c)./2-(HA_pre+HA_pre)./2)  nanmean((LA2_c+LA2_c)./2-(LA_pre+LA_pre)./2) ],...
    [nanstd((HA2_c+HA2_c)./2-(HA_pre+HA_pre)./2)  nanstd((LA2_c+LA2_c)./2-(LA_pre+LA_pre)./2)]./sqrt(length(Pp)),'m:','linewidth',2), hold on


errorbar([1.5 3.4],[ nanmean((HV1_c+HV1_c)./2-(HV_pre+HV_pre)./2) nanmean((LV1_c+LV1_c)./2-(LV_pre+LV_pre)./2)   ],...
    [nanstd((HV1_c+HV1_c)./2-(HV_pre+HV_pre)./2)  nanstd((LV1_c+LV1_c)./2-(LV_pre+LV_pre)./2) ]./sqrt(length(Pp)),'c','linewidth',2), hold on
errorbar([1.5 3.5],[ nanmean((HV2_c+HV2_c)./2-(HV_pre+HV_pre)./2) nanmean((LV2_c+LV2_c)./2-(LV_pre+LV_pre)./2) ],...
    [nanstd((HV2_c+HV2_c)./2-(HV_pre+HV_pre)./2) nanstd((LV2_c+LV2_c)./2-(LV_pre+LV_pre)./2)  ]./sqrt(length(Pp)),':c','linewidth',2), hold on

% errorbar([5 ],[  nanmean(N1_c-N_pre)],...
%     [ nanstd(N1_c-N_pre)  ]./sqrt(length(Pp)),'kd','linewidth',2)
% 


errorbar([1.3 3.3]+5,[ nanmean((HA1_e+HA1_e)./2-(HA_pre+HA_pre)./2) nanmean((LA1_e+LA1_e)./2-(LA_pre+LA_pre)./2)  ],...
    [nanstd((HA1_e+HA1_e)./2-(HA_pre+HA_pre)./2)  nanstd((LA1_e+LA1_e)./2-(LA_pre+LA_pre)./2)  ]./sqrt(length(Pp)),'m','linewidth',3), hold on
errorbar([1.5 3.4]+5,[ nanmean((HA2_e+HA2_e)./2-(HA_pre+HA_pre)./2)  nanmean((LA2_e+LA2_e)./2-(LA_pre+LA_pre)./2) ],...
    [nanstd((HA2_e+HA2_e)./2-(HA_pre+HA_pre)./2)  nanstd((LA2_e+LA2_e)./2-(LA_pre+LA_pre)./2)]./sqrt(length(Pp)),'m:','linewidth',3), hold on

errorbar([1.5 3.5]+5,[ nanmean((HV1_e+HV1_e)./2-(HV_pre+HV_pre)./2) nanmean((LV1_e+LV1_e)./2-(LV_pre+LV_pre)./2)   ],...
    [nanstd((HV1_e+HV1_e)./2-(HV_pre+HV_pre)./2)  nanstd((LV1_e+LV1_e)./2-(LV_pre+LV_pre)./2) ]./sqrt(length(Pp)),'c','linewidth',3), hold on
errorbar([1.5 3.5]+5,[ nanmean((HV2_e+HV2_e)./2-(HV_pre+HV_pre)./2) nanmean((LV2_e+LV2_e)./2-(LV_pre+LV_pre)./2) ],...
    [nanstd((HV2_e+HV2_e)./2-(HV_pre+HV_pre)./2) nanstd((LV2_e+LV2_e)./2-(LV_pre+LV_pre)./2)  ]./sqrt(length(Pp)),':c','linewidth',3), hold on

% errorbar([30 ],[  nanmean(N1_e-N_pre)],...
%     [ nanstd(N1_e-N_pre)  ]./sqrt(length(Pp)),'kd','linewidth',3)
% 
% 
% errorbar([1.3 3.3]+10,[ nanmean((H1_c+H1_c)./2-(HA_pre+HV_pre)./2) nanmean((L1_c+L1_c)./2-(LA_pre+LV_pre)./2)  ],...
%     [nanstd((H1_c+H1_c)./2-(HA_pre+HV_pre)./2)  nanstd((L1_c+L1_c)./2-(LA_pre+LV_pre)./2)  ]./sqrt(length(Pp)),'r','linewidth',2), hold on
% errorbar([1.5 3.5]+10,[ nanmean((H2_c+H2_c)./2-(HA_pre+HV_pre)./2)  nanmean((L2_c+L2_c)./2-(LA_pre+LV_pre)./2) ],...
%     [nanstd((H2_c+H2_c)./2-(HA_pre+HV_pre)./2)  nanstd((L2_c+L2_c)./2-(LA_pre+LV_pre)./2)]./sqrt(length(Pp)),'b:','linewidth',2), hold on
% 
% errorbar([1.3 3.3]+20,[ nanmean((H1_e+H1_e)./2-(HA_pre+HV_pre)./2) nanmean((L1_e+L1_e)./2-(LA_pre+LV_pre)./2)  ],...
%     [nanstd((H1_e+H1_e)./2-(HA_pre+HV_pre)./2)  nanstd((L1_e+L1_e)./2-(LA_pre+LV_pre)./2)  ]./sqrt(length(Pp)),'k','linewidth',3), hold on
% errorbar([1.5 3.5]+20,[ nanmean((H2_e+H2_e)./2-(HA_pre+HV_pre)./2)  nanmean((L2_e+L2_e)./2-(LA_pre+LV_pre)./2) ],...
%     [nanstd((H2_e+H2_e)./2-(HA_pre+HV_pre)./2)  nanstd((L2_e+L2_e)./2-(LA_pre+LV_pre)./2)]./sqrt(length(Pp)),'b:','linewidth',3), hold on

ax = gca;
ax.XTickLabel = {'High','Low','Neutral'};
ax.XTick = [ 1  3  5 ];
ax.YLim = myYLim;
plot([0 6],[0 0],':k')
[h p_hh]= ttest((HV+HA)./2-(HV_pre+HA_pre)./2)  %%%%% this is switch versus no switch
[h p_ll]= ttest((LV+LA)./2-(LV_pre+LA_pre)./2)  %%%%% this is switch versus no switch
[h p_n]= ttest((N)-(N_pre))  %%%%% this is switch versus no switch
[h p_hl]= ttest(((HV+HA)./2-(HV_pre+HA_pre)./2),((LV+LA)./2-(LV_pre+LA_pre)./2 ))
[h p_hla]= ttest(((HA+HA)./2-(HA_pre+HA_pre)./2),((LA+LA)./2-(LA_pre+LA_pre)./2 ))
%[h p_hl]= ttest((HV+HA)./2-(HV_pre+HA_pre)./2,N-N_pre)
[h p_hla2]= ttest(((HA1+HV1)./2-(HA_pre+HV_pre)./2),((LA1+LV1)./2-(LA_pre+LV_pre)./2 ))
[h p_hla3]= ttest(((HA1)./1-(HA_pre)./1),((LA1)./1-(LA_pre)./1 ))
[h p_hla4]= ttest(((HA1+HV1)./2-(HA_pre+HV_pre)./2)-((LA1+LV1)./2-(LA_pre+LV_pre)./2 ),((HA2+HV2)./2-(HA_pre+HV_pre)./2)-((LA2+LV2)./2-(LA_pre+LV_pre)./2 ))



title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', Learning_H P= ' num2str(round(p_hh,3)) ', Learning_L P= ' num2str(round(p_ll,3))...
    ', Learning_N P= ' num2str(round(p_n,3)) ', Difference HIgh and Low P= ' num2str(round(p_hl,3))]);



figure(17431), axis square, hold on;

errorbar([1.3 3.3],[ nanmean((H1_c+H1_c)./2-(HA_pre+HV_pre)./2) nanmean((L1_c+L1_c)./2-(LA_pre+LV_pre)./2)  ],...
    [nanstd((H1_c+H1_c)./2-(HA_pre+HV_pre)./2)  nanstd((L1_c+L1_c)./2-(LA_pre+LV_pre)./2)  ]./sqrt(length(Pp)),'color',[1 0.5 0],'marker','d','linewidth',3), hold on
errorbar([1.5 3.5],[ nanmean((H2_c+H2_c)./2-(HA_pre+HV_pre)./2)  nanmean((L2_c+L2_c)./2-(LA_pre+LV_pre)./2) ],...
    [nanstd((H2_c+H2_c)./2-(HA_pre+HV_pre)./2)  nanstd((L2_c+L2_c)./2-(LA_pre+LV_pre)./2)]./sqrt(length(Pp)),'color',[0.4 0.5 0],'marker','x','linestyle',':','linewidth',3), hold on

% errorbar([1.7 3.7],[ nanmean((Hn_c+Hn_c)./2-(HA_pre+HV_pre)./2) nanmean((Ln_c+Ln_c)./2-(LA_pre+LV_pre)./2)  ],...
%     [nanstd((Hn_c+Hn_c)./2-(HA_pre+HV_pre)./2)  nanstd((Ln_c+Ln_c)./2-(LA_pre+LV_pre)./2)  ]./sqrt(length(Pp)),'color',[0.5 0.5 0.5],'marker','d','linewidth',1), hold on


errorbar([1.3 3.3]+5,[ nanmean((H1_e+H1_e)./2-(HA_pre+HV_pre)./2) nanmean((L1_e+L1_e)./2-(LA_pre+LV_pre)./2)  ],...
    [nanstd((H1_e+H1_e)./2-(HA_pre+HV_pre)./2)  nanstd((L1_e+L1_e)./2-(LA_pre+LV_pre)./2)  ]./sqrt(length(Pp)),'color',[1 0.5 0],'marker','d','linewidth',3), hold on
errorbar([1.5 3.5]+5,[ nanmean((H2_e+H2_e)./2-(HA_pre+HV_pre)./2)  nanmean((L2_e+L2_e)./2-(LA_pre+LV_pre)./2) ],...
    [nanstd((H2_e+H2_e)./2-(HA_pre+HV_pre)./2)  nanstd((L2_e+L2_e)./2-(LA_pre+LV_pre)./2)]./sqrt(length(Pp)),'color',[0.4 0.5 0],'marker','x','linestyle',':','linewidth',3), hold on
% errorbar([1.7 3.7]+5,[ nanmean((Hn_e+Hn_e)./2-(HA_pre+HV_pre)./2) nanmean((Ln_e+Ln_e)./2-(LA_pre+LV_pre)./2)  ],...
%     [nanstd((Hn_e+Hn_e)./2-(HA_pre+HV_pre)./2)  nanstd((Ln_e+Ln_e)./2-(LA_pre+LV_pre)./2)  ]./sqrt(length(Pp)),'color',[0.5 0.5 0.5],'marker','d','linewidth',1), hold on



legend('after-High','after-Low','Location','NorthEast')
text(1.5, 1,'Trial n-1 Correct')
text(6.5, 1,'Trial n-1 Error')

ax = gca;
xlabel('Reward Current Trial')
if shoulddoRT
    ylabel('Reaction Time')
else
ylabel('Performance Difference from Baseline')
end
ax.XTickLabel = {'High','Low','High','Low'};
ax.YLim = myYLim;
ax.XTick = [ 1  3  6 8 ];
plot([0 9],[0 0],':k')
[h p_hlce1]= ttest((H1_c-(HA_pre+HV_pre)./2)-(L1_c-(LA_pre+LV_pre)./2),(H2_c-(HA_pre+HV_pre)./2)-(L2_c-(LA_pre+LV_pre)./2))
[h p_hlce2]= ttest((H1_e-(HA_pre+HV_pre)./2)+(H2_e-(HA_pre+HV_pre)./2), (L1_e-(LA_pre+LV_pre)./2)+(L2_e-(LA_pre+LV_pre)./2))
[h p_hlce3]= ttest((H2_c-(HA_pre+HV_pre)./2)-(L2_c-(LA_pre+LV_pre)./2), (H2_e-(HA_pre+HV_pre)./2)-(L2_e-(LA_pre+LV_pre)./2))
[h p_hlce4]= ttest((Hn_c-(HA_pre+HV_pre)./2)-(Ln_c-(LA_pre+LV_pre)./2), (Hn_e-(HA_pre+HV_pre)./2)-(Ln_e-(LA_pre+LV_pre)./2))
%%
anovdata_rew_modality_names = {'HV1_c';'LV1_c';'HA1_c';'LA1_c';...
    'HV2_c';'LV2_c';'HA2_c';'LA2_c';...
    'HV1_e';'LV1_e';'HA1_e';'LA1_e';...
    'HV2_e';'LV2_e';'HA2_e';'LA2_e'};

% HV_pre= zeros(length(Pp),1)';
% LV_pre= zeros(length(Pp),1)';
% HA_pre= zeros(length(Pp),1)';
% LA_pre= zeros(length(Pp),1)';
% N_pre= zeros(length(Pp),1)';
%

anovdata_rew_modality = [HV1_c-HV_pre;LV1_c-LV_pre;HA1_c-HA_pre;LA1_c-LA_pre;...
    HV2_c-HV_pre;LV2_c-LV_pre;HA2_c-HA_pre;LA2_c-LA_pre;...
    HV1_e-HV_pre;LV1_e-LV_pre;HA1_e-HA_pre;LA1_e-LA_pre;...
    HV2_e-HV_pre;LV2_e-LV_pre;HA2_e-HA_pre;LA2_e-LA_pre]';

bimodncond=length(anovdata_rew_modality_names);
subfactor=cell(bimodncond,1);

for  i=1:bimodncond
    subfactor{i}= ['F' num2str(i)];
end

t = array2table([anovdata_rew_modality (1:length(Pp))'],'VariableNames',[subfactor;{'Sujs'}]);
%factorNames = {'modality','reward','latencybin','part'};
factorNames = {'modality','reward','posthl','cor_err'}; % for each part


within = table({'V';'V';'A';'A';'V';'V';'A';'A';'V';'V';'A';'A';'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
    {'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L'},... %reward: high or low
    {'post_h';'post_h';'post_h';'post_h';'post_l';'post_l';'post_l';'post_l';'post_h';'post_h';'post_h';'post_h';'post_l';'post_l';'post_l';'post_l'},... %phase: pre or post
    {'cor';'cor';'cor';'cor';'cor';'cor';'cor';'cor';'err';'err';'err';'err';'err';'err';'err';'err'},... %phase: pre or post
    'VariableNames',factorNames);
%
% fit the repeated measures model
rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);

% run my repeated measures anova here
%[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*posthl*cor_err')
%% Use lme instead

anovdata_rew_modality_names = {'HV1_c';'LV1_c';'HA1_c';'LA1_c';...
    'HV2_c';'LV2_c';'HA2_c';'LA2_c';...
    'HV1_e';'LV1_e';'HA1_e';'LA1_e';...
    'HV2_e';'LV2_e';'HA2_e';'LA2_e'};

% HV_pre= zeros(length(Pp),1)';
% LV_pre= zeros(length(Pp),1)';
% HA_pre= zeros(length(Pp),1)';
% LA_pre= zeros(length(Pp),1)';
% N_pre= zeros(length(Pp),1)';
%

anovdata_rew_modality = [HV1_c-HV_pre;LV1_c-LV_pre;HA1_c-HA_pre;LA1_c-LA_pre;...
    HV2_c-HV_pre;LV2_c-LV_pre;HA2_c-HA_pre;LA2_c-LA_pre;...
    HV1_e-HV_pre;LV1_e-LV_pre;HA1_e-HA_pre;LA1_e-LA_pre;...
    HV2_e-HV_pre;LV2_e-LV_pre;HA2_e-HA_pre;LA2_e-LA_pre]';


rewconds={'H';'L';'H';'L';'H';'L';'H';'L';...
    'H';'L';'H';'L';'H';'L';'H';'L'}';
modalityconds={'Visual';'Visual';'Auditory';'Auditory';'Visual';'Visual';'Auditory';'Auditory';...
    'Visual';'Visual';'Auditory';'Auditory';'Visual';'Visual';'Auditory';'Auditory'}';
pconds={'post_h';'post_h';'post_h';'post_h';'post_l';'post_l';'post_l';'post_l';'post_h';'post_h';'post_h';'post_h';'post_l';'post_l';'post_l';'post_l'}';
corconds ={'cor';'cor';'cor';'cor';'cor';'cor';'cor';'cor';'err';'err';'err';'err';'err';'err';'err';'err'};
  
nsel = length(Pp);
ncond = length(rewconds);
rewfactor = repmat(rewconds,nsel,1); %%%%
modalityfactor = repmat(modalityconds,nsel,1); %%%%
pfactor = repmat(pconds,nsel,1); %%%%
corfactor = repmat(corconds,nsel,1); %%%%

subj = repmat(1:length(Pp),[1,ncond]);

%%%%%now reshape them: column vector
anovdata = anovdata_rew_modality(:);
rewfactor = rewfactor(:); %%%%
modalityfactor = modalityfactor(:); %%%%
pfactor = pfactor(:);
subj = subj(:); %%%%;
%tbl2 = table(subj,modalityfactor,rewfactor,pfactor,anovdata,'VariableNames',{'subjects'  'modalityfactor','rewardfactor','phase','accuracy' });
tbl2 = table(subj,pfactor ,corfactor, modalityfactor,rewfactor,anovdata,'VariableNames',{'subjs' 'previous' 'PreviousAcu' 'modalityfactor','rewardfactor','accuracy' });
lme162 = fitlme(tbl2,'accuracy~(rewardfactor*modalityfactor*previous*PreviousAcu)+ (1|subjs)','DummyVarCoding','effects');%%%%%%%%%%% fixed and random effects: check DummyVArCoding
lme1602 = fitlme(tbl2,'accuracy~(rewardfactor*modalityfactor*previous*PreviousAcu)+ (rewardfactor*modalityfactor*previous*PreviousAcu|subjs)','DummyVarCoding','effects')%%%%%%%%%%% fixed and random effects: check DummyVArCoding
%% Also getting Neutral condition
anovdata_rew_modality_names = {'HV1_c';'LV1_c';'HA1_c';'LA1_c';...
    'HV2_c';'LV2_c';'HA2_c';'LA2_c';...
    'HV1_e';'LV1_e';'HA1_e';'LA1_e';...
    'HV2_e';'LV2_e';'HA2_e';'LA2_e';...
    'HVn_c';'LVn_c';'HAc_c';'Ln_c';...
    'HVn_e';'LVn_e';'HAn_e';'LAn_e'};
anovdata_rew_modality = [HV1_c-HV_pre;LV1_c-LV_pre;HA1_c-HA_pre;LA1_c-LA_pre;...
    HV2_c-HV_pre;LV2_c-LV_pre;HA2_c-HA_pre;LA2_c-LA_pre;...
    HV1_e-HV_pre;LV1_e-LV_pre;HA1_e-HA_pre;LA1_e-LA_pre;...
    HV2_e-HV_pre;LV2_e-LV_pre;HA2_e-HA_pre;LA2_e-LA_pre;...
    HVn_c-HV_pre;LVn_c-LV_pre;HAn_c-HA_pre;LAn_c-LA_pre;...
    HVn_e-HV_pre;LVn_e-LV_pre;HAn_e-HA_pre;LAn_e-LA_pre]';

bimodncond=length(anovdata_rew_modality_names);
subfactor=cell(bimodncond,1);

for  i=1:bimodncond
    subfactor{i}= ['F' num2str(i)];
end

t = array2table([anovdata_rew_modality (1:length(Pp))'],'VariableNames',[subfactor;{'Sujs'}]);
%factorNames = {'modality','reward','latencybin','part'};
factorNames = {'modality','reward','posthl','cor_err'}; % for each part


within = table({'V';'V';'A';'A';'V';'V';'A';'A';'V';'V';'A';'A';'V';'V';'A';'A';'V';'V';'A';'A';'V';'V';'A';'A'},... %modality: visual, auditory, bimodal
    {'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L'},... %reward: high or low
    {'post_h';'post_h';'post_h';'post_h';'post_l';'post_l';'post_l';'post_l';'post_h';'post_h';'post_h';'post_h';'post_l';'post_l';'post_l';'post_l';...
    'post_n';'post_n';'post_n';'post_n';'post_n';'post_n';'post_n';'post_n'},... %phase: pre or post
    {'cor';'cor';'cor';'cor';'cor';'cor';'cor';'cor';'err';'err';'err';'err';'err';'err';'err';'err';'cor';'cor';'cor';'cor';'err';'err';'err';'err'},... %phase: pre or post
    'VariableNames',factorNames);
%
% fit the repeated measures model
rm = fitrm(t,['F1-F' num2str(bimodncond) '~1'],'WithinDesign',within);

% run my repeated measures anova here
%[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*latencybin*part')
[ranovatbl] = ranova(rm, 'WithinModel','modality*reward*posthl*cor_err')
%% Doing LME with also getting Neutral condition: note that we also do LME modelling on data of all trials taken together
anovdata_rew_modality_names = {'HV1_c';'LV1_c';'HA1_c';'LA1_c';...
    'HV2_c';'LV2_c';'HA2_c';'LA2_c';...
    'HV1_e';'LV1_e';'HA1_e';'LA1_e';...
    'HV2_e';'LV2_e';'HA2_e';'LA2_e';...
    'HVn_c';'LVn_c';'HAc_c';'Ln_c';...
    'HVn_e';'LVn_e';'HAn_e';'LAn_e'};
anovdata_rew_modality = [HV1_c-HV_pre;LV1_c-LV_pre;HA1_c-HA_pre;LA1_c-LA_pre;...
    HV2_c-HV_pre;LV2_c-LV_pre;HA2_c-HA_pre;LA2_c-LA_pre;...
    HV1_e-HV_pre;LV1_e-LV_pre;HA1_e-HA_pre;LA1_e-LA_pre;...
    HV2_e-HV_pre;LV2_e-LV_pre;HA2_e-HA_pre;LA2_e-LA_pre;...
    HVn_c-HV_pre;LVn_c-LV_pre;HAn_c-HA_pre;LAn_c-LA_pre;...
    HVn_e-HV_pre;LVn_e-LV_pre;HAn_e-HA_pre;LAn_e-LA_pre]';

rewconds={'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L';'H';'L'}';
modalityconds={'V';'V';'A';'A';'V';'V';'A';'A';'V';'V';'A';'A';'V';'V';'A';'A';'V';'V';'A';'A';'V';'V';'A';'A'}';
pconds={'post_h';'post_h';'post_h';'post_h';'post_l';'post_l';'post_l';'post_l';'post_h';'post_h';'post_h';'post_h';'post_l';'post_l';'post_l';'post_l';...
    'post_n';'post_n';'post_n';'post_n';'post_n';'post_n';'post_n';'post_n'};
corconds = {'cor';'cor';'cor';'cor';'cor';'cor';'cor';'cor';'err';'err';'err';'err';'err';'err';'err';'err';'cor';'cor';'cor';'cor';'err';'err';'err';'err'};
  
nsel = length(Pp);
ncond = length(rewconds);
rewfactor = repmat(rewconds,nsel,1); %%%%
modalityfactor = repmat(modalityconds,nsel,1); %%%%
pfactor = repmat(pconds,nsel,1); %%%%
corfactor = repmat(corconds,nsel,1); %%%%

subj = repmat(1:length(Pp),[1,ncond]);

%%%%%now reshape them: column vector
anovdata = anovdata_rew_modality(:);
rewfactor = rewfactor(:); %%%%
modalityfactor = modalityfactor(:); %%%%
pfactor = pfactor(:);
subj = subj(:); %%%%;
%tbl2 = table(subj,modalityfactor,rewfactor,pfactor,anovdata,'VariableNames',{'subjects'  'modalityfactor','rewardfactor','phase','accuracy' });
tbl2 = table(subj,pfactor ,corfactor, modalityfactor,rewfactor,anovdata,'VariableNames',{'subjs' 'previous' 'PreviousAcu' 'modalityfactor','rewardfactor','accuracy' });
lme163 = fitlme(tbl2,'accuracy~(rewardfactor*modalityfactor*previous*PreviousAcu)+ (1|subjs)','DummyVarCoding','effects');%%%%%%%%%%% fixed and random effects: check DummyVArCoding
lme1603 = fitlme(tbl2,'accuracy~(rewardfactor*modalityfactor*previous*PreviousAcu)+ (rewardfactor*modalityfactor*previous*PreviousAcu|subjs)','DummyVarCoding','effects'); %%%%%%%%%%% fixed and random effects: check DummyVArCoding
bla=compare(lme163,lme1603)
%%

%%
%%%%%%%%%%%%%%%%%%%%% to look at all trials and account for the effect of
pre=load('pre_beh5.mat','s','Pp');
%%%%%%%%%%%%%%%%%%%%% to look at all trials and account for the effect of
%%%%%%%%%%%%%%%%%%%%% the previous trial see figure 11833
shoulddoRT =0;
if shoulddoRT
    pre=load('pre_beh5_RT.mat','s','Pp');
else
    pre=load('pre_beh5.mat','s','Pp');
end
HV_pre= pre.s.subBCvH(Pp)';
LV_pre= pre.s.subBCvL(Pp)';
HA_pre= pre.s.subSPvH(Pp)';
LA_pre= pre.s.subSPvL(Pp)';
N_pre = pre.s.subNeut(Pp)';

% HV_pre= zeros(length(Pp),1);
% LV_pre= zeros(length(Pp),1);
% HA_pre= zeros(length(Pp),1);
% LA_pre= zeros(length(Pp),1);
% N_pre= zeros(length(Pp),1);

maxn = max(max(cellfun(@length,s.vecAccu)));
HV = nan(length(Pp),maxn);
LV = nan(length(Pp),maxn);
HA = nan(length(Pp),maxn);
LA = nan(length(Pp),maxn);
N = nan(length(Pp),maxn);
subj = nan(maxn,length(Pp),4);
rewconds={'H';'L';'H';'L'}';
modalityconds={'Visual';'Visual';'Auditory';'Auditory'}';
binconds={'1st';'2ns'}';
prevtrial = {'high';'low';'neutr'};
prevtrialacu = {'err';'corr'};
%prevtrialacu2 = {'high_ncorrect';'high_correct';'low_ncorrect';'low_correct';'neut'};
prevtrialacu2 = {'high-err';'high-cor';'low-err';'low-cor';'neut'};
prevtrialmod = {'Visual';'Auditory';'Neutr'};
rewfactor = cell(maxn,length(Pp),length(rewconds));
modalityfactor = cell(maxn,length(Pp),length(rewconds));
binfactor = cell(maxn,length(Pp),length(binconds));
accufac = nan(maxn,length(Pp),length(rewconds));
prevtrialfac = cell(maxn,length(Pp),length(binconds));
prevtrialacufac = cell(maxn,length(Pp),length(binconds));
prevtrialacufac2 = cell(maxn,length(Pp),length(binconds));
prevtrialmodfac2 = cell(maxn,length(Pp),length(binconds));

for p=1:length(Pp)
    if shoulddoRT        
        HV(p,1:length(s.vecAccu{Pp(p),3}))= s.vecRT{Pp(p),3}-HV_pre(p);
        LV(p,1:length(s.vecAccu{Pp(p),2}))= s.vecRT{Pp(p),2}-LV_pre(p);
        HA(p,1:length(s.vecAccu{Pp(p),5}))= s.vecRT{Pp(p),5}-HA_pre(p);
        LA(p,1:length(s.vecAccu{Pp(p),4}))= s.vecRT{Pp(p),4}-LA_pre(p);
        N(p,1:length(s.vecAccu{Pp(p),1})) = s.vecRT{Pp(p),1}-N_pre(p);
    else
        HV(p,1:length(s.vecAccu{Pp(p),3}))= s.vecAccu{Pp(p),3}-HV_pre(p);
        LV(p,1:length(s.vecAccu{Pp(p),2}))= s.vecAccu{Pp(p),2}-LV_pre(p);
        HA(p,1:length(s.vecAccu{Pp(p),5}))= s.vecAccu{Pp(p),5}-HA_pre(p);
        LA(p,1:length(s.vecAccu{Pp(p),4}))= s.vecAccu{Pp(p),4}-LA_pre(p);
        N(p,1:length(s.vecAccu{Pp(p),1})) = s.vecAccu{Pp(p),1}-N_pre(p);        
    end
    subj(1:maxn,p,1:4)= p;
    for j =1:length(rewconds)
        rewfactor(1:maxn,p,j) = [ repmat(rewconds(j),maxn,1)]; %%%%
        modalityfactor(1:maxn,p,j) = [ repmat(modalityconds(j),maxn,1)]; %%%%
        binfactor(1:maxn,p,j) = [ repmat(binconds(1),floor(maxn/2),1) ; repmat(binconds(2),ceil(maxn/2),1)]; %%%%
        accufac(1:maxn,p,j) = [ repmat(s.subAccu(Pp(p)),maxn,1)];
        %%%%%% this way for high reward trials we use labels according
        %%%%%% to wehtehr reward assignments stayed the same or not. This is as opposed to look at trials before high or low
        if j==1 | j==3
            repvec = [2 1 0];
        elseif j==2 | j==4
            repvec = [2 1 0];
        end
        
        for n =1:maxn
            if n== 1
                prevtrialfac(n,p,j) = prevtrial(3); %%%%
                prevtrialacufac(n,p,j) = prevtrialacu(1); %%%%
                prevtrialacufac2(n,p,j) = prevtrialacu2(5); %%%%
                prevtrialmodfac2 (n,p,j) = prevtrialmod(3); %%%%
            elseif n>1 & n<= length(s.vecAccu{Pp(p),j})
                %%%%%%%%%%%%%%%%%%%%% consider only reward  of previous trial
                if s.vecCondRew0{Pp(p),j}(n)  == repvec(1)
                    prevtrialfac (n,p,j) = prevtrial(1); %%%%
                elseif s.vecCondRew0{Pp(p),j}(n)  == repvec(2)
                    prevtrialfac (n,p,j) = prevtrial(2); %%%%
                elseif s.vecCondRew0{Pp(p),j}(n)  == 0
                    prevtrialfac (n,p,j) = prevtrial(3); %%%%
                end
                %%%%%%%%%%%%%%%%%%%%% consider only accuracy of previous trial
                if s.vecAccu0{Pp(p),j}(n)  == 1
                    prevtrialacufac(n,p,j) = prevtrialacu(2); %%%%
                elseif s.vecAccu0{Pp(p),j}(n)  == 0
                    prevtrialacufac(n,p,j) = prevtrialacu(1); %%%%
                end
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%this is the main thing: consider the conjunction of  accuracy and reward of previous trial
                if s.vecCondRew0{Pp(p),j}(n)  == repvec(1) && s.vecAccu0{Pp(p),j}(n)  == 1
                    prevtrialacufac2 (n,p,j) = prevtrialacu2(2); %%%%
                elseif s.vecCondRew0{Pp(p),j}(n)  == repvec(1) && s.vecAccu0{Pp(p),j}(n)  == 0
                    prevtrialacufac2 (n,p,j) = prevtrialacu2(1); %%%%
                elseif s.vecCondRew0{Pp(p),j}(n)  == repvec(2) && s.vecAccu0{Pp(p),j}(n)  == 1
                    prevtrialacufac2 (n,p,j) = prevtrialacu2(4); %%%%
                elseif s.vecCondRew0{Pp(p),j}(n)  == repvec(2) && s.vecAccu0{Pp(p),j}(n)  == 0
                    prevtrialacufac2 (n,p,j) = prevtrialacu2(3); %%%%
                elseif s.vecCondRew0{Pp(p),j}(n)  == repvec(3)
                    prevtrialacufac2 (n,p,j) = prevtrialacu2(5); %%%%
                end
                
                
                
                if s.vecCondMod0{Pp(p),j}(n)  == repvec(1)
                    prevtrialmodfac2 (n,p,j) = prevtrialmod(2); %%%%
                elseif s.vecCondMod0{Pp(p),j}(n)  == repvec(2)
                    prevtrialmodfac2 (n,p,j) = prevtrialmod(1); %%%%
                elseif s.vecCondMod0{Pp(p),j}(n)  == repvec(3)
                    prevtrialmodfac2 (n,p,j) = prevtrialmod(3); %%%%
                end
                %%%%%%%%%%%%%%%%%%%%%this is the main thing
            elseif n> length(s.vecCondRew0{Pp(p),j})
                prevtrialfac (n,p,j) = prevtrial(3); %%%%
                prevtrialacufac(n,p,j) = prevtrialacu(1); %%%%
                prevtrialacufac2(n,p,j) = prevtrialacu2(5); %%%%
                prevtrialmodfac2 (n,p,j) = prevtrialmod(3); %%%%
            end
        end
        
    end
end

anovdata = cat(3,HV',LV', HA', LA');
maxi = max(squeeze(max((anovdata)))');
%%%%%%%%%%%%%%%%%%% there has been a lot of discussions about whether RT
%%%%%%%%%%%%%%%%%%% should be transformed or not. Eventually, I preferred
%%%%%%%%%%%%%%%%%%% to follow Kliegle paper: http://read.psych.uni-potsdam.de/PMR2/attachments/article/5/Kliegl.Masson.Richter.VisualCognition.2010.Preprint.pdf
%%%%%%%%%%%%% and not transform. But we subtract RT of baseline
% if shoulddoRT
%     %%%%%%%%%%%%%%%for reaction time we use log of RT because RT distribution
%     %%%%%%%%%%%%%%%is not normal see:  http://read.psych.uni-potsdam.de/PMR2/attachments/article/5/Kliegl.Masson.Richter.VisualCognition.2010.Preprint.pdf
%     %%%%%%%%%%%%%%%%%%https://www.frontiersin.org/articles/10.3389/fpsyg.2015.01171/full 
%     %%%%%%%%%%%%%% http://www.sfs.uni-tuebingen.de/~hbaayen/publications/BaayenMilin2010.pdf
%     anovdata = log(anovdata);
%     % %%%%%%%this is how nomrality is test
%     % This is how it is tested (if it is normal or not): 
%     %%%%%%bla=((squeeze(anovdata(:,1,:))));
%     % [h p] = lillietest(bla(:))
%     %%%%%now reshape them: column vector
% end
anovdata = anovdata(:);
subj = subj(:); %%%%;
rewfactor = rewfactor(:); %%%%
modalityfactor = modalityfactor(:); %%%%
binfactor = binfactor(:); %%%%
prevtrialfac = prevtrialfac(:); %%%%
prevtrialacufac = prevtrialacufac(:);
prevtrialacufac2 = prevtrialacufac2(:);
accufac =  accufac(:);
prevtrialmodfac2 = prevtrialmodfac2(:);
%%%%%%%%%%%%%this one should be worked out
% [h p1]= anovan(anovdata,{subj rewfactor  modalityfactor },'model',[1 0 0  ;0 1 0  ;0 0 1   ;0 1 1   ],'random',1,'varnames',{ 'Subject' 'rewardfactor'   'modalityfactor'    }); %%%%% repeated measure ANOVA

% [h p2 stats1]= anovan(anovdata,{subj rewfactor  modalityfactor prevtrialfac },'model',[1 0 0 0 ;0 1 0 0 ;0 0 1 0 ;0 0 0 1 ;0 1 1 0 ;0 1 0 1 ;...
%     0 0 1 1 ;0 1 1 1 ],'random',1,'varnames',{ 'Subject' 'rewardfactor'   'modalityfactor'  'previous'  }); %%%%% repeated measure ANOVA

% [h p3]= anovan(anovdata,{subj rewfactor  modalityfactor prevtrialfac prevtrialacufac },'model',[1 0 0 0 0;0 1 0 0 0;0 0 1 0 0;0 0 0 1 0;0 0 0 0 1;...
%     0 1 1 0 0;0 1 0 1 0;0 1 0 0 1 ;...
%     0 0 1 1 0 ;0 0 1 0 1; 0 1 1 1 1 ],'random',1,'varnames',{ 'Subject' 'rewardfactor'   'modalityfactor'  'previous' 'previousacu' }); %%%%% repeated measure ANOVA
%
% [h p4]= anovan(anovdata,{subj rewfactor  modalityfactor  prevtrialacufac },'model',[1 0 0 0;0 1 0  0;0 0 1  0;0 0 0  1;...
%     0 1 1  0 ; 0 1 0  1 ;...
%     0 0 1 1  ; 0 1 1  1 ],'random',1,'varnames',{ 'Subject' 'rewardfactor'   'modalityfactor'  'previousacu' }); %%%%% repeated measure ANOVA

[h p22 stats4]= anovan(anovdata,{subj rewfactor  modalityfactor prevtrialfac prevtrialacufac },'model',[1 0 0 0 0 ;0 1 0 0 0;0 0 1 0 0 ;0 0 0 1 0;0 0 0 0 1;...
    0 1 1 0 0 ;0 1 0 1 0; 0 1 0 0 1;...
    0 0 1 1 0; 0 0 1 0 1 ; 0 1 0 1 1; 0 0 1 1 1;0 1 1 1 1 ],'random',1,'varnames',{ 'Subject' 'rewardfactor'   'moalityfactor' 'previus'  'PreviousAcu'  }); %%%%% repeated measure ANOVA


% [h p3]= anovan(anovdata,{subj rewfactor  modalityfactor prevtrialfac binfactor},'model',[1 0 0 0 0;0 1 0 0 0;0 0 1 0 0;0 0 0 1 0 ;0 0 0 0 1; ...
%     0 1 1 0 0;0 1 0 1 0;0 1 0 0 1;...
%     0 0 1 1 0;0 0 1 0 1;...
%     0 1 1 1 1],'random',1,'varnames',{ 'Subject' 'rewardfactor'   'modalityfactor'  'previous'  'bins'}); %%%%% repeated measure ANOVA
%
% [h p4]= anovan(anovdata,{subj rewfactor  modalityfactor binfactor },'model',[1 0 0 0 ;0 1 0 0 ;0 0 1 0 ;0 0 0 1 ;0 1 1 0 ;0 1 0 1 ;...
%     0 0 1 1 ;0 1 1 1 ],'random',1,'varnames',{ 'Subject' 'rewardfactor'   'modalityfactor' 'bins'  }); %%%%% repeated measure ANOVA

tbl2 = table(subj,prevtrialfac,prevtrialacufac,prevtrialacufac2,prevtrialmodfac2, binfactor, modalityfactor,rewfactor,accufac,anovdata,'VariableNames',{'subjs' 'previous' 'PreviousAcu' 'PreviousAcuRew' 'PreviousMod' 'bins' 'modalityfactor','rewardfactor','genaccu','accuracy' });
%%%%%%%%%%%%%%% to set a certain order for dummyfactors codes: you can
%%%%%%%%%%%%%%% comment this
tbl2.PreviousAcuRew = categorical(tbl2.PreviousAcuRew,{'high-err';'high-cor';'low-err';'low-cor';'neut'});
tbl2.modalityfactor = categorical(tbl2.modalityfactor,{'Visual';'Auditory'}); %%%% visual first and then auditory see: https://de.mathworks.com/matlabcentral/answers/146623-reference-dummy-coding-with-matlab-fitlme
tbl2.rewardfactor = categorical(tbl2.rewardfactor,{'H';'L'});
tbl2.PreviousMod = categorical(tbl2.PreviousMod,{'Visual';'Auditory';'Neutr'}); %%%% visual first and then auditory see: https://de.mathworks.com/matlabcentral/answers/146623-reference-dummy-coding-with-matlab-fitlme
tbl2.previous = categorical(tbl2.previous,{'high';'low';'neutr'}); %%%% visual first and then auditory see: https://de.mathworks.com/matlabcentral/answers/146623-reference-dummy-coding-with-matlab-fitlme
tbl2.PreviousAcu = categorical(tbl2.PreviousAcu,{'err';'corr'});
% lme0 = fitlme(tbl2,'accuracy~genaccu+(rewardfactor|subjects)+(modalityfactor|subjects)+(1|subjects)')
% lme1 = fitlme(tbl2,'accuracy~1+(rewardfactor|subjs)+(modalityfactor|subjs)')%%%%%%%%%%% only random effect has nans because data has nans
% lme2 = fitlme(tbl2,'accuracy~1+(rewardfactor|subjs)+(modalityfactor|subjs)+ (previous|subjs)')%%%%%%%%%%% fuxed and random effects
%lme3 = fitlme(tbl2,'accuracy~1+(rewardfactor+modalityfactor+PreviousAcuRew)+(1|subjs)')%%%%%%%%%%% fuxed and random effects
% lme4 = fitlme(tbl2,'accuracy~1+(rewardfactor|subjs)+(modalityfactor|subjs)+ (PreviousAcuRew|subjs)')%%%%%%%%%%% fuxed and random effects
%% Comparison of the results of ANOVAN and linear mixed model (which should be identical)
%%%%%%%%%%%%%%%without interaction term: they re identical
[h p22 stats4]= anovan(anovdata,{subj rewfactor  modalityfactor prevtrialacufac2 },'model',[1 0 0 0 ;0 1 0 0 ;0 0 1 0 ;0 0 0 1 ],'random',1,'varnames',{ 'Subject' 'rewardfactor'   'modalityfactor'  'PreviousAcuRew'  }) %%%%% repeated measure ANOVA
lme3 = fitlme(tbl2,'accuracy~1+(rewardfactor+modalityfactor+PreviousAcuRew)+(1|subjs)','DummyVarCoding','effects')%%%%%%%%%%% fuxed and random effects

%%%%%%%%%%%%%%%with interaction term
%%%%Comparison of the results of ANOVAN and linear mixed model (which should be identical)
%%%%%%%%%%%%%%%with interaction term
%%%%%%%%%%%%%%%% the model assumes main and interaction effect of reward
%%%%%%%%%%%%%%%% and modality and enters previoustrial factors as a
%%%%%%%%%%%%%%%% covariate too
[h p220 stats40]= anovan(anovdata,{subj rewfactor  modalityfactor prevtrialacufac2 },'model',[1 0 0 0 ;0 1 0 0 ;0 0 1 0 ;0 0 0 1 ;0 1 1 0  ],'random',1,'varnames',{ 'Subject' 'rewardfactor'   'modalityfactor'  'PreviousAcuRew'  }) %%%%% repeated measure ANOVA
lme4 = fitlme(tbl2,'accuracy~(rewardfactor*modalityfactor+PreviousAcuRew)+ (1|subjs)','DummyVarCoding','effects')%%%%%%%%%%% fuxed and random effects: check DummyVArCoding
%%%%%%%%%%%%%%%% main reference: https://cran.r-project.org/web/packages/lme4/vignettes/lmer.pdf
%%%%%%%%%%%%https://revistas.usb.edu.co/index.php/IJPR/article/view/807
%%%%%%%%%%http://www.bodowinter.com/tutorial/bw_LME_tutorial2.pdf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% very good review https://psych.wisc.edu/Brauer/BrauerLab/wp-content/uploads/2014/04/Brauer_and_Curtin_LMEMs-2017-Psych_Methods.pdf
%%%%%%%%%%%%%%%%%https://www.frontiersin.org/articles/10.3389/fpsyg.2015.00002/full
%%%%%%%%%%%%%https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5366825/#
%%%%%%%%%%%see https://stats.stackexchange.com/questions/282646/two-way-repeated-measures-linear-mixed-model
%%%%%%%%%%%%%%%%%%to visualize this
%%%%%%%%%%%%%%%%%%%%%https://stats.stackexchange.com/questions/387699/which-random-effects-to-include-in-a-mixed-effects-model
figure(1900),bar([1 2 ],[nanmean(nanmean(HV,2)) nanmean(nanmean(LV,2)) ],'c'), hold on
plot([1 2 ],[(nanmean(HV,2)) (nanmean(LV,2)) ],'oc'), hold on
bar([ 4 5],[ nanmean(nanmean(HA,2)) nanmean(nanmean(LA,2))],'m'), hold on
plot([4 5 ],[(nanmean(HA,2)) (nanmean(LA,2)) ],'om'), hold on
ylabel('Performance difference from base line')
%%%%%%%%%%%%%%%% to plot fitlme estimates
figure(2000),bar(1:7,double(lme4.Coefficients(2:end,2)),'y'), hold on
plot([1:7; 1:7],[double(lme4.Coefficients(2:end,end-1)), double(lme4.Coefficients(2:end,end))]','k'), hold on
set (gca, 'xtick', 1:7, 'xticklabel', lme4.CoefficientNames(2:end))

% % %%%%%%%%%%%if we want to include other fixed effects (e.g. between subjects factor such as overall accuracy of each subject) for which the subjs
% % %%%%%%%%%%%should not be considered as random slope we do
% %  %%%%%%%%%%this is based on the example from Matlab help on lme
% % %  Fit a linear mixed-effects model for miles per gallon in the city, with fixed effects for horsepower, and uncorrelated random effect for intercept and horsepower grouped by the engine type.
% % %
% % % lme = fitlme(tbl,'CityMPG~Horsepower+(1|EngineType)+(Horsepower-1|EngineType)');
% %
% %
% %  lme5 = fitlme(tbl2,'accuracy~(rewardfactor*modalityfactor+PreviousAcuRew+genaccu)+ (1|subjs)+(genaccu-1|subjs)','DummyVarCoding','effects')%%%%%%%%%%% fuxed and random effects: check DummyVArCoding

% lme6 = fitlme(tbl2,'accuracy~(rewardfactor*modalityfactor+PreviousAcuRew)+ (rewardfactor*modalityfactor+PreviousAcuRew|subjs)','DummyVarCoding','effects')%%%%%%%%%%% fixed and random effects: check DummyVArCoding
% lme6 = fitlme(tbl2,'accuracy~(1+rewardfactor*modalityfactor+PreviousAcuRew)+ (1+rewardfactor*modalityfactor+PreviousAcuRew|subjs)','DummyVarCoding','effects')%%%%%%%%%%% fixed and random effects: check DummyVArCoding
% lme5 = fitlme(tbl2,'accuracy~(rewardfactor*PreviousAcuRew+modalityfactor)+ (1|subjs)','DummyVarCoding','effects')%%%%%%%%%%% 
% lme6 = fitlme(tbl2,'accuracy~(rewardfactor*modalityfactor*PreviousAcuRew)+ (1|subjs)','DummyVarCoding','effects')%%%%%%%%%%% fixed and random effects: check DummyVArCoding:https://hal.archives-ouvertes.fr/hal-01668131/document
% lme7 = fitlme(tbl2,'accuracy~(rewardfactor+PreviousAcuRew+modalityfactor)+ (rewardfactor+PreviousAcuRew+modalityfactor|subjs)','DummyVarCoding','effects')%%%%%%%%%%% 
% lme9 = fitlme(tbl2,'accuracy~(rewardfactor*modalityfactor)+ (rewardfactor*PreviousAcuRew*modalityfactor|subjs)','DummyVarCoding','effects')%%%%%%%%%%% 
% 
% lme8 = fitlme(tbl2,'accuracy~(rewardfactor*PreviousAcuRew*modalityfactor)+ (rewardfactor*PreviousAcuRew*modalityfactor|subjs)','DummyVarCoding','effects')%%%%%%%%%%% https://www.frontiersin.org/articles/10.3389/fnhum.2017.00491/full
% 
% bla=compare(lme6,lme8)%%%%%%%%%%%%%%% model 6 and 8 are the best models and model 6 could be selected with only random intercept see https://www.nature.com/articles/s41598-018-31526-y?WT.feed_name=subjects_perception
%%%%%%%%%%%%%%%Random slopes for within-subject factors (trial type) were dropped based on likelihood ratio tests on nested models

lme16 = fitlme(tbl2,'accuracy~(rewardfactor*modalityfactor*previous*PreviousAcu)+ (1|subjs)','DummyVarCoding','effects');%%%%%%%%%%% fixed and random effects: check DummyVArCoding
%  lme16000 = fitlme(tbl2,'accuracy~(rewardfactor*previous*PreviousAcu+modalityfactor)+ (rewardfactor*previous*PreviousAcu+modalityfactor|subjs)','DummyVarCoding','effects')%%%%%%%%%%% fixed and random effects: check DummyVArCoding
%%%%%%%%%%%%%this is the best model 
lme160 = fitlme(tbl2,'accuracy~(rewardfactor*modalityfactor*previous*PreviousAcu)+ (rewardfactor*modalityfactor*previous*PreviousAcu|subjs)','DummyVarCoding','effects')%%%%%%%%%%% fixed and random effects: check DummyVArCoding
%%%%%%%%%%%%%this is the best model
 %lme1600 = fitlme(tbl2,'accuracy~(rewardfactor*modalityfactor*previous)+ (rewardfactor*modalityfactor*previous*PreviousAcu|subjs)','DummyVarCoding','effects')%%%%%%%%%%% fixed and random effects: check DummyVArCoding
save('roman_lmes.mat')
 %%
 %%%%%%%%%%%%%%%% to plot fitlme estimates
nb =size(lme160.Coefficients,1)-1;
mylm =lme160.Coefficients;
mylm_names = lme160.CoefficientNames;
figure(2000),bar(1:nb,double( mylm(2:end,2)),'y'), hold on
plot([1:nb; 1:nb],[double( mylm(2:end,end-1)), double( mylm(2:end,end))]','k'), hold on
set (gca, 'xtick', 1:nb, 'xticklabel', mylm_names(2:end))