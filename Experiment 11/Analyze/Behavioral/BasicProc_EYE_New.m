%% A little bit of GUI
close all;
variab = menu('What?', {'Accuracy','RT','keyPressed','dist'}); variabInd = {'Accuracy','RT','keyPressed','dist'};
prePost = menu('Conditioning?', {'Pre','Post','Conditioning'});

%% Load Data
myPath= 'W:\#Common\Projects\Vakhrushev\Exp11_EEG_EYE_Circle\DATA';             % Folder with subjects
Pp = {'163250929','19997234','215370668','233910659','418529483','855920445'};  % Selected subjects

condInd = {'neutral','VisLowVal','VisHighVal','SndLowVal','SndHighVal'};        % Naiming convention for my conditions
condVal = {[9 10], [1 5], [2 6], [3 7], [4 8]};                                 % And their representation in triggers
meanVal = struct;


%% Calculate performance
for PpFor = 1:length(Pp)
    
    allBlock = [];
    % Extract block structure
    load([myPath filesep Pp{PpFor} filesep Pp{PpFor} '__allDataEyE.mat']);  % Load file with participant's data (eyeLink+)
    for blo = 2:length(block), allBlock = [allBlock block(blo).trials]; end % Combine trials in one block

    % Remove error
    allBlock = allBlock([allBlock.BlPrePost] == prePost);                   % Select desired blocks
    allBlock = allBlock([allBlock.error]==0);                               % Remove error trials
    allBlock = allBlock(abs(zscore([allBlock.RT]))<300 & [allBlock.dist]<1);  % Remove Eye movements
    
    % Mean performance per condition
    for condFor = 1:length(condInd)
        meanVal(PpFor).(condInd{condFor}) = ...
               nanmean([allBlock(ismember([allBlock.conditionType],condVal{condFor})).(variabInd{variab})]); % sum trials per condition
    end
end
meanValues = [nanmean([meanVal.neutral]) nanmean([meanVal.VisHighVal]) nanmean([meanVal.VisLowVal]) nanmean([meanVal.SndHighVal]) nanmean([meanVal.SndLowVal])];
stdValues  = [nanstd([meanVal.neutral]) nanstd([meanVal.VisHighVal]) nanstd([meanVal.VisLowVal]) nanstd([meanVal.SndHighVal]) nanstd([meanVal.SndLowVal])]./sqrt(length(Pp));

%% PLOT
bar(1:5,meanValues,'w'), hold on
errorbar(1:5,meanValues,stdValues,'k','linewidth',3, 'linestyle','none')

ax = gca;
ax.XTickLabel = {'Neut','VisHighVal','VisLowVal','AudioHighVal','AudioLowVal'};
ax.YLim = [min(meanValues)-min(stdValues) max(meanValues)+max(stdValues)];    % Adjust the scale
hline(nanmean([meanVal.neutral]));
ylabel((variabInd{variab}) , 'FontSize', 14);

ax = gca;
title('Effect of Reward value and modality', 'FontSize', 16);
[~, p1]= ttest([meanVal.VisHighVal],[meanVal.VisLowVal]);           % Performing t-test High-low VISUAL
[~, p2]= ttest([meanVal.SndHighVal],[meanVal.SndLowVal]);           % Performing t-test High-low SOUND
title(['PrePost(' num2str(prePost)...
    ') N= ' num2str(length(Pp)) ', V(' num2str(round(p1,2)) ') S(' num2str(round(p2,2)) ')']);