
clear all
close all
fclose all

clc
withEye                 = abs(menu('With Eye Data?', {'Yes','No'})-2);
experiment              = menu('What Experiment?', {'1','2','3','4','5','6','7','8','9','10'});
prepost                 = menu('Conditioning?', {'Pre','Post','Conditioning'});
group                   = menu('Group?', {'ALL','1-4','5-7'});
whichone                = menu('Which measure to use?', {'dPrime','Accuracy','RT','Dist','Efficiency','Key'});
preTrl                  = menu('Only trls aftr:', {'No','CondCondSame','CondCondDiff','ValValSame','ValValDiff',...
    'CueSCueSSame','CueSCueSDiff','RelPosCueSSame','RelPosCueSDiff','GabSGabSSame',...
    'GabSGabSDiff','CueSGabSSame','CueSGabSDiff','GabSCueSSame','GabSCueSDiff'});
betweenCond             = menu('Between PP type?', {'No','SoundHH','SoundHL','VisualMH','VisualML'});
rootdir_fig                 = 'C:\Work\69. SCM Experiment With Reward_05.03\';
prepostVec              = {'Precond','Postcond'};
groupVec                = {'ALL','1-4','5-7'};
zcrit1=6;
zcrit2=1;
scalesel1 = [1 3];%[1 3]
scalesel2 = [.7 .9]; % [.7 0.9]
scalesel3 = [.5 0.9];%[.3 0.9]
scalesel5 = [0 2];%[1 3]
scalesel6 = [1 2];%[1 3]

%% Load DATA
if experiment == 1
    rootdir= 'Y:\#Common\Projects\Behavioral\Data\55.1 SCM SimpleCrossModal\';
    subjects = {'18855566'; '19283746';'29384756';'31463624';'44751960';'96709616';'Roman06';'Tomke'}; %wrong set og Pp
    % subjects = {'18855566';'34214459'; '31463624';'44751960';'96709616'};
    subjects = {'18855566'; '19283746';'29384756';'31463624';'44751960';'96709616';'Tomke';'34214459'}; %wrong set og Pp,'34214459' added by Arezoo: why this subject was not included?
    experiment_name = 'Exp1';
    rel_blocks= 2:5;
    rel_blocks= [9:2:21];
elseif experiment == 2
    rootdir= 'W:\#Common\Projects\Behavioral\Data\Sound only Test 16.01\';
    subjects = {'Adem'; 'Barbara';'Helge';'Hendrik';'Kilian';'Roman1'};
    experiment_name = 'Exp2';
    rel_blocks= 2:9;
elseif experiment == 3
    rootdir= 'W:\#Common\Projects\Behavioral\Data\55.1 SCM Visual_Sound_Only Test 31.01\';
    subjects = {'R9'; 'Helge_01.02';'Hendrik_31.01';'Kilian_02.02';'R7';'R8'};
    experiment_name = 'Exp3';
    rel_blocks = 3:10;
elseif experiment == 4
    rootdir= 'W:\#Common\Projects\Behavioral\Data\60. SCM NewInstructions 21.02\';
    subjects = {'39417064';'31104305';'17012172';'68773507'};
    % subjects = {'31104305'};
    experiment_name = 'Exp4';
    rel_blocks= 2:5;
    rel_blocks= 7:2:21;
elseif experiment == 5
    rootdir= 'Y:\apoores\Data\fromRoman\';
    subjects = {'14592466';'48965498';'58372543';'37074631';'38252522';'47090933';'78749075';'48745299';'60515942';'56624400';'72410876';'21300406';'73028404';'59714179'};
    experiment_name = 'Exp5';
    rel_blocks= 2:5;
    %rel_blocks= 7:2:21;
    %     rel_blocks= 7:2:21;
    rel_blocks = [7:2:21];
elseif experiment == 6
    rootdir= 'Y:\#Common\Projects\Behavioral\Data\79 Simple Cross-Modal - low 26.03\';
    subjects = {'88476163' ,'87238769','58748256','99170697','88476163' ,'87238769','58748256'}; % 88476163 87238769
    experiment_name = 'Exp6';
    rel_blocks= [2 3 10 11];
    %  rel_blocks= [5  15];
    rel_blocks= [5 7 9   13 15 17];
    % rel_blocks= [4 6 8 12 14 16 20 22 24]; % conditioning
    % rel_blocks= [19 21 23 25]; % mixed
elseif experiment == 7
    rootdir= 'W:\apoores\Data\fromRoman\';
    subjects = {'88476163' ,'87238769','58748256','99170697','88476163' ,'87238769','58748256','74620667','18231554','93881044','89140978',...
        '23123662','47130574','74553204','24509065','68630199','71548078','66597433','10579857','13589008'}; %%% subject ,'45003453' had not learned auditory or visual tasks: performance ~ 0.55
    experiment_name = 'Exp7';
    rel_blocks= [2 3 10 11];
    %  rel_blocks= [5  15];
    rel_blocks= [5 7 9 13 15 17];
    % rel_blocks= [4 6 8 12 14 16 20 22 24]; % conditioning
    % rel_blocks= [19 21 23 25]; % mixed
elseif experiment == 8 %(all together)
    rootdir= 'W:\rvakhru\AllParticipants\';
    rootdir= 'C:\Users\rvakhru\Desktop\AllParticipants\';
    if group == 1
        subjects = {'88476163' ,'87238769','58748256','99170697','88476163' ,'87238769','58748256',...%%%% exp7
            '74620667','18231554','93881044','89140978','23123662','47130574','74553204','24509065','68630199','71548078','66597433','10579857','13589008',...%%%% exp6
            '14592466','48965498','58372543','37074631','38252522','47090933','78749075','48745299','60515942','56624400','72410876','21300406','73028404','59714179',...%%%% exp5
            '39417064','31104305','17012172','68773507',...%%%% exp4
            '18855566', '19283746','29384756','31463624','44751960','96709616','34214459'}; %,'Tomke'%%% exp1
    elseif group == 3
        subjects = {'88476163' ,'87238769','58748256','99170697','88476163' ,'87238769','58748256',...%%%% exp7
            '74620667','18231554','93881044','89140978','23123662','47130574','74553204','24509065','68630199','71548078','66597433','10579857','13589008',...%%%% exp6
            '14592466','48965498','58372543','37074631','38252522','47090933','78749075','48745299','60515942','56624400','72410876','21300406','73028404','59714179'};%%%% exp5
    elseif group == 2
        subjects = {'39417064','31104305','17012172','68773507',...%%%% exp4
            '18855566', '19283746','29384756','31463624','44751960','96709616','34214459'}; %,'Tomke'%%% exp1
    end
    experiment_name = 'Exp8';
elseif experiment == 9 %(all together)
    rootdir= 'W:\#Common\Projects\Behavioral\Data\83_SimpleCrossModal_140618_29d2d_EYE\';
    subjects = {'41497164','98326155','50618557','13946359','77749274'};
    experiment_name = 'Exp9';
elseif experiment == 10 %(all together)
    rootdir= 'C:\Users\rvakhru\Desktop\New_folder\';
    %     subjects = {'443322','1'};
    start_path = fullfile(rootdir);
    myPath = uigetdir(start_path);
    folders = dir(myPath);
    
    subjects ={folders.name};
    subjects = subjects([folders.isdir]);
    subjects = subjects(3:end);
    experiment_name = 'Exp10';
end
delete ([myPath filesep 'sizeData.txt']);
textfile = fopen([myPath filesep 'sizeData.txt'],'wt');
fprintf(textfile,'Pp\texpMode\tPrepost\tV_S_L\tV_S_H\tV_M_L\tV_M_H\tV_L_L\tV_L_H\tA_S_L\tA_S_H\tA_M_L\tA_M_H\tA_L_L\tA_L_H\tN_S_N\tN_M_N\tN_L_N\n');
%% Predefine Variables
s.subAccu = zeros(length(subjects),1);
s.subSP_H = zeros(length(subjects),1);
s.subSP_L = zeros(length(subjects),1);
s.subBC_M = zeros(length(subjects),1);
s.subBC_O = zeros(length(subjects),1);
s.subNeut = zeros(length(subjects),1);
s.subSVol = zeros(length(subjects),3);
s.subBCvH = zeros(length(subjects),1);
s.subBCvL = zeros(length(subjects),1);
s.subSPvH = zeros(length(subjects),1);
s.subSPvL = zeros(length(subjects),1);
s.subRewV = zeros(length(subjects),2);
s.subDPri = zeros(length(subjects),2);

% RelPosition SOUND
s.subA    = zeros(length(subjects),1);
s.subAs   = zeros(length(subjects),1);
s.subAd   = zeros(length(subjects),1);
s.subSPdH = zeros(length(subjects),1);
s.subSPdL = zeros(length(subjects),1);
s.subSPsH = zeros(length(subjects),1);
s.subSPsL = zeros(length(subjects),1);
s.subSPvdH = zeros(length(subjects),1);
s.subSPvdL = zeros(length(subjects),1);
s.subSPvsH = zeros(length(subjects),1);
s.subSPvsL = zeros(length(subjects),1);

% RelPosition VISUAL
s.subV    = zeros(length(subjects),1);
s.subVs   = zeros(length(subjects),1);
s.subVd   = zeros(length(subjects),1);
s.subBCdO = zeros(length(subjects),1);
s.subBCdM = zeros(length(subjects),1);
s.subBCsO = zeros(length(subjects),1);
s.subBCsM = zeros(length(subjects),1);
s.subBCvdH = zeros(length(subjects),1);
s.subBCvdL = zeros(length(subjects),1);
s.subBCvsH = zeros(length(subjects),1);
s.subBCvsL = zeros(length(subjects),1);
s.moveav_subBCvH= nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.moveav_subBCvL= nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.moveav_subSPvH= nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.moveav_subSPvL= nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.effect_ColorValue = nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials
s.effect_SoundValue = nan(length(subjects),100); %%%%%%%%%%%% initialize for 100 trials

% FrameSize
s.subFSvL = zeros(length(subjects),1);
s.subFSvH = zeros(length(subjects),1);
s.subFMvL = zeros(length(subjects),1);
s.subFMvH = zeros(length(subjects),1);
s.subFLvL = zeros(length(subjects),1);
s.subFLvH = zeros(length(subjects),1);
s.subFSBvL= zeros(length(subjects),1);
s.subFSBvH= zeros(length(subjects),1);
s.subFMBvL= zeros(length(subjects),1);
s.subFMBvH= zeros(length(subjects),1);
s.subFLBvL= zeros(length(subjects),1);
s.subFLBvH= zeros(length(subjects),1);
s.subFSAvL= zeros(length(subjects),1);
s.subFSAvH= zeros(length(subjects),1);
s.subFMAvL= zeros(length(subjects),1);
s.subFMAvH= zeros(length(subjects),1);
s.subFLAvL= zeros(length(subjects),1);
s.subFLAvH= zeros(length(subjects),1);
s.subFSN  = zeros(length(subjects),1);
s.subFMN  = zeros(length(subjects),1);
s.subFLN  = zeros(length(subjects),1);

for i=[1:length(subjects)-1]
    
    %     if experiment ==8 && i<=7
    %         rel_blocks = [5 7 9 13 15 17 19 21 23 25 ]; %%%last 4 blocks are mixed
    %         rel_blocks = [5 7 9 13 15 17 ];
    %         %rel_blocks= [2 3 10 11];    %%%%baseline
    %     elseif   experiment ==8 && i>7  && i<21  %%%% Arezoo's crossmodal, Roman's visual
    %         rel_blocks = [5 7 9];
    %         %rel_blocks= [2 3 ];        %%%%baseline
    %     elseif experiment ==8 && i>=21  && i<=38
    %         rel_blocks = [7:2:21];
    %         %rel_blocks= 2:5;           %%%%baseline
    %     elseif experiment ==8 && ismember(i,[40  41 ])
    %         rel_blocks = [9:2:23];
    %         %rel_blocks= 2:5;           %%%%baseline
    %     elseif experiment ==8 && ismember(i,[39  42 43 44 45 ])
    %         rel_blocks = [7:2:21];
    %         %rel_blocks= 2:5;           %%%%baseline
    %     end
    %
    %     if experiment ==7 && i>7
    %         rel_blocks = [5 7 9];
    %         % rel_blocks = [ 2 3];
    %     elseif experiment ==7 && i<=7
    %         rel_blocks = [13 15 17 ];
    %         % rel_blocks = [ 10 11];
    %         rel_blocks= [2 3 10 11];
    %     end
    %
    %     if experiment ==1 && ismember(i,[2 3 7])
    %         rel_blocks = [9:2:23];
    %         % rel_blocks = [ 2 3];
    %     elseif experiment ==1 && ~ismember(i,[2 3 7])
    %         rel_blocks = [7:2:21 ];
    %         % rel_blocks = [ 10 11];
    %     end
    if withEye, load([myPath filesep subjects{i} filesep subjects{i} '__allDataEyE.mat']); vecDist = [];
    else        load([myPath filesep subjects{i} filesep subjects{i} '__allData.mat']);
    end
    vecAccu =[];  vecSoPi =[]; vecGabOALL = [];
    vecCond =[];  vecBoxC =[]; vecBoxS = [];
    vecSoVo =[];  vecValu =[];
    vecGabO =[];  vecRelP =[]; vecRT=[];
    vccboxXLoc= []; vccgaborXLoc= [];
    vectonset= [];
    vecError =[];
    %rel_blocks =rel_blocks(1:end);
    rel_blocks =2:length(block);
    for j=rel_blocks % gather information into vectors
        if ~isempty(block(j).trials)
            RelTrl = [block(j).trials.BlPrePost] == prepost;%&[block(j).trials.BlNumber] ~= 1;
            % combine information from Pp into one vector
            
            %%% to analyze only the first 16 trials, put the rest to error
            %         for b=[17:length([block(j).trials.error])]
            %             block(j).trials(b).error=1;
            %         end
            tempSt = sum([[block(j).trials.SL]; [block(j).trials.BL]]);
            for oi = length(block(j).trials):-1:2
                if preTrl == 1
                    % do nothing
                elseif preTrl == 2
                    if block(j).trials(oi-1).condition == block(j).trials(oi).condition % only trl that followed by same cond.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 3
                    if block(j).trials(oi-1).condition == block(j).trials(oi).condition % only trl that followed by same cond.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 4
                    if block(j).trials(oi).StimValue == 1 || block(j).trials(oi).StimValue == 0 % only trl after highReward.
                        block(j).trials(oi-1) = [];
                    end
                elseif preTrl == 5
                    if block(j).trials(oi).StimValue == 2 || block(j).trials(oi).StimValue == 0 % only trl after highReward.
                        block(j).trials(oi-1) = [];
                    end
                elseif preTrl == 6
                    if tempSt(oi) ~= tempSt(oi-1) % only trl after highReward.
                        block(j).trials(oi-1) = [];
                    end
                elseif preTrl == 7
                    if tempSt(oi) == tempSt(oi-1) % only trl after highReward.
                        block(j).trials(oi-1) = [];
                    end
                elseif preTrl == 8
                    if block(j).trials(oi-1).relativePos ~= tempSt(oi) % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 9
                    if block(j).trials(oi-1).relativePos == tempSt(oi) % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 10
                    if block(j).trials(oi-1).GL ~= block(j).trials(oi).GL  % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 11
                    if block(j).trials(oi-1).GL == block(j).trials(oi).GL  % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 12
                    if tempSt(oi-1) ~= block(j).trials(oi).GL  % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 13
                    if tempSt(oi-1) == block(j).trials(oi).GL  % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 14
                    if block(j).trials(oi-1).GL ~= tempSt(oi)  % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                elseif preTrl == 15
                    if block(j).trials(oi-1).GL == tempSt(oi)  % only trl after highReward.
                        block(j).trials(oi) = [];
                    end
                end
            end
            
            if whichone == 6
                vecAccu = [vecAccu block(j).trials([block(j).trials.error]==0&RelTrl).keyPressed]; % Accuracy keyPressed
            else
                vecAccu = [vecAccu block(j).trials([block(j).trials.error]==0&RelTrl).Accuracy]; % Accuracy keyPressed
            end
            
            
            %             block(j).trials = block(j).trials([block(j).trials.gaborOri]==-1);
            %             RelTrl = [block(j).trials.BlPrePost] == prepost%&[block(j).trials.BlNumber] ~= 1;
            vecRT   = [vecRT   block(j).trials([block(j).trials.error]==0&RelTrl).RT];
            vecSoPi = [vecSoPi block(j).trials([block(j).trials.error]==0&RelTrl).SP];
            vecBoxC = [vecBoxC block(j).trials([block(j).trials.error]==0&RelTrl).BC];
            vecCond = [vecCond block(j).trials([block(j).trials.error]==0&RelTrl).condition];
            vecValu = [vecValu block(j).trials([block(j).trials.error]==0&RelTrl).StimValue];
            vecSoVo = [vecSoVo block(j).trials([block(j).trials.error]==0&RelTrl).SVol];
            vecRelP = [vecRelP block(j).trials([block(j).trials.error]==0&RelTrl).relativePos];
            vecGabO = [vecGabO block(j).trials([block(j).trials.error]==0&RelTrl).gaborOri];
            vecBoxS = [vecBoxS block(j).trials([block(j).trials.error]==0&RelTrl).boxSize];
            vccboxXLoc= [vccboxXLoc block(j).trials([block(j).trials.error]==0&RelTrl).boxXLoc];
            if withEye, vecDist = [vecDist block(j).trials([block(j).trials.error]==0&RelTrl).dist];end
            gaborXLoc =[block(j).trials([block(j).trials.error]==0&RelTrl).GL];
            vccgaborXLoc= [vccgaborXLoc gaborXLoc];
            vectonset = [vectonset [ block(j).trials([block(j).trials.error]==0&RelTrl).targetOnset]-...
                [block(j).trials([block(j).trials.error]==0&RelTrl).fixationOnset]];
            vecGabOALL = [vecGabOALL block(j).trials(RelTrl).gaborOri];
            vecError = [vecError block(j).trials(RelTrl).error];
            fprintf('Block:%d Rel:%d\n',j,sum(RelTrl));
        end
    end
    %     vecBoxS = repmat(1,1,length(vecRelP));
    %     % combine information from Pp into one vector
    %     trls=10:40;%length(vecAccu);
    %     trls = find(abs(zscore(vecRT))<zcrit1 & vecDist<=median(vecDist));
    if withEye, trls = find(abs(zscore(vecRT))<zcrit1 & vecDist<zcrit2);
    else trls = (1:length(vecRT));
    end
    %     if any([block(2).trials.DRIFTCORRECT_t]>0), driftT = 1; else driftT = 0; end
    if withEye,
        fprintf('%d | %s : %d %d\n',length(subjects)-i,subjects{i},...
            length(find(abs(vecDist)>zcrit2)),length(find(abs(vecDist)<zcrit2)));
    end
    s.strange_RT{i}=vecRT(abs(zscore(vecRT))>zcrit1);
    trls =trls(1:end);
    vecAccu = vecAccu(trls);
    vecRT   = vecRT(trls);
    if withEye,vecDist = vecDist(trls);end
    vecSoPi = vecSoPi(trls);
    vecBoxC = vecBoxC(trls);
    vecCond = vecCond(trls);
    vecValu = vecValu(trls);
    vecSoVo = vecSoVo(trls);
    vecRelP = vecRelP(trls);
    vecGabO = vecGabO(trls);
    vecBoxS = vecBoxS(trls);
    vccboxXLoc= vccboxXLoc(trls);
    vccgaborXLoc= vccgaborXLoc(trls);
    vectonset = vectonset(trls);
    
    vecError = vecError(trls);
    %%%%%%%%%%% to remove outliers
    outlier = abs(zscore(vecRT))>zcrit1; %%%% to remove slow RTs use>4;
    %%%%%%%%%%%%% to keep track of repetitions
    GL_reps = [1 diff(vccgaborXLoc)]; %%% keeps track of repetitions of the side
    
    
    
    % fill with nanmean values of desired conditions across subjects
    s.subAccu(i)  = nanmean(vecAccu);                            % accuracy
    s.subRT(i)    = nanmean(vecRT);                            % RT
    
    
    hits       =     nanmean(vecAccu(vecGabO== 1));
    fA      = nanmean( 1 - vecAccu(vecGabO==-1));
    N = sum(vecGabO==-1); %%%%% for correction of h and fA when they are 1 or 0 see http://www.kangleelab.com/sdt-d-prime-calculation---other-tips.html
    s.subDPri(i)  = Dprime2(hits,fA,N);                % dPrime
    %     s.subSVol(i,1)= unique(vecSoVo(vecSoPi==0));  % No    sound
    %     s.subSVol(i,2)= unique(vecSoVo(vecSoPi==1));  % Quite sound
    %     s.subSVol(i,3)= unique(vecSoVo(vecSoPi==2));  % Loud  sound
    s.subRewV(i,:)= inf.rewardValueB;             % Value of cues (mode)
    s.subRewS(i,:)= inf.rewardValueS;
    s.subleanV{i}= [inf.learn.Visual];
    s.rel_blocks{i}= rel_blocks;
    s.age{i}= [inf.age];
    if isfield(inf.learn,'Sound')
        s.subleanS{i}= [inf.learn.Sound];
    else
        s.subleanS{i}= 1;
    end
    s.subPSE(i)= unique([block(2).trials.PSE]);
    %% Process in dPrime
    selection{1} = (vecCond==0);                            % SubNeut
    
    selection{2} = (vecCond==1 & vecBoxC==1);               % SubBC_O
    selection{3} = (vecCond==1 & vecBoxC==2);               % SubBC_M
    selection{4} = (vecCond==1 & vecValu==1);               % SubBCvL
    selection{5} = (vecCond==1 & vecValu==2);               % SubBCvH
    
    selection{6} = (vecCond==2 & vecSoPi==1);               % SubSP_L
    selection{7} = (vecCond==2 & vecSoPi==2);               % SubSP_H
    selection{8} = (vecCond==2 & vecValu==1);               % SubSPvL
    selection{9} = (vecCond==2 & vecValu==2);               % SubSPvH
    
    %RelPosition VISUAL SIDE
    selection{10} = (vecCond==1 & vecBoxC==1 & vecRelP==1); % SubBCsO
    selection{11} = (vecCond==1 & vecBoxC==2 & vecRelP==1); % SubBCsM
    selection{12} = (vecCond==1 & vecBoxC==1 & vecRelP==2); % SubBCdO
    selection{13} = (vecCond==1 & vecBoxC==2 & vecRelP==2); % SubBCdM
    %RelPosition SOUND SIDE
    selection{14} = (vecCond==2 & vecSoPi==1 & vecRelP==1); % SubSPsL
    selection{15} = (vecCond==2 & vecSoPi==2 & vecRelP==1); % SubSPsH
    selection{16} = (vecCond==2 & vecSoPi==1 & vecRelP==2); % SubSPdL
    selection{17} = (vecCond==2 & vecSoPi==2 & vecRelP==2); % SubSPdH
    
    %RelPosition VISUAL VALUE
    selection{18} = (vecCond==1 & vecValu==1 & vecRelP==1); % SubBCvsL
    selection{19} = (vecCond==1 & vecValu==2 & vecRelP==1); % SubBCvsH
    selection{20} = (vecCond==1 & vecValu==1 & vecRelP==2); % SubBCvdL
    selection{21} = (vecCond==1 & vecValu==2 & vecRelP==2); % SubBCvdH
    
    %RelPosition SOUND VALUE
    selection{22} = (vecCond==2 & vecValu==1 & vecRelP==1); % SubSPvsL
    selection{23} = (vecCond==2 & vecValu==2 & vecRelP==1); % SubSPvsH
    selection{24} = (vecCond==2 & vecValu==1 & vecRelP==2); % SubSPvdL
    selection{25} = (vecCond==2 & vecValu==2 & vecRelP==2); % SubSPvdH
    
    selection{26} = (vecCond==1 );                          % SubV
    selection{27} = (vecCond==2 );                          % SubA
    
    selection{28} = (vecCond==1 & vecRelP==1);               % SubVs
    selection{29} = (vecCond==1 & vecRelP==2);               % SubVd
    
    selection{30} = (vecCond==2 & vecRelP==1);               % SubAs
    selection{31} = (vecCond==2 & vecRelP==2);               % SubAd
    
    selection{32} = (vecBoxS==1 & vecValu==1);              % SubFSvL
    selection{33} = (vecBoxS==1 & vecValu==2);              % SubFSvH
    selection{34} = (vecBoxS==2 & vecValu==1);              % SubFMvL
    selection{35} = (vecBoxS==2 & vecValu==2);              % SubFMvH
    selection{36} = (vecBoxS==3 & vecValu==1);              % SubFLvL
    selection{37} = (vecBoxS==3 & vecValu==2);              % SubFLvH
    
    selection{38} = (vecBoxS==1 & vecValu==1& vecCond==1);              % SubFSBvL
    selection{39} = (vecBoxS==1 & vecValu==2& vecCond==1);              % SubFSBvH
    selection{40} = (vecBoxS==2 & vecValu==1& vecCond==1);              % SubFMBvL
    selection{41} = (vecBoxS==2 & vecValu==2& vecCond==1);              % SubFMBvH
    selection{42} = (vecBoxS==3 & vecValu==1& vecCond==1);              % SubFLBvL
    selection{43} = (vecBoxS==3 & vecValu==2& vecCond==1);              % SubFLBvH
    
    selection{44} = (vecBoxS==1 & vecValu==1& vecCond==2);              % SubFSAvL
    selection{45} = (vecBoxS==1 & vecValu==2& vecCond==2);              % SubFSAvH
    selection{46} = (vecBoxS==2 & vecValu==1& vecCond==2);              % SubFMAvL
    selection{47} = (vecBoxS==2 & vecValu==2& vecCond==2);              % SubFMAvH
    selection{48} = (vecBoxS==3 & vecValu==1& vecCond==2);              % SubFLAvL
    selection{49} = (vecBoxS==3 & vecValu==2& vecCond==2);              % SubFLAvH
    
    selection{50} = (vecBoxS==1 & vecCond==0);              % SubFSN
    selection{51} = (vecBoxS==2 & vecCond==0);              % SubFSN
    selection{52} = (vecBoxS==3 & vecCond==0);              % SubFSN
    
    forProcess = {'subNeut';...
        'subBC_O';'subBC_M';'subBCvL';'subBCvH';...
        'subSP_L';'subSP_H';'subSPvL';'subSPvH';...
        'subBCsO';'subBCsM';'subBCdO';'subBCdM';...
        'subSPsL';'subSPsH';'subSPdL';'subSPdH';...
        'subBCvsL';'subBCvsH';'subBCvdL';'subBCvdH';...
        'subSPvsL';'subSPvsH';'subSPvdL';'subSPvdH';...
        'subV';'subA';'subVs';'subVd';...
        'subAs';'subAd';'subFSvL';'subFSvH';...
        'subFMvL';'subFMvH';'subFLvL';'subFLvH';...
        'subFSBvL';'subFSBvH';'subFMBvL';'subFMBvH';...
        'subFLBvL';'subFLBvH';'subFSAvL';'subFSAvH';...
        'subFMAvL';'subFMAvH';'subFLAvL';'subFLAvH';...
        'subFSN';'subFMN';'subFLN'};
    for step = 1:length(forProcess)
        
        mytrls = find(selection{step}&outlier==0);
        hits                =   nanmean(vecAccu(mytrls(vecGabO(mytrls)== 1)));
        fA               =   nanmean(1-nanmean(vecAccu(mytrls(vecGabO(mytrls)== -1))));
        N = length((mytrls(vecGabO(mytrls)== -1)));
%         N = 100;
        if whichone ==1
            s.(forProcess{step})(i) = Dprime2(hits,fA,N);                % dPrime
            l(i).(forProcess{step}) = Dprime2(hits,fA,N);                % dPrime
        elseif whichone ==2
            s.(forProcess{step})(i) = nanmean(vecAccu(selection{step}&  outlier==0))./nanmean(vecRT(selection{step}&  outlier==0)); %%%%% for efficiency, i.e. accuracy/RT
            s.(forProcess{step})(i) = nanmean(vecAccu(selection{step}&  outlier==0)); %%%%% for accuracy;
            s.(forProcess{step})(i) = nanmean(vecAccu(mytrls))./nanmean(vecRT(mytrls)); %%%%% efficiency score, see https://www.journalofcognition.org/articles/10.5334/joc.6/;
            s.(forProcess{step})(i) = nanmean(vecAccu(mytrls)); %%%%% for accuracy;
        elseif whichone ==3
            s.(forProcess{step})(i) = nanmean(vecRT(mytrls)); %%%%% for accuracy;
        elseif whichone ==4
            s.(forProcess{step})(i) = nanmean(vecDist(mytrls)); %%%%% for accuracy;
        elseif whichone ==5
            s.(forProcess{step})(i) = nanmean(vecAccu(mytrls))./nanmean(vecRT(mytrls)); %%%%% efficiency score, see https://www.journalofcognition.org/articles/10.5334/joc.6/;
        elseif whichone ==6
            s.(forProcess{step})(i) = nanmean(vecAccu(mytrls)); %%%%% for accuracy;
        end
    end
    maxi = max ([s.subFLN(i) s.subFLBvL(i) s.subFLBvH(i)  s.subFLAvL(i) s.subFLAvH(i)...
             s.subFSN(i) s.subFSBvL(i) s.subFSBvH(i)  s.subFSAvL(i) s.subFSAvH(i)...
             s.subFMN(i) s.subFMBvL(i) s.subFMBvH(i)  s.subFMAvL(i) s.subFMAvH(i)]')/100;
     %    maxi = 1;
%          maxi = 1;
    fprintf(textfile,'%d\t%d\t%d\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\t%1.4f\n',i,inf.expMode,prepost,...
        s.subFSBvL(i)./maxi,s.subFSBvH(i)./maxi,s.subFMBvL(i)./maxi,s.subFMBvH(i)./maxi,s.subFLBvL(i)./maxi,s.subFLBvH(i)./maxi,...
        s.subFSAvL(i)./maxi,s.subFSAvH(i)./maxi,s.subFMAvL(i)./maxi,s.subFMAvH(i)./maxi,s.subFLAvL(i)./maxi,s.subFLAvH(i)./maxi,...
        s.subFSN(i)./maxi,s.subFMN(i)./maxi,s.subFLN(i)./maxi);
    mytrls1 = find(selection{4}&outlier==0);   %%%%% moving average of high  reward color trials
    mytrls2 = find(selection{5}&outlier==0);   %%%%% moving average of low   reward color trials
    mintrl = min(length(mytrls1),length(mytrls2)); %%%% find which of the two has lower number of trials and get from both those trials
    s.mintrl_BC(i)= mintrl;
    s.moveav_subBCvH(i,1:mintrl) = cumsum(vecAccu(mytrls2(1:mintrl)))./(1:mintrl);  %%%%%%% compute moving average of your effect
    s.moveav_subBCvL(i,1:mintrl) = cumsum(vecAccu(mytrls1(1:mintrl)))./(1:mintrl);
    s.effect_ColorValue(i,1:mintrl) = s.moveav_subBCvH(i,1:mintrl)-s.moveav_subBCvL(i,1:mintrl);  %%%%%%%% effect is Hight minus Low
    
    
    mytrls1 = find(selection{8}&outlier==0);   %%%%% moving average of high  reward sound trials
    mytrls2 = find(selection{9}&outlier==0);   %%%%% moving average of low   reward sound trials
    mintrl = min(length(mytrls1),length(mytrls2)); %%%% find which of the two has lower number of trials and get from both those trials
    s.mintrl_SP(i)= mintrl;
    s.moveav_subSPvH(i,1:mintrl) = cumsum(vecAccu(mytrls2(1:mintrl)))./(1:mintrl);
    s.moveav_subSPvL(i,1:mintrl) = cumsum(vecAccu(mytrls1(1:mintrl)))./(1:mintrl);
    s.effect_SoundValue(i,1:mintrl) = s.moveav_subSPvH(i,1:mintrl)-s.moveav_subSPvL(i,1:mintrl);  %%%%%%%% effect is Hight minus Low
    
end
%%
close all hidden
Pp = [1:length(subjects)-1]; %%%5 7:21 were with Arezoo's crossmodal
% Pp(s.subNeut(Pp)>0.85 | s.subNeut(Pp)<0.55)=[];
% if whichone == 6
% %     Pp(38)=[];
% else
%     Pp(s.subAccu(Pp)>0.95 | s.subAccu(Pp)<0.55)=[];
% end
% Pp=Pp(cellfun(@any,s.subleanS(Pp)) & cellfun(@any,s.subleanV(Pp)));

rel_blocks = [2];
if betweenCond == 1
    %Do nothing
elseif betweenCond == 2
    Pp = Pp(s.subRewS(Pp,2)==2);
elseif betweenCond == 3
    Pp = Pp(s.subRewS(Pp,2)==1);
elseif betweenCond == 4
    Pp = Pp(s.subRewV(Pp,2)==2);
elseif betweenCond == 5
    Pp = Pp(s.subRewV(Pp,2)==1);
end

% maxi = max([s.subNeut(Pp) s.subSP_H(Pp)  s.subSP_L(Pp) s.subNeut(Pp) s.subBCvH(Pp) s.subBCvL(Pp)]'); %%%%% maximum across conditions


maxi = max ([s.subFLN(Pp) s.subFLBvL(Pp) s.subFLBvH(Pp)  s.subFLAvL(Pp) s.subFLAvH(Pp)...
             s.subFSN(Pp) s.subFSBvL(Pp) s.subFSBvH(Pp)  s.subFSAvL(Pp) s.subFSAvH(Pp)...
             s.subFMN(Pp) s.subFMBvL(Pp) s.subFMBvH(Pp)  s.subFMAvL(Pp) s.subFMAvH(Pp)]')';








%% Figure 100 change of effect size across trials for visual
%
figure(777), axis square, hold on;
effect_mean = (nanmean(s.effect_ColorValue(Pp,:),1));
eff_sem=(nanstd(s.effect_ColorValue(Pp,:),0,1)./sqrt(length(Pp)));
eff_sem=1.*eff_sem; %%%% for confiedence interval (now is 90% for 95% should multiply by 1.96)

y1=effect_mean-eff_sem; y2=effect_mean+eff_sem;
y2(isnan(y1))=[];y1(isnan(y1))=[];
c1 = plot(1:size(effect_mean,2), nanmean(effect_mean,1),'b')
h=patch([1:length(y1) fliplr(1:length(y1))], [y2  fliplr(y1)],[0.5 0.5 1],'FaceAlpha',.4,'EdgeAlpha',.2)
h.DisplayName = 'Visual';
plot([1:size(effect_mean,2)],zeros(1,size(effect_mean,2)),':k','linewidth',2)
axis([-1 80 -0.15 0.15])
xlabel('Trials')
ylabel('Performance Difference High reward minus Low Reward')
for t=1:size(s.effect_SoundValue(Pp,:),2)
    [h pp(t)]=ttest(s.effect_SoundValue(Pp,t),0);
    if pp(t)<0.05
        plot(t,-0.5,'sk','MarkerFaceColor',[0 0 0])
    end
end

effect_mean = (nanmean(s.effect_SoundValue(Pp,:),1));
eff_sem=(nanstd(s.effect_SoundValue(Pp,:),0,1)./sqrt(length(Pp)));
eff_sem=1.*eff_sem; %%%% for confiedence interval (now is 90% for 95% should multiply by 1.96)

y1=effect_mean-eff_sem; y2=effect_mean+eff_sem;
y2(isnan(y1))=[];y1(isnan(y1))=[];
c2 = plot(1:size(effect_mean,2), nanmean(effect_mean,1),'r')
h=patch([1:length(y1) fliplr(1:length(y1))], [y2  fliplr(y1)],[1 0.5 0.5],'FaceAlpha',.4,'EdgeAlpha',.2);
h.DisplayName = 'Sound';
plot([1:size(effect_mean,2)],zeros(1,size(effect_mean,2)),':k','linewidth',2)
axis([-1 80 -0.15 0.15])

xlabel('ntrials')
ylabel('Performance Difference High reward minus Low Reward')
for t=1:size(s.effect_SoundValue(Pp,:),2)
    [h pp(t)]=ttest(s.effect_SoundValue(Pp,t),0);
    if pp(t)<0.05
        plot(t,-0.5,'sk','MarkerFaceColor',[1 0 0])
    end
end
% legend([c1,c2],{'Color','Sound'});
%% Figure 100 change of effect size across trials for visual
%
figure(200), axis square, hold on;
effect_mean = (nanmean(s.effect_ColorValue(Pp,:),1));
eff_sem=(nanstd(s.effect_ColorValue(Pp,:),0,1)./sqrt(length(Pp)));
eff_sem=1.*eff_sem; %%%% for confiedence interval (now is 90% for 95% should multiply by 1.96)

y1=effect_mean-eff_sem; y2=effect_mean+eff_sem;
y2(isnan(y1))=[];y1(isnan(y1))=[];
h=patch([1:length(y1) fliplr(1:length(y1))], [y2  fliplr(y1)],[0.5 0.5 0.5])
plot(1:size(effect_mean,2), nanmean(effect_mean,1),'r')
plot([1:size(effect_mean,2)],zeros(1,size(effect_mean,2)),':k','linewidth',2)
axis([-1 80 -0.15 0.15])
xlabel('ntrials')
ylabel('Performance Difference High reward minus Low Reward')
for t=1:size(s.effect_SoundValue(Pp,:),2)
    [h pp(t)]=ttest(s.effect_SoundValue(Pp,t),0);
    if pp(t)<0.05
        plot(t,-0.5,'sk','MarkerFaceColor',[0 0 0])
    end
end
%%
figure(201), axis square, hold on;
effect_mean = (nanmean(s.effect_SoundValue(Pp,:),1));
eff_sem=(nanstd(s.effect_SoundValue(Pp,:),0,1)./sqrt(length(Pp)));
eff_sem=1.*eff_sem; %%%% for confiedence interval (now is 90% for 95% should multiply by 1.96)

y1=effect_mean-eff_sem; y2=effect_mean+eff_sem;
y2(isnan(y1))=[];y1(isnan(y1))=[];
h=patch([1:length(y1) fliplr(1:length(y1))], [y2  fliplr(y1)],[0.5 0.5 0.5])
plot(1:size(effect_mean,2), nanmean(effect_mean,1),'r')
plot([1:size(effect_mean,2)],zeros(1,size(effect_mean,2)),':k','linewidth',2)
axis([-1 80 -0.15 0.15])

xlabel('ntrials')
ylabel('Performance Difference High reward minus Low Reward')
for t=1:size(s.effect_SoundValue(Pp,:),2)
    [h pp(t)]=ttest(s.effect_SoundValue(Pp,t),0);
    if pp(t)<0.05
        plot(t,-0.5,'sk','MarkerFaceColor',[0 0 0])
    end
end
%% Figure 1 Sound Pitch High-Low value
figure(1);
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSP_H(Pp))  nanmean(s.subSP_L(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSP_H(Pp))  nanmean(s.subSP_L(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSP_H(Pp)) nanstd(s.subSP_L(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none');
ax = gca;
ax.XTickLabel = {'neutral','HighPitch','LowPitch'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest(s.subSP_H(Pp),s.subSP_L(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
% print ('-f1' ,'-dpsc2',[rootdir_fig 'sounds.ps'])
% ax.YLim = [1.3 1.6];
%% Figure 2 Sound
figure(2);
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSPvH(Pp)) nanmean(s.subSPvL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean( s.subSPvH(Pp)) nanmean(s.subSPvL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSPvH(Pp)) nanstd(s.subSPvL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Nautral','HighValSound','LowValSound'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest(s.subSPvH(Pp),s.subSPvL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%% Figure 3 Box Color Orange-Magenta value
figure(3);
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBC_M(Pp))  nanmean(s.subBC_O(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBC_M(Pp))  nanmean(s.subBC_O(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBC_M(Pp)) nanstd(s.subBC_O(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'neutral','Magenta','Orange'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest( s.subBC_M(Pp),s.subBC_O(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'colors.ps'])

%% Figure 4 Box Color value Low High
figure(4);
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvH(Pp)) nanmean(s.subBCvL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvH(Pp)) nanmean(s.subBCvL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBCvH(Pp)) nanstd(s.subBCvL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Nautral','HighValCol','LowValCol'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest(s.subBCvH(Pp),s.subBCvL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%% Figure 5
figure(5)
bar(1, nanmean(s.subBCvH(Pp)-s.subBCvL(Pp))), hold on;
errorbar(1, nanmean(s.subBCvH(Pp)-s.subBCvL(Pp)),nanstd(s.subBCvH(Pp)-s.subBCvL(Pp))./sqrt(length(Pp))...
    ,'k','linewidth',3, 'linestyle','none')
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])

%% Figure 6
figure(6)
bar(1, nanmean(s.subSPvH(Pp)-s.subSPvL(Pp))), hold on;
errorbar(1, nanmean(s.subSPvH(Pp)-s.subSPvL(Pp)),nanstd(s.subSPvH(Pp)-s.subSPvL(Pp))./sqrt(length(Pp))...
    ,'k','linewidth',3, 'linestyle','none')
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
%% Figure 7 Box Color value Low High
figure(7);
subplot(1,2,1)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCsM(Pp)) nanmean(s.subBCsO(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCsM(Pp)) nanmean(s.subBCsO(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBCsM(Pp)) nanstd(s.subBCsO(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','MCoS','OCoS'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest(s.subBCsM(Pp),s.subBCsO(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%% Figure 8 Box Color value Low High
figure(7);
subplot(1,2,2)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCdM(Pp)) nanmean(s.subBCdO(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCdM(Pp)) nanmean(s.subBCdO(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBCdM(Pp)) nanstd(s.subBCdO(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','MCoD','OCoD'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest(s.subBCdM(Pp),s.subBCdO(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])


%% Figure 9 Box Color value Low High
figure(9);
subplot(1,2,1)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvsH(Pp)) nanmean(s.subBCvsL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvsH(Pp)) nanmean(s.subBCvsL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBCvsH(Pp)) nanstd(s.subBCvsL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HVCoS','LVCoS'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest(s.subBCvsH(Pp),s.subBCvsL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%% Figure 10 Box Color value Low High
figure(9);
subplot(1,2,2)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvdH(Pp)) nanmean(s.subBCvdL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvdH(Pp)) nanmean(s.subBCvdL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBCvdH(Pp)) nanstd(s.subBCvdL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HVCoD','LVCoD'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest(s.subBCvdH(Pp),s.subBCvdL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%% Figure 11 Sound
figure(11);
subplot(1,2,1)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSPsH(Pp)) nanmean(s.subSPsL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean( s.subSPsH(Pp)) nanmean(s.subSPsL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSPsH(Pp)) nanstd(s.subSPsL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HPSoS','LPSoS'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest(s.subSPsH(Pp),s.subSPsL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(p)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%% Figure 12 Sound
figure(11);
subplot(1,2,2)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSPdH(Pp)) nanmean(s.subSPdL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean( s.subSPdH(Pp)) nanmean(s.subSPdL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSPdH(Pp)) nanstd(s.subSPdL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HPSoD','LPSoD'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest(s.subSPdH(Pp),s.subSPdL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%% Figure 13 Sound
figure(13);
subplot(1,2,1)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSPvsH(Pp)) nanmean(s.subSPvsL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean( s.subSPvsH(Pp)) nanmean(s.subSPvsL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSPvsH(Pp)) nanstd(s.subSPvsL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HVSoS','LVSoS'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest(s.subSPvsH(Pp),s.subSPvsL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
%print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%% Figure 14 Sound
figure(13);
subplot(1,2,2)
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subSPvdH(Pp)) nanmean(s.subSPvdL(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean( s.subSPvdH(Pp)) nanmean(s.subSPvdL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSPvdH(Pp)) nanstd(s.subSPvdL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HVSoD','LVSoD'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest(s.subSPvdH(Pp),s.subSPvdL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P= ' num2str(round(p,2))])
%print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%
%% Figure 13 Sound
figure(130);
bar(1:5,[nanmean(s.subNeut(Pp)) nanmean(s.subSPvsH(Pp)) nanmean(s.subSPvsL(Pp))  nanmean(s.subSPvdH(Pp)) nanmean(s.subSPvdL(Pp))],'w'), hold on
errorbar(1:5,[nanmean(s.subNeut(Pp)) nanmean( s.subSPvsH(Pp)) nanmean(s.subSPvsL(Pp))  nanmean(s.subSPvdH(Pp)) nanmean(s.subSPvdL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subSPvsH(Pp)) nanstd(s.subSPvsL(Pp)) nanstd(s.subSPvdH(Pp)) nanstd(s.subSPvdL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HVSoS','LVSoS','HVSoD','LVSoD'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p1]= ttest(s.subSPvsH(Pp),s.subSPvsL(Pp));
[h p2]= ttest(s.subSPvdH(Pp),s.subSPvdL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P-sameside= ' num2str(p1) ', P-diffside= ' num2str(p2)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P-sameside= ' num2str(round(p1,2)) ', P-diffside= ' num2str(round(p2,2))])
%print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%
display(['nanmean sound volume across subjects: ' num2str(nanmean(s.subSVol))])
%%
%% Figure 13 Sound
figure(131);
bar(1:5,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvsH(Pp)) nanmean(s.subBCvsL(Pp))  nanmean(s.subBCvdH(Pp)) nanmean(s.subBCvdL(Pp))],'w'), hold on
errorbar(1:5,[nanmean(s.subNeut(Pp)) nanmean( s.subBCvsH(Pp)) nanmean(s.subBCvsL(Pp))  nanmean(s.subBCvdH(Pp)) nanmean(s.subBCvdL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBCvsH(Pp)) nanstd(s.subBCvsL(Pp)) nanstd(s.subBCvdH(Pp)) nanstd(s.subBCvdL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'N','HVViS','LVViS','HVViD','LVViD'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p1]= ttest(s.subBCvsH(Pp),s.subBCvsL(Pp));
[h p2]= ttest(s.subBCvdH(Pp),s.subBCvdL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P-sameside= ' num2str(p1) ', P-diffside= ' num2str(p2)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P-sameside= ' num2str(round(p1,2)) ', P-diffside= ' num2str(round(p2,2))])
%print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%
display(['nanmean sound volume across subjects: ' num2str(nanmean(s.subSVol))])
%%
%% Figure 2 Sound
figure(14);
bar(1:3,[nanmean(s.subNeut(Pp)) nanmean(s.subV(Pp)) nanmean(s.subA(Pp))],'r'), hold on
errorbar(1:3,[nanmean(s.subNeut(Pp)) nanmean( s.subV(Pp)) nanmean(s.subA(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subV(Pp)) nanstd(s.subA(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Nautral','AllVisual','AllAuditory'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p]= ttest(s.subV(Pp),s.subA(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', visual vs auditory P= ' num2str(round(p,2))])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%% Figure 2 Sound
figure(15);
bar(1:5,[nanmean(s.subNeut(Pp)) nanmean(s.subVs(Pp)) nanmean(s.subVd(Pp)) nanmean(s.subAs(Pp)) nanmean(s.subAd(Pp))],'r'), hold on
errorbar(1:5,[nanmean(s.subNeut(Pp)) nanmean(s.subVs(Pp)) nanmean(s.subVd(Pp)) nanmean(s.subAs(Pp)) nanmean(s.subAd(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subVs(Pp)) nanstd(s.subVd(Pp)) nanstd(s.subAs(Pp)) nanstd(s.subAd(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Nautral','V-S','V-D','A-S','A-D'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p1]= ttest(s.subVs(Pp),s.subVd(Pp));
[h p2]= ttest(s.subAs(Pp),s.subAd(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', visual P= ' num2str(round(p1,2)) ', auditory P= ' num2str(round(p2,2))])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])
%% Figure 2 Sound
figure(16);
bar(1:9,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvsH(Pp)) nanmean(s.subBCvdH(Pp)) nanmean(s.subBCvsL(Pp)) nanmean(s.subBCvdL(Pp)) nanmean(s.subSPvsH(Pp))...
    nanmean(s.subSPvdH(Pp)) nanmean(s.subSPvsL(Pp)) nanmean(s.subSPvdL(Pp))],'r'), hold on
errorbar(1:9,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvsH(Pp)) nanmean(s.subBCvdH(Pp)) nanmean(s.subBCvsL(Pp)) nanmean(s.subBCvdL(Pp)) nanmean(s.subSPvsH(Pp))...
    nanmean(s.subSPvdH(Pp)) nanmean(s.subSPvsL(Pp)) nanmean(s.subSPvdL(Pp))],...
    [[nanstd(s.subNeut(Pp)) nanstd(s.subBCvsH(Pp)) nanstd(s.subBCvdH(Pp)) nanstd(s.subBCvsL(Pp)) nanstd(s.subBCvdL(Pp)) nanstd(s.subSPvsH(Pp))...
    nanstd(s.subSPvdH(Pp)) nanstd(s.subSPvsL(Pp)) nanstd(s.subSPvdL(Pp))]]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Nautral','HV-S','HV-D','LV-S','LV-D','HA-S','HA-D','LA-S','LA-D'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h p1]= ttest(s.subVs(Pp),s.subVd(Pp));
[h p2]= ttest(s.subAs(Pp),s.subAd(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', visual P= ' num2str(round(p1,2)) ', auditory P= ' num2str(round(p2,2))])
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%%%%%%
%% Figure 2 Sound
figure(17);
bar(1:5,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvH(Pp)) nanmean(s.subBCvL(Pp)) nanmean(s.subSPvH(Pp))...
    nanmean(s.subSPvL(Pp))],'w'), hold on
errorbar(1:5,[nanmean(s.subNeut(Pp)) nanmean(s.subBCvH(Pp)) nanmean(s.subBCvL(Pp)) nanmean(s.subSPvH(Pp))...
    nanmean(s.subSPvL(Pp))],...
    [nanstd(s.subNeut(Pp)) nanstd(s.subBCvH(Pp)) nanstd(s.subBCvL(Pp)) nanstd(s.subSPvH(Pp))...
    nanstd(s.subSPvL(Pp))]./sqrt(length(Pp)),'k','linewidth',3, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Neut','VisHighVal','VisLowVal','AudioHighVal','AudioLowVal'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
ax = gca;
% ax.YLim = [1.4 1.6];
title('Effect of Reward value and modality', 'FontSize', 16);
[h p1]= ttest(s.subBCvH(Pp),s.subBCvL(Pp));
[h p2]= ttest(s.subSPvH(Pp),s.subSPvL(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', visual P= ' num2str(round(p1,2)) ', auditory P= ' num2str(round(p2,2))]);
% print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%%
%% Figure 18 FRAME SIZE
figure(18);
bar(1:6,[nanmean(s.subFSvL(Pp)) nanmean(s.subFSvH(Pp)) nanmean(s.subFMvL(Pp))  nanmean(s.subFMvH(Pp)) nanmean(s.subFLvL(Pp)) nanmean(s.subFLvH(Pp))],'w'), hold on
errorbar(1:6,[nanmean(s.subFSvL(Pp)) nanmean( s.subFSvH(Pp)) nanmean(s.subFMvL(Pp))  nanmean(s.subFMvH(Pp)) nanmean(s.subFLvL(Pp)) nanmean(s.subFLvH(Pp))],...
    [nanstd(s.subFSvL(Pp)) nanstd(s.subFSvH(Pp)) nanstd(s.subFMvL(Pp)) nanstd(s.subFMvH(Pp)) nanstd(s.subFLvL(Pp)) nanstd(s.subFLvH(Pp))]./sqrt(length(Pp)),'g','linewidth',4, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'FSvL','FSvH','FMvL','FMvH','FLvL','FLvH'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h pFS]= ttest(s.subFSvL(Pp),s.subFSvH(Pp));
[h pFM]= ttest(s.subFMvL(Pp),s.subFMvH(Pp));
[h pFL]= ttest(s.subFLvL(Pp),s.subFLvH(Pp));
title([ experiment_name ', Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P-Sma= ' num2str(pFS) ', P-Med= ' num2str(pFM) ', P-Lar= ' num2str(pFL)])
title([ experiment_name...
    ', N= ' num2str(length(Pp)) ', P-Sma= ' num2str(round(pFS,2)) ', P-Med= ' num2str(round(pFM,2)) ', P-Lar= ' num2str(round(pFL,2))])
%print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%
display(['nanmean sound volume across subjects: ' num2str(nanmean(s.subSVol))])

%% Figure 19 FRAME SIZE Sma Value
figure(19);
bar(1:5,[nanmean(s.subFSN(Pp)) nanmean(s.subFSBvL(Pp)) nanmean(s.subFSBvH(Pp))  nanmean(s.subFSAvL(Pp)) nanmean(s.subFSAvH(Pp))],'w'), hold on
errorbar(1:5,[nanmean(s.subFSN(Pp)) nanmean(s.subFSBvL(Pp)) nanmean(s.subFSBvH(Pp))  nanmean(s.subFSAvL(Pp)) nanmean(s.subFSAvH(Pp))],...
    [nanstd(s.subFSN(Pp)) nanstd(s.subFSBvL(Pp)) nanstd(s.subFSBvH(Pp))  nanstd(s.subFSAvL(Pp)) nanstd(s.subFSAvH(Pp))]./sqrt(length(Pp)),'g','linewidth',4, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Neut','Box Low','Box High','Aud Low','Aud High'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h pFSA]= ttest(s.subFSAvL(Pp),s.subFSAvH(Pp));
[h pFSB]= ttest(s.subFSBvL(Pp),s.subFSBvH(Pp));
title([ experiment_name ', Sma , Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P-col= ' num2str(pFSB) ', P-snd= ' num2str(pFSA)])
title([ experiment_name...
    ',Sma , N= ' num2str(length(Pp)) ', P-col= ' num2str(round(pFSB,2)) ', P-snd= ' num2str(round(pFSA,2))])
%print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%
display(['nanmean sound volume across subjects: ' num2str(nanmean(s.subSVol))])

%% Figure 20 FRAME SIZE MED Value
figure(20);
bar(1:5,[nanmean(s.subFMN(Pp)) nanmean(s.subFMBvL(Pp)) nanmean(s.subFMBvH(Pp))  nanmean(s.subFMAvL(Pp)) nanmean(s.subFMAvH(Pp))],'w'), hold on
errorbar(1:5,[nanmean(s.subFMN(Pp)) nanmean(s.subFMBvL(Pp)) nanmean(s.subFMBvH(Pp))  nanmean(s.subFMAvL(Pp)) nanmean(s.subFMAvH(Pp))],...
    [nanstd(s.subFMN(Pp)) nanstd(s.subFMBvL(Pp)) nanstd(s.subFMBvH(Pp))  nanstd(s.subFMAvL(Pp)) nanstd(s.subFMAvH(Pp))]./sqrt(length(Pp)),'g','linewidth',4, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Neut','Box Low','Box High','Aud Low','Aud High'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h pFMA]= ttest(s.subFMAvL(Pp),s.subFMAvH(Pp));
[h pFMB]= ttest(s.subFMBvL(Pp),s.subFMBvH(Pp));
title([ experiment_name ',Med , Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P-col= ' num2str(pFMB) ', P-snd= ' num2str(pFMA)])
title([ experiment_name...
    ', Med , N= ' num2str(length(Pp)) ', P-col= ' num2str(round(pFMB,2)) ', P-snd= ' num2str(round(pFMA,2))])
%print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%
display(['nanmean sound volume across subjects: ' num2str(nanmean(s.subSVol))])
%% Figure 21 FRAME SIZE Sma Value
figure(21);
bar(1:5,[nanmean(s.subFLN(Pp)) nanmean(s.subFLBvL(Pp)) nanmean(s.subFLBvH(Pp))  nanmean(s.subFLAvL(Pp)) nanmean(s.subFLAvH(Pp))],'w'), hold on
errorbar(1:5,[nanmean(s.subFLN(Pp)) nanmean(s.subFLBvL(Pp)) nanmean(s.subFLBvH(Pp))  nanmean(s.subFLAvL(Pp)) nanmean(s.subFLAvH(Pp))],...
    [nanstd(s.subFLN(Pp)) nanstd(s.subFLBvL(Pp)) nanstd(s.subFLBvH(Pp))  nanstd(s.subFLAvL(Pp)) nanstd(s.subFLAvH(Pp))]./sqrt(length(Pp)),'g','linewidth',4, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Neut','Box Low','Box High','Aud Low','Aud High'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
[h pFLA]= ttest(s.subFLAvL(Pp)./maxi,s.subFLAvH(Pp)./maxi);
[h pFLB]= ttest(s.subFLBvL(Pp)./maxi,s.subFLBvH(Pp)./maxi);
title([ experiment_name ',Lar , Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P-col= ' num2str(pFLB) ', P-snd= ' num2str(pFLA)])
title([ experiment_name...
    ', Lar , N= ' num2str(length(Pp)) ', P-col= ' num2str(round(pFLB,2)) ', P-snd= ' num2str(round(pFLA,2))])
%print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%
display(['nanmean sound volume across subjects: ' num2str(nanmean(s.subSVol))])

%% Figure 22 FRAME SIZE Sma Value
figure(22);
bar(1:12,[nanmean(s.subFSBvL(Pp)) nanmean(s.subFSBvL(Pp))  nanmean(s.subFSBvL(Pp))...
    nanmean(s.subFSBvH(Pp)) nanmean(s.subFMBvH(Pp))  nanmean(s.subFLBvH(Pp))...
    nanmean(s.subFSAvL(Pp)) nanmean(s.subFMAvL(Pp))  nanmean(s.subFLAvL(Pp))...
    nanmean(s.subFSAvH(Pp)) nanmean(s.subFMAvH(Pp))  nanmean(s.subFLAvH(Pp))],'w'), hold on
errorbar(1:12,[nanmean(s.subFSBvL(Pp)) nanmean(s.subFSBvL(Pp))  nanmean(s.subFSBvL(Pp))...
    nanmean(s.subFSBvH(Pp)) nanmean(s.subFMBvH(Pp))  nanmean(s.subFLBvH(Pp))...
    nanmean(s.subFSAvL(Pp)) nanmean(s.subFMAvL(Pp))  nanmean(s.subFLAvL(Pp))...
    nanmean(s.subFSAvH(Pp)) nanmean(s.subFMAvH(Pp))  nanmean(s.subFLAvH(Pp))],...
    [nanstd(s.subFSBvL(Pp)) nanstd(s.subFSBvL(Pp))  nanstd(s.subFSBvL(Pp))...
    nanstd(s.subFSBvH(Pp)) nanstd(s.subFMBvH(Pp))  nanstd(s.subFLBvH(Pp))...
    nanstd(s.subFSAvL(Pp)) nanstd(s.subFMAvL(Pp))  nanstd(s.subFLAvL(Pp))...
    nanstd(s.subFSAvH(Pp)) nanstd(s.subFMAvH(Pp))  nanstd(s.subFLAvH(Pp))]./sqrt(length(Pp)),'g','linewidth',4, 'linestyle','none')
ax = gca;
ax.XTickLabel = {'Sma','Med','Lar'};
if whichone==1
    ax.YLim = scalesel1;
    ylabel('DPrime' , 'FontSize', 14);
elseif whichone==2
    ax.YLim = scalesel2;
    ylabel('Accuracy' , 'FontSize', 14);
elseif whichone==3
    ax.YLim = scalesel3;
    ylabel('RT' , 'FontSize', 14);
elseif whichone==5
    ax.YLim = scalesel5;
    ylabel('Efficiency' , 'FontSize', 14);
elseif whichone==6
    ax.YLim = scalesel6;
    ylabel('Key (1-UP, 2-Down)' , 'FontSize', 14);
end
ax.YLim = [1,3.5];
[h pFLA]= ttest(s.subFLAvL(Pp),s.subFLAvH(Pp));
[h pFLB]= ttest(s.subFLBvL(Pp),s.subFLBvH(Pp));
title([ experiment_name ',Lar , Blocks:' prepostVec{prepost} ' ' groupVec{group}...
    ', N= ' num2str(length(Pp)) ', P-col= ' num2str(pFLB) ', P-snd= ' num2str(pFLA)])
title([ experiment_name...
    ', Lar , N= ' num2str(length(Pp)) ', P-col= ' num2str(round(pFLB,2)) ', P-snd= ' num2str(round(pFLA,2))])
%print ('-f2' ,'-dpsc2',[rootdir_fig 'valsounds.ps'])

%
display(['nanmean sound volume across subjects: ' num2str(nanmean(s.subSVol))])

%%
figure(23)
hold on;
plot([1:3]+0.04,[nanmean(s.subFSBvL(Pp)) nanmean(s.subFMBvL(Pp)) nanmean(s.subFLBvL(Pp))],'*--b','linewidth',2);
plot([1:3]-0.02,[nanmean(s.subFSBvH(Pp)) nanmean(s.subFMBvH(Pp)) nanmean(s.subFLBvH(Pp))],'b','linewidth',2);
plot([1:3]-0.04,[nanmean(s.subFSAvL(Pp)) nanmean(s.subFMAvL(Pp)) nanmean(s.subFLAvL(Pp))],'*--r','linewidth',2);
plot([1:3],[nanmean(s.subFSAvH(Pp)) nanmean(s.subFMAvH(Pp)) nanmean(s.subFLAvH(Pp))],'r','linewidth',2);
plot([1:3]+0.02,[nanmean(s.subFSN(Pp)) nanmean(s.subFMN(Pp)) nanmean(s.subFLN(Pp))],'*--k','linewidth',2);

ax = gca;
% axis tight
ylabel('DPrime' , 'FontSize', 14);
xlabel('Surrounding frame size (degrees)' , 'FontSize', 14);
set(gca,'XTick',[1:3], 'FontSize', 14);
ax.XTickLabel = {'4�','7�','10�'};
legend('Visual Low Value','Visual High Value','Auditory Low Value','Auditory High Value','Neutral')

errorbar([1 2 3]+0.04,[nanmean(s.subFSBvL(Pp)) nanmean(s.subFMBvL(Pp)) nanmean(s.subFLBvL(Pp))],...
    [nanstd(s.subFSBvL(Pp)) nanstd(s.subFMBvL(Pp)) nanstd(s.subFLBvL(Pp))]./sqrt(length(Pp)),'b','linewidth',2, 'linestyle','none')
errorbar([1 2 3]-0.02,[nanmean(s.subFSBvH(Pp)) nanmean(s.subFMBvH(Pp)) nanmean(s.subFLBvH(Pp))],...
    [nanstd(s.subFSBvH(Pp)) nanstd(s.subFMBvH(Pp)) nanstd(s.subFLBvH(Pp))]./sqrt(length(Pp)),'b','linewidth',2, 'linestyle','none')
errorbar([1 2 3]-0.04,[nanmean(s.subFSAvL(Pp)) nanmean(s.subFMAvL(Pp)) nanmean(s.subFLAvL(Pp))],...
    [nanstd(s.subFSAvL(Pp)) nanstd(s.subFMAvL(Pp)) nanstd(s.subFLAvL(Pp))]./sqrt(length(Pp)),'r','linewidth',2, 'linestyle','none')
errorbar([1 2 3]     ,[nanmean(s.subFSAvH(Pp)) nanmean(s.subFMAvH(Pp)) nanmean(s.subFLAvH(Pp))],...
    [nanstd(s.subFSAvH(Pp)) nanstd(s.subFMAvH(Pp)) nanstd(s.subFLAvH(Pp))]./sqrt(length(Pp)),'r','linewidth',2, 'linestyle','none')
errorbar([1 2 3]+0.02,[nanmean(s.subFSN(Pp)) nanmean(s.subFMN(Pp)) nanmean(s.subFLN(Pp))],...
    [nanstd(s.subFSN(Pp)) nanstd(s.subFMN(Pp)) nanstd(s.subFLN(Pp))]./sqrt(length(Pp)),'k','linewidth',2, 'linestyle','none')
[h pFLA]= ttest(nanmean([s.subFSBvL(Pp) s.subFSBvH(Pp)],2),nanmean([s.subFMBvL(Pp) s.subFMBvH(Pp)],2));
hold off;
[h pFLA]= ttest(s.subFLBvL(Pp),s.subFLN(Pp));

%%
figure(24)
hold on;
plot([1:3],[nanmean(s.subFSBvL(Pp))-nanmean(s.subFSN(Pp)) nanmean(s.subFMBvL(Pp))-nanmean(s.subFMN(Pp)) nanmean(s.subFLBvL(Pp))-nanmean(s.subFLN(Pp))],'*-.b','linewidth',2);
plot([1:3],[nanmean(s.subFSBvH(Pp))-nanmean(s.subFSN(Pp)) nanmean(s.subFMBvH(Pp))-nanmean(s.subFMN(Pp)) nanmean(s.subFLBvH(Pp))-nanmean(s.subFLN(Pp))],'*-r','linewidth',2);
% plot([1:3],[nanmean(s.subFSAvL(Pp)) nanmean(s.subFMAvL(Pp)) nanmean(s.subFLAvL(Pp))],'*--b','linewidth',2);
% plot([1:3],[nanmean(s.subFSAvH(Pp)) nanmean(s.subFMAvH(Pp)) nanmean(s.subFLAvH(Pp))],'*--r','linewidth',2);
% plot([1:3],[nanmean(s.subFSN(Pp)) nanmean(s.subFMN(Pp)) nanmean(s.subFLN(Pp))],'*-k','linewidth',2);

ax = gca;
% axis tight
ylim([-1 1])
ylabel('Dprime (substruct neutral)' , 'FontSize', 14);
xlabel('Gabor-Frame size ratio (degrees)' , 'FontSize', 14);
set(gca,'XTick',[1:3], 'FontSize', 14);
ax.XTickLabel = {'1/2�','1/3.5�','1/5�'};
legend('Low Value','High Value')

errorbar([1 2 3]+0.04,[nanmean(s.subFSBvL(Pp))-nanmean(s.subFSN(Pp)) nanmean(s.subFMBvL(Pp))-nanmean(s.subFMN(Pp)) nanmean(s.subFLBvL(Pp))-nanmean(s.subFLN(Pp))],...
    [nanstd(s.subFSBvL(Pp)) nanstd(s.subFMBvL(Pp)) nanstd(s.subFLBvL(Pp))]./sqrt(length(Pp)),'b','linewidth',2, 'linestyle','none')
errorbar([1 2 3]-0.02,[nanmean(s.subFSBvH(Pp))-nanmean(s.subFSN(Pp)) nanmean(s.subFMBvH(Pp))-nanmean(s.subFMN(Pp)) nanmean(s.subFLBvH(Pp))-nanmean(s.subFLN(Pp))],...
    [nanstd(s.subFSBvH(Pp)) nanstd(s.subFMBvH(Pp)) nanstd(s.subFLBvH(Pp))]./sqrt(length(Pp)),'r','linewidth',2, 'linestyle','none')
% errorbar([1 2 3]-0.04,[nanmean(s.subFSAvL(Pp)) nanmean(s.subFMAvL(Pp)) nanmean(s.subFLAvL(Pp))],...
%     [nanstd(s.subFSAvL(Pp)) nanstd(s.subFMAvL(Pp)) nanstd(s.subFLAvL(Pp))]./sqrt(length(Pp)),'b','linewidth',2, 'linestyle','none')
% errorbar([1 2 3]     ,[nanmean(s.subFSAvH(Pp)) nanmean(s.subFMAvH(Pp)) nanmean(s.subFLAvH(Pp))],...
%     [nanstd(s.subFSAvH(Pp)) nanstd(s.subFMAvH(Pp)) nanstd(s.subFLAvH(Pp))]./sqrt(length(Pp)),'g','linewidth',2, 'linestyle','none')
% errorbar([1 2 3]+0.02,[nanmean(s.subFSN(Pp)) nanmean(s.subFMN(Pp)) nanmean(s.subFLN(Pp))],...
%     [nanstd(s.subFSN(Pp)) nanstd(s.subFMN(Pp)) nanstd(s.subFLN(Pp))]./sqrt(length(Pp)),'k','linewidth',2, 'linestyle','none')
[h pFLA]= ttest(nanmean([s.subFSBvL(Pp) s.subFSBvH(Pp)],2),nanmean([s.subFMBvL(Pp) s.subFMBvH(Pp)],2));
hold off;

%%
figure(25)
hold on;
plot([1:3],[nanmean(s.subFSAvL(Pp))-nanmean(s.subFSN(Pp)) nanmean(s.subFMAvL(Pp))-nanmean(s.subFMN(Pp)) nanmean(s.subFLAvL(Pp))-nanmean(s.subFLN(Pp))],'*-.b','linewidth',2);
plot([1:3],[nanmean(s.subFSAvH(Pp))-nanmean(s.subFSN(Pp)) nanmean(s.subFMAvH(Pp))-nanmean(s.subFMN(Pp)) nanmean(s.subFLAvH(Pp))-nanmean(s.subFLN(Pp))],'*-r','linewidth',2);
% plot([1:3],[nanmean(s.subFSAvL(Pp)) nanmean(s.subFMAvL(Pp)) nanmean(s.subFLAvL(Pp))],'*--b','linewidth',2);
% plot([1:3],[nanmean(s.subFSAvH(Pp)) nanmean(s.subFMAvH(Pp)) nanmean(s.subFLAvH(Pp))],'*--r','linewidth',2);
% plot([1:3],[nanmean(s.subFSN(Pp)) nanmean(s.subFMN(Pp)) nanmean(s.subFLN(Pp))],'*-k','linewidth',2);

ax = gca;
% axis tight
% ylim([-1.5 .5])
ylabel('Dprime (substruct neutral)' , 'FontSize', 14);
xlabel('Gabor-Frame size ratio (degrees)' , 'FontSize', 14);
set(gca,'XTick',[1:3], 'FontSize', 14);
ax.XTickLabel = {'1/2�','1/3.5�','1/5�'};
legend('Low Value','High Value')

errorbar([1 2 3]+0.04,[nanmean(s.subFSAvL(Pp))-nanmean(s.subFSN(Pp)) nanmean(s.subFMAvL(Pp))-nanmean(s.subFMN(Pp)) nanmean(s.subFLAvL(Pp))-nanmean(s.subFLN(Pp))],...
    [nanstd(s.subFSAvL(Pp)) nanstd(s.subFMAvL(Pp)) nanstd(s.subFLAvL(Pp))]./sqrt(length(Pp)),'b','linewidth',2, 'linestyle','none')
errorbar([1 2 3]-0.02,[nanmean(s.subFSAvH(Pp))-nanmean(s.subFSN(Pp)) nanmean(s.subFMAvH(Pp))-nanmean(s.subFMN(Pp)) nanmean(s.subFLAvH(Pp))-nanmean(s.subFLN(Pp))],...
    [nanstd(s.subFSAvH(Pp)) nanstd(s.subFMAvH(Pp)) nanstd(s.subFLAvH(Pp))]./sqrt(length(Pp)),'r','linewidth',2, 'linestyle','none')
% errorbar([1 2 3]-0.04,[nanmean(s.subFSAvL(Pp)) nanmean(s.subFMAvL(Pp)) nanmean(s.subFLAvL(Pp))],...
%     [nanstd(s.subFSAvL(Pp)) nanstd(s.subFMAvL(Pp)) nanstd(s.subFLAvL(Pp))]./sqrt(length(Pp)),'b','linewidth',2, 'linestyle','none')
% errorbar([1 2 3]     ,[nanmean(s.subFSAvH(Pp)) nanmean(s.subFMAvH(Pp)) nanmean(s.subFLAvH(Pp))],...
%     [nanstd(s.subFSAvH(Pp)) nanstd(s.subFMAvH(Pp)) nanstd(s.subFLAvH(Pp))]./sqrt(length(Pp)),'g','linewidth',2, 'linestyle','none')
% errorbar([1 2 3]+0.02,[nanmean(s.subFSN(Pp)) nanmean(s.subFMN(Pp)) nanmean(s.subFLN(Pp))],...
%     [nanstd(s.subFSN(Pp)) nanstd(s.subFMN(Pp)) nanstd(s.subFLN(Pp))]./sqrt(length(Pp)),'k','linewidth',2, 'linestyle','none')
[h pFLA]= ttest(nanmean([s.subFSAvL(Pp) s.subFSAvH(Pp)],2),nanmean([s.subFMAvL(Pp) s.subFMAvH(Pp)],2));
hold off;

%%
figure(26)
hold on;
plot([1:3],[nanmean(s.subFSAvL(Pp))-nanmean(s.subFSN(Pp)) nanmean(s.subFMAvL(Pp))-nanmean(s.subFMN(Pp)) nanmean(s.subFLAvL(Pp))-nanmean(s.subFLN(Pp))],'*-.b','linewidth',2);
plot([1:3],[nanmean(s.subFSAvH(Pp))-nanmean(s.subFSN(Pp)) nanmean(s.subFMAvH(Pp))-nanmean(s.subFMN(Pp)) nanmean(s.subFLAvH(Pp))-nanmean(s.subFLN(Pp))],'*-r','linewidth',2);
% plot([1:3],[nanmean(s.subFSAvL(Pp)) nanmean(s.subFMAvL(Pp)) nanmean(s.subFLAvL(Pp))],'*--b','linewidth',2);
% plot([1:3],[nanmean(s.subFSAvH(Pp)) nanmean(s.subFMAvH(Pp)) nanmean(s.subFLAvH(Pp))],'*--r','linewidth',2);
% plot([1:3],[nanmean(s.subFSN(Pp)) nanmean(s.subFMN(Pp)) nanmean(s.subFLN(Pp))],'*-k','linewidth',2);

ax = gca;
% axis tight
% ylim([-1.5 .5])
ylabel('Dprime (substruct neutral)' , 'FontSize', 14);
xlabel('Gabor-Frame size ratio (degrees)' , 'FontSize', 14);
set(gca,'XTick',[1:3], 'FontSize', 14);
ax.XTickLabel = {'1/2�','1/3.5�','1/5�'};
legend('Low Value','High Value','Auditory Low Value','Auditory High Value','Neutral')

errorbar([1 2 3]+0.04,[nanmean(s.subFSAvL(Pp))-nanmean(s.subFSN(Pp)) nanmean(s.subFMAvL(Pp))-nanmean(s.subFMN(Pp)) nanmean(s.subFLAvL(Pp))-nanmean(s.subFLN(Pp))],...
    [nanstd(s.subFSAvL(Pp)) nanstd(s.subFMAvL(Pp)) nanstd(s.subFLAvL(Pp))]./sqrt(length(Pp)),'b','linewidth',2, 'linestyle','none')
errorbar([1 2 3]-0.02,[nanmean(s.subFSAvH(Pp))-nanmean(s.subFSN(Pp)) nanmean(s.subFMAvH(Pp))-nanmean(s.subFMN(Pp)) nanmean(s.subFLAvH(Pp))-nanmean(s.subFLN(Pp))],...
    [nanstd(s.subFSAvH(Pp)) nanstd(s.subFMAvH(Pp)) nanstd(s.subFLAvH(Pp))]./sqrt(length(Pp)),'r','linewidth',2, 'linestyle','none')
% errorbar([1 2 3]-0.04,[nanmean(s.subFSAvL(Pp)) nanmean(s.subFMAvL(Pp)) nanmean(s.subFLAvL(Pp))],...
%     [nanstd(s.subFSAvL(Pp)) nanstd(s.subFMAvL(Pp)) nanstd(s.subFLAvL(Pp))]./sqrt(length(Pp)),'b','linewidth',2, 'linestyle','none')
% errorbar([1 2 3]     ,[nanmean(s.subFSAvH(Pp)) nanmean(s.subFMAvH(Pp)) nanmean(s.subFLAvH(Pp))],...
%     [nanstd(s.subFSAvH(Pp)) nanstd(s.subFMAvH(Pp)) nanstd(s.subFLAvH(Pp))]./sqrt(length(Pp)),'g','linewidth',2, 'linestyle','none')
% errorbar([1 2 3]+0.02,[nanmean(s.subFSN(Pp)) nanmean(s.subFMN(Pp)) nanmean(s.subFLN(Pp))],...
%     [nanstd(s.subFSN(Pp)) nanstd(s.subFMN(Pp)) nanstd(s.subFLN(Pp))]./sqrt(length(Pp)),'k','linewidth',2, 'linestyle','none')
[h pFLA]= ttest(nanmean([s.subFSAvL(Pp) s.subFSAvH(Pp)],2),nanmean([s.subFMAvL(Pp) s.subFMAvH(Pp)],2));

%%
figure(27),hold on;
plot([1:3],[nanmean(s.subFSN(Pp)) nanmean(s.subFMN(Pp)) nanmean(s.subFLN(Pp))],'*-k','linewidth',2);
errorbar([1 2 3],[nanmean(s.subFSN(Pp)) nanmean(s.subFMN(Pp)) nanmean(s.subFLN(Pp))],...
    [nanstd(s.subFSN(Pp)) nanstd(s.subFMN(Pp)) nanstd(s.subFLN(Pp))]./sqrt(length(Pp)),'k','linewidth',2, 'linestyle','none')
ax = gca;
ylabel('d` (Hits - False Alarms)' , 'FontSize', 14);
xlabel('Distance of Box from Gabor Center (degrees)' , 'FontSize', 14);
set(gca,'XTick',[1:3], 'FontSize', 14);
ax.XTickLabel = {'2�','3.5�','5�'};
legend('Neutral')
%%
figure(28)

hold on;
plot([1:3],[nanmean(s.subFSAvH(Pp))-nanmean(s.subFSAvL(Pp)) nanmean(s.subFMAvH(Pp))-nanmean(s.subFMAvL(Pp)) nanmean(s.subFLAvH(Pp))-nanmean(s.subFLAvL(Pp))],'*--r','linewidth',4);
plot([1:3]+0.1,[nanmean(s.subFSBvH(Pp))-nanmean(s.subFSBvL(Pp)) nanmean(s.subFMBvH(Pp))-nanmean(s.subFMBvL(Pp)) nanmean(s.subFLBvH(Pp))-nanmean(s.subFLBvL(Pp))],'*-b','linewidth',4);

ax = gca;
% axis tight
% ylim([-1.5 .5])
ylabel('d` (High minus Low Reward)' , 'FontSize', 20);
xlabel('Distance of Box from Gabor Center (degrees)' , 'FontSize', 20);
set(gca,'XTick',[1:3], 'FontSize', 14);
ax.XTickLabel = {'2�','3.5�','5�'};
legend('Auditory','Visual')

d = errorbar([1 2 3],[nanmean(s.subFSAvH(Pp)-s.subFSAvL(Pp)) nanmean(s.subFMAvH(Pp)-s.subFMAvL(Pp)) nanmean(s.subFLAvH(Pp)-s.subFLAvL(Pp))],...
    [nanstd(s.subFSAvH(Pp)-s.subFSAvL(Pp)) nanstd(s.subFMAvH(Pp)-s.subFMAvL(Pp)) nanstd(s.subFLAvH(Pp)-s.subFLAvL(Pp))]./sqrt(length(Pp)),'r','linewidth',4, 'linestyle','none')
errorbar([1 2 3]+0.1,[nanmean(s.subFSBvH(Pp)-s.subFSBvL(Pp)) nanmean(s.subFMBvH(Pp)-s.subFMBvL(Pp)) nanmean(s.subFLBvH(Pp)-s.subFLBvL(Pp))],...
    [nanstd(s.subFSBvH(Pp)-s.subFSBvL(Pp)) nanstd(s.subFMBvH(Pp)-s.subFMBvL(Pp)) nanstd(s.subFLBvH(Pp)-s.subFLBvL(Pp))]./sqrt(length(Pp)),'b','linewidth',4, 'linestyle','none')
d.Bar.LineStyle = 'Dashed';
set(gca,'XTick',[1:3], 'FontSize', 16, 'FontWeight', 'bold');
ax.XTickLabel = {'2�','3.5�','5�'};

[h pFLA]= ttest(nanmean([s.subFLBvL(Pp) s.subFLBvH(Pp)],2),...
                nanmean([s.subFLAvL(Pp) s.subFLAvH(Pp)],2))
hold off;
fclose all;

%%
% 
% maxi = max([s.subNeut(Pp) s.subSP_H(Pp)  s.subSP_L(Pp) s.subBCvH(Pp) s.subBCvL(Pp)]')'; %%%%% maximum across conditions
% maxi =1;
figure(29),hold on;
plot([1:3],[nanmean(s.subFSN(Pp)./maxi) nanmean(s.subFMN(Pp)./maxi) nanmean(s.subFLN(Pp)./maxi)],'*--k','linewidth',4);
plot([1:3]+0.05,[nanmean(nanmean([s.subFSAvH(Pp)./maxi; s.subFSAvL(Pp)./maxi])) nanmean(nanmean([s.subFMAvH(Pp)./maxi; s.subFMAvL(Pp)./maxi])) nanmean(nanmean([s.subFLAvH(Pp)./maxi; s.subFLAvL(Pp)./maxi]))],'*-r','linewidth',4);
plot([1:3]+0.1,[nanmean(nanmean([s.subFSBvH(Pp)./maxi; s.subFSBvL(Pp)./maxi])) nanmean(nanmean([s.subFMBvH(Pp)./maxi; s.subFMBvL(Pp)./maxi])) nanmean(nanmean([s.subFLBvH(Pp)./maxi; s.subFLBvL(Pp)./maxi]))],'*-b','linewidth',4);

errorbar([1 2 3],[nanmean(s.subFSN(Pp)./maxi) nanmean(s.subFMN(Pp)./maxi) nanmean(s.subFLN(Pp)./maxi)],...
    [nanstd(s.subFSN(Pp)./maxi) nanstd(s.subFMN(Pp)./maxi) nanstd(s.subFLN(Pp)./maxi)]./sqrt(length(Pp)),'k','linewidth',4, 'linestyle','none')
errorbar([1:3]+0.05,[nanmean(nanmean([s.subFSAvH(Pp)./maxi s.subFSAvL(Pp)./maxi],2)) nanmean(nanmean([s.subFMAvH(Pp)./maxi s.subFMAvL(Pp)./maxi],2)) nanmean(nanmean([s.subFLAvH(Pp)./maxi s.subFLAvL(Pp)./maxi],2))],...
    [std(nanmean([s.subFSAvH(Pp)./maxi s.subFSAvL(Pp)./maxi],2)) std(nanmean([s.subFMAvH(Pp)./maxi s.subFMAvL(Pp)./maxi],2)) std(nanmean([s.subFLAvH(Pp)./maxi s.subFLAvL(Pp)./maxi],2))]./sqrt(length(Pp)),'r','linewidth',4);
errorbar([1:3]+0.1,[nanmean(nanmean([s.subFSBvH(Pp)./maxi s.subFSBvL(Pp)./maxi],2)) nanmean(nanmean([s.subFMBvH(Pp)./maxi s.subFMBvL(Pp)./maxi],2)) nanmean(nanmean([s.subFLBvH(Pp)./maxi s.subFLBvL(Pp)./maxi],2))],...
    [std(nanmean([s.subFSBvH(Pp)./maxi s.subFSBvL(Pp)./maxi],2)) std(nanmean([s.subFMBvH(Pp)./maxi s.subFMBvL(Pp)./maxi],2)) std(nanmean([s.subFLBvH(Pp)./maxi s.subFLBvL(Pp)./maxi],2))]./sqrt(length(Pp)),'b','linewidth',4);
ax = gca;
ylabel('d`(normalized)' , 'FontSize', 20);
xlabel('Distance of Box from Gabor Center (degrees)' , 'FontSize', 20);
set(gca,'XTick',[1:3], 'FontSize', 14, 'FontWeight', 'bold');
ax.XTickLabel = {'2�','3.5�','5�'};
legend({'Neutral','Auditory', 'Visual'}, 'EdgeColor','w', 'FontSize', 30,'Orientation','Horizonal')
ylim([0.55 .95])
[h pFLA]= ttest(nanmean([s.subFLAvH(Pp)./maxi s.subFLAvL(Pp)./maxi],2),...
                nanmean([s.subFLBvH(Pp)./maxi s.subFLBvL(Pp)./maxi],2))