close all
types = {'LowS','HighS','LowV','HighV'}; % 
clock
for type = 1:length(types)
    if type == 1
        magentaTrlSel = allData([allData.Reward]>0&...
            [allData.condition]==2&[allData.StimValue]==1);
    elseif type == 2
        magentaTrlSel = allData([allData.Reward]>0&...
            [allData.condition]==2&[allData.StimValue]==2);
    elseif type == 3
        magentaTrlSel = allData([allData.Reward]>0&...
            [allData.condition]==1&[allData.StimValue]==1);
    elseif type == 4
        magentaTrlSel = allData([allData.Reward]>0&...
            [allData.condition]==1&[allData.StimValue]==2);
    end
%     if type == 1
%         magentaTrlSel = allData([allData.Reward]>=0&...
%             [allData.condition]==2);
%     elseif type == 2
%         magentaTrlSel = allData([allData.Reward]>=0&...
%             [allData.condition]==1);
%     end
    randomOrder = randperm(size(magentaTrlSel,2)); %create random order from that.
    magentaTrlSel = magentaTrlSel(randomOrder);
    magentaTrlSel = magentaTrlSel(1:round(length(magentaTrlSel)));
    fprintf('%d/4 %d\n',type,length(magentaTrlSel));
    
    snipSaccades(magentaTrlSel,...
        'TARGET_t','TRIAL_END_t','plot',1,'pupil',1, 'baselinesub','FIXATION_t','meancriteria',1,'smoothing',100);
    clock
end