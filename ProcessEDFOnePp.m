% function ProcessEDF(block)
% edition 11.12

start_path = fullfile('C:\');
myPath = uigetdir(start_path);
cd(myPath);
SubFold = cd;
% filePattern = fullfile(SubFold, '*.*');
folders = dir;
if 2 == exist('allData.mat','file')
    load('allData'); % problem here
else
    allData = [];
    doneFile = [];
end

for fls = 3:length(folders) % for each folder (Participant)
    if folders(fls).isdir && ~any(doneFile == str2double(folders(fls).name)) && ~isnan(str2double(folders(fls).name)) % It must be new folder
        participant = [];
        inFolder = [SubFold filesep folders(fls).name];
        load([inFolder filesep strcat(folders(fls).name,'_', '_allData','.mat')]); % read in everithing about current participant
        allBlocks = [myVar.strGabPre myVar.strGabPost];
        for i = 1:length(block)
            [block(i).trials(1:length(block(i).trials)).Pp] = deal(folders(fls).name); % Assign proper name to this participent (Should be in experiment)
        end
        files = dir(inFolder);
        for targFiles = 3:length(files) % For each file (Block)
            for targBlocks = 1:length([myVar.strGabPre myVar.strGabPost]) % For each target block
                if strcmp(files(targFiles).name,[folders(fls).name '_Block_' num2str(allBlocks(targBlocks)) '.edf'])
                    name = [inFolder filesep files(targFiles).name]; % take a name and feed into parsing script (in NewEye folder)
                    tempASC = readEDFASC(name,1,1); % Process current block
                    if ~any(allBlocks(targBlocks)==[myVar.strGabPre myVar.strGabPost])
                        block(allBlocks(targBlocks)).trials([block(allBlocks(targBlocks)).trials.condition]==0) = [];
                    end
                    tempComb = catstruct(block(allBlocks(targBlocks)).trials,tempASC); % Combine Horisonaly previous block with Eye
                    if ~isfield(tempComb,'SACCFix_DETECT_m'), % Sometimes, if Pp dont make errors we need to feelUp your structure or dementions will not match
                        [tempComb.SACCFix_DETECT_m] = deal([]);
                        [tempComb.SACCFix_DETECT_t] = deal([]);
                    end
                    if ~isfield(tempComb,'TRIAL_ERROR_END_m'),
                        [tempComb.TRIAL_ERROR_END_m] = deal([]);
                        [tempComb.TRIAL_ERROR_END_t] = deal([]);
                    end
                    if ~isfield(tempComb,'DRIFTCORRECT_m')
                        [tempComb.DRIFTCORRECT_m] = deal([]);
                        [tempComb.DRIFTCORRECT_t] = deal([]);
                    end
                    if any(allBlocks(targBlocks)==myVar.strGabPre)
                        [tempComb.BlPrePost] = deal(1);
                    elseif any(allBlocks(targBlocks)==myVar.strGabPost)
                        [tempComb.BlPrePost] = deal(2);
                    else
                        [tempComb.BlPrePost] = deal(0);
                    end
                    if isfield(tempComb,'VALIDATE_t')
                        tempComb = rmfield(tempComb,'VALIDATE_t');
                        tempComb = rmfield(tempComb,'VALIDATE_m');
                    end
                    if isfield(tempComb,'CAL_m')
                        tempComb = rmfield(tempComb,'CAL_m');
                        tempComb = rmfield(tempComb,'CAL_t');
                    end
                    if isfield(tempComb,'SACCADE_DETECT_t')
                        tempComb = rmfield(tempComb,'SACCADE_DETECT_m');
                        tempComb = rmfield(tempComb,'SACCADE_DETECT_t');
                    end
                    if 1 == exist('allData','var')
                        allData = [allData tempComb];
                        participant = [participant tempComb];
                    else
                        allData = tempComb;
                    end
                end
            end
        end
        doneFile = [doneFile str2double(folders(fls).name)];
        save([inFolder filesep strcat(folders(fls).name,'_', '_allDataEyE','.mat')],'participant','Scr','myVar','inf');
    end
    save('allData','allData','doneFile','Scr','myVar','inf');
end


% cont = menu('continue?','Yes','No');
% if cont

%     load('allData'); % problem here
allData([allData.error]==1)=[]; % cleanUp from error trials
emptyIndex = find(arrayfun(@(allData) isempty(allData.RECCFG_t),allData));
allData(emptyIndex)=[];

selectionFixation = arrayfun(@(x)...                Select only inportant part from eye
    allData(x).pos(allData(x).FIXATION_t+1:allData(x).TARGET_t,:),...
    1:length(allData), 'UniformOutput',false);

selection = arrayfun(@(x)...                Select only inportant part from eye
    allData(x).pos(allData(x).TARGET_t+1:allData(x).RESPONSE_t,:),...
    1:length(allData), 'UniformOutput',false);

maxDistSample = arrayfun(@(x) abs(max(selection{x}(:,2))-myVar.centerX), 1:length(selection), 'UniformOutput', 0);

dist = arrayfun(@(x) ...
    sqrt((nanmean(selection{x}(:,2)) - nanmean(selectionFixation{x}(end-198:end,2)))^2 +...
    (nanmean(selection{x}(:,3)) - nanmean(selectionFixation{x}(end-198:end,3)))^2),...
    1:length(selection), 'UniformOutput', 0);
for i=1:length(selection)
    allData(i).posSel = selection{i};
    allData(i).maxDist = maxDistSample{i}/Scr.pixelsperdegree;
    allData(i).dist = dist{i}/Scr.pixelsperdegree;
end % put it back to dataSet



%     dest(:,1) = arrayfun(@(y) nanmean(arrayfun(@(x) selection{x}(y,3),1:length(selection))),1:249)'; % calculate means for each timepoint
%     dest(:,2) = arrayfun(@(y)  nanstd(arrayfun(@(x) selection{x}(y,3),1:length(selection))),1:249)'; % calculate    Sd for each timepoint
%
%     maxDistSample = arrayfun(@(x) abs(max(selection{x}(:,2))-myVar.centerX), 1:length(selection))'./Scr.pixelsperdegree;
%     maxDistanceList =   [maxDistSample'; [allData.Accuracy]]';   % Get the accuracy of this trial and add both values into the global list
save('allData','allData','doneFile','Scr','myVar','inf');
% end